#![doc(hidden)]
extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;

#[proc_macro_derive(MetaFields)]
pub fn derive_meta_fields(input: TokenStream) -> TokenStream {
    let syn::DeriveInput {
        ident,
        data,
        generics,
        ..
    } = syn::parse_macro_input!(input as syn::DeriveInput);
    let field_defs = parse_fields(data);
    impl_tuple_chain(&ident, &field_defs, &generics)
}

#[proc_macro_derive(MetaStruct)]
pub fn derive_meta_struct(input: TokenStream) -> TokenStream {
    let syn::DeriveInput {
        ident,
        data,
        generics,
        ..
    } = syn::parse_macro_input!(input as syn::DeriveInput);
    let field_defs = parse_fields(data);
    impl_meta_struct(&ident, &field_defs, &generics)
}

fn parse_fields(data: syn::Data) -> Vec<FieldDef> {
    let syn::Data::Struct(struct_info) = data else {
        panic!("Must be a struct!")
    };
    let syn::Fields::Named(syn::FieldsNamed { named, .. }) = struct_info.fields else {
        panic!("Must be an struct with named fields!");
    };
    let fields: Vec<_> = named
        .iter()
        .filter_map(|f| extract_doc(f).map(|doc| (f.ident.clone(), doc, f.ty.clone())))
        .collect();
    fields
        .into_iter()
        .filter_map(|(ident, doc, ty)| {
            let (symbol, label, unit, label_after_unit, description) = parse_doc(&doc);
            ident.map(|ident| FieldDef {
                symbol: symbol.map(str::to_string),
                label: if label_after_unit.is_empty() {
                    label.to_string()
                } else {
                    label.to_string() + " " + label_after_unit
                },
                unit: unit.map(str::to_string),
                ident,
                description: description.map(str::to_string),
                ty,
            })
        })
        .collect()
}

struct FieldDef {
    pub ident: syn::Ident,
    pub symbol: Option<String>,
    pub label: String,
    pub unit: Option<String>,
    pub description: Option<String>,
    pub ty: syn::Type,
}

impl FieldDef {
    fn to_type(&self, ident: &syn::Ident, generics: &syn::Generics) -> impl quote::ToTokens {
        let ty = &self.ty;
        quote! { metastruct::MetaField<#ident #generics, #ty> }
    }
    fn to_meta(&self) -> impl quote::ToTokens {
        let FieldDef {
            ident,
            symbol,
            label,
            unit,
            description,
            ..
        } = self;
        let symbol = match symbol {
            None => quote! { None },
            Some(s) => quote! { Some(#s) },
        };
        let unit = match unit {
            None => quote! { None },
            Some(u) => quote! { Some(#u) },
        };
        let description = match description {
            None => quote! { None },
            Some(d) => quote! { Some(#d) },
        };
        quote! {
            metastruct::MetaField {
                metadata: metastruct::MetaData{
                    ident: stringify!(#ident),
                    symbol: #symbol,
                    label: #label,
                    unit: #unit,
                    description: #description,
                },
                access: |data: &Self| &data.#ident,
                access_mut: |data: &mut Self| &mut data.#ident,
            }
        }
    }
}

fn impl_meta_struct(
    struct_ident: &syn::Ident,
    field_defs: &[FieldDef],
    generics: &syn::Generics,
) -> TokenStream {
    let (struct_fields, struct_values): (Vec<_>, Vec<_>) = field_defs
        .iter()
        .map(|field_def| {
            let meta_type = field_def.to_type(struct_ident, generics);
            let meta = field_def.to_meta();
            let ident = &field_def.ident;
            (quote! {pub #ident: #meta_type}, quote! {#ident: #meta})
        })
        .unzip();
    let meta_ident = quote::format_ident!("Meta{struct_ident}");
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    quote! {
        #[doc(hidden)]
        #[allow(non_snake_case)]
        pub struct #meta_ident #ty_generics {
            #(#struct_fields),*
        }
        impl #impl_generics metastruct::MetaStruct for #struct_ident #ty_generics #where_clause {
            type MetaType = #meta_ident #ty_generics;
            const META: Self::MetaType = Self::MetaType {
                #(#struct_values),*
            };
        }
    }
    .into()
}

fn impl_tuple_chain(
    struct_ident: &syn::Ident,
    field_defs: &[FieldDef],
    generics: &syn::Generics,
) -> TokenStream {
    let (ty, fields) =
        if let Some([field_def_prelast, field_def_last]) = field_defs.last_chunk::<2>() {
            // >= 2 fields
            let (quoted_field_last, ty_last) = (
                field_def_last.to_meta(),
                field_def_last.to_type(struct_ident, generics),
            );
            let (quoted_field_prelast, ty_prelast) = (
                field_def_prelast.to_meta(),
                field_def_prelast.to_type(struct_ident, generics),
            );
            // (c,d)
            let mut quoted_fields_type = quote! { (#ty_prelast, #ty_last) };
            let mut quoted_meta_fields = quote! { (#quoted_field_prelast, #quoted_field_last) };
            // (a,(b,(c,d)))
            for field_def in field_defs[..field_defs.len() - 2].iter().rev() {
                let (quoted_field, ty) = (
                    field_def.to_meta(),
                    field_def.to_type(struct_ident, generics),
                );
                quoted_fields_type = quote! { (#ty, #quoted_fields_type) };
                quoted_meta_fields = quote! { (#quoted_field, #quoted_meta_fields) };
            }
            (quoted_fields_type, quoted_meta_fields)
        } else if let Some(field_def) = field_defs.first() {
            // 1 field
            let (quoted_field, ty) = (
                field_def.to_meta(),
                field_def.to_type(struct_ident, generics),
            );
            (quote! { (#ty,) }, quote! { (#quoted_field,) })
        } else {
            // 0 fields
            (quote! {()}, quote! {()})
        };
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    quote! {
    impl #impl_generics metastruct::MetaFields for #struct_ident #ty_generics #where_clause {
        type FieldsType = #ty;
        const META_FIELDS: Self::FieldsType = #fields;
    }}
    .into()
}

fn extract_doc(field: &syn::Field) -> Option<String> {
    let docs = field
        .attrs
        .iter()
        .filter_map(|attr| attr.parse_meta().ok())
        .filter_map(|meta| match meta {
            syn::Meta::NameValue(syn::MetaNameValue {
                path,
                lit: syn::Lit::Str(doc),
                ..
            }) if path.get_ident().is_some() && *path.get_ident().unwrap() == "doc" => {
                Some(doc.value().trim().to_string())
            }
            _ => None,
        })
        .collect::<Vec<_>>();
    (!docs.is_empty()).then(|| docs.join("\n"))
}

/// Parses a doc string in the format
/// sym: label [unit]
/// or
/// sym: label \[unit]
/// returns the parts sym, label, unit
/// potentially followed by all following lines as "description"
fn parse_doc(doc: &str) -> (Option<&str>, &str, Option<&str>, &str, Option<&str>) {
    let doc = doc.trim();
    let (doc, description) = doc
        .split_once("\n\n")
        .map(|(doc, description)| (doc, Some(description.trim())))
        .unwrap_or((doc, None));
    let (sym, doc) = doc
        .split_once(':')
        .and_then(|(s, d)| {
            let s = s.trim();
            (!s.contains(' ')).then(|| (Some(s), d.trim()))
        })
        .unwrap_or((None, doc));
    let (doc, unit, doc_after_unit) = doc
        .split_once('[')
        .map(|(doc, unit)| {
            let (unit, doc_after_unit) = unit.split_once(']').unwrap_or((unit, ""));
            (
                doc.trim_end_matches('\\').trim(),
                Some(unit.trim()),
                doc_after_unit.trim(),
            )
        })
        .unwrap_or((doc, None, ""));
    let doc = doc.trim();
    (sym, doc, unit, doc_after_unit, description)
}
