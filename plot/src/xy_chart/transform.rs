//! Transform from data space to drawing space.
//!
//! The central object is [Transformation]. It does not define the actual transformation
//! function itself but delegates specific implementations to the [TransformableInto] trait.
//! Any object implementing [TransformableInto] can be passed to [Transformation::apply],
//! which will call [TransformableInto::transform].
//! This way, it is convenient to define an implementation not only for point space,
//! but also for tangent space (a direction vector transforms like $(v_x, v_y) \mapsto (\lambda_x v_x, \lambda_y v_y)$ and does not receive a translation).
use numeric::bezier::CubicBezierPoint;

use super::gfx::BezierPoint;

/// Affine transform composed of a scaling, followed by a translation:
/// $T((x,y)) = (t_x + \lambda_x x, t_y + \lambda_y y)$.
#[derive(Debug)]
pub struct Transformation {
    /// Translation vector $(t_x, t_y)$.
    pub translation: (f64, f64),
    /// Scale factors $\lambda_x$, $\lambda_y$.
    pub scale: (f64, f64),
}

impl Transformation {
    /// Create a new [Transformation], mapping `domain_x` $\times$ `domain_y` to
    /// `image_x` $\times$ `image_y`.
    pub fn new_from_domain_to_image(
        (domain_x, domain_y): &(std::ops::Range<f64>, std::ops::Range<f64>),
        (image_x, image_y): &(std::ops::Range<f64>, std::ops::Range<f64>),
    ) -> Self {
        let kx = (image_x.end - image_x.start) / (domain_x.end - domain_x.start);
        let ky = (image_y.end - image_y.start) / (domain_y.end - domain_y.start);
        Self {
            translation: (
                image_x.start - domain_x.start * kx,
                image_y.start - domain_y.start * ky,
            ),
            scale: (kx, ky),
        }
    }

    /// Apply `self` to an object which implements [TransformableInto].
    pub fn apply<J>(&self, obj: impl TransformableInto<J>) -> J {
        obj.transform(self)
    }
}

/// Trait defining how an object transforms under a [Transformation].
/// This usually is different for point space and tangent space.
pub trait TransformableInto<T> {
    fn transform(&self, transformation: &Transformation) -> T;
}

impl TransformableInto<(f64, f64)> for [f64; 2] {
    fn transform(&self, transformation: &Transformation) -> (f64, f64) {
        (
            self[0] * transformation.scale.0 + transformation.translation.0,
            self[1] * transformation.scale.1 + transformation.translation.1,
        )
    }
}
impl TransformableInto<BezierPoint> for CubicBezierPoint {
    fn transform(&self, transformation: &Transformation) -> BezierPoint {
        BezierPoint {
            p: transformation.apply(self.point),
            q_f: transformation.apply(self.forward_control_point),
            q_b: transformation.apply(self.backward_control_point),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_transformation_new_from_domain_to_image_works() {
        let t = Transformation::new_from_domain_to_image(
            &(0.0..1.0, 0.0..1.0),
            &(10.0..110.0, -10.0..90.0),
        );
        assert_eq!(t.apply([0., 0.]), (10.0, -10.0));
        assert_eq!(t.apply([1., 0.]), (110.0, -10.0));
        assert_eq!(t.apply([0., 1.]), (10.0, 90.0));
        assert_eq!(t.apply([1., 1.]), (110.0, 90.0));
    }
}
