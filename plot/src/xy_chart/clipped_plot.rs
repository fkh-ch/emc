//! Clip plots
use crate::gfx;
use crate::xy_chart::{
    legend::{ExtendLegendEntries, ExtendLegendEntriesWithTitle},
    DataRegion, FallbackValue, PlotTo, Region,
};

/// Wrapper for a type implementing [PlotTo] which clips the contained plot using `make_clip`
pub struct ClippedPlot<P, C> {
    /// Plot to be clipped
    pub plot: P,
    /// Callback to create the clip path for given data region
    pub make_clip: C,
}

/// Converts a [Region] to a quadrilateral
pub fn to_polygon_from_region(r: &Region) -> [(f64, f64); 4] {
    [
        (r.x, r.y),
        (r.x + r.w, r.y),
        (r.x + r.w, r.y + r.h),
        (r.x, r.y + r.h),
    ]
}

/// Converts a [DataRegion] to a quadrilateral
pub fn to_polygon(r: &DataRegion) -> [(f64, f64); 4] {
    to_polygon_from_region(r.region)
}

/// Shorthand type for a function which can be used in [ClippedPlot] and returns a quadrilateral
pub type QuadrilateralClip = fn(&DataRegion) -> [(f64, f64); 4];

/// Trait for plots supporting clipping
pub trait ClippedAt: Sized {
    /// Generate a clipped version of the plot using clip
    fn clipped_at<C>(self, clip: C) -> ClippedPlot<Self, C> {
        ClippedPlot {
            plot: self,
            make_clip: clip,
        }
    }
    /// Generate a clipped version of the plot using a box as the clip path
    fn clipped_at_region(self) -> ClippedPlot<Self, QuadrilateralClip> {
        ClippedPlot {
            plot: self,
            make_clip: to_polygon,
        }
    }
}

impl<P: PlotTo, I: IntoIterator<Item = (f64, f64)>, C: Fn(&DataRegion) -> I> PlotTo
    for ClippedPlot<P, C>
where
    P::ReturnType: FallbackValue,
{
    type ReturnType = P::ReturnType;
    fn plot_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        data_region: &DataRegion,
    ) -> Self::ReturnType {
        drawable
            .clip_polygon((self.make_clip)(data_region), |drawable| {
                self.plot.plot_to(drawable, data_region)
            })
            .unwrap_or_else(P::ReturnType::fallback_value)
    }
}

impl<P, C> ExtendLegendEntries for ClippedPlot<P, C>
where
    P: ExtendLegendEntries,
{
    fn extend_legend_entries(&self, entries: &mut Vec<super::legend::LegendEntry>) {
        self.plot.extend_legend_entries(entries)
    }
}

impl<P, C> ExtendLegendEntriesWithTitle for ClippedPlot<P, C>
where
    P: ExtendLegendEntriesWithTitle,
{
    fn extend_legend_entries_with_title<S: Into<String>>(
        &self,
        title: S,
        entries: &mut Vec<super::legend::LegendEntry>,
    ) {
        self.plot.extend_legend_entries_with_title(title, entries)
    }
}
