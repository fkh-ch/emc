//! Scatter plots of data series
//!
//! Can be plotted via [crate::xy_chart::ExtrinsicChartLayout::plot] and included in a legend
//! by using [crate::xy_chart::legend::Legend::add_entries].

pub use crate::xy_chart::marker::{Marker, MarkerShape};
use crate::{
    gfx,
    xy_chart::{
        iter::AsIterator,
        legend::{ExtendLegendEntriesWithTitle, LegendEntry},
        marker::DrawMarker,
        periodicity::Periodicity,
        DataRegion, PlotTo,
    },
};

/// Scatter plot properties.
#[derive(Debug, Clone)]
pub struct ScatterPlot<C, L, PX: Periodicity, PY: Periodicity> {
    /// Coordinates at which to draw a marker
    pub coordinates: C,
    /// Shape / style of the markers
    pub marker_style: Option<Marker>,
    /// Optional labels (will have a type of [NoLabels] if no labels should be drawn)
    pub labels: L,
    /// Periodicity in x and y direction.
    /// `PX` and `PY` can be set to [crate::xy_chart::periodicity::Periodic] (markers will be wrapped)
    /// or [crate::xy_chart::periodicity::NotPeriodic]  (markers will not be wrapped)
    pub periodicity: (PX, PY),
}

/// Single label which will be placed next to a marker
#[derive(Debug, Clone)]
pub struct Label<'a, S: AsRef<str>, F: AsRef<str>> {
    /// Text to display
    pub text: S,
    /// Font of the label
    pub font: &'a gfx::Font<F>,
    /// Direction of the labels anchor which is closest to the marker, relative to the marker
    pub offset_angle: f64,
}

/// Technical trait. Variant of [std::iter::IntoIterator] but carries life time and a `FontName`
/// type instead of the item type
///
/// This is needed because in the following [std::iter::IntoIterator]
/// which does not explicitly carry a life time can not be used in all trait bounds.
pub trait IntoLabelIterator<'a> {
    /// Type for the text contained in the generated labels
    type TextType: AsRef<str> + 'a;
    /// Type for the font contained in the generated labels
    type FontName: AsRef<str> + 'a;
    /// Iterator type returned by [Self::generate_labels]
    type IntoLabelIterator: Iterator<Item = Option<Label<'a, Self::TextType, Self::FontName>>>;
    /// Generate labels with font
    fn generate_labels(self) -> Self::IntoLabelIterator;
}

impl<C, L, PX: Periodicity, PY: Periodicity> ExtendLegendEntriesWithTitle
    for ScatterPlot<C, L, PX, PY>
{
    fn extend_legend_entries_with_title<S: Into<String>>(
        &self,
        title: S,
        entries: &mut Vec<LegendEntry>,
    ) {
        entries.push(LegendEntry {
            title: title.into(),
            line_style: None,
            marker_style: self.marker_style.clone(),
        });
    }
}

/// Type to be used in [ScatterPlot] without labels as the type of `labels`
#[derive(Debug, Clone, Copy)]
pub struct NoLabels;

impl<'a> IntoLabelIterator<'a> for &'a NoLabels {
    type FontName = &'a str;
    type TextType = &'static str;
    type IntoLabelIterator = std::iter::Repeat<Option<Label<'a, Self::TextType, Self::FontName>>>;
    fn generate_labels(self) -> Self::IntoLabelIterator {
        std::iter::repeat(None)
    }
}

/// Type to be used in [ScatterPlot] with labels as the type of `labels`.
///
/// `labels` must implement `Fn() -> impl IntoIterator<Item = Option<impl AsRef<str>>>` so that
/// the [ScatterPlot] can be plotted with labels.
#[derive(Debug, Clone)]
pub struct ScatterPlotLabels<V, S: AsRef<str>> {
    /// Labels for each marker
    pub labels: V,
    /// Font of the labels
    pub font: gfx::Font<S>,
    /// Angle around the marker center where the labels should be placed
    pub offset_angle: f64,
}

impl<'a, S: AsRef<str> + 'a, F: AsRef<str> + 'a, I: Iterator<Item = Option<S>>, V: Fn() -> I>
    IntoLabelIterator<'a> for &'a ScatterPlotLabels<V, F>
{
    type FontName = F;
    type TextType = S;
    type IntoLabelIterator = std::iter::Map<
        std::iter::Zip<I, std::iter::Repeat<(&'a gfx::Font<F>, f64)>>,
        fn((I::Item, (&'a gfx::Font<F>, f64))) -> Option<Label<'a, S, Self::FontName>>,
    >;
    fn generate_labels(self) -> Self::IntoLabelIterator {
        fn move_font_into_option<'a, S: AsRef<str> + 'a, F: AsRef<str> + 'a>(
            (label, (font, offset_angle)): (Option<S>, (&'a gfx::Font<F>, f64)),
        ) -> Option<Label<'a, S, F>> {
            label.map(|l| Label {
                text: l,
                font,
                offset_angle,
            })
        }
        (self.labels)()
            .zip(std::iter::repeat((&self.font, self.offset_angle)))
            .map(move_font_into_option)
    }
}

impl<PX: Periodicity, PY: Periodicity, C, L> PlotTo for ScatterPlot<C, L, PX, PY>
where
    for<'c> &'c C: AsIterator<[f64; 2]>,
    for<'c> &'c L: IntoLabelIterator<'c>,
{
    type ReturnType = ();
    fn plot_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        DataRegion {
            xy_interval,
            chart_to_canvas,
            region,
            ..
        }: &DataRegion,
    ) {
        let marker_positions = || {
            self.coordinates
                .as_iter()
                .filter(|p| xy_interval.contains(*p))
                .map(|p| chart_to_canvas.apply(p))
        };
        if let Some(marker_style) = &self.marker_style {
            for (x, y) in marker_positions() {
                drawable.draw_marker(x, y, marker_style);
            }
        }
        for ((x, y), maybe_label) in marker_positions().zip((&self.labels).generate_labels()) {
            if let Some(Label {
                text,
                font,
                offset_angle,
            }) = maybe_label
            {
                let (dx, dy) = f64::sin_cos(offset_angle);
                let eps = 0.01;
                let r = 1.5 * self.marker_style.as_ref().map(|m| m.size).unwrap_or(0.);
                let (text_w, text_h) = drawable.get_text_extents(text.as_ref(), font);
                let pull_inside_interval = |x: f64, dx: f64, w, (a, b)| {
                    if (dx > eps && x + dx + w > b) || (dx < eps && x + r * dx - w < a) {
                        return -dx;
                    };
                    dx
                };
                let (dx, dy) = (
                    pull_inside_interval(x, dx, text_w, (region.x, region.x + region.w)),
                    -pull_inside_interval(y, -dy, text_h, (region.y, region.y + region.h)),
                );

                let baseline = if dy.abs() < eps {
                    gfx::Baseline::Middle
                } else if dy < 0. {
                    gfx::Baseline::Hanging
                } else {
                    gfx::Baseline::Alphabetic
                };
                let anchor = if dx.abs() < eps {
                    gfx::TextAnchor::Middle
                } else if dx < 0. {
                    gfx::TextAnchor::End
                } else {
                    gfx::TextAnchor::Start
                };
                drawable.draw_text(
                    &gfx::Text {
                        x: x + r * dx,
                        y: y - r * dy,
                        text,
                        anchor,
                        baseline,
                        angle: 0.,
                    },
                    font,
                );
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::gfx::testing::{self, Matcher};
    use crate::xy_chart::periodicity::NotPeriodic;
    use crate::xy_chart::testing::empty_chart;

    use super::*;

    fn circle((x, y): (f64, f64), r: f64) -> gfx::Circle {
        gfx::Circle { x, y, r }
    }

    fn marker_style(shape: MarkerShape) -> Option<Marker> {
        Some(Marker {
            shape,
            size: 5.0,
            color: Default::default(),
            line_style: Default::default(),
        })
    }

    #[test]
    fn test_xy_chart_draws_scatter_plot_without_labels() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = empty_chart();
        let plot = chart
            .prepare_layout(crate::xy_chart::AspectRatio::Unconstrained, &drawable)
            .fit_into(400., 200.);
        let plot = plot.draw_axes(&mut drawable);

        let coordinates: &[[f64; 2]] = &[[0.5, 0.5], [0.75, 0.5], [0.75, 0.75]];
        let scatter_plot = ScatterPlot {
            coordinates,
            marker_style: marker_style(MarkerShape::CIRCLE),
            labels: NoLabels,
            periodicity: (NotPeriodic, NotPeriodic),
        };
        plot.plot(&mut drawable, &scatter_plot);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![
                gfx::Rectangle {
                    x: 10.0,
                    y: 10.0,
                    width: 380.,
                    height: 180.,
                }
                .into(),
                circle((200.0, 100.0), 5.0).into(),
                circle((295.0, 100.0), 5.0).into(),
                circle((295.0, 55.0), 5.0).into(),
            ],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    #[test]
    fn test_xy_chart_draws_scatter_plot_with_coordinate_labels() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = empty_chart();
        let plot = chart
            .prepare_layout(crate::xy_chart::AspectRatio::Unconstrained, &drawable)
            .fit_into(400., 200.);
        let plot = plot.draw_axes(&mut drawable);

        let coordinates = &[[0.5, 0.5], [0.75, 0.5], [0.9, 0.75]];
        let scatter_plot = ScatterPlot {
            labels: ScatterPlotLabels::<_, &'static str> {
                labels: || coordinates.iter().map(|[x, y]| Some(format!("({x}, {y})"))),
                font: Default::default(),
                offset_angle: std::f64::consts::FRAC_PI_4,
            },
            coordinates,
            marker_style: marker_style(MarkerShape::CIRCLE),
            periodicity: (NotPeriodic, NotPeriodic),
        };
        plot.plot(&mut drawable, &scatter_plot);
        let text = |x, y, tx, size| {
            gfx::testing::UnstyledText::new(x, y, tx, size)
                .anchor(gfx::TextAnchor::Start)
                .baseline(gfx::Baseline::Alphabetic)
        };

        use std::f64::consts::SQRT_2;
        let d = 1.5 * scatter_plot.marker_style.unwrap().size / SQRT_2;
        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![
                gfx::Rectangle {
                    x: 10.0,
                    y: 10.0,
                    width: 380.,
                    height: 180.,
                }
                .into(),
                circle((200.0, 100.0), 5.0).into(),
                circle((295.0, 100.0), 5.0).into(),
                circle((352.0, 55.0), 5.0).into(),
                text(200.0 + d, 100.0 - d, "(0.5, 0.5)", 10.0).into(),
                text(295.0 + d, 100.0 - d, "(0.75, 0.5)", 10.0).into(),
                // Last label would be outside of data region. Therefore it is moved to the left:
                text(352.0 - d, 55.0 - d, "(0.9, 0.75)", 10.0)
                    .anchor(gfx::TextAnchor::End)
                    .into(),
            ],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
}
