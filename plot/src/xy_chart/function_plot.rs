//! Plot graphs of real valued functions of one variable
//!
//! Can be plotted via [crate::xy_chart::ExtrinsicChartLayout::plot] and included in a legend
//! by using [crate::xy_chart::legend::Legend::add_entries].

use crate::{
    gfx,
    xy_chart::{
        clip::ClipBox,
        legend::{ExtendLegendEntriesWithTitle, LegendEntry},
        periodicity::Periodicity,
        DataRegion, PlotTo,
    },
};

use super::clip::ClipPathForEach;

/// Function plot properties.
pub struct FunctionPlot<F, PX: Periodicity, PY: Periodicity> {
    /// Real valued function.
    pub f: F,
    /// Line style for the graph of the function.
    pub line_style: gfx::LineStyle,
    /// Periodicity in x and y direction.
    /// `PX` and `PY` can be set to [crate::xy_chart::periodicity::Periodic] (line will be wrapped)
    /// or [crate::xy_chart::periodicity::NotPeriodic]  (line will not be wrapped)
    pub periodicity: (PX, PY),
}

impl<PX: Periodicity, PY: Periodicity, F: Fn(f64) -> f64> PlotTo for FunctionPlot<F, PX, PY>
where
    for<'b> &'b super::clip::ClipBox<'b, PX, PY>: ClipPathForEach,
{
    type ReturnType = ();
    fn plot_to(
        &self,
        drawable: &mut impl gfx::Drawable,

        DataRegion {
            xy_interval,
            chart_to_canvas,
            region,
            ..
        }: &DataRegion,
    ) {
        if self.line_style.width <= 0. {
            return;
        }
        let dx = 1. / chart_to_canvas.scale.0;
        let ax_w = region.w;
        let graph = (0..=ax_w as usize).map(|i| {
            let x = xy_interval.x.min + i as f64 * dx;
            [x, (self.f)(x)]
        });
        let clip_box = ClipBox::<PX, PY>::from(xy_interval);
        clip_box.clip_path_for_each(graph, |path_component| {
            drawable.stroke_path(
                path_component.iter().map(|p| chart_to_canvas.apply(*p)),
                &self.line_style,
            );
        });
    }
}

impl<F, PX: Periodicity, PY: Periodicity> ExtendLegendEntriesWithTitle for FunctionPlot<F, PX, PY> {
    fn extend_legend_entries_with_title<S: Into<String>>(
        &self,
        title: S,
        entries: &mut Vec<LegendEntry>,
    ) {
        entries.push(LegendEntry {
            title: title.into(),
            line_style: Some(self.line_style.clone()),
            marker_style: None,
        })
    }
}

#[cfg(test)]
mod test {
    use crate::gfx::testing::{self, Matcher};
    use crate::xy_chart::periodicity::NotPeriodic;
    use crate::xy_chart::testing::empty_chart;

    use super::*;

    #[test]
    fn test_xy_chart_draws_function_plot() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = empty_chart();
        let plot = chart
            .prepare_layout(crate::xy_chart::AspectRatio::Unconstrained, &drawable)
            .fit_into(400., 200.);
        let plot = plot.draw_axes(&mut drawable);

        let function_plot = FunctionPlot {
            f: |x| x,
            line_style: Default::default(),
            periodicity: (NotPeriodic, NotPeriodic),
        };
        plot.plot(&mut drawable, &function_plot);

        let expected_rectangle = gfx::Rectangle {
            x: 10.0,
            y: 10.0,
            width: 380.,
            height: 180.,
        };
        assert_eq!(drawable.elements.len(), 2);
        testing::assert_match!(
            &drawable.elements[0],
            testing::Approx(&gfx::testing::Element::from(expected_rectangle))
        );
        let gfx::testing::Element::Primitive(
            gfx::testing::UnstyledPrimitiveElement::PolygonalChain(points),
        ) = &drawable.elements[1]
        else {
            panic!("Element 2 is not a polygonal chain")
        };
        testing::assert_match!(points[0], testing::Approx(&(10., 190.)));
        testing::assert_match!(points.last().unwrap(), testing::Approx(&(390., 10.)));
    }
}
