//! Clipping / periodic folding of chart data points

use crate::xy_chart::{
    periodicity::{DomainVisibility, IsPeriodic, NotPeriodic, Periodic, Periodicity},
    Interval, XyInterval,
};

/// Region inside which data points are visible.
///
/// If a dimension is periodic, fold data points outside the region into the region.
pub struct ClipBox<'a, XPeriodicity: Periodicity, YPeriodicity: Periodicity> {
    interval: &'a XyInterval,
    x_periodicity: std::marker::PhantomData<XPeriodicity>,
    y_periodicity: std::marker::PhantomData<YPeriodicity>,
}

impl<'a, XPeriodicity: Periodicity, YPeriodicity: Periodicity> From<&'a XyInterval>
    for ClipBox<'a, XPeriodicity, YPeriodicity>
{
    fn from(value: &'a XyInterval) -> Self {
        Self {
            interval: value,
            x_periodicity: std::marker::PhantomData,
            y_periodicity: std::marker::PhantomData,
        }
    }
}

/// Trait defining clipping / periodic folding of a path.
pub trait ClipPath {
    type ComponentIteratorState;
    fn clip_path<I: IntoIterator<Item = [f64; 2]>>(
        self,
        chain: I,
    ) -> ClipPathComponentIterator<Self::ComponentIteratorState, std::iter::Fuse<I::IntoIter>>;
}

/// Variant of [ClipPath] suitable for further processing each clipped component during clipping.
///
/// This variant can unify [ClipPath] implementations with different [ClipPath::Componentiteratorstate] types / does not carry an associated iterator type.
pub trait ClipPathForEach {
    fn clip_path_for_each<I: IntoIterator<Item = [f64; 2]>, F: FnMut(&[[f64; 2]])>(
        self,
        chain: I,
        f: F,
    );
}

impl<'a> ClipPath for &'a XyInterval {
    type ComponentIteratorState = ClipPathComponentIteratorData<'a>;
    fn clip_path<I: IntoIterator<Item = [f64; 2]>>(
        self,
        chain: I,
    ) -> ClipPathComponentIterator<Self::ComponentIteratorState, std::iter::Fuse<I::IntoIter>> {
        ClipPathComponentIterator {
            state: ClipPathComponentIteratorData {
                previous: None,
                clip_box: self,
            },
            chain: chain.into_iter().fuse(),
        }
    }
}

/// Iterator type for [ClipPath]. The type `D` either holds [ClipPathComponentIteratorData] (not periodic, or [ClipPathPeriodicComponentIteratorData] (periodic case).
pub struct ClipPathComponentIterator<D, I: Iterator<Item = [f64; 2]>> {
    chain: I,
    state: D,
}

/// State part of [ClipPathComponentIterator] (non periodic case).
pub struct ClipPathComponentIteratorData<'b> {
    previous: Option<[f64; 2]>,
    clip_box: &'b XyInterval,
}

impl<I: Iterator<Item = [f64; 2]>> Iterator
    for ClipPathComponentIterator<ClipPathComponentIteratorData<'_>, I>
{
    type Item = Vec<[f64; 2]>;
    fn next(&mut self) -> Option<Self::Item> {
        for p in &mut self.chain {
            if is_undefined(p) {
                self.state.previous = None;
                continue;
            }
            if self.state.clip_box.contains(p) {
                let mut head = if let Some(previous) = self.state.previous {
                    vec![self.state.clip_box.intersect_from_interior(p, previous), p]
                } else {
                    vec![p]
                };
                (head, self.state.previous) = self
                    .state
                    .clip_box
                    .collect_connected_component(head, &mut self.chain);
                return Some(head);
            } else if let Some(previous) = self.state.previous {
                if let Some(intersection) = self.state.clip_box.intersect_from_exterior(previous, p)
                {
                    self.state.previous = Some(p);
                    return Some(intersection.to_vec());
                }
            }
            self.state.previous = Some(p);
        }
        None
    }
}

impl Interval {
    /// Number of interval lengths, `x` has to be shifted so it would be contained in the interval.
    fn offset_of(&self, x: f64) -> i32 {
        (x - self.min).div_euclid(self.length()) as i32
    }
    /// Shift an interval by `n` times its length.
    fn shifted_to(&self, n: i32) -> Self {
        let d = self.length();
        Self {
            min: self.min + n as f64 * d,
            max: self.max + n as f64 * d,
        }
    }
}

impl XyInterval {
    /// 2D version of [Interval::offset_of].
    fn offset_of(&self, [x, y]: [f64; 2]) -> (i32, i32) {
        (self.x.offset_of(x), self.y.offset_of(y))
    }

    /// Check if a line segment from `[x0, y0]`, `[x1, y1]` intersects the [Xyinterval].
    fn intersects_with_line(&self, [x0, y0]: [f64; 2], [x1, y1]: [f64; 2]) -> bool {
        let tx = x1 - x0;
        let ty = y1 - y0;
        let sign = tx * (self.x.min - x0) + ty * (self.y.min - y0) > 0.;
        for (x, y) in [
            (self.x.min, self.y.max),
            (self.x.max, self.y.max),
            (self.x.max, self.y.min),
        ] {
            if (tx * (x - x0) + ty * (y - y0) < 0.) == sign {
                return true;
            }
        }
        false
    }
    /// Precondition: [x0, y0] is inside box.
    fn intersect_from_interior(&self, [x0, y0]: [f64; 2], [x1, y1]: [f64; 2]) -> [f64; 2] {
        let x_line = if x1 > x0 { self.x.max } else { self.x.min };
        let y_line = if y1 > y0 { self.y.max } else { self.y.min };
        let t_x = if x1 == x0 {
            f64::INFINITY
        } else {
            (x_line - x0) / (x1 - x0)
        };
        let t_y = if y1 == y0 {
            f64::INFINITY
        } else {
            (y_line - y0) / (y1 - y0)
        };
        if t_y < 0. || (0. ..t_y).contains(&t_x) {
            [x_line, y0 + t_x * (y1 - y0)]
        } else {
            [x0 + t_y * (x1 - x0), y_line]
        }
    }
    fn intersect_from_exterior(
        &self,
        [x0, y0]: [f64; 2],
        [x1, y1]: [f64; 2],
    ) -> Option<[[f64; 2]; 2]> {
        let v_x = x1 - x0;
        let v_y = y1 - y0;
        // | x0 + t v_x - m_x | = (x1-x0)/2
        let a_x = 0.5 * (self.x.max + self.x.min - (self.x.max - self.x.min).copysign(v_x));
        if x0.is_infinite() {
            if (x1 > a_x) == (x0 < 0.) && x1.is_finite() && y1.is_finite() {
                return Some([[a_x, y1], [x1.clamp(self.x.min, self.x.max), y1]]);
            }
            return None;
        }
        if (x0 < a_x) != (v_x > 0.) {
            return None;
        }
        let y = y0 + (a_x - x0) / v_x * v_y;
        let p1 = if (self.y.min..=self.y.max).contains(&y) {
            [a_x, y]
        } else {
            // | y0 + t v_y - m_y | = (y1-y0)/2
            let a_y = 0.5 * (self.y.max + self.y.min - (self.y.max - self.y.min).copysign(v_y));
            if (y0 < a_y) != (v_y > 0.) {
                return None;
            }
            let x = x0 + (a_y - y0) / v_y * v_x;
            if (self.x.min..=self.x.max).contains(&x) {
                [x, a_y]
            } else {
                return None;
            }
        };
        let p2 = self.intersect_from_interior([x0, y0], [x1, y1]);
        Some([p1, p2])
    }

    fn collect_connected_component(
        &self,
        mut head: Vec<[f64; 2]>,
        chain: &mut impl Iterator<Item = [f64; 2]>,
    ) -> (Vec<[f64; 2]>, Option<[f64; 2]>) {
        let mut next = None;
        for p in chain {
            if !self.contains(p) {
                next = (!is_undefined(p)).then_some(p);
                break;
            }
            head.push(p);
        }
        if let (Some(interior), Some(exterior)) = (head.last(), next) {
            let x = self.intersect_from_interior(*interior, exterior);
            if f64::abs(x[0] - interior[0]) > f64::EPSILON
                || f64::abs(x[1] - interior[1]) > f64::EPSILON
            {
                head.push(x);
            }
        }
        (head, next)
    }

    /// 2D version of [Interval::shifted_to].
    fn shifted_to(&self, (i, j): (i32, i32)) -> Self {
        Self {
            x: self.x.shifted_to(i),
            y: self.y.shifted_to(j),
        }
    }
}

/// State part of [ClipPathComponentIterator] (periodic case).
pub struct ClipPathPeriodicComponentIteratorData<
    'a,
    XPeriodicity: Periodicity,
    YPeriodicity: Periodicity,
> {
    previous: Option<[f64; 2]>,
    current: Option<[f64; 2]>,
    origin_clip_box: &'a XyInterval,
    clip_box: XyInterval,
    tile: (i32, i32),
    x_periodicity: std::marker::PhantomData<XPeriodicity>,
    y_periodicity: std::marker::PhantomData<YPeriodicity>,
}
impl<XPeriodicity: Periodicity, YPeriodicity: Periodicity>
    ClipPathPeriodicComponentIteratorData<'_, XPeriodicity, YPeriodicity>
{
    fn map_to_fundamental_tile(&self, [x, y]: [f64; 2]) -> [f64; 2] {
        let dx = self.origin_clip_box.x.length();
        let dy = self.origin_clip_box.y.length();
        let (i, j) = self.tile;
        [x - i as f64 * dx, y - j as f64 * dy]
    }

    fn goto_tile(&mut self, (i, j): (i32, i32)) {
        self.tile = (i, j);
        self.clip_box = self.origin_clip_box.shifted_to((i, j));
    }

    fn try_skip_to_next_visible_tile(&mut self, [x0, y0]: [f64; 2], [x1, y1]: [f64; 2]) {
        let t_intersect = |a0, a1, a_range: &Interval| {
            if f64::is_infinite(a0) {
                return 1.;
            }
            let t_1: f64 = (a_range.min - a0) / (a1 - a0);
            let t_2: f64 = (a_range.max - a0) / (a1 - a0);
            if t_1.is_nan() || t_2.is_nan() {
                if (a_range.min..=a_range.max).contains(&a0) {
                    return 0.;
                }
                return -f64::INFINITY;
            }
            if (t_1 < 0.) ^ (t_2 < 0.) {
                return 0.;
            }
            let t_min = f64::min(t_1, t_2);
            if t_min < 0. {
                return f64::max(t_1, t_2);
            }
            t_min
        };
        let n_intersect = |t: f64, b0: f64, b1: f64, b_range: &Interval| {
            let b = b0 + t * (b1 - b0);
            let n = b_range.offset_of(b);
            let shifted_min = b_range.min + n as f64 * b_range.length();
            if b == shifted_min && b1 < b0 {
                n - 1
            } else {
                n
            }
        };
        let tile = match (XPeriodicity::IS_PERIODIC, YPeriodicity::IS_PERIODIC) {
            (false, false) => {
                if !self.clip_box.intersects_with_line([x0, y0], [x1, y1]) {
                    return;
                }
                (0, 0)
            }
            (true, false) => {
                let t = t_intersect(y0, y1, &self.origin_clip_box.y);
                if t < 0. {
                    return;
                }
                (n_intersect(t, x0, x1, &self.origin_clip_box.x), 0)
            }
            (false, true) => {
                let t = t_intersect(x0, x1, &self.origin_clip_box.x);
                if t < 0. {
                    return;
                }
                (0, n_intersect(t, y0, y1, &self.origin_clip_box.y))
            }
            (true, true) => {
                let mut tile = self.origin_clip_box.offset_of([x0, y0]);
                let tile_box = self.origin_clip_box.shifted_to(tile);
                if x0 == tile_box.x.min && x1 < x0 {
                    tile.0 -= 1;
                }
                if y0 == tile_box.y.min && y1 < y0 {
                    tile.1 -= 1;
                }
                tile
            }
        };
        self.goto_tile(tile);
    }

    fn neighboring_tile(&self, p: [f64; 2], p_to: [f64; 2]) -> (i32, i32) {
        let d_tile = Self::neighboring_tile_step(&self.clip_box, p, p_to);
        (self.tile.0 + d_tile.0, self.tile.1 + d_tile.1)
    }

    fn neighboring_tile_step(
        clip_box: &XyInterval,
        [x, y]: [f64; 2],
        [x_to, y_to]: [f64; 2],
    ) -> (i32, i32) {
        if (clip_box.x.min == x || clip_box.x.max == x)
            && (clip_box.y.min == y || clip_box.y.max == y)
        {
            let step_x = if x_to < x && clip_box.x.min == x {
                -1
            } else {
                i32::from(x_to > x && clip_box.x.max == x)
            };
            let step_y = if y_to < y && clip_box.y.min == y {
                -1
            } else {
                i32::from(y_to > y && clip_box.y.max == y)
            };
            return (step_x, step_y);
        }
        let distances = [
            (f64::abs(clip_box.x.min - x), (-1, 0)),
            (f64::abs(clip_box.x.max - x), (1, 0)),
            (f64::abs(clip_box.y.min - y), (0, -1)),
            (f64::abs(clip_box.y.max - y), (0, 1)),
        ];
        distances
            .into_iter()
            .reduce(|(t1, step_1), (t2, step_2)| if t1 < t2 { (t1, step_1) } else { (t2, step_2) })
            .unwrap()
            .1
    }

    fn advance_to(&mut self, p: Option<[f64; 2]>) {
        self.previous = self.current;
        self.current = p;
        if let (Some([p_x, p_y]), Some([c_x, c_y])) = (self.previous, self.current) {
            let finite_previous = if p_x.is_infinite() && XPeriodicity::IS_PERIODIC {
                [c_x + self.origin_clip_box.x.length().copysign(p_x), c_y]
            } else if p_y.is_infinite() && YPeriodicity::IS_PERIODIC {
                [c_x, c_y + self.origin_clip_box.y.length().copysign(p_y)]
            } else {
                return;
            };
            self.try_skip_to_next_visible_tile(finite_previous, [c_x, c_y]);
            self.previous = Some(finite_previous);
        }
    }
}

impl<T: Iterator<Item = [f64; 2]>, XPeriodicity: Periodicity, YPeriodicity: Periodicity>
    ClipPathComponentIterator<
        ClipPathPeriodicComponentIteratorData<'_, XPeriodicity, YPeriodicity>,
        T,
    >
{
    fn next_defined(chain: &mut T) -> Option<[f64; 2]> {
        for p in chain {
            if is_undefined(p) {
                continue;
            }
            return Some(p);
        }
        None
    }

    fn advance(&mut self) {
        let next = self.chain.next();
        self.state.advance_to(next);
    }

    fn insert_next_defined(&mut self, p: [f64; 2]) {
        self.state.advance_to(Some(p));
        self.skip_undefined();
    }
    fn skip_undefined(&mut self) {
        let Some(current) = self.state.current else {
            return;
        };
        if is_undefined(current) {
            self.state.previous = None;
            self.state.current = Self::next_defined(&mut self.chain);
        }
    }
    fn goto_neighboring_tile(&mut self, p: [f64; 2], p_to: [f64; 2]) {
        self.state.goto_tile(self.state.neighboring_tile(p, p_to));
        if !Self::tile_is_visible(self.state.tile) {
            self.skip_to_next_defined();
        }
    }
    fn tile_is_visible(p: (i32, i32)) -> bool {
        <(XPeriodicity, YPeriodicity)>::nth_domain_is_visible(p)
    }

    fn skip_to_next_defined(&mut self) {
        let Some(p) = self.chain.next() else {
            self.state.current = None;
            return;
        };
        self.insert_next_defined(p);
    }

    fn collect_connected_component(&mut self, head: Vec<[f64; 2]>) -> Vec<[f64; 2]> {
        let (component, next) = self
            .state
            .clip_box
            .collect_connected_component(head, &mut self.chain);
        let mapped_component = component
            .iter()
            .map(|p| self.state.map_to_fundamental_tile(*p))
            .collect::<Vec<_>>();
        if let Some(next) = next {
            self.state.current = component.last().cloned();
            self.insert_next_defined(next);
            if let Some(boundary_point) = self.state.previous {
                self.goto_neighboring_tile(boundary_point, next);
            }
        } else {
            self.skip_to_next_defined();
        }
        mapped_component
    }

    fn next_raw_component(&mut self) -> Option<Vec<[f64; 2]>> {
        loop {
            let current = self.state.current?;
            let Some(previous) = self.state.previous else {
                self.advance();
                if self.state.current.map(is_undefined).unwrap_or(true) {
                    let tile = self.state.origin_clip_box.offset_of(current);
                    self.skip_undefined();
                    if Self::tile_is_visible(tile) {
                        self.state.goto_tile(tile);
                        return Some(vec![self.state.map_to_fundamental_tile(current)]);
                    }
                }

                if let (Some(previous), Some(current)) = (self.state.previous, self.state.current) {
                    self.state.try_skip_to_next_visible_tile(previous, current);
                }
                continue;
            };
            if !Self::tile_is_visible(self.state.tile) {
                self.state.try_skip_to_next_visible_tile(previous, current);
            }
            if Self::tile_is_visible(self.state.tile) {
                match (
                    self.state.clip_box.contains(previous),
                    self.state.clip_box.contains(current),
                ) {
                    (false, true) => {
                        let path = self.collect_connected_component(vec![
                            self.state
                                .clip_box
                                .intersect_from_interior(current, previous),
                            current,
                        ]);
                        return Some(path);
                    }
                    (true, true) => {
                        let path = self.collect_connected_component(vec![previous, current]);
                        return Some(path);
                    }
                    (true, false) => {
                        let intersection_point = self
                            .state
                            .clip_box
                            .intersect_from_interior(previous, current);
                        let (skip_to_infinity, path) = if intersection_point == previous {
                            (false, vec![self.state.map_to_fundamental_tile(previous)])
                        } else {
                            let p = vec![
                                self.state.map_to_fundamental_tile(previous),
                                self.state.map_to_fundamental_tile(intersection_point),
                            ];
                            (
                                (current[0].is_infinite()
                                    && f64::abs(p[1][0] - p[0][0])
                                        == self.state.origin_clip_box.x.length())
                                    || (current[1].is_infinite()
                                        && f64::abs(p[1][1] - p[0][1])
                                            == self.state.origin_clip_box.y.length()),
                                p,
                            )
                        };
                        if skip_to_infinity {
                            self.skip_to_next_defined();
                        } else {
                            self.goto_neighboring_tile(intersection_point, current);
                        }
                        return Some(path);
                    }
                    (false, false) => {
                        if current[0].is_infinite() || current[1].is_infinite() {
                            self.skip_to_next_defined();
                            continue;
                        }
                        if let Some([a, b]) = self
                            .state
                            .clip_box
                            .intersect_from_exterior(previous, current)
                        {
                            let mapped_segment = vec![
                                self.state.map_to_fundamental_tile(a),
                                self.state.map_to_fundamental_tile(b),
                            ];
                            self.goto_neighboring_tile(b, current);
                            return Some(mapped_segment);
                        }
                    }
                }
            }
            self.skip_to_next_defined();
            if let Some(previous) = self.state.previous {
                self.state
                    .goto_tile(self.state.origin_clip_box.offset_of(previous));
            }
        }
    }
}

type ClipPathPeriodicComponentIterator<'a, XPeriodicity, YPeriodicity, I> =
    ClipPathComponentIterator<
        ClipPathPeriodicComponentIteratorData<'a, XPeriodicity, YPeriodicity>,
        I,
    >;

impl<T: Iterator<Item = [f64; 2]>, XPeriodicity: Periodicity, YPeriodicity: Periodicity> Iterator
    for ClipPathComponentIterator<
        ClipPathPeriodicComponentIteratorData<'_, XPeriodicity, YPeriodicity>,
        T,
    >
{
    type Item = Vec<[f64; 2]>;
    fn next(&mut self) -> Option<Self::Item> {
        self.next_raw_component()
    }
}

impl<'a, XPeriodicity: Periodicity> ClipPath for &'a ClipBox<'a, XPeriodicity, Periodic> {
    type ComponentIteratorState = ClipPathPeriodicComponentIteratorData<'a, XPeriodicity, Periodic>;
    fn clip_path<I: IntoIterator<Item = [f64; 2]>>(
        self,
        chain: I,
    ) -> ClipPathComponentIterator<Self::ComponentIteratorState, std::iter::Fuse<I::IntoIter>> {
        clip_path_periodic(self, chain)
    }
}
impl<'a, XPeriodicity: Periodicity> ClipPathForEach for &'a ClipBox<'a, XPeriodicity, Periodic> {
    fn clip_path_for_each<I: IntoIterator<Item = [f64; 2]>, F: FnMut(&[[f64; 2]])>(
        self,
        chain: I,
        mut f: F,
    ) {
        for path_component in self.clip_path(chain) {
            f(&path_component);
        }
    }
}

impl<'a> ClipPath for &'a ClipBox<'a, Periodic, NotPeriodic> {
    type ComponentIteratorState = ClipPathPeriodicComponentIteratorData<'a, Periodic, NotPeriodic>;
    fn clip_path<I: IntoIterator<Item = [f64; 2]>>(
        self,
        chain: I,
    ) -> ClipPathComponentIterator<Self::ComponentIteratorState, std::iter::Fuse<I::IntoIter>> {
        clip_path_periodic(self, chain)
    }
}
impl<'a> ClipPathForEach for &'a ClipBox<'a, Periodic, NotPeriodic> {
    fn clip_path_for_each<I: IntoIterator<Item = [f64; 2]>, F: FnMut(&[[f64; 2]])>(
        self,
        chain: I,
        mut f: F,
    ) {
        for path_component in self.clip_path(chain) {
            f(&path_component);
        }
    }
}

impl<'a> ClipPath for &'a ClipBox<'a, NotPeriodic, NotPeriodic> {
    type ComponentIteratorState = ClipPathComponentIteratorData<'a>;
    fn clip_path<I: IntoIterator<Item = [f64; 2]>>(
        self,
        chain: I,
    ) -> ClipPathComponentIterator<Self::ComponentIteratorState, std::iter::Fuse<I::IntoIter>> {
        self.interval.clip_path(chain)
    }
}
impl<'a> ClipPathForEach for &'a ClipBox<'a, NotPeriodic, NotPeriodic> {
    fn clip_path_for_each<I: IntoIterator<Item = [f64; 2]>, F: FnMut(&[[f64; 2]])>(
        self,
        chain: I,
        mut f: F,
    ) {
        for path_component in self.clip_path(chain) {
            f(&path_component);
        }
    }
}

fn clip_path_periodic<
    'a,
    XPeriodicity: Periodicity,
    YPeriodicity: Periodicity,
    I: IntoIterator<Item = [f64; 2]>,
>(
    clip_box: &'a ClipBox<'a, XPeriodicity, YPeriodicity>,
    chain: I,
) -> ClipPathComponentIterator<
    ClipPathPeriodicComponentIteratorData<'a, XPeriodicity, YPeriodicity>,
    std::iter::Fuse<I::IntoIter>,
>
where
    (XPeriodicity, YPeriodicity): IsPeriodic, // Exclude not periodic case
{
    let mut chain = chain.into_iter().fuse();
    let current =
        ClipPathPeriodicComponentIterator::<'_, XPeriodicity, YPeriodicity, _>::next_defined(
            &mut chain,
        );
    ClipPathPeriodicComponentIterator {
        chain,
        state: ClipPathPeriodicComponentIteratorData {
            previous: None,
            current,
            origin_clip_box: clip_box.interval,
            clip_box: clip_box.interval.clone(),
            tile: (0, 0),
            x_periodicity: std::marker::PhantomData::<XPeriodicity>,
            y_periodicity: std::marker::PhantomData::<YPeriodicity>,
        },
    }
}

fn is_undefined([x, y]: [f64; 2]) -> bool {
    x.is_nan() || y.is_nan() || (x.is_infinite() && y.is_infinite())
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::xy_chart::periodicity::{NotPeriodic, Periodic};

    impl XyInterval {
        fn with_periodicity<'a, XPeriodicity: Periodicity, YPeriodicity: Periodicity>(
            &'a self,
        ) -> ClipBox<'a, XPeriodicity, YPeriodicity> {
            ClipBox {
                interval: self,
                x_periodicity: std::marker::PhantomData,
                y_periodicity: std::marker::PhantomData,
            }
        }
    }

    const UNIT_CLIP_BOX: XyInterval = XyInterval {
        x: Interval { min: 0., max: 1. },
        y: Interval { min: 0., max: 1. },
    };

    #[test]
    fn test_clip_path_returns_empty_iterator_for_empty_path() {
        let chain = [];
        let mut components = UNIT_CLIP_BOX.clip_path(chain);
        assert_eq!(components.next(), None);
    }
    #[test]
    fn test_clip_path_returns_one_segment_for_interior_point() {
        let chain = [[0.5, 0.5]];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.5, 0.5]]]);
    }
    #[test]
    fn test_clip_path_returns_empty_iterator_for_exterior_point() {
        let chain = [[2.0, 2.0]];
        let mut components = UNIT_CLIP_BOX.clip_path(chain);
        assert_eq!(components.next(), None);
    }
    #[test]
    fn test_clip_path_returns_multiple_segments_for_intersecting_path() {
        let chain = [
            [3.0, 0.5],
            [-3.0, 0.5],
            [0.5, 3.0],
            [0.5, 0.5],
            [0.75, 0.75],
        ];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(
            &components,
            &[
                vec![[1.0, 0.5], [0.0, 0.5]],
                vec![[0.5, 1.0], [0.5, 0.5], [0.75, 0.75]]
            ]
        );
    }
    #[test]
    fn test_clip_path_breaks_path_at_nan() {
        let chain = [
            [0.25, 0.5],
            [f64::NAN, 0.5],
            [0.5, f64::NAN],
            [0.75, 0.5],
            [0.5, 0.75],
        ];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(
            &components,
            &[vec![[0.25, 0.5]], vec![[0.75, 0.5], [0.5, 0.75]]]
        );
    }
    #[test]
    fn test_clip_path_handles_infinity_to_finite() {
        let chain = [[f64::INFINITY, 0.25], [-1.0, 0.5]];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[1.0, 0.5], [0.0, 0.5]]]);

        let chain = [[f64::INFINITY, 0.25], [2.0, 0.5]];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(components, Vec::<Vec<[f64; 2]>>::new());

        let chain = [[f64::INFINITY, 0.25], [0.5, 0.5]];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[1.0, 0.5], [0.5, 0.5]]]);

        let chain = [[0.25, -f64::INFINITY], [0.5, 1.0]];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.5, 0.0], [0.5, 1.0]]]);

        let chain = [[0.25, -f64::INFINITY], [0.5, -1.0]];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(components, Vec::<Vec<[f64; 2]>>::new());

        let chain = [[0.25, -f64::INFINITY], [0.5, 0.5]];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.5, 0.0], [0.5, 0.5]]]);
    }
    #[test]
    fn test_clip_path_handles_infinity_to_finite_to_infinity() {
        let chain = [
            [0.5, f64::INFINITY],
            [0.75, 0.5],
            [f64::INFINITY, 0.25],
            [-1.0, 0.5],
            [-f64::INFINITY, 0.25],
        ];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(
            &components,
            &[
                vec![[0.75, 1.0], [0.75, 0.5], [1., 0.5]],
                vec![[1.0, 0.5], [0.0, 0.5]]
            ]
        );
    }
    #[test]
    fn test_clip_path_handles_infinity_to_infinity() {
        for chain in [
            [[f64::INFINITY, 0.25], [-f64::INFINITY, 0.5]],
            [[f64::INFINITY, 0.25], [f64::INFINITY, 0.5]],
            [[0.25, f64::INFINITY], [0.5, -f64::INFINITY]],
            [[0.25, f64::INFINITY], [0.5, f64::INFINITY]],
            [[f64::INFINITY, 0.25], [0.5, f64::INFINITY]],
            [[0.25, f64::INFINITY], [f64::INFINITY, 0.5]],
        ] {
            let mut components = UNIT_CLIP_BOX.clip_path(chain);
            assert_eq!(components.next(), None);
        }

        let chain = [[0.0, 0.0], [f64::INFINITY, 0.25], [-f64::INFINITY, 0.5]];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.0, 0.0], [1.0, 0.0]]]);

        let chain = [[0.0, 0.0], [f64::INFINITY, 0.25], [f64::INFINITY, 0.5]];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.0, 0.0], [1.0, 0.0]]]);

        let chain = [[f64::INFINITY, 0.25], [0.5, f64::INFINITY], [0.0, 0.0]];
        let components = UNIT_CLIP_BOX.clip_path(chain).collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.0, 1.0], [0.0, 0.0]]]);
    }

    #[test]
    fn test_clip_path_periodic_returns_empty_iterator_for_empty_path() {
        let clip_box = UNIT_CLIP_BOX.with_periodicity::<Periodic, Periodic>();
        let mut components = clip_box.clip_path([].into_iter());
        assert_eq!(components.next(), None);
        let clip_box = UNIT_CLIP_BOX.with_periodicity::<NotPeriodic, Periodic>();
        let mut components = clip_box.clip_path([].into_iter());
        assert_eq!(components.next(), None);
        let clip_box = UNIT_CLIP_BOX.with_periodicity::<Periodic, NotPeriodic>();
        let mut components = clip_box.clip_path([].into_iter());
        assert_eq!(components.next(), None);
    }
    #[test]
    fn test_clip_path_periodic_returns_one_segment_for_interior_point() {
        let chain = [[0.5, 0.5]];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.5, 0.5]]]);
        let components = UNIT_CLIP_BOX
            .with_periodicity::<NotPeriodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.5, 0.5]]]);
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, NotPeriodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.5, 0.5]]]);
    }
    #[test]
    fn test_clip_path_periodic_folds_exterior_point_into_primary_domain() {
        let chain = [[2.5, 2.5]];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.5, 0.5]]]);
        let clip_box = UNIT_CLIP_BOX.with_periodicity::<NotPeriodic, Periodic>();
        let mut components = clip_box.clip_path(chain.clone().into_iter());
        assert_eq!(components.next(), None);
        let clip_box = UNIT_CLIP_BOX.with_periodicity::<Periodic, NotPeriodic>();
        let mut components = clip_box.clip_path(chain.clone().into_iter());
        assert_eq!(components.next(), None);
    }
    #[test]
    fn test_clip_path_xy_periodic_returns_multiple_segments_for_intersecting_path() {
        let chain = [
            [1.5, 0.5],
            [-2.0, 0.5],
            [0.5, 3.0],
            [0.5, 0.5],
            [0.75, 0.75],
        ];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(
            &components,
            &[
                // (3/2, 1/2) -> (-2, 1/2):
                vec![[0.5, 0.5], [0.0, 0.5]], // (3/2, 1/2) -> (1, 1/2)
                vec![[1.0, 0.5], [0.0, 0.5]], // (1, 1/2) -> (0, 1/2)
                vec![[1.0, 0.5], [0.0, 0.5]], // (0, 1/2) -> (-1, 1/2)
                // -> (-2, 1/2) ->
                vec![[1.0, 0.5], [0.0, 0.5], [0.5, 1.0]], // (-1, 1/2) -> (-2, 1/2) -> (-3/2, 1)
                // (-3/2, 1) -> (1/2, 3):
                vec![[0.5, 0.0], [1.0, 0.5]], // (-3/2, 1) -> (-1, 3/2)
                vec![[0.0, 0.5], [0.5, 1.0]], // (-1, 1.5) -> (-1/2, 2)
                vec![[0.5, 0.0], [1.0, 0.5]], // (-1/2, 2) -> (0, 2.5)
                // -> (1/2, 3) ->
                vec![[0.0, 0.5], [0.5, 1.0], [0.5, 0.0]], // (0, 2.5) -> (1/2, 3) -> (1/2, 2)
                // (1/2, 2) -> (1/2, 1):
                vec![[0.5, 1.0], [0.5, 0.0]], // (1/2, 2) -> (1/2, 1)
                // (1/2, 1) -> (1/2, 1/2) -> (3/4, 3/4):
                vec![[0.5, 1.0], [0.5, 0.5], [0.75, 0.75]]
            ]
        );
    }
    #[test]
    fn test_clip_path_y_periodic_returns_multiple_segments_for_intersecting_path() {
        let chain = [
            [1.5, 0.5],
            [-2.0, 0.5],
            [0.5, 3.0],
            [0.5, 0.5],
            [0.75, 0.75],
        ];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<NotPeriodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(
            &components,
            &[
                // (1.5, 1/2) -> (-2, 1/2):
                vec![[1.0, 0.5], [0.0, 0.5]],
                // -> (-2, 1/2) ->:
                vec![[0.0, 0.5], [0.5, 1.0], [0.5, 0.0]], // (0, 2.5) -> (1/2, 3) -> (1/2, 2)
                // (1/2, 3) -> (1/2, 1/2):
                vec![[0.5, 1.0], [0.5, 0.0]], // (1/2, 2) -> (1/2, 1)
                // (1/2, 1) -> (1/2, 1/2) -> (3/4, 3/4):
                vec![[0.5, 1.0], [0.5, 0.5], [0.75, 0.75]]
            ]
        );
    }
    #[test]
    fn test_clip_path_x_periodic_returns_multiple_segments_for_intersecting_path() {
        let chain = [
            [1.5, 0.5],
            [-2.0, 0.5],
            [0.5, 3.0],
            [0.5, 0.5],
            [0.75, 0.75],
        ];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, NotPeriodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(
            &components,
            &[
                // (3/2, 1/2) -> (-2, 1/2):
                vec![[0.5, 0.5], [0.0, 0.5]], // (3/2, 1/2) -> (1, 1/2)
                vec![[1.0, 0.5], [0.0, 0.5]], // (1, 1/2) -> (0, 1/2)
                vec![[1.0, 0.5], [0.0, 0.5]], // (0, 1/2) -> (-1, 1/2)
                // -> (-2, 1/2) ->:
                vec![[1.0, 0.5], [0.0, 0.5], [0.5, 1.0]], // (-1, 1/2) -> (-2, 1/2) -> (-3/2, 1)
                // (-2, 1/2) -> (1/2, 3):
                // (1/2, 3) -> (1/2, 1): not visible
                // (1/2, 1) -> (1/2, 1/2) -> (3/4, 3/4):
                vec![[0.5, 1.0], [0.5, 0.5], [0.75, 0.75]]
            ]
        );
    }

    #[test]
    fn test_clip_path_x_and_xy_periodic_does_not_generate_unnecessary_points_on_domain_boundary() {
        let chain = [[1.5, 0.5], [1.0, 0.5], [0.5, 0.5]];
        let expected_components = [
            // (3/2, 1/2) -> (1, 1/2):
            vec![[0.5, 0.5], [0.0, 0.5]],
            // (1, 1/2) -> (1/2, 1/2):
            vec![[1.0, 0.5], [0.5, 0.5]],
        ];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &expected_components);
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, NotPeriodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &expected_components);
    }
    #[test]
    fn test_clip_path_y_periodic_does_not_generate_unnecessary_points_on_domain_boundary() {
        let chain = [[0.5, 1.5], [0.5, 1.0], [0.5, 0.5]];
        let expected_components = [
            // (1/2, 3/2) -> (1/2, 1):
            vec![[0.5, 0.5], [0.5, 0.0]],
            // (1/2, 1) -> (1/2, 1/2):
            vec![[0.5, 1.0], [0.5, 0.5]],
        ];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<NotPeriodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &expected_components);
    }
    #[test]
    fn test_clip_path_periodic_breaks_path_at_nan() {
        let chain = [
            [0.25, 0.5],
            [f64::NAN, 0.5],
            [0.5, f64::NAN],
            [0.75, 0.5],
            [0.5, 0.75],
        ];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(
            &components,
            &[vec![[0.25, 0.5]], vec![[0.75, 0.5], [0.5, 0.75]]]
        );
        let components = UNIT_CLIP_BOX
            .with_periodicity::<NotPeriodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(
            &components,
            &[vec![[0.25, 0.5]], vec![[0.75, 0.5], [0.5, 0.75]]]
        );
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, NotPeriodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(
            &components,
            &[vec![[0.25, 0.5]], vec![[0.75, 0.5], [0.5, 0.75]]]
        );
    }

    #[test]
    fn test_clip_path_xy_periodic_does_not_generate_unnecessary_points_on_domain_corner() {
        for (end, expected_segment) in [
            ([0.5, 0.0], [[0.0, 0.0], [0.5, 0.0]]),
            ([0.5, 0.5], [[0.0, 0.0], [0.5, 0.5]]),
            ([0.0, 0.5], [[0.0, 0.0], [0.0, 0.5]]),
            ([-0.5, 0.5], [[1.0, 0.0], [0.5, 0.5]]),
            ([-0.5, 0.0], [[1.0, 0.0], [0.5, 0.0]]),
            ([-0.5, -0.5], [[1.0, 1.0], [0.5, 0.5]]),
            ([0.0, -0.5], [[0.0, 1.0], [0.0, 0.5]]),
            ([0.5, -0.5], [[0.0, 1.0], [0.5, 0.5]]),
        ] {
            let chain = [[0.0, 0.0], end];
            let expected_components = [expected_segment.to_vec()];
            let components = UNIT_CLIP_BOX
                .with_periodicity::<Periodic, Periodic>()
                .clip_path(chain.clone().into_iter())
                .collect::<Vec<_>>();
            assert_eq!(&components, &expected_components);
        }
    }
    #[test]
    fn test_clip_path_x_periodic_does_not_generate_unnecessary_points_on_domain_corner() {
        for (end, expected_segment) in [
            ([0.5, 0.0], vec![[0.0, 0.0], [0.5, 0.0]]),
            ([0.5, 0.5], vec![[0.0, 0.0], [0.5, 0.5]]),
            ([0.0, 0.5], vec![[0.0, 0.0], [0.0, 0.5]]),
            ([-0.5, 0.5], vec![[1.0, 0.0], [0.5, 0.5]]),
            ([-0.5, 0.0], vec![[1.0, 0.0], [0.5, 0.0]]),
            ([-0.5, -0.5], vec![[1.0, 0.0]]),
            ([0.0, -0.5], vec![[0.0, 0.0]]),
            ([0.5, -0.5], vec![[0.0, 0.0]]),
        ] {
            let chain = [[0.0, 0.0], end];
            let expected_components = [expected_segment];
            let components = UNIT_CLIP_BOX
                .with_periodicity::<Periodic, NotPeriodic>()
                .clip_path(chain.clone().into_iter())
                .collect::<Vec<_>>();
            assert_eq!(&components, &expected_components);
        }
    }
    #[test]
    fn test_clip_path_y_periodic_does_not_generate_unnecessary_points_on_domain_corner() {
        for (end, expected_segment) in [
            ([0.5, 0.0], vec![[0.0, 0.0], [0.5, 0.0]]),
            ([0.5, 0.5], vec![[0.0, 0.0], [0.5, 0.5]]),
            ([0.0, 0.5], vec![[0.0, 0.0], [0.0, 0.5]]),
            ([-0.5, 0.5], vec![[0.0, 0.0]]),
            ([-0.5, 0.0], vec![[0.0, 0.0]]),
            ([-0.5, -0.5], vec![[0.0, 1.0]]),
            ([0.0, -0.5], vec![[0.0, 1.0], [0.0, 0.5]]),
            ([0.5, -0.5], vec![[0.0, 1.0], [0.5, 0.5]]),
        ] {
            let chain = [[0.0, 0.0], end];
            let expected_components = [expected_segment];
            let components = UNIT_CLIP_BOX
                .with_periodicity::<NotPeriodic, Periodic>()
                .clip_path(chain.clone().into_iter())
                .collect::<Vec<_>>();
            assert_eq!(&components, &expected_components);
        }
    }

    #[test]
    fn test_clip_path_xy_periodic_handles_infinity() {
        let chain = [
            [0.5, f64::INFINITY],
            [0.75, 0.5],
            [f64::INFINITY, 0.25],
            [-1.0, 0.75],
            [-f64::INFINITY, 0.25],
        ];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(
            &components,
            &[
                vec![[0.75, 0.5], [0.75, 0.0]], // (1/2, ∞) -> (1/2, 1)
                vec![[0.75, 1.0], [0.75, 0.5], [1.0, 0.5]],
                vec![[0.0, 0.5], [1.0, 0.5]],   // (1, 1/2) -> (∞, 1/4)
                vec![[1.0, 0.75], [0.0, 0.75]], // (∞, 1/4) -> (-1, 3/4)
                vec![[1.0, 0.75], [0.0, 0.75]], // (-1, 3/4) -> (-∞, 1/4)
            ]
        );
    }
    #[test]
    fn test_clip_path_y_periodic_handles_infinity() {
        let chain = [
            [0.5, f64::INFINITY],
            [0.75, 0.5],
            [f64::INFINITY, 0.25],
            [-1.0, 0.75],
            [-f64::INFINITY, 0.25],
        ];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<NotPeriodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(
            &components,
            &[
                vec![[0.75, 0.5], [0.75, 0.0]], // (1/2, ∞) -> (1/2, 1)
                vec![[0.75, 1.0], [0.75, 0.5], [1., 0.5]],
                vec![[1.0, 0.75], [0.0, 0.75]] // (∞, 1/4) -> (-1, 3/4)
            ]
        );
    }
    #[test]
    fn test_clip_path_x_periodic_handles_infinity() {
        let chain = [
            [0.5, f64::INFINITY],
            [0.75, 0.5],
            [f64::INFINITY, 0.25],
            [-1.0, 0.75],
            [-f64::INFINITY, 0.25],
        ];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, NotPeriodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(
            &components,
            &[
                vec![[0.75, 1.0], [0.75, 0.5], [1., 0.5]],
                vec![[0.0, 0.5], [1.0, 0.5]],   // (1, 1/2) -> (∞, 1/4)
                vec![[1.0, 0.75], [0.0, 0.75]], // (∞, 1/4) -> (-1, 3/4)
                vec![[1.0, 0.75], [0.0, 0.75]], // (-1, 3/4) -> (-∞, 1/4)
            ]
        );
    }

    #[test]
    fn test_clip_path_periodic_handles_infinity_to_infinity() {
        for chain in [
            [[f64::INFINITY, 0.25], [-f64::INFINITY, 0.5]],
            [[f64::INFINITY, 0.25], [f64::INFINITY, 0.5]],
            [[f64::INFINITY, 0.25], [0.5, f64::INFINITY]],
        ] {
            let clip_box = UNIT_CLIP_BOX.with_periodicity::<Periodic, Periodic>();
            let mut components = clip_box.clip_path(chain.clone().into_iter());
            assert_eq!(components.next(), None);
            let clip_box = UNIT_CLIP_BOX.with_periodicity::<NotPeriodic, Periodic>();
            let mut components = clip_box.clip_path(chain.clone().into_iter());
            assert_eq!(components.next(), None);
            let clip_box = UNIT_CLIP_BOX.with_periodicity::<Periodic, NotPeriodic>();
            let mut components = clip_box.clip_path(chain.clone().into_iter());
            assert_eq!(components.next(), None);
        }

        let chain = [[0.0, 0.0], [f64::INFINITY, 0.25], [-f64::INFINITY, 0.5]];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.0, 0.0], [1.0, 0.0]]]);
        let components = UNIT_CLIP_BOX
            .with_periodicity::<NotPeriodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.0, 0.0], [1.0, 0.0]]]);
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, NotPeriodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.0, 0.0], [1.0, 0.0]]]);

        let chain = [[0.0, 0.0], [f64::INFINITY, 0.25], [f64::INFINITY, 0.5]];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.0, 0.0], [1.0, 0.0]]]);
        let chain = [[0.0, 0.0], [f64::INFINITY, 0.25], [f64::INFINITY, 0.5]];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<NotPeriodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.0, 0.0], [1.0, 0.0]]]);
        let chain = [[0.0, 0.0], [f64::INFINITY, 0.25], [f64::INFINITY, 0.5]];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, NotPeriodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.0, 0.0], [1.0, 0.0]]]);

        let chain = [[f64::INFINITY, 0.25], [0.5, f64::INFINITY], [0.0, 0.0]];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.0, 1.0], [0.0, 0.0]]]);
        let chain = [[f64::INFINITY, 0.25], [0.5, f64::INFINITY], [0.0, 0.0]];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<NotPeriodic, Periodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.0, 1.0], [0.0, 0.0]]]);
        let chain = [[f64::INFINITY, 0.25], [0.5, f64::INFINITY], [0.0, 0.0]];
        let components = UNIT_CLIP_BOX
            .with_periodicity::<Periodic, NotPeriodic>()
            .clip_path(chain.clone().into_iter())
            .collect::<Vec<_>>();
        assert_eq!(&components, &[vec![[0.0, 1.0], [0.0, 0.0]]]);
    }
}
