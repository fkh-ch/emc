//! Combine multiple plots
use crate::gfx;
use crate::xy_chart::{
    legend::{ExtendLegendEntries, LegendEntry},
    DataRegion, PlotTo,
};

/// Overlay two plots. Can be used nested.
pub struct PlotStack<A, B>(pub A, pub B);

/// Use a plot implementing [PlotTo] with `()` return type as [PlotTo] with a `Result<(), Infallible>`.
pub struct AsTryPlotTo<P>(pub P);

impl<T: PlotTo> PlotTo for AsTryPlotTo<&T> {
    type ReturnType = Result<(), std::convert::Infallible>;

    fn plot_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        data_region: &DataRegion,
    ) -> Self::ReturnType {
        self.0.plot_to(drawable, data_region);
        Ok(())
    }
}

impl<A: PlotTo, B: PlotTo<ReturnType = ()>> PlotTo for PlotStack<A, B> {
    type ReturnType = A::ReturnType;
    fn plot_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        data_region: &super::DataRegion,
    ) -> Self::ReturnType {
        let result = self.0.plot_to(drawable, data_region);
        self.1.plot_to(drawable, data_region);
        result
    }
}

impl<A, B> ExtendLegendEntries for PlotStack<A, B>
where
    A: ExtendLegendEntries,
    B: ExtendLegendEntries,
{
    fn extend_legend_entries(&self, entries: &mut Vec<LegendEntry>) {
        self.0.extend_legend_entries(entries);
        self.1.extend_legend_entries(entries);
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::xy_chart::legend::{
        ExtendLegendEntriesWithTitle, Legend, LegendEntry, NoLegendEntry,
        WithLegendEntriesFromPlotWithTitle,
    };

    struct FakePlot;

    impl ExtendLegendEntriesWithTitle for FakePlot {
        fn extend_legend_entries_with_title<S: Into<String>>(
            &self,
            title: S,
            entries: &mut Vec<LegendEntry>,
        ) {
            entries.push(LegendEntry {
                title: title.into(),
                line_style: None,
                marker_style: None,
            });
        }
    }

    #[test]
    fn test_plot_stack_adds_entries_to_legend() {
        let stack = PlotStack(
            NoLegendEntry(FakePlot),
            WithLegendEntriesFromPlotWithTitle("Fake Plot", FakePlot),
        );
        let legend = Legend::default();
        let legend = legend.add_entries(&stack);
        assert_eq!(
            legend
                .entries
                .into_iter()
                .map(|entry| entry.title)
                .collect::<Vec<_>>(),
            vec!["Fake Plot".to_string()]
        );
    }

    #[test]
    fn test_plot_stack_adds_optional_entries_to_legend() {
        let stack = PlotStack(
            PlotStack(
                NoLegendEntry(FakePlot),
                None::<WithLegendEntriesFromPlotWithTitle<&str, FakePlot>>,
            ),
            Some(WithLegendEntriesFromPlotWithTitle("Fake Plot", FakePlot)),
        );
        let legend = Legend::default();
        let legend = legend.add_entries(&stack);
        assert_eq!(
            legend
                .entries
                .into_iter()
                .map(|entry| entry.title)
                .collect::<Vec<_>>(),
            vec!["Fake Plot".to_string()]
        );
    }
}
