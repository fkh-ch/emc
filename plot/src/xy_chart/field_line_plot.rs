//! Plot field lines of a 2D vector field with singularities using [numeric::field_lines::trace_field_lines]
//!
//! Can be plotted via [crate::xy_chart::ExtrinsicChartLayout::plot] and included in a legend
//! by using [crate::xy_chart::legend::Legend::add_entries].

use crate::{
    gfx,
    xy_chart::{
        clipped_plot::ClippedAt,
        legend::{ExtendLegendEntriesWithTitle, LegendEntry},
        DataRegion, Interval, PlotTo,
    },
};
use numeric::bezier::{into_cubic_bezier_curve, CubicBezierPoint};
use numeric::field_lines::trace_field_lines;

/// Properties of a field line plot.
///
/// See [numeric::field_lines::trace_field_lines] for details about how
/// `n_lines_at_singularity` will affect the number of outgoing field lines around singularities
pub struct FieldLinePlot<F> {
    /// Vector field to draw field lines for
    pub f: F,
    /// Singularities of the vector field for which there is a
    /// neighbourhood where the vector field is outward pointing
    pub source_singularities: Vec<(f64, f64)>,
    /// Singularities of the vector field for which there is a
    /// neighbourhood where the vector field is inward pointing
    pub sink_singularities: Vec<(f64, f64)>,
    /// Radius around singularities where field lines should start / stop
    pub r_singularity: f64,
    /// Number of field lines around source singularities
    pub n_lines_at_singularity: usize,
    /// Line style of the field lines
    pub line_style: gfx::LineStyle,
}

impl<F> ClippedAt for FieldLinePlot<F> {}

impl<F: Fn(f64, [f64; 2]) -> [f64; 2]> PlotTo for FieldLinePlot<F> {
    type ReturnType = Result<(), Vec<numeric::odeint::Error>>;
    fn plot_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        data_region: &DataRegion,
    ) -> Self::ReturnType {
        let (lines, warnings) = self.trace_field_lines(data_region);
        for field_line in lines {
            let bezier_curve = into_cubic_bezier_curve(field_line);
            drawable.stroke_path(
                bezier_curve.map(|p: CubicBezierPoint| data_region.chart_to_canvas.apply(p)),
                &self.line_style,
            );
        }
        if !warnings.is_empty() {
            Err(warnings)
        } else {
            Ok(())
        }
    }
}

impl<F: Fn(f64, [f64; 2]) -> [f64; 2]> FieldLinePlot<F> {
    fn trace_field_lines(
        &self,
        DataRegion { xy_interval, .. }: &DataRegion,
    ) -> (
        Vec<Vec<numeric::curve::C1Point>>,
        Vec<numeric::odeint::Error>,
    ) {
        let Interval {
            min: x_min,
            max: x_max,
        } = &xy_interval.y;
        let Interval {
            min: y_min,
            max: y_max,
        } = &xy_interval.y;
        let center = [0.5 * (x_min + x_max), 0.5 * (y_min + y_max)];
        trace_field_lines(
            &self.f,
            center,
            f64::hypot(x_max - center[0], y_max - center[1]),
            &self.source_singularities,
            &self.sink_singularities,
            self.r_singularity,
            self.n_lines_at_singularity,
        )
    }
}

impl<F> ExtendLegendEntriesWithTitle for FieldLinePlot<F> {
    fn extend_legend_entries_with_title<S: Into<String>>(
        &self,
        title: S,
        entries: &mut Vec<LegendEntry>,
    ) {
        entries.push(LegendEntry {
            title: title.into(),
            line_style: Some(self.line_style.clone()),
            marker_style: None,
        });
    }
}

#[cfg(test)]
mod test {
    use crate::gfx::testing::{self, Matcher};
    use crate::xy_chart::testing::empty_chart_from_ranges;

    use super::*;

    fn assert_is_straight_line(
        path: impl AsRef<[gfx::BezierPoint]>,
        from: (f64, f64),
        direction: (f64, f64),
        tolerance: f64,
    ) {
        let path = path.as_ref();
        assert!(
            (path[0].p.0 - from.0).abs() < tolerance && (path[0].p.1 - from.1).abs() < tolerance,
            "Path must start at {from:?}"
        );
        let cross = |p: (f64, f64), q: (f64, f64)| p.0 * q.1 - p.1 * q.0;
        for p in &path[1..] {
            assert!(
                cross(direction, (p.p.0 - from.0, p.p.1 - from.1)).abs() < tolerance,
                "{from:?}-{:?} is not along line in direction {direction:?}",
                p.p
            );
        }
        for p in path {
            assert!(
                cross(direction, (p.q_b.0 - p.p.0, p.q_b.1 - p.p.1)).abs() < tolerance,
                "{p:?}: Control point q_b is not along line in direction {direction:?}"
            );
            assert!(
                cross(direction, (p.q_f.0 - p.p.0, p.q_f.1 - p.p.1)).abs() < tolerance,
                "{p:?}: Control point q_f is not along line in direction {direction:?}"
            );
        }
    }

    #[test]
    fn test_xy_chart_draws_field_line_plot() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = empty_chart_from_ranges(-1.0..1.0, -1.0..1.0);
        let plot = chart
            .prepare_layout(crate::xy_chart::AspectRatio::Unconstrained, &drawable)
            .fit_into(400., 200.);
        let plot = plot.draw_axes(&mut drawable);

        let field_line_plot = FieldLinePlot {
            f: |_t, [x, y]: [f64; 2]| {
                let rr = x * x + y * y;
                [x / rr, y / rr]
            },
            source_singularities: vec![(0., 0.)],
            sink_singularities: vec![],
            r_singularity: 0.125,
            n_lines_at_singularity: 4,
            line_style: Default::default(),
        }
        .clipped_at_region();
        plot.try_plot(&mut drawable, &field_line_plot).unwrap();
        let expected_rectangle = gfx::Rectangle {
            x: 10.0,
            y: 10.0,
            width: 380.,
            height: 180.,
        };
        assert_eq!(drawable.elements.len(), 4);
        testing::assert_match!(
            &drawable.elements[0],
            testing::Approx(&gfx::testing::Element::from(expected_rectangle.clone()))
        );
        testing::assert_match!(
            &drawable.elements[1],
            testing::Approx(&gfx::testing::Element::from(vec![
                (10.0, 100.0),
                (390.0, 100.0)
            ]))
        );
        testing::assert_match!(
            &drawable.elements[2],
            testing::Approx(&gfx::testing::Element::from(vec![
                (200.0, 190.0),
                (200.0, 10.0)
            ]))
        );
        let gfx::testing::Element::ClippedRegion(gfx::testing::ClippedRegion {
            polygon,
            primitives,
        }) = &drawable.elements[3]
        else {
            panic!("drawable.elements[3] is not a clip region")
        };
        testing::assert_match!(
            polygon,
            testing::Approx(&vec![(10., 10.), (390., 10.), (390., 190.), (10., 190.)])
        );
        assert_eq!(primitives.len(), 4);
        let expected_center = (200., 100.);
        let paths = primitives
            .iter()
            .enumerate()
            .map(|(n, primitive)| {
                let gfx::testing::UnstyledPrimitiveElement::BezierCurve(path) = primitive else {
                    panic!("element[{n}] is not a beziergon");
                };
                path
            })
            .collect::<Vec<_>>();
        let tolerance = 1.0E-5;
        for path in paths.iter() {
            let start = path[0].p;
            let expected_direction = (start.0 - expected_center.0, start.1 - expected_center.1);
            assert_is_straight_line(path, start, expected_direction, tolerance);
            let x_range = expected_rectangle.x..expected_rectangle.x + expected_rectangle.width;
            let y_range = expected_rectangle.y..expected_rectangle.y + expected_rectangle.height;
            let (x, y) = path.last().unwrap().p;
            assert!(
                !x_range.contains(&x) || !y_range.contains(&y),
                "Endpoint {:?} must be outside of draw range.",
                (x, y)
            );
        }
        let start_points: Vec<_> = paths
            .iter()
            .map(|path| {
                (
                    path[0].p.0 - expected_center.0,
                    path[0].p.1 - expected_center.1,
                )
            })
            .collect();
        let start_point_sum = start_points
            .iter()
            .copied()
            .reduce(|(p_x, p_y), (q_x, q_y)| (p_x + q_x, p_y + q_y))
            .unwrap();
        assert!(
            start_point_sum.0.abs() < tolerance && start_point_sum.1.abs() < tolerance,
            "Sum of start points must be 0"
        );
        let (kx, ky) = (
            expected_rectangle.width / chart.x_axis.interval.length(),
            expected_rectangle.height / chart.y_axis.interval.length(),
        );
        let r = field_line_plot.plot.r_singularity;
        assert!(
            start_points
                .into_iter()
                .map(|(x, y)| (x / kx / r).powi(2) + (y / ky / r).powi(2))
                .all(|r| (r - 1.).abs() < tolerance),
            "Start points must lie on an ellipis"
        );
    }
}
