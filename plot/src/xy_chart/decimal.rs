//! Decimal floating point type.

/// Decimal floating point.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Decimal<T: SignedInteger> {
    pub mantissa: T,
    pub exp: T,
}

impl<T: SignedInteger> From<T> for Decimal<T> {
    fn from(value: T) -> Self {
        Self::new(value, 0.into())
    }
}

impl<T: SignedInteger> From<&Decimal<T>> for f64
where
    f64: From<T>,
{
    fn from(value: &Decimal<T>) -> Self {
        f64::from(value.mantissa) * f64::powf(10.0, f64::from(value.exp))
    }
}

impl<T: SignedInteger> Decimal<T> {
    /// Create a new [Decimal].
    pub fn new(mut mantissa: T, mut exp: T) -> Self {
        if mantissa == 0.into() {
            return Self {
                mantissa: 0.into(),
                exp: 0.into(),
            };
        }
        while (mantissa / 10.into()) * 10.into() == mantissa {
            mantissa /= 10.into();
            exp += 1.into();
        }
        Self { mantissa, exp }
    }

    /// Returns 0 if `self == 0`, `floor(log10(self))` otherwise.
    pub fn mag10(&self) -> T {
        if self.mantissa == 0.into() {
            return T::from(0);
        }
        T::from(mag10(self.mantissa) as u16) + self.exp
    }
}
impl<T: SignedInteger> Decimal<T>
where
    for<'a> f64: From<&'a Decimal<T>>,
{
    /// Check if `value` is near `self` with respect to a `reference` scale.
    pub fn is_near(&self, value: f64, reference: f64) -> bool {
        if !value.is_finite() {
            return false;
        }
        let d = ((f64::from(self) - value) / reference).abs();
        d <= f64::EPSILON
    }
}

impl<T: SignedInteger> std::ops::Add<&Decimal<T>> for &Decimal<T> {
    type Output = Decimal<T>;
    fn add(self, rhs: &Decimal<T>) -> Self::Output {
        let ten: T = 10.into();
        let (lhs_mantissa, rhs_mantissa, exp) = if self.exp >= rhs.exp {
            (
                self.mantissa * ten.pow((self.exp - rhs.exp).unsigned_abs()),
                rhs.mantissa,
                rhs.exp,
            )
        } else {
            (
                self.mantissa,
                rhs.mantissa * ten.pow((rhs.exp - self.exp).unsigned_abs()),
                self.exp,
            )
        };
        Decimal::new(lhs_mantissa + rhs_mantissa, exp)
    }
}

/// Abstraction for [u32::pow].
pub trait Pow {
    fn pow(self, exp: u32) -> Self;
}

/// Abstraction for [u32::unsigned_abs].
pub trait UnsignedAbs {
    fn unsigned_abs(self) -> u32;
}

pub trait SignedInteger:
    std::ops::Neg<Output = Self>
    + std::ops::Add<Self, Output = Self>
    + std::ops::AddAssign<Self>
    + std::ops::Sub<Self, Output = Self>
    + std::ops::SubAssign<Self>
    + std::ops::Neg<Output = Self>
    + std::ops::Mul<Self, Output = Self>
    + std::ops::MulAssign<Self>
    + std::ops::Div<Self, Output = Self>
    + std::ops::DivAssign<Self>
    + std::ops::Rem<Self, Output = Self>
    + std::cmp::PartialOrd
    + From<u16>
    + TryInto<usize>
    + Pow
    + UnsignedAbs
    + Sized
    + Copy
{
}

impl Pow for i32 {
    fn pow(self, exp: u32) -> Self {
        self.pow(exp)
    }
}
impl Pow for i64 {
    fn pow(self, exp: u32) -> Self {
        self.pow(exp)
    }
}

impl UnsignedAbs for i32 {
    fn unsigned_abs(self) -> u32 {
        self.unsigned_abs()
    }
}
impl UnsignedAbs for i64 {
    fn unsigned_abs(self) -> u32 {
        self.unsigned_abs() as u32
    }
}

impl SignedInteger for i32 {}
impl SignedInteger for i64 {}

/// Todo: Replace with i64::ilog10 as soon as it is stable.
pub(crate) fn mag10<T: SignedInteger>(mut n: T) -> u32 {
    if n == 0.into() {
        return 0;
    }
    // u32::ilog10(T::unsigned_abs(n))
    let mut exp = 0;
    if n < 0.into() {
        n = -n;
    }
    while n > 10.into() {
        n /= 10.into();
        exp += 1;
    }
    exp
}

impl<T: SignedInteger + std::fmt::Display> Decimal<T> {
    pub fn format_engineering(&self) -> String {
        if self.mantissa == 0.into() {
            return String::from("0E00");
        }
        let mag = self.mag10();
        let e = mag - mag % 3.into();
        format!(
            "{}E{e}",
            Decimal {
                mantissa: self.mantissa,
                exp: self.exp - e
            }
        )
    }
}

impl<T: SignedInteger + std::fmt::Display> std::fmt::Display for Decimal<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        const DEC_SEP: char = '.';
        if self.mantissa == 0.into() {
            return write!(f, "0");
        }
        // Reduce fraction:
        let Decimal { mantissa, exp } = Decimal::new(self.mantissa, self.exp);
        let mag = self.mag10();
        let mantissa = if mantissa < 0.into() {
            write!(f, "-")?;
            -mantissa
        } else {
            mantissa
        };
        if -T::from(3) <= mag && mag < 6.into() {
            if mag < 0.into() {
                let width = (-mag - 1.into()).unsigned_abs() as usize;
                write!(f, "0{DEC_SEP}{:0<width$}{mantissa}", "")
            } else if exp >= 0.into() {
                let width = exp.unsigned_abs() as usize;
                write!(f, "{mantissa}{:0<width$}", "")
            } else {
                let d = T::pow(10.into(), exp.unsigned_abs());
                let (before, after) = (mantissa / d, mantissa % d);
                write!(f, "{before}{DEC_SEP}{after}")
            }
        } else {
            let mantissa_exp = mag - exp;
            if mantissa_exp > 0.into() {
                let d = T::pow(10.into(), mantissa_exp.unsigned_abs());
                let (before, after) = (mantissa / d, mantissa % d);
                write!(f, "{before}{DEC_SEP}{after}")?;
            } else {
                write!(f, "{mantissa}")?;
            }
            write!(f, "E{mag}")
        }
    }
}
#[cfg(test)]
mod test {
    use super::*;
    use numeric::assert_almost_eq;

    #[test]
    fn decimal_new_works_for_1() {
        let dec = Decimal::new(1, 0);
        assert_eq!(
            dec,
            Decimal {
                mantissa: 1,
                exp: 0
            }
        );
    }
    #[test]
    fn decimal_new_works_for_0() {
        let dec = Decimal::new(0, 1);
        assert_eq!(
            dec,
            Decimal {
                mantissa: 0,
                exp: 0
            }
        );
    }

    #[test]
    fn decimal_add_works_for_equal_exp() {
        let a = Decimal::new(-1, 1);
        let b = Decimal::new(2, 1);
        assert_eq!(
            &a + &b,
            Decimal {
                mantissa: 1,
                exp: 1
            }
        );
    }
    #[test]
    fn decimal_add_works_for_exp_a_gt_exp_b() {
        let a = Decimal::new(-1, 2); // -100
        let b = Decimal::new(2, 1); // 20
        assert_eq!(
            &a + &b,
            Decimal {
                mantissa: -8,
                exp: 1
            }
        );
    }
    #[test]
    fn decimal_add_works_for_exp_b_gt_exp_a() {
        let a = Decimal::new(-1, -2); // -0.01
        let b = Decimal::new(2, -1); // 0.2
        assert_eq!(
            &a + &b,
            Decimal {
                mantissa: 19,
                exp: -2
            }
        );
    }

    #[test]
    fn decimal_is_near_works_for_0_dist() {
        let a = Decimal::new(1, 0);
        let b = 1.0;
        assert!(a.is_near(b, 1.0));
    }
    #[test]
    fn decimal_is_near_works_for_eps_dist() {
        let a = Decimal::new(0, 0);
        assert!(a.is_near(f64::EPSILON, 1.0));
        assert!(!a.is_near(2. * f64::EPSILON, 1.0));
    }
    #[test]
    fn decimal_is_near_works_for_fractions() {
        assert!(Decimal::new(1, -1).is_near(0.1, 1.0));
        assert!(Decimal::new(-1, -10).is_near(-1.0e-10, 1.0));
        assert!(Decimal::new(2, -5).is_near(2.0e-5, 1.0));
    }

    #[test]
    fn decimal_to_f64_works_for_1() {
        let dec = Decimal::new(1, 0);
        assert_eq!(f64::from(&dec), 1.0);
    }
    #[test]
    fn decimal_to_f64_works_for_1_1() {
        let dec = Decimal::new(11, -1);
        assert_almost_eq!(f64::from(&dec), 1.1);
    }

    #[test]
    fn decimal_from_i32_works_for_1() {
        let dec: Decimal<i32> = 1.into();
        let expected_dec = Decimal::<i32> {
            mantissa: 1,
            exp: 0,
        };
        assert_eq!(dec, expected_dec);
    }
    #[test]
    fn decimal_from_i32_works_for_42() {
        let dec: Decimal<i32> = 42.into();
        let expected_dec = Decimal::<i32> {
            mantissa: 42,
            exp: 0,
        };
        assert_eq!(dec, expected_dec);
    }

    #[test]
    fn test_decimal_mag10_works_for_positive_numbers() {
        let dec = Decimal::new(1230, -3);
        assert_eq!(dec.mag10(), 0);
        let dec = Decimal::new(123, -2);
        assert_eq!(dec.mag10(), 0);
        let dec = Decimal::new(1, -2);
        assert_eq!(dec.mag10(), -2);
        let dec = Decimal::new(1, 3);
        assert_eq!(dec.mag10(), 3);
        let dec = Decimal::new(1, 0);
        assert_eq!(dec.mag10(), 0);
        let dec = Decimal::new(3498, 2);
        assert_eq!(dec.mag10(), 5);
    }
    #[test]
    fn test_decimal_mag10_works_for_negative_numbers() {
        let dec = Decimal::new(-1230, -3);
        assert_eq!(dec.mag10(), 0);
        let dec = Decimal::new(-123, -2);
        assert_eq!(dec.mag10(), 0);
        let dec = Decimal::new(-1, -2);
        assert_eq!(dec.mag10(), -2);
        let dec = Decimal::new(-1, 3);
        assert_eq!(dec.mag10(), 3);
        let dec = Decimal::new(-1, 0);
        assert_eq!(dec.mag10(), 0);
        let dec = Decimal::new(-3498, 2);
        assert_eq!(dec.mag10(), 5);
    }
    #[test]
    fn test_decimal_mag10_works_for_zero() {
        let dec = Decimal::new(0, 0);
        assert_eq!(dec.mag10(), 0);
        let dec = Decimal::new(0, 1);
        assert_eq!(dec.mag10(), 0);
        let dec = Decimal::new(0, -1);
        assert_eq!(dec.mag10(), 0);
    }

    #[test]
    fn test_decimal_format_works_for_zero() {
        let dec = Decimal::new(0, -3);
        assert_eq!(&format!("{}", dec), "0");
        let dec = Decimal::new(0, 0);
        assert_eq!(&format!("{}", dec), "0");
    }

    #[test]
    fn test_decimal_format_works_for_small_exponents_and_positive_numbers() {
        let dec = Decimal::new(1230, -3);
        assert_eq!(&format!("{}", dec), "1.23");
        let dec = Decimal {
            mantissa: 1230,
            exp: -3,
        };
        assert_eq!(&format!("{}", dec), "1.23");
        let dec = Decimal::new(123, -3);
        assert_eq!(&format!("{}", dec), "0.123");
        let dec = Decimal::new(123, -5);
        assert_eq!(&format!("{}", dec), "0.00123");
        let dec = Decimal::new(123, 3);
        assert_eq!(&format!("{}", dec), "123000");
    }
    #[test]
    fn test_decimal_format_works_for_small_exponents_and_negative_numbers() {
        let dec = Decimal::new(-1230, -3);
        assert_eq!(&format!("{}", dec), "-1.23");
        let dec = Decimal {
            mantissa: -1230,
            exp: -3,
        };
        assert_eq!(&format!("{}", dec), "-1.23");
        let dec = Decimal::new(-123, -3);
        assert_eq!(&format!("{}", dec), "-0.123");
        let dec = Decimal::new(-123, -5);
        assert_eq!(&format!("{}", dec), "-0.00123");
        let dec = Decimal::new(-123, 3);
        assert_eq!(&format!("{}", dec), "-123000");
    }

    #[test]
    fn test_decimal_format_works_for_large_exponents_and_positive_numbers() {
        let dec = Decimal::new(1230, -8);
        assert_eq!(&format!("{}", dec), "1.23E-5");
        let dec = Decimal {
            mantissa: 1230,
            exp: -8,
        };
        assert_eq!(&format!("{}", dec), "1.23E-5");
        let dec = Decimal::new(123, 8);
        assert_eq!(&format!("{}", dec), "1.23E10");
        let dec = Decimal::new(1, -8);
        assert_eq!(&format!("{}", dec), "1E-8");
    }
    #[test]
    fn test_decimal_format_works_for_large_exponents_and_negative_numbers() {
        let dec = Decimal::new(-1230, -8);
        assert_eq!(&format!("{}", dec), "-1.23E-5");
        let dec = Decimal {
            mantissa: -1230,
            exp: -8,
        };
        assert_eq!(&format!("{}", dec), "-1.23E-5");
        let dec = Decimal::new(-123, 8);
        assert_eq!(&format!("{}", dec), "-1.23E10");
        let dec = Decimal::new(-1, -8);
        assert_eq!(&format!("{}", dec), "-1E-8");
    }

    #[test]
    fn test_decimal_format_engineering_works_for_zero() {
        let dec = Decimal::new(0, -8);
        assert_eq!(&dec.format_engineering(), "0E00");
        let dec = Decimal::new(0, 0);
        assert_eq!(&dec.format_engineering(), "0E00");
    }
    #[test]
    fn test_decimal_format_engineering_works_for_positive_numbers() {
        let dec = Decimal::new(1230, -4);
        assert_eq!(&dec.format_engineering(), "0.123E0");
        let dec = Decimal {
            mantissa: 1230,
            exp: -4,
        };
        assert_eq!(&dec.format_engineering(), "0.123E0");
        let dec = Decimal::new(123, 8);
        assert_eq!(&dec.format_engineering(), "12.3E9");
        let dec = Decimal::new(1, -8);
        assert_eq!(&dec.format_engineering(), "0.01E-6");
    }
    #[test]
    fn test_decimal_format_engineering_works_for_negative_numbers() {
        let dec = Decimal::new(-1230, -4);
        assert_eq!(&dec.format_engineering(), "-0.123E0");
        let dec = Decimal {
            mantissa: -1230,
            exp: -4,
        };
        assert_eq!(&dec.format_engineering(), "-0.123E0");
        let dec = Decimal::new(-123, 8);
        assert_eq!(&dec.format_engineering(), "-12.3E9");
        let dec = Decimal::new(-1, -8);
        assert_eq!(&dec.format_engineering(), "-0.01E-6");
    }
}
