//! Periodicity helper types and traits

use super::{Interval, XyInterval};

/// Defines how to fold a real number or vector to the fundamental domain represented by `interval`.
pub trait FoldToFundamentalDomain {
    /// Space (scalar or vector)
    type Space;
    /// Interval type (1D, 2D, ...)
    type Interval;
    /// Fold a value `x` to the fundamental domain
    fn fold_to_fundamental_domain(interval: &Self::Interval, x: Self::Space) -> Self::Space;
}

/// Defines if points on the nth domain (fundamental domain, translated by n times its length) are folded to the fundamental domain (are visible).
pub trait DomainVisibility {
    /// Index or multi index type
    type Index;
    /// Check if the image of folding is visible
    fn nth_domain_is_visible(n: Self::Index) -> bool;
}

/// Trait holding a boolean constant whether `Self` is considered periodic.
pub trait IsPeriodic {
    /// True if `Self` is periodic
    const IS_PERIODIC: bool;
}

/// Trait about periodic and non-periodic dimensions.
pub trait Periodicity:
    DomainVisibility<Index = i32>
    + FoldToFundamentalDomain<Space = f64, Interval = Interval>
    + IsPeriodic
    + 'static
{
}

/// Helper type implementing traits for a non periodic dimension.
#[derive(Debug, Clone, Copy)]
pub struct NotPeriodic;

impl FoldToFundamentalDomain for NotPeriodic {
    type Space = f64;
    type Interval = Interval;
    fn fold_to_fundamental_domain(_interval: &Self::Interval, x: f64) -> f64 {
        x
    }
}
impl DomainVisibility for NotPeriodic {
    type Index = i32;
    fn nth_domain_is_visible(n: Self::Index) -> bool {
        n == 0
    }
}

impl IsPeriodic for NotPeriodic {
    const IS_PERIODIC: bool = false;
}

impl Periodicity for NotPeriodic {}

/// Helper type implementing traits for a periodic dimension.
#[derive(Debug, Clone, Copy)]
pub struct Periodic;
impl FoldToFundamentalDomain for Periodic {
    type Space = f64;
    type Interval = Interval;
    fn fold_to_fundamental_domain(interval: &Self::Interval, x: f64) -> f64 {
        let min = interval.min;
        let max = interval.max;
        if min <= x && x <= max {
            return x;
        }
        let d = max - min;
        let dx = x - min;
        min + dx.rem_euclid(d)
    }
}
impl DomainVisibility for Periodic {
    type Index = i32;
    fn nth_domain_is_visible(_n: Self::Index) -> bool {
        true
    }
}

impl IsPeriodic for Periodic {
    const IS_PERIODIC: bool = true;
}

impl Periodicity for Periodic {}

impl<
        S,
        A: FoldToFundamentalDomain<Interval = Interval, Space = S>,
        B: FoldToFundamentalDomain<Interval = Interval, Space = S>,
    > FoldToFundamentalDomain for (A, B)
{
    type Space = [S; 2];
    type Interval = XyInterval;
    fn fold_to_fundamental_domain(interval: &Self::Interval, [x, y]: Self::Space) -> Self::Space {
        [
            A::fold_to_fundamental_domain(&interval.x, x),
            B::fold_to_fundamental_domain(&interval.y, y),
        ]
    }
}

impl<A: DomainVisibility, B: DomainVisibility> DomainVisibility for (A, B) {
    type Index = (A::Index, B::Index);
    fn nth_domain_is_visible((n, m): Self::Index) -> bool {
        A::nth_domain_is_visible(n) && B::nth_domain_is_visible(m)
    }
}

impl<XPeriodicity: IsPeriodic, YPeriodicity: IsPeriodic> IsPeriodic
    for (XPeriodicity, YPeriodicity)
{
    const IS_PERIODIC: bool = XPeriodicity::IS_PERIODIC || YPeriodicity::IS_PERIODIC;
}
