//! Markers for scatter plots / legends.
use crate::gfx;

/// Marker made up of rectangles, circles, line segments, polygons
pub type Marker = GenericMarker<MarkerShape>;

/// Marker for scatter plots.
#[derive(Debug, Clone)]
pub struct GenericMarker<T> {
    /// Shape of the marker.
    pub shape: T,
    /// Pixel size of the marker.
    pub size: f64,
    /// Fill color of the marker.
    pub color: Option<gfx::Color>,
    /// Contour line style of the marker.
    pub line_style: Option<gfx::LineStyle>,
}

/// Unscaled marker shape.
///
/// Can be a marker primitive (rectangle, circle, line segment, polygon)
/// or any composition of primitives.
#[derive(Debug, Clone, Copy)]
pub struct MarkerShape {
    primitives: &'static [MarkerPrimitive],
}

/// Convenience extension to drawable, so it can draw markers.
pub trait DrawMarker<T> {
    /// Draw a marker at a given point `(x, y)`.
    fn draw_marker(&mut self, x: f64, y: f64, marker: &GenericMarker<T>);
}

impl<D: gfx::Drawable, T: DrawMarkerTo> DrawMarker<T> for D {
    fn draw_marker(&mut self, x: f64, y: f64, marker: &GenericMarker<T>) {
        marker.shape.draw_marker_to(
            self,
            x,
            y,
            marker.size,
            marker.line_style.as_ref(),
            marker.color.as_ref(),
        );
    }
}

/// Trait for markers defining how they draw themselves to a drawable.
pub trait DrawMarkerTo {
    /// Draw `self` to `drawable` at coordinates `x`, `y` with `size`, `line_style` and `color`.
    fn draw_marker_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        x: f64,
        y: f64,
        size: f64,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    );
}

impl DrawMarkerTo for MarkerShape {
    fn draw_marker_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        x: f64,
        y: f64,
        size: f64,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        for primitive in self.primitives {
            primitive.draw_marker_to(drawable, x, y, size, line_style, color);
        }
    }
}

/// Primitive shapes, a `MarkerShape` can be made of.
#[derive(Debug, Clone)]
pub enum MarkerPrimitive {
    /// Rectangle
    Rectangle(Rectangle),
    /// Line segment
    LineSegment(LineSegment),
    /// Circle
    Circle(Circle),
    /// Polygon
    Polygon(Polygon),
}
impl DrawMarkerTo for MarkerPrimitive {
    fn draw_marker_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        x: f64,
        y: f64,
        size: f64,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        match self {
            Self::Rectangle(obj) => obj.draw_marker_to(drawable, x, y, size, line_style, color),
            Self::LineSegment(obj) => obj.draw_marker_to(drawable, x, y, size, line_style, color),
            Self::Circle(obj) => obj.draw_marker_to(drawable, x, y, size, line_style, color),
            Self::Polygon(obj) => obj.draw_marker_to(drawable, x, y, size, line_style, color),
        }
    }
}

/// Rectangle marker shape primitive.
#[derive(Debug, Clone)]
pub struct Rectangle {
    /// x offset relative to a markers center.
    pub x: f64,
    /// y offset relative to a markers center.
    pub y: f64,
    /// Width for marker size=1.
    pub w: f64,
    /// Height for marker size=1.
    pub h: f64,
}
impl DrawMarkerTo for Rectangle {
    fn draw_marker_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        x: f64,
        y: f64,
        size: f64,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        let r = size / 2.;
        drawable.draw(
            &gfx::Rectangle {
                x: x - r * self.x,
                y: y - r * self.y,
                width: r * self.w,
                height: r * self.h,
            },
            line_style,
            color,
        );
    }
}

/// Circle marker shape primitive.
#[derive(Debug, Clone)]
pub struct Circle {
    /// x offset relative to a markers center.
    pub x: f64,
    /// y offset relative to a markers center.
    pub y: f64,
    /// Radius for marker size=1.
    pub r: f64,
}
impl DrawMarkerTo for Circle {
    fn draw_marker_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        x: f64,
        y: f64,
        size: f64,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        drawable.draw(
            &gfx::Circle {
                x: x + size * self.x,
                y: y + size * self.y,
                r: size * self.r,
            },
            line_style,
            color,
        );
    }
}

/// Line segment marker shape primitive.
/// Consists of start and end point.
/// Can be used to form crosses.
#[derive(Debug, Clone)]
pub struct LineSegment(pub (f64, f64), pub (f64, f64));

impl DrawMarkerTo for LineSegment {
    fn draw_marker_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        x: f64,
        y: f64,
        size: f64,
        line_style: Option<&gfx::LineStyle>,
        _color: Option<&gfx::Color>,
    ) {
        let LineSegment((x0, y0), (x1, y1)) = self;
        if let Some(line_style) = line_style {
            drawable.stroke_path(
                [
                    (x + size * x0, y + size * y0),
                    (x + size * x1, y + size * y1),
                ],
                line_style,
            );
        }
    }
}

/// Polygon marker shape primitive.
#[derive(Debug, Clone)]
pub struct Polygon(pub &'static [(f64, f64)]);

impl DrawMarkerTo for Polygon {
    fn draw_marker_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        x: f64,
        y: f64,
        size: f64,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        let scaled_points = self
            .0
            .iter()
            .map(move |(dx, dy)| (x + size * dx, y + size * dy));
        drawable.draw_polygon(scaled_points, line_style, color);
    }
}

use numeric::consts::SQRT_3;
impl MarkerShape {
    /// □ marker shape
    pub const SQUARE: Self = MarkerShape {
        primitives: &[MarkerPrimitive::Rectangle(Rectangle {
            x: -0.5,
            y: -0.5,
            w: 1.0,
            h: 1.0,
        })],
    };
    /// ◇ marker shape
    pub const DIAMOND: Self = MarkerShape {
        primitives: &[MarkerPrimitive::Polygon(Polygon(&[
            (-1., 0.),
            (0., -1.),
            (1., 0.),
            (0., 1.),
        ]))],
    };
    /// △ marker shape
    pub const TRIANGLE: Self = MarkerShape {
        primitives: &[MarkerPrimitive::Polygon(Polygon(&[
            (-0.25 * SQRT_3, 0.25),
            (0.25 * SQRT_3, 0.25),
            (0., -0.5),
        ]))],
    };
    /// ○ marker shape
    pub const CIRCLE: Self = MarkerShape {
        primitives: &[MarkerPrimitive::Circle(Circle {
            x: 0.,
            y: 0.,
            r: 1.,
        })],
    };
    /// + marker shape
    pub const PLUS: Self = MarkerShape {
        primitives: &[
            MarkerPrimitive::LineSegment(LineSegment((-1., 0.), (1., 0.))),
            MarkerPrimitive::LineSegment(LineSegment((0., -1.), (0., 1.))),
        ],
    };
    /// ⨉ marker shape
    pub const TIMES: Self = MarkerShape {
        primitives: &[
            MarkerPrimitive::LineSegment(LineSegment((-1., -1.), (1., 1.))),
            MarkerPrimitive::LineSegment(LineSegment((1., -1.), (-1., 1.))),
        ],
    };
    /// ⨂ marker shape
    pub const OTIMES: Self = MarkerShape {
        primitives: &[
            MarkerPrimitive::LineSegment(LineSegment((-1., -1.), (1., 1.))),
            MarkerPrimitive::LineSegment(LineSegment((1., -1.), (-1., 1.))),
            MarkerPrimitive::Circle(Circle {
                x: 0.,
                y: 0.,
                r: 1.,
            }),
        ],
    };
}
