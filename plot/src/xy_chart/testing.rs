//! Testing utilities for [crate::xy_chart]

use crate::xy_chart::{Axis, Interval, Ticks, XyChart};

/// Create an axis without ticks over the range 0.0 .. 1.0.
pub fn empty_axis() -> Axis<'static> {
    Axis::new_from_interval_and_ticks(Interval { min: 0., max: 1. }, Ticks::no_ticks())
}

/// Create an axis without ticks over the range `interval`.
pub fn empty_axis_from_range(interval: std::ops::Range<f64>) -> Axis<'static> {
    Axis::new_from_interval_and_ticks(
        Interval {
            min: interval.start,
            max: interval.end,
        },
        Ticks::no_ticks(),
    )
}

/// Create a [XyChart] without ticks over the range (0.0 .. 1.0) x (0.0 .. 1.0).
pub fn empty_chart() -> XyChart<'static> {
    XyChart {
        grid_line_style: None,
        x_axis: empty_axis(),
        y_axis: empty_axis(),
        ..Default::default()
    }
}

/// Create a [XyChart] without ticks over the cartesian product of `x` and `y`.
pub fn empty_chart_from_ranges(
    x: std::ops::Range<f64>,
    y: std::ops::Range<f64>,
) -> XyChart<'static> {
    XyChart {
        grid_line_style: None,
        x_axis: empty_axis_from_range(x),
        y_axis: empty_axis_from_range(y),
        ..Default::default()
    }
}
