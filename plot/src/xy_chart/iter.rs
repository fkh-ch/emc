//! Variant of [std::iter::IntoIterator] which allows multiple implementations for different item types.

/// Create an iterator over items in the implementor.
/// In contrast to [std::iter::IntoIterator], this trait is designed such that
/// `&[T]`, `&Vec<T>` and `Iterator<Item=T>` all have the same item type.
pub trait AsIterator<T>: Copy {
    /// Iterator type returned by [Self::as_iter]
    type AsIter: Iterator<Item = T>;

    /// Convert to an iterator
    fn as_iter(self) -> Self::AsIter;
}

impl<'a, T: Copy + 'a> AsIterator<T> for &'a [T] {
    type AsIter = std::iter::Copied<std::slice::Iter<'a, T>>;

    fn as_iter(self) -> Self::AsIter {
        self.iter().copied()
    }
}

impl<'a, T: Copy + 'a> AsIterator<T> for &'a &[T] {
    type AsIter = std::iter::Copied<std::slice::Iter<'a, T>>;

    fn as_iter(self) -> Self::AsIter {
        self.iter().copied()
    }
}

impl<'a, T: Copy + 'a, const N: usize> AsIterator<T> for &'a [T; N] {
    type AsIter = std::iter::Copied<std::slice::Iter<'a, T>>;

    fn as_iter(self) -> Self::AsIter {
        self.iter().copied()
    }
}

impl<'a, T: Copy + 'a, const N: usize> AsIterator<T> for &'a &[T; N] {
    type AsIter = std::iter::Copied<std::slice::Iter<'a, T>>;

    fn as_iter(self) -> Self::AsIter {
        self.iter().copied()
    }
}

impl<'a, T: Copy + 'a> AsIterator<T> for &'a Vec<T> {
    type AsIter = std::iter::Copied<std::slice::Iter<'a, T>>;

    fn as_iter(self) -> Self::AsIter {
        self.iter().copied()
    }
}

impl<'a, T: Copy + 'a> AsIterator<T> for &'a &Vec<T> {
    type AsIter = std::iter::Copied<std::slice::Iter<'a, T>>;

    fn as_iter(self) -> Self::AsIter {
        self.iter().copied()
    }
}

impl<T, I: Iterator<Item = T>, F: Fn() -> I + Copy> AsIterator<T> for F {
    type AsIter = I;

    fn as_iter(self) -> Self::AsIter {
        (self)()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn tests_as_iter_works_for_slice() {
        let slc = &[1, 2, 3];
        let mut iter = slc.as_iter();
        assert_eq!(iter.next().unwrap(), 1);
        assert_eq!(iter.next().unwrap(), 2);
        assert_eq!(iter.next().unwrap(), 3);
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn tests_as_iter_works_for_reference_to_slice() {
        let slc = &[[0., 1.], [0., 2.], [0., 3.]];
        let mut iter = (&slc).as_iter();
        assert_eq!(iter.next().unwrap(), [0., 1.]);
        assert_eq!(iter.next().unwrap(), [0., 2.]);
        assert_eq!(iter.next().unwrap(), [0., 3.]);
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn tests_as_iter_works_for_vec() {
        let v = vec![1, 2, 3];
        let mut iter = v.as_iter();
        assert_eq!(iter.next().unwrap(), 1);
        assert_eq!(iter.next().unwrap(), 2);
        assert_eq!(iter.next().unwrap(), 3);
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn tests_as_iter_works_for_closure() {
        let v = || 1..4;
        let mut iter = v.as_iter();
        assert_eq!(iter.next().unwrap(), 1);
        assert_eq!(iter.next().unwrap(), 2);
        assert_eq!(iter.next().unwrap(), 3);
        assert_eq!(iter.next(), None);
    }
}
