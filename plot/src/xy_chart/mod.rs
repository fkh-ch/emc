//! Chart plot library
//!
//! To support both drawing directly to a file as will as using this in a GUI,
//! drawing a chart is split into several steps:
//! 1. Create a [XyChart]
//! 2. Prepare an intrinsic layout using [XyChart::prepare_layout]
//!    - Optionally use [IntrinsicChartLayout::align_data_region_left_at] and [IntrinsicChartLayout::align_data_region_top_at]
//!
//! Until here, everything can be reused for consecutive redraws of a GUI window with changing size.
//!
//! 3. Specify the output chart extents using [IntrinsicChartLayout::fit_into] or [IntrinsicChartLayout::with_axes_extents]
//!    - Optionally use [ExtrinsicChartLayout::with_offset] here
//! 4. Draw chart using [ExtrinsicChartLayout::draw_axes] and plot data using [ExtrinsicChartLayout::plot] / [ExtrinsicChartLayout::try_plot]
//! 5. Optionally draw a legend using [legend::Legend] or [ExtrinsicChartLayout::draw].

mod clip;
pub mod clipped_plot;
pub mod contour_line_plot;
mod decimal;
pub mod field_line_plot;
pub mod function_plot;
pub mod iter;
pub mod legend;
pub mod line_plot;
pub mod marker;
pub mod periodicity;
pub mod plot_stack;
pub mod scatter_plot;
mod transform;

#[cfg(test)]
pub mod testing;

use crate::gfx;

use decimal::Decimal;

/// Properties of a chart (output size agnostic)
pub struct XyChart<'a> {
    /// Chart title
    pub title: Option<String>,
    /// Font of the title
    pub title_font: gfx::Font<&'a str>,
    /// x axis (including periodicity)
    pub x_axis: Axis<'a>,
    /// y axis (including periodicity)
    pub y_axis: Axis<'a>,
    /// Line style of the grid lines (set to None to disable grid lines)
    pub grid_line_style: Option<gfx::LineStyle>,
    /// Line style of the sub-grid lines (set to None to disable sub-grid lines)
    pub sub_grid_line_style: Option<gfx::LineStyle>,
    /// Line width for axes and border rectangle
    pub line_width: f64,
}

impl Default for XyChart<'_> {
    fn default() -> Self {
        Self {
            grid_line_style: Some(gfx::LineStyle::default()),
            sub_grid_line_style: None,
            title: Default::default(),
            title_font: Default::default(),
            x_axis: Default::default(),
            y_axis: Default::default(),
            line_width: 1.0,
        }
    }
}

impl<'a> XyChart<'a> {
    fn xy_interval(&self) -> XyInterval {
        XyInterval {
            x: self.x_axis.interval.clone(),
            y: self.y_axis.interval.clone(),
        }
    }

    /// Embed into a layout, knowing about the aspect ratio of the output and
    /// the size a text typeset with a font will obtain
    pub fn prepare_layout(
        &'a self,
        aspect_ratio: AspectRatio,
        font_metrics: &impl gfx::FontMetrics,
    ) -> IntrinsicChartLayout<'a> {
        let (_, mut d_title) = if let Some(title) = &self.title {
            font_metrics.get_text_extents(title, &self.title_font)
        } else {
            (0., 0.)
        };
        const DEFAULT_SPC_TITLE_U: f64 = 5.;
        const DEFAULT_SPC_TITLE_L: f64 = 5.;
        if d_title > 0. {
            d_title += DEFAULT_SPC_TITLE_U + DEFAULT_SPC_TITLE_L;
        }
        const DEFAULT_BORDER: f64 = 10.;

        IntrinsicChartLayout {
            chart: self,
            d_title,
            spc_title_u: DEFAULT_SPC_TITLE_U,
            data_region_left_align: 0.,
            data_region_top_align: 0.,
            border: DEFAULT_BORDER,
            x_axis_layout: self.x_axis.layout::<Horizontal>(font_metrics),
            y_axis_layout: self.y_axis.layout::<Vertical>(font_metrics),
            aspect_ratio,
        }
    }
}

fn multiply_10_exp(mut x: f64, exp: i32) -> f64 {
    if exp > 0 {
        for _ in 0..exp {
            x *= 10.;
        }
    } else {
        for _ in 0..-exp {
            x /= 10.;
        }
    }
    x
}

/// Ticks on an axis. At the moment only equi spaced ticks are possible.
#[derive(PartialEq, Clone, Debug)]
pub struct Ticks {
    /// Position value (data space) of first tick on the axis
    pub min: Decimal<i32>,
    /// Distance between two ticks
    pub step: Decimal<i32>,
    /// Number of ticks
    pub n_ticks: usize,
    /// A scale factor, usually set to 1.0.
    /// This is to also support multiples of ᴨ
    pub scale: f64,
    /// Symbol to use for the tick labels (e.g. "ᴨ" when setting `scale` to ᴨ)
    pub scale_symbol: Option<&'static str>,
}

impl Ticks {
    /// Use this to disable ticks for an axis
    pub fn no_ticks() -> Self {
        Self {
            min: 0.into(),
            step: 1.into(),
            n_ticks: 0,
            scale: 1.,
            scale_symbol: None,
        }
    }
    fn iter(&self) -> impl Iterator<Item = Decimal<i32>> {
        let d_exp = self.step.exp - self.min.exp;
        let step = self.step.mantissa * i32::pow(10, i32::max(0, d_exp) as u32);
        let mantissa = self.min.mantissa * i32::pow(10, i32::max(0, -d_exp) as u32);
        let exp = self.min.exp - i32::max(0, -d_exp);
        (0..self.n_ticks).map(move |i| Decimal::new(mantissa + i as i32 * step, exp))
    }

    fn values(&self) -> impl Iterator<Item = f64> {
        let step: f64 = (&self.step).into();
        let min: f64 = (&self.min).into();
        (0..self.n_ticks).map(move |i| min + i as f64 * step)
    }

    fn max(&self) -> Decimal<i32> {
        &self.min
            + &Decimal::new(
                (self.n_ticks as i32 - 1) * self.step.mantissa,
                self.step.exp,
            )
    }
}

/// min b: d <= N b 10^exp
/// d/N <= b 10^exp <=> log(d/N) <= log(b) + exp
/// 0 <= log(b) <= log(5)
fn tick_distance_from_length(d: impl ToReferenceScale + IsZero, dn: u32) -> Decimal<i32> {
    const MAX_SUB_INTERVALS: usize = 10;
    const DISTANCE_MANTISSAE: [i32; 3] = [1, 2, 5];

    if d.is_zero() {
        return Decimal {
            mantissa: 1,
            exp: -1,
        };
    }
    let (n_d, exp) = d.to_reference_scale((MAX_SUB_INTERVALS as i32 - dn as i32) as usize);

    for mantissa in DISTANCE_MANTISSAE {
        if n_d <= mantissa as u32 * (MAX_SUB_INTERVALS as u32 + dn) {
            return Decimal { mantissa, exp };
        }
    }
    Decimal {
        mantissa: 1,
        exp: exp + 1,
    }
}

trait IsZero {
    fn is_zero(&self) -> bool;
}

impl IsZero for f64 {
    fn is_zero(&self) -> bool {
        *self == 0.
    }
}
impl IsZero for &Decimal<i32> {
    fn is_zero(&self) -> bool {
        self.mantissa == 0
    }
}

trait ToReferenceScale {
    fn to_reference_scale(self, n_sub_intervals: usize) -> (u32, i32);
}

impl ToReferenceScale for f64 {
    fn to_reference_scale(self, n_sub_intervals: usize) -> (u32, i32) {
        let x = self.abs();
        let exp = f64::floor(f64::log10(x / n_sub_intervals as f64)) as i32;
        (f64::ceil(multiply_10_exp(x, -exp)) as u32, exp)
    }
}

impl ToReferenceScale for u32 {
    fn to_reference_scale(self, n_sub_intervals: usize) -> (u32, i32) {
        let exp = decimal::mag10(self as i32 * 10 / n_sub_intervals as i32);
        if exp == 0 {
            (self * 10, exp as i32 - 1)
        } else {
            let exp = exp as i32 - 1;
            let e10 = i32::pow(10, exp as u32);
            let d = self / e10 as u32 + u32::from(self % e10 as u32 > 0);
            (d, exp)
        }
    }
}
impl ToReferenceScale for &Decimal<i32> {
    fn to_reference_scale(self, n_sub_intervals: usize) -> (u32, i32) {
        let (x_scaled, exp) =
            u32::to_reference_scale(self.mantissa.unsigned_abs(), n_sub_intervals);
        (x_scaled, exp + self.exp)
    }
}

impl Ticks {
    const NAN_TICKS: Self = Self {
        min: Decimal {
            mantissa: 0,
            exp: 0,
        },
        n_ticks: 0,
        step: Decimal {
            mantissa: 1,
            exp: 0,
        },
        scale: 1.,
        scale_symbol: None,
    };
    /// [0, ᴨ/4, ᴨ/2, 3/4 ᴨ, ᴨ, ..., 2ᴨ]
    const RADIAN_TICKS: Self = Self {
        min: Decimal {
            mantissa: 0,
            exp: 0,
        },
        n_ticks: 9,
        step: Decimal {
            mantissa: 25,
            exp: -2,
        },
        scale: std::f64::consts::PI,
        scale_symbol: Some("ᴨ"),
    };
    /// [0°, 45°, 90°, 135°, 180°, ..., 360°]
    const DEGREE_TICKS: Self = Self {
        min: Decimal {
            mantissa: 0,
            exp: 0,
        },
        n_ticks: 9,
        step: Decimal {
            mantissa: 45,
            exp: 0,
        },
        scale: 1.,
        scale_symbol: Some("°"),
    };

    fn new_subticks(tick_distance: &Decimal<i32>) -> Self {
        let step = tick_distance_from_length(tick_distance, 0);
        let scaled_distance =
            tick_distance.mantissa * i32::pow(10, (tick_distance.exp - step.exp) as u32);
        let n_ticks = (scaled_distance / step.mantissa
            - if scaled_distance % step.mantissa == 0 {
                1
            } else {
                0
            }) as usize;
        Self {
            min: step.clone(),
            step,
            n_ticks,
            scale: 1.,
            scale_symbol: None,
        }
    }
    fn new_covered_by_interval(interval: &Interval) -> Self {
        Self::new_from_interval_and_step(
            interval,
            tick_distance_from_length(interval.length(), 0),
            true,
        )
    }
    /// Create ticks such that
    /// - First tick is below or at lower value of the interval
    /// - Last tick is above or at upper value of the interval
    /// - Values of first and last ticks (and thus also the distance between 2 ticks)
    ///   is one of 1, 2, 5, multiplied by a power of 10
    pub fn new_covering_interval(interval: &Interval) -> Self {
        Self::new_from_interval_and_step(
            interval,
            tick_distance_from_length(interval.length(), 0),
            false,
        )
    }
    fn new_from_interval_and_step(
        Interval { min, max }: &Interval,
        step: Decimal<i32>,
        open: bool,
    ) -> Self {
        if min.is_nan() || max.is_nan() {
            return Ticks::NAN_TICKS;
        }
        let u_d = f64::from(&step);
        let (n_min, n_max) = if open {
            (f64::ceil(min / u_d), f64::floor(max / u_d))
        } else {
            (f64::floor(min / u_d), f64::ceil(max / u_d))
        };
        Self {
            n_ticks: (n_max - n_min + 1.) as usize,
            min: Decimal::new(step.mantissa * n_min as i32, step.exp),
            step,
            scale: 1.,
            scale_symbol: None,
        }
    }
    fn to_interval(&self) -> Interval {
        let n = if self.n_ticks > 0 {
            self.n_ticks - 1
        } else {
            1
        };
        Interval {
            min: (&self.min).into(),
            max: f64::from(&self.min) + n as f64 * f64::from(&self.step),
        }
    }
    /// Create a pair of ticks for an x and an y interval such that the distance between 2 ticks
    /// is same on both axes
    pub fn new_pair_with_equal_units(XyInterval { x, y }: &XyInterval) -> (Self, Self) {
        let u = tick_distance_from_length(f64::max(x.length(), y.length()), 2);
        (
            Self::new_from_interval_and_step(x, u.clone(), false),
            Self::new_from_interval_and_step(y, u, false),
        )
    }
}

/// Tick label format
#[derive(Debug, Clone)]
pub enum TickLabelFormat {
    /// Use `Decimal as std::fmt::Display` to format tick labels
    DEFAULT,
    /// Format as `{mantissa}E{exp}`, where `{exp}` is always a multiple of 3
    ENGINEERING,
}
impl Default for TickLabelFormat {
    fn default() -> Self {
        Self::DEFAULT
    }
}
impl TickLabelFormat {
    fn format<T: decimal::SignedInteger + std::fmt::Display>(
        &self,
        (scale_symbol, value): (Option<&'static str>, Decimal<T>),
    ) -> String {
        match self {
            Self::DEFAULT => format!("{}{}", value, scale_symbol.unwrap_or("")),
            Self::ENGINEERING => value.format_engineering(),
        }
    }
}

/// Axis used in [XyChart]
#[derive(Debug, Clone)]
pub struct Axis<'a> {
    /// Interval / range of the axis
    pub interval: Interval,
    /// Locations of ticks on the axis
    pub ticks: Ticks,
    /// Locations of sub-ticks, relative to the first tick
    pub sub_ticks: Ticks,
    /// Axis label
    pub label: String,
    /// Font for the axis label
    pub label_font: gfx::Font<&'a str>,
    /// Font for tick labels
    pub tick_label_font: gfx::Font<&'a str>,
    /// Format for the tick labels
    pub tick_label_format: TickLabelFormat,
    /// Length of upper (and lower) part (above / below axis) of a tick in chart space
    pub half_tick_length: f64,
    /// Draw ticks on the lower border of the data region (x axis) / on the
    /// left border of the data region (y axis)
    pub show_primary: bool,
    /// Draw ticks on the upper border of the data region (x axis) / on the
    /// right border of the data region (y axis)
    pub show_secondary: bool,
}

impl Default for Axis<'_> {
    fn default() -> Self {
        let ticks = Ticks {
            min: Decimal {
                mantissa: 0,
                exp: 0,
            },
            step: Decimal {
                mantissa: 1,
                exp: -1,
            },
            n_ticks: 11,
            scale: 1.,
            scale_symbol: None,
        };
        Self {
            interval: Interval { min: 0., max: 1. },
            sub_ticks: Ticks::new_subticks(&ticks.step),
            ticks,
            half_tick_length: 5.,
            show_primary: true,
            show_secondary: false,
            label: Default::default(),
            label_font: Default::default(),
            tick_label_font: Default::default(),
            tick_label_format: Default::default(),
        }
    }
}

impl<'a> Axis<'a> {
    /// Create a new axis with given interval and ticks
    pub fn new_from_interval_and_ticks(interval: Interval, ticks: Ticks) -> Self {
        Self {
            interval,
            sub_ticks: Ticks::new_subticks(&ticks.step),
            ticks,
            ..Default::default()
        }
    }
    /// Create a new axis with given interval and populate ticks such that
    /// - First tick is above or at lower value of the interval
    /// - Last tick is below or at upper value of the interval
    /// - Values of first and last ticks (and thus also the distance between 2 ticks)
    ///   is one of 1, 2, 5, multiplied by a power of 10
    pub fn new_from_interval(interval: Interval) -> Self {
        let ticks = Ticks::new_covered_by_interval(&interval);
        Self::new_from_interval_and_ticks(interval, ticks)
    }
    /// Create a new axis with given ticks and an interval
    /// from the first tick to the last tick
    pub fn new_from_ticks(ticks: Ticks) -> Self {
        Self::new_from_interval_and_ticks(ticks.to_interval(), ticks)
    }

    /// Create a pair of axes with ticks covering a [XyInterval]
    pub fn new_pair_covering_interval_with_equal_units(xy_interval: &XyInterval) -> (Self, Self) {
        let (x_ticks, y_ticks) = Ticks::new_pair_with_equal_units(xy_interval);
        (Self::new_from_ticks(x_ticks), Self::new_from_ticks(y_ticks))
    }

    /// Set the label
    pub fn with_label(self, label: impl Into<String>) -> Self {
        Self {
            label: label.into(),
            ..self
        }
    }

    /// Set the font
    pub fn with_font(
        self,
        label_font: gfx::Font<&'a str>,
        tick_label_font: gfx::Font<&'a str>,
    ) -> Self {
        Self {
            label_font,
            tick_label_font,
            ..self
        }
    }

    /// Create a new periodic axis from `min*ᴨ` to `(min+2)ᴨ`
    pub fn new_from_radian(min: Decimal<i32>) -> Self {
        let min_val = f64::from(&min) * std::f64::consts::PI;
        let ticks = Ticks {
            min,
            ..Ticks::RADIAN_TICKS
        };
        Self {
            interval: Interval {
                min: min_val,
                max: min_val + 2. * std::f64::consts::PI,
            },
            sub_ticks: Ticks::new_subticks(&ticks.step),
            ticks,
            ..Default::default()
        }
    }
    /// Create a new periodic axis from `min` to `min + 360°`
    pub fn new_from_degree(min: Decimal<i32>) -> Self {
        let min_val = f64::from(&min);
        let ticks = Ticks {
            min,
            ..Ticks::DEGREE_TICKS
        };
        Self {
            interval: Interval {
                min: min_val,
                max: min_val + 360.,
            },
            sub_ticks: Ticks::new_subticks(&ticks.step),
            ticks,
            ..Default::default()
        }
    }

    fn layout<O>(&'a self, font_metrics: &impl gfx::FontMetrics) -> AxisLayout<'a, O> {
        const DEFAULT_SPC_DIAL_OUTER: f64 = 5.;
        const DEFAULT_SPC_DIAL_INNER: f64 = 10.;
        let tick_labels: Vec<_> = self
            .ticks
            .iter()
            .map(|val| {
                self.tick_label_format
                    .format((self.ticks.scale_symbol, val))
            })
            .collect();
        let (text_w, text_h) =
            if self.tick_label_font.size > 0. && (self.show_primary || self.show_secondary) {
                tick_labels
                    .iter()
                    .map(|s| font_metrics.get_text_extents(s, &self.tick_label_font))
                    .reduce(tuple_max)
                    .unwrap_or((0., 0.))
            } else {
                (0., 0.)
            };
        let (spc_dial_outer, spc_dial_inner) = if text_w == 0. && text_h == 0. {
            (0., 0.)
        } else {
            (DEFAULT_SPC_DIAL_OUTER, DEFAULT_SPC_DIAL_INNER)
        };
        let (dl_label, mut dt_label) = if self.label.is_empty() {
            (0., 0.)
        } else {
            font_metrics.get_text_extents(&self.label, &self.label_font)
        };
        const DEFAULT_SPC_LABEL_OUTER: f64 = 5.;
        const DEFAULT_SPC_LABEL_INNER: f64 = 5.;
        if dt_label > 0. {
            dt_label += DEFAULT_SPC_LABEL_OUTER + DEFAULT_SPC_LABEL_INNER;
        }
        AxisLayout {
            axis: self,
            tick_labels,
            text_w,
            text_h,
            dl_label,
            dt_label,
            spc_dial_inner,
            spc_dial_outer,
            orientation: std::marker::PhantomData,
            spc_label_outer: DEFAULT_SPC_LABEL_OUTER,
        }
    }
}

/// Chart space aspect ratio for [IntrinsicChartLayout]
pub enum AspectRatio {
    /// Automatically choose the aspect ratio
    Unconstrained,
    /// 1:1 aspect ratio
    Square,
    /// `x_len:y_len` aspect ratio, where `x_len` / `y_len` is the length of
    /// the x / y axis respectively in data space
    AnglePreserving,
    /// Aspect ratio choosen such that the distance of 2 ticks match for x and y axes
    EqualTicksDistance,
    /// The chart will exactly have the given width and height
    Fixed(f64, f64),
}

/// Interval in data space
#[derive(Clone, PartialEq, Debug)]
pub struct Interval {
    /// Start value for the interval
    pub min: f64,
    /// End value for the interval
    pub max: f64,
}

impl Interval {
    /// Length of the interval (`max` - `min`)
    pub fn length(&self) -> f64 {
        self.max - self.min
    }
    fn expand(self, x: f64) -> Self {
        if !x.is_finite() {
            return self;
        }
        Self {
            min: if self.min.is_nan() || self.min > x {
                x
            } else {
                self.min
            },
            max: if self.max.is_nan() || self.max < x {
                x
            } else {
                self.max
            },
        }
    }
    fn padded(self) -> Self {
        if self.min == self.max {
            return Self {
                min: self.min - 0.5,
                max: self.max + 0.5,
            };
        }
        self
    }
}

impl From<&Interval> for std::ops::Range<f64> {
    fn from(value: &Interval) -> Self {
        value.min..value.max
    }
}
impl From<&std::ops::Range<f64>> for Interval {
    fn from(value: &std::ops::Range<f64>) -> Self {
        Self {
            min: value.start,
            max: value.end,
        }
    }
}

/// Pair of [Interval] instances for x and y axes
#[derive(Clone, PartialEq, Debug)]
pub struct XyInterval {
    /// Horizontal factor
    pub x: Interval,
    /// Vertical factor
    pub y: Interval,
}

impl XyInterval {
    /// Create a new 2 dimensional interval
    pub const fn new(x: Interval, y: Interval) -> Self {
        Self { x, y }
    }
    fn expand(self, [x, y]: [f64; 2]) -> Self {
        Self {
            x: self.x.expand(x),
            y: self.y.expand(y),
        }
    }

    fn padded(self) -> Self {
        Self {
            x: self.x.padded(),
            y: self.y.padded(),
        }
    }

    fn contains(&self, [x, y]: [f64; 2]) -> bool {
        self.x.min <= x && x <= self.x.max && self.y.min <= y && y <= self.y.max
    }

    /// Minimal [XyInterval] which contains all points in `coordinates`
    pub fn new_fitting_points(coordinates: impl IntoIterator<Item = [f64; 2]>) -> Self {
        let bounding_box = Self {
            x: Interval {
                min: f64::NAN,
                max: f64::NAN,
            },
            y: Interval {
                min: f64::NAN,
                max: f64::NAN,
            },
        };
        coordinates
            .into_iter()
            .fold(bounding_box, |b, crd| b.expand(crd))
            .padded()
    }
}

/// Chart layout holding computed internal spacings / labels only depending to the aspect ratio
/// and the font size but still agnostic of the final size of the chart in chart space
///
/// Obtained by [XyChart::prepare_layout]
pub struct IntrinsicChartLayout<'a> {
    chart: &'a XyChart<'a>,
    aspect_ratio: AspectRatio,
    d_title: f64,
    data_region_left_align: f64,
    data_region_top_align: f64,
    border: f64,
    spc_title_u: f64,
    x_axis_layout: AxisLayout<'a, Horizontal>,
    y_axis_layout: AxisLayout<'a, Vertical>,
}

/// Chart layout holding computed internal spacings / positions / labels determined by
/// the final size of the chart in chart space
///
/// Obtained by [IntrinsicChartLayout::fit_into] and [IntrinsicChartLayout::with_axes_extents]
pub struct ExtrinsicChartLayout<'a> {
    chart: &'a XyChart<'a>,
    layout: IntrinsicChartLayout<'a>,
    x_tick_positions: Vec<f64>,
    y_tick_positions: Vec<f64>,
    x_grid_range: std::ops::Range<usize>,
    y_grid_range: std::ops::Range<usize>,
    draw_region: Region,
    data_region: Region,
    chart_to_canvas: transform::Transformation,
    vertical_mode: bool,
}

/// Relation from data space to chart space. Used in the [PlotTo] trait
#[derive(Debug)]
pub struct DataRegion<'a> {
    /// [XyInterval] as the data region in data space
    pub xy_interval: XyInterval,
    /// [Region] as the data region in chart space
    pub region: &'a Region,
    /// Mapping from data space to chart space
    pub chart_to_canvas: &'a transform::Transformation,
}

impl<'a> From<&'a ExtrinsicChartLayout<'a>> for DataRegion<'a> {
    fn from(value: &'a ExtrinsicChartLayout<'a>) -> Self {
        Self {
            xy_interval: value.chart.xy_interval(),
            region: &value.data_region,
            chart_to_canvas: &value.chart_to_canvas,
        }
    }
}

fn compute_tick_positions(axis: &Axis, axis_length: f64) -> Vec<f64> {
    let k = axis_length / axis.interval.length();
    axis.ticks
        .values()
        .map(|v| k * (v - axis.interval.min))
        .collect()
}

fn grid_slice(axis: &Axis) -> std::ops::Range<usize> {
    let reference = (&axis.ticks.step).into();
    let start = usize::from(axis.ticks.min.is_near(axis.interval.min, reference));
    let end =
        axis.ticks.n_ticks - usize::from(axis.ticks.max().is_near(axis.interval.max, reference));
    std::ops::Range {
        start: usize::min(start, end),
        end,
    }
}

/// All implementors can be drawn using [ExtrinsicChartLayout::draw].
///
/// In contrast to [DrawToRegion], drawing is relative to the draw region and not the data region.
pub trait DrawToRegion {
    /// Draw `self` to the `draw_region` of a chart
    fn draw_to_region(&self, drawable: &mut impl gfx::Drawable, draw_region: &Region);
}

/// All instances of types implementing this trait can be plotted using [ExtrinsicChartLayout::plot] (`ReturnType = ()`) or [ExtrinsicChartLayout::try_plot] (`ReturnType = Result<(), _>`).
///
/// This is relative to the data region.
pub trait PlotTo {
    /// Return type of [Self::plot_to].
    /// Usually () or a [std::result::Result] if the plot can fail
    type ReturnType;
    /// Draw `self` to the `data_region` of a chart
    fn plot_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        data_region: &DataRegion,
    ) -> Self::ReturnType;
}

impl<A: PlotTo> PlotTo for Option<A>
where
    A::ReturnType: FallbackValue,
{
    type ReturnType = A::ReturnType;
    fn plot_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        data_region: &DataRegion,
    ) -> Self::ReturnType {
        if let Some(p) = self {
            p.plot_to(drawable, data_region)
        } else {
            Self::ReturnType::fallback_value()
        }
    }
}

trait FallbackValue {
    fn fallback_value() -> Self;
}

impl FallbackValue for () {
    fn fallback_value() -> Self {}
}

impl<E> FallbackValue for Result<(), E> {
    fn fallback_value() -> Self {
        Ok(())
    }
}

impl<'a> IntrinsicChartLayout<'a> {
    /// Align the left border of the data region to the `x` value (chart space).
    /// If `x` is smaller than the spacing resulting from axis label and axis ticks,
    /// this has no effect
    pub fn align_data_region_left_at(mut self, x: f64) -> Self {
        self.data_region_left_align = x;
        self
    }
    /// Align the top border of the data region to the `y` value (chart space).
    /// If `y` is smaller than the spacing resulting from axis label and chart title,
    /// this has no effect
    pub fn align_data_region_top_at(mut self, y: f64) -> Self {
        self.data_region_top_align = y;
        self
    }

    /// Add info about the output size to compute internal spacings / positions
    /// depending on the output size. The resulting [ExtrinsicChartLayout] will be ready to draw.
    pub fn fit_into(self, width: f64, height: f64) -> ExtrinsicChartLayout<'a> {
        let chart = self.chart;
        if width <= 0.
            || height <= 0.
            || f64::from(&chart.x_axis.ticks.step) <= 0.
            || f64::from(&chart.y_axis.ticks.step) <= 0.
        {
            panic!("Negative distance: width | height | unit of axis");
        }
        let h_border = self.get_total_h_border(false);
        let vertical_mode = self.width_requires_vertical_mode(width - h_border);
        let data_region = self.data_region(width, height, vertical_mode);

        ExtrinsicChartLayout {
            chart,
            layout: self,
            x_tick_positions: compute_tick_positions(&chart.x_axis, data_region.w),
            y_tick_positions: compute_tick_positions(&chart.y_axis, data_region.h),
            x_grid_range: grid_slice(&chart.x_axis),
            y_grid_range: grid_slice(&chart.y_axis),
            chart_to_canvas: transform::Transformation::new_from_domain_to_image(
                &(
                    (&chart.x_axis.interval).into(),
                    (&chart.y_axis.interval).into(),
                ),
                &((&data_region).into()),
            ),
            data_region,
            draw_region: Region {
                x: 0.,
                y: 0.,
                w: width,
                h: height,
            },
            vertical_mode,
        }
    }

    /// Use the specified axes extents to determine the output size and to compute
    /// internal spacings / positions depending on the output size.
    /// The resulting [ExtrinsicChartLayout] will be ready to draw.
    pub fn with_axes_extents(mut self, ax_width: f64, ax_height: f64) -> ExtrinsicChartLayout<'a> {
        let chart = self.chart;
        if ax_width <= 0.
            || ax_height <= 0.
            || f64::from(&chart.x_axis.ticks.step) <= 0.
            || f64::from(&chart.y_axis.ticks.step) <= 0.
        {
            panic!("Negative distance: width | height | unit of axis");
        }
        self.aspect_ratio = AspectRatio::Fixed(ax_width, ax_height);
        let vertical_mode = self.width_requires_vertical_mode(ax_width);
        let data_region = self.data_region_from_axes_extents(ax_width, ax_height, vertical_mode);
        let w = ax_width + self.get_total_h_border(vertical_mode);
        let h = ax_height + self.get_total_v_border(vertical_mode);
        ExtrinsicChartLayout {
            chart,
            layout: self,
            chart_to_canvas: transform::Transformation::new_from_domain_to_image(
                &(
                    (&chart.x_axis.interval).into(),
                    (&chart.y_axis.interval).into(),
                ),
                &((&data_region).into()),
            ),
            x_tick_positions: compute_tick_positions(&chart.x_axis, data_region.w),
            y_tick_positions: compute_tick_positions(&chart.y_axis, data_region.h),
            x_grid_range: grid_slice(&chart.x_axis),
            y_grid_range: grid_slice(&chart.y_axis),
            data_region,
            draw_region: Region { x: 0., y: 0., w, h },
            vertical_mode,
        }
    }
    /// Suitable width for a given height.
    pub fn natural_width_for_height(&self, height: f64) -> f64 {
        let vertical = self.height_allows_vertical_mode(height);
        let h = self.y_axis_length_for_vertical_mode(height, vertical);
        let w = match self.aspect_ratio {
            AspectRatio::Square => h,
            AspectRatio::AnglePreserving => {
                let dx = self.chart.x_axis.interval.length();
                let dy = self.chart.y_axis.interval.length();
                h * dx / dy
            }
            AspectRatio::Unconstrained | AspectRatio::EqualTicksDistance => {
                let dx = self.chart.x_axis.ticks.n_ticks - 1;
                let dy = self.chart.y_axis.ticks.n_ticks - 1;
                h * dx as f64 / dy as f64
            }
            AspectRatio::Fixed(w, _) => w,
        };
        let w_text = if vertical {
            self.x_axis_layout.text_h
        } else {
            self.x_axis_layout.text_w
        };
        w + self.get_total_h_border_for_text_width(w_text / 2.)
    }

    /// Suitable height for a given width.
    pub fn natural_height_for_width(&self, width: f64) -> f64 {
        let h_border = self.get_total_h_border(false);
        let vertical_mode = self.width_requires_vertical_mode(width - h_border);
        let w = self.x_axis_length_for_vertical_mode(width, vertical_mode);
        let h = match self.aspect_ratio {
            AspectRatio::Square => w,
            AspectRatio::AnglePreserving => {
                let dx = self.chart.x_axis.interval.length();
                let dy = self.chart.y_axis.interval.length();
                w * dy / dx
            }
            AspectRatio::Unconstrained | AspectRatio::EqualTicksDistance => {
                let dx = self.chart.x_axis.ticks.n_ticks - 1;
                let dy = self.chart.y_axis.ticks.n_ticks - 1;
                w * dy as f64 / dx as f64
            }
            AspectRatio::Fixed(_, h) => h,
        };
        self.height(h, vertical_mode)
    }
    fn height(&self, axis_h: f64, vertical_mode: bool) -> f64 {
        let h_text = if vertical_mode {
            self.x_axis_layout.text_w
        } else {
            self.x_axis_layout.text_h
        };

        let axis_h = if let AspectRatio::Fixed(_, h) = self.aspect_ratio {
            h
        } else {
            axis_h
        };

        axis_h + self.get_total_v_border_for_text_height(h_text)
    }

    fn data_region(&self, width: f64, height: f64, vertical_mode: bool) -> Region {
        let (ax_width, ax_height) = self.axes_extents(width, height, vertical_mode);
        let (ax_width, ax_height) = match &self.aspect_ratio {
            AspectRatio::Unconstrained => (ax_width, ax_height),
            AspectRatio::Square => {
                if ax_width < ax_height {
                    (ax_width, ax_width)
                } else {
                    (ax_height, ax_height)
                }
            }
            AspectRatio::AnglePreserving => {
                let dx = self.chart.x_axis.interval.length();
                let dy = self.chart.y_axis.interval.length();
                if ax_width * dy < ax_height * dx {
                    (ax_width, ax_width * dy / dx)
                } else {
                    (ax_height * dx / dy, ax_height)
                }
            }
            AspectRatio::EqualTicksDistance => {
                let dx = (self.chart.x_axis.ticks.n_ticks - 1) as f64;
                let dy = (self.chart.y_axis.ticks.n_ticks - 1) as f64;
                if ax_width * dy < ax_height * dx {
                    (ax_width, ax_width * dy / dx)
                } else {
                    (ax_height * dx / dy, ax_height)
                }
            }
            AspectRatio::Fixed(w, h) => (*w, *h),
        };
        self.data_region_from_axes_extents(ax_width, ax_height, vertical_mode)
    }
    fn data_region_from_axes_extents(
        &self,
        ax_width: f64,
        ax_height: f64,
        vertical_mode: bool,
    ) -> Region {
        Region {
            x: self.get_dg_x(vertical_mode),
            y: self.get_dg_y(vertical_mode),
            w: ax_width,
            h: ax_height,
        }
    }
    fn get_dg_x(&self, vertical_mode: bool) -> f64 {
        if vertical_mode {
            self.get_left_border(self.x_axis_layout.text_h / 2.)
        } else {
            self.get_left_border(self.x_axis_layout.text_w / 2.)
        }
    }
    fn get_dg_y(&self, vertical_mode: bool) -> f64 {
        if vertical_mode {
            self.get_top_border(self.x_axis_layout.text_w / 2.)
        } else {
            self.get_top_border(self.x_axis_layout.text_h / 2.)
        }
    }
    fn get_left_border(&self, w: f64) -> f64 {
        let dx_x_dial = if self.chart.x_axis.show_primary || self.chart.x_axis.show_secondary {
            w
        } else {
            0.
        };
        let dx_y_dial = if self.chart.y_axis.show_primary {
            self.y_axis_layout.text_w
                + self.y_axis_layout.spc_dial_outer
                + self.y_axis_layout.spc_dial_inner
        } else {
            0.
        };

        f64::max(
            self.data_region_left_align,
            self.border + f64::max(dx_x_dial, self.x_axis_layout.dt_label + dx_y_dial),
        )
    }
    fn get_right_border(&self, w: f64) -> f64 {
        let dx_x_dial = if self.chart.y_axis.show_primary || self.chart.y_axis.show_secondary {
            w
        } else {
            0.
        };
        let dx_y_dial = if self.chart.y_axis.show_secondary {
            self.y_axis_layout.text_w
                + self.y_axis_layout.spc_dial_outer
                + self.y_axis_layout.spc_dial_inner
        } else {
            0.
        };
        f64::max(dx_x_dial, dx_y_dial) + self.border
    }
    fn get_top_border(&self, h: f64) -> f64 {
        let dy_x_dial = if self.chart.x_axis.show_secondary {
            h + self.x_axis_layout.spc_dial_outer + self.x_axis_layout.spc_dial_inner
        } else {
            0.
        };
        let dy_y_dial = if self.chart.y_axis.show_primary || self.chart.y_axis.show_secondary {
            self.y_axis_layout.text_h / 2.
        } else {
            0.
        };
        f64::max(
            self.data_region_top_align,
            self.d_title + f64::max(dy_y_dial, dy_x_dial) + self.border,
        )
    }
    fn get_bottom_border(&self, h: f64) -> f64 {
        let dy_x_dial = if self.chart.x_axis.show_primary {
            h + self.x_axis_layout.spc_dial_outer + self.x_axis_layout.spc_dial_inner
        } else {
            0.
        };
        let dy_y_dial = if self.chart.y_axis.show_primary || self.chart.y_axis.show_secondary {
            self.y_axis_layout.text_h / 2.
        } else {
            0.
        };
        f64::max(dy_y_dial, dy_x_dial) + self.y_axis_layout.dt_label + self.border
    }
    fn axes_extents(&self, rg_w: f64, rg_h: f64, vertical_mode: bool) -> (f64, f64) {
        (
            self.x_axis_length_for_vertical_mode(rg_w, vertical_mode),
            self.y_axis_length_for_vertical_mode(rg_h, vertical_mode),
        )
    }
    fn x_axis_length_for_vertical_mode(&self, rg_w: f64, vertical_mode: bool) -> f64 {
        if let AspectRatio::Fixed(w, _) = self.aspect_ratio {
            w
        } else {
            let h_border = self.get_total_h_border(vertical_mode);
            rg_w - h_border
        }
    }
    fn y_axis_length_for_vertical_mode(&self, rg_h: f64, vertical_mode: bool) -> f64 {
        if let AspectRatio::Fixed(_, h) = self.aspect_ratio {
            h
        } else {
            let v_border = self.get_total_v_border(vertical_mode);
            rg_h - v_border
        }
    }
    fn get_total_h_border(&self, vertical_mode: bool) -> f64 {
        self.get_total_h_border_for_text_width(if vertical_mode {
            self.x_axis_layout.text_h / 2.
        } else {
            self.x_axis_layout.text_w / 2.
        })
    }
    fn get_total_h_border_for_text_width(&self, text_width: f64) -> f64 {
        self.get_left_border(text_width) + self.get_right_border(text_width)
    }
    fn get_total_v_border(&self, vertical_mode: bool) -> f64 {
        self.get_total_v_border_for_text_height(if vertical_mode {
            self.x_axis_layout.text_w
        } else {
            self.x_axis_layout.text_h
        })
    }
    fn get_total_v_border_for_text_height(&self, text_height: f64) -> f64 {
        self.get_top_border(text_height) + self.get_bottom_border(text_height)
    }
    fn height_allows_vertical_mode(&self, height: f64) -> bool {
        let h_ax = if let AspectRatio::Fixed(_, h) = self.aspect_ratio {
            h
        } else {
            self.y_min_axis_length()
        };
        let min_height = h_ax + self.get_total_v_border(true);
        self.x_axis_layout.text_w > self.x_axis_layout.text_h && min_height <= height
    }
    fn width_requires_vertical_mode(&self, w: f64) -> bool {
        self.x_axis_layout.text_w > self.x_axis_layout.text_h
            && f64::max(0., w)
                <= (self.y_axis_layout.text_w
                    + self.y_axis_layout.spc_dial_outer
                    + self.y_axis_layout.spc_dial_inner)
                    * self.chart.x_axis.ticks.n_ticks as f64
    }
    fn y_min_axis_length(&self) -> f64 {
        f64::max(
            (self.y_axis_layout.text_h
                + self.y_axis_layout.spc_dial_outer
                + self.y_axis_layout.spc_dial_inner)
                * (self.chart.y_axis.ticks.n_ticks - 1) as f64,
            self.y_axis_layout.dl_label,
        )
    }
}

impl ExtrinsicChartLayout<'_> {
    /// Pixel width of the chart draw region
    pub fn width(&self) -> f64 {
        self.draw_region.w
    }
    /// Pixel height of the chart draw region
    pub fn height(&self) -> f64 {
        self.draw_region.h
    }
    /// Draw at `(x, y)` instead of `(0.0, 0.0)`
    pub fn with_offset(mut self, x: f64, y: f64) -> Self {
        self.draw_region.x += x;
        self.draw_region.y += y;
        self.data_region.x += x;
        self.data_region.y += y;
        self
    }
    /// Draw the empty chart (axes / ticks / labels but no data)
    pub fn draw_axes(self, drawable: &mut impl gfx::Drawable) -> Self {
        let ExtrinsicChartLayout {
            chart,
            layout,
            draw_region,
            data_region: rg,
            x_tick_positions,
            y_tick_positions,
            x_grid_range,
            y_grid_range,
            ..
        } = &self;
        let Self {
            chart_to_canvas: transformation,
            vertical_mode,
            ..
        } = &self;
        let (kx, ky) = transformation.scale;

        let line_style = gfx::LineStyle {
            width: self.chart.line_width,
            ..Default::default()
        };

        // ........................ axes region / border ........................
        drawable.stroke(
            &gfx::Rectangle {
                x: rg.x,
                y: rg.y,
                width: rg.w,
                height: rg.h,
            },
            &line_style,
        );

        // .............................. caption ..............................
        if let Some(title) = &chart.title {
            drawable.draw_text(
                &gfx::Text {
                    x: rg.x + rg.w / 2.,
                    y: draw_region.y + layout.spc_title_u,
                    text: title,
                    anchor: gfx::TextAnchor::Middle,
                    baseline: gfx::Baseline::Hanging,
                    angle: 0.,
                },
                &chart.title_font,
            );
        }

        // .............................. subgrid ..............................

        if let Some(line_style) = &chart.sub_grid_line_style {
            Self::draw_subgrid::<Horizontal>(
                drawable,
                &chart.x_axis,
                x_tick_positions,
                rg.x..rg.x + rg.w,
                rg.y..rg.y + rg.h,
                kx,
                line_style,
            );
            Self::draw_subgrid::<Vertical>(
                drawable,
                &chart.y_axis,
                y_tick_positions,
                rg.y + rg.h..rg.y,
                rg.x..rg.x + rg.w,
                ky,
                line_style,
            );
        }
        if let Some(line_style) = &chart.grid_line_style {
            Self::draw_grid::<Horizontal>(
                drawable,
                &x_tick_positions[x_grid_range.clone()],
                line_style,
                rg.x,
                &(rg.y + rg.h..rg.y),
            );
            Self::draw_grid::<Vertical>(
                drawable,
                &y_tick_positions[y_grid_range.clone()],
                line_style,
                rg.y + rg.h,
                &(rg.x..rg.x + rg.w),
            );
        }

        if chart.x_axis.show_primary {
            ExtrinsicAxisLayout {
                intrinsic: &layout.x_axis_layout,
                tick_positions: x_tick_positions,
                pos_axial: rg.x,
                pos_transversal: rg.y + rg.h,
                tick_padding: layout.x_axis_layout.spc_dial_inner,
                vertical_mode: *vertical_mode,
            }
            .draw_ticks(drawable, &line_style);
        }
        if chart.x_axis.show_secondary {
            ExtrinsicAxisLayout {
                intrinsic: &layout.x_axis_layout,
                tick_positions: x_tick_positions,
                pos_axial: rg.x,
                pos_transversal: rg.y,
                tick_padding: -layout.x_axis_layout.spc_dial_inner,
                vertical_mode: *vertical_mode,
            }
            .draw_ticks(drawable, &line_style);
        }
        if chart.y_axis.show_primary {
            ExtrinsicAxisLayout {
                intrinsic: &layout.y_axis_layout,
                tick_positions: y_tick_positions,
                pos_axial: rg.y + rg.h,
                pos_transversal: rg.x,
                tick_padding: -layout.y_axis_layout.spc_dial_inner,
                vertical_mode: false,
            }
            .draw_ticks(drawable, &line_style);
        }
        if chart.y_axis.show_secondary {
            ExtrinsicAxisLayout {
                intrinsic: &layout.y_axis_layout,
                tick_positions: y_tick_positions,
                pos_axial: rg.y + rg.h,
                pos_transversal: rg.x + rg.w,
                tick_padding: layout.y_axis_layout.spc_dial_inner,
                vertical_mode: false,
            }
            .draw_ticks(drawable, &line_style);
        }

        // ............................... axis  ...............................
        let mut x_offset = 0.;
        if chart.x_axis.interval.min < 0. && 0. < chart.x_axis.interval.max {
            x_offset = -chart.x_axis.interval.min * kx;
        }
        let mut y_offset = 0.;
        if chart.y_axis.interval.min < 0. && 0. < chart.y_axis.interval.max {
            y_offset = -chart.y_axis.interval.min * (-ky);
        }

        if chart.x_axis.show_primary && y_offset != 0. {
            let pos_y = rg.y + rg.h - y_offset;
            drawable.stroke_path([(rg.x, pos_y), (rg.x + rg.w, pos_y)], &line_style);
        }
        if chart.x_axis.show_secondary && y_offset != 0. {
            let pos_y = rg.y - y_offset;
            drawable.stroke_path([(rg.x, pos_y), (rg.x + rg.w, pos_y)], &line_style);
        }
        if chart.y_axis.show_primary && x_offset != 0. {
            let pos_x = rg.x + x_offset;
            drawable.stroke_path([(pos_x, rg.y + rg.h), (pos_x, rg.y)], &line_style);
        }
        if chart.y_axis.show_secondary && x_offset != 0. {
            let pos_x = rg.x + rg.w + x_offset;
            drawable.stroke_path([(pos_x, rg.y + rg.h), (pos_x, rg.y)], &line_style);
        }
        // ........................... axis caption  ...........................
        if !chart.x_axis.label.is_empty() {
            drawable.draw_text(
                &gfx::Text {
                    x: rg.x + rg.w / 2.,
                    y: draw_region.y + draw_region.h
                        - layout.border
                        - layout.x_axis_layout.spc_label_outer,
                    text: &chart.x_axis.label,
                    anchor: gfx::TextAnchor::Middle,
                    baseline: gfx::Baseline::Alphabetic,
                    angle: 0.,
                },
                &chart.x_axis.label_font,
            );
        }
        if !chart.y_axis.label.is_empty() {
            drawable.draw_text(
                &gfx::Text {
                    x: draw_region.x + layout.border + layout.y_axis_layout.spc_label_outer,
                    y: rg.y + rg.h / 2.,
                    text: &chart.y_axis.label,
                    anchor: gfx::TextAnchor::Middle,
                    baseline: gfx::Baseline::Hanging,
                    angle: -std::f64::consts::FRAC_PI_2,
                },
                &chart.y_axis.label_font,
            );
        }
        self
    }

    fn draw_subgrid<O: AxisOrientation>(
        drawable: &mut impl gfx::Drawable,
        axis: &Axis,
        tick_positions: &[f64],
        axial_range: std::ops::Range<f64>,
        transversal_range: std::ops::Range<f64>,
        k: f64,
        line_style: &gfx::LineStyle,
    ) {
        if axis.ticks.n_ticks == 0 {
            return;
        }
        let is_beyond_start = |pos: f64| {
            pos == axial_range.start
                || (pos < axial_range.start) == (axial_range.end > axial_range.start)
        };
        let dir = O::DIRECTION;
        let pos_ax_tick = axial_range.start + dir * tick_positions[0];
        for pos_ax_subtick in axis.sub_ticks.values().map(|val| pos_ax_tick - val * k) {
            if is_beyond_start(pos_ax_subtick) {
                break;
            }
            O::draw_transversal_segment(drawable, pos_ax_subtick, &transversal_range, line_style);
        }
        let is_beyond_end = |pos: f64| {
            pos == axial_range.end
                || (pos > axial_range.end) == (axial_range.end > axial_range.start)
        };
        for pos_ax_tick in tick_positions
            .iter()
            .map(|val| axial_range.start + dir * val)
        {
            for pos_ax_subtick in axis.sub_ticks.values().map(|val| pos_ax_tick + val * k) {
                if is_beyond_end(pos_ax_subtick) {
                    break;
                }
                O::draw_transversal_segment(
                    drawable,
                    pos_ax_subtick,
                    &transversal_range,
                    line_style,
                );
            }
        }
    }

    fn draw_grid<O: AxisOrientation>(
        drawable: &mut impl gfx::Drawable,
        tick_positions: &[f64],
        line_style: &gfx::LineStyle,
        pos_axial: f64,
        transversal_range: &std::ops::Range<f64>,
    ) {
        let dir = O::DIRECTION;
        for tick_pos in tick_positions {
            O::draw_transversal_segment(
                drawable,
                pos_axial + dir * tick_pos,
                transversal_range,
                line_style,
            );
        }
    }

    /// Determine the minimal width for the given height
    pub fn natural_width_for_height(&self, height: f64) -> f64 {
        self.layout.natural_width_for_height(height)
    }
    /// Determine the minimal height for the given width
    pub fn natural_height_for_width(&self, width: f64) -> f64 {
        self.layout.natural_height_for_width(width)
    }

    /// Plot an instance of a type implementing [DrawToRegion] (relative to the draw region).
    pub fn draw(self, drawable: &mut impl gfx::Drawable, object: &impl DrawToRegion) -> Self {
        object.draw_to_region(drawable, &self.draw_region);
        self
    }

    /// Plot an instance of a type implementing [PlotTo] with a `()` return type.
    /// Possible types:
    /// - [scatter_plot::ScatterPlot]
    /// - [line_plot::LinePlot]
    /// - [function_plot::FunctionPlot]
    /// - [contour_line_plot::ContourLinePlot]
    pub fn plot(
        self,
        drawable: &mut impl gfx::Drawable,
        plot: &impl PlotTo<ReturnType = ()>,
    ) -> Self {
        plot.plot_to(drawable, &(&self).into());
        self
    }

    /// Try to plot an instance of a type implementing [PlotTo] with a [std::result::Result] return type.
    /// Possible types:
    /// - [field_line_plot::FieldLinePlot]
    pub fn try_plot<T: PlotTo>(
        &self,
        drawable: &mut impl gfx::Drawable,
        plot: &T,
    ) -> T::ReturnType {
        plot.plot_to(drawable, &self.into())
    }
}

struct AxisLayout<'a, O> {
    orientation: std::marker::PhantomData<O>,
    axis: &'a Axis<'a>,
    tick_labels: Vec<String>,
    text_w: f64,
    text_h: f64,
    dl_label: f64,
    dt_label: f64,
    spc_dial_outer: f64,
    spc_dial_inner: f64,
    spc_label_outer: f64,
}

struct ExtrinsicAxisLayout<'a, O> {
    intrinsic: &'a AxisLayout<'a, O>,
    tick_positions: &'a [f64],
    pos_axial: f64,
    pos_transversal: f64,
    tick_padding: f64,
    vertical_mode: bool,
}

impl<O: AxisOrientation> ExtrinsicAxisLayout<'_, O> {
    fn draw_ticks(&self, drawable: &mut impl gfx::Drawable, line_style: &gfx::LineStyle) {
        let ExtrinsicAxisLayout {
            tick_positions,
            pos_axial,
            pos_transversal,
            tick_padding,
            vertical_mode,
            intrinsic: AxisLayout {
                axis, tick_labels, ..
            },
        } = self;
        let direction_l = O::DIRECTION;
        let direction_t = O::Transversal::DIRECTION;
        for (tick_pos, tick_label) in tick_positions.iter().zip(tick_labels.iter()) {
            let axial_pos = pos_axial + direction_l * tick_pos;
            O::draw_transversal_segment(
                drawable,
                axial_pos,
                &(pos_transversal - axis.half_tick_length..pos_transversal + axis.half_tick_length),
                line_style,
            );

            if axis.tick_label_font.size > 0. {
                let mut pos: [f64; 2] = [0., 0.];
                pos[O::INDEX] = axial_pos;
                pos[O::Transversal::INDEX] = pos_transversal + tick_padding;
                let axis_is_horizontal = O::INDEX == 0;
                let (anchor, baseline) = match (axis_is_horizontal, vertical_mode) {
                    (true, false) | (false, true) => (
                        gfx::TextAnchor::Middle,
                        if tick_padding * direction_t >= 0. {
                            gfx::Baseline::Alphabetic
                        } else {
                            gfx::Baseline::Hanging
                        },
                    ),
                    (true, true) | (false, false) => (
                        if tick_padding * direction_t >= 0. {
                            gfx::TextAnchor::Start
                        } else {
                            gfx::TextAnchor::End
                        },
                        gfx::Baseline::Middle,
                    ),
                };
                use std::f64::consts::FRAC_PI_2;
                let angle = if *vertical_mode { -FRAC_PI_2 } else { 0. };
                drawable.draw_text(
                    &gfx::Text {
                        x: pos[0],
                        y: pos[1],
                        text: tick_label,

                        angle,
                        anchor,
                        baseline,
                    },
                    &axis.tick_label_font,
                );
            }
        }
    }
}

struct Horizontal;
struct Vertical;

trait DrawTransversalSegment {
    fn draw_transversal_segment(
        drawable: &mut impl gfx::Drawable,
        pos_trans: f64,
        long: &std::ops::Range<f64>,
        line_style: &gfx::LineStyle,
    );
}
trait AxisDirection {
    const DIRECTION: f64;
}
trait AxisIndex {
    const INDEX: usize;
}
trait AxisTransversalOrientation {
    type Transversal: AxisOrientation;
}
trait AxisOrientation:
    DrawTransversalSegment + AxisDirection + AxisTransversalOrientation + AxisIndex
{
}

impl AxisOrientation for Horizontal {}
impl AxisDirection for Horizontal {
    const DIRECTION: f64 = 1.;
}
impl AxisIndex for Horizontal {
    const INDEX: usize = 0;
}
impl AxisTransversalOrientation for Horizontal {
    type Transversal = Vertical;
}
impl DrawTransversalSegment for Horizontal {
    fn draw_transversal_segment(
        drawable: &mut impl gfx::Drawable,
        pos_trans: f64,
        long: &std::ops::Range<f64>,
        line_style: &gfx::LineStyle,
    ) {
        drawable.stroke_path([(pos_trans, long.start), (pos_trans, long.end)], line_style)
    }
}
impl AxisOrientation for Vertical {}
impl AxisDirection for Vertical {
    const DIRECTION: f64 = -1.;
}
impl AxisIndex for Vertical {
    const INDEX: usize = 1;
}
impl AxisTransversalOrientation for Vertical {
    type Transversal = Horizontal;
}
impl DrawTransversalSegment for Vertical {
    fn draw_transversal_segment(
        drawable: &mut impl gfx::Drawable,
        pos_trans: f64,
        long: &std::ops::Range<f64>,
        line_style: &gfx::LineStyle,
    ) {
        drawable.stroke_path([(long.start, pos_trans), (long.end, pos_trans)], line_style)
    }
}

fn tuple_max((x1, y1): (f64, f64), (x2, y2): (f64, f64)) -> (f64, f64) {
    (f64::max(x1, x2), f64::max(y1, y2))
}

/// Region in chart space
#[derive(Debug)]
pub struct Region {
    /// Left border
    pub x: f64,
    /// Top border
    pub y: f64,
    /// Width
    pub w: f64,
    /// Height
    pub h: f64,
}

impl From<&Region> for (std::ops::Range<f64>, std::ops::Range<f64>) {
    fn from(value: &Region) -> Self {
        (value.x..value.x + value.w, value.y + value.h..value.y)
    }
}

#[cfg(test)]
mod test {
    use crate::gfx::testing::{self, Matcher};

    use super::*;
    use numeric::{assert_almost_eq, assert_vec_almost_eq};

    #[test]
    fn test_tick_distance_from_length_works_for_1() {
        let unit = tick_distance_from_length(1.0, 0);
        assert_eq!(
            unit,
            Decimal {
                mantissa: 1,
                exp: -1
            }
        );
    }
    #[test]
    fn test_tick_distance_from_length_works_for_negative_values() {
        let unit = tick_distance_from_length(-238.0, 0);
        assert_eq!(
            unit,
            Decimal {
                mantissa: 5,
                exp: 1
            }
        );
    }
    #[test]
    fn test_tick_distance_from_length_works_for_small_numbers() {
        let unit = tick_distance_from_length(0.9e-10, 0);
        assert_eq!(
            unit,
            Decimal {
                mantissa: 1,
                exp: -11
            }
        );
    }
    #[test]
    fn test_tick_distance_from_length_works_for_boarder_case() {
        let unit = tick_distance_from_length(1.1, 0);
        assert_eq!(
            unit,
            Decimal {
                mantissa: 2,
                exp: -1
            }
        );
    }

    #[test]
    fn test_tick_distance_from_length_works_for_decimal_1() {
        let unit = tick_distance_from_length(
            &Decimal {
                mantissa: 1,
                exp: 0,
            },
            0,
        );
        assert_eq!(
            unit,
            Decimal {
                mantissa: 1,
                exp: -1
            }
        );
    }
    #[test]
    fn test_tick_distance_from_length_works_for_decimal_negative_values() {
        let unit = tick_distance_from_length(
            &Decimal {
                mantissa: -238,
                exp: 0,
            },
            0,
        );
        assert_eq!(
            unit,
            Decimal {
                mantissa: 5,
                exp: 1
            }
        );
    }
    #[test]
    fn test_tick_distance_from_length_works_for_decimal_small_numbers() {
        let unit = tick_distance_from_length(
            &Decimal {
                mantissa: 9,
                exp: -11,
            },
            0,
        );
        assert_eq!(
            unit,
            Decimal {
                mantissa: 1,
                exp: -11
            }
        );
    }
    #[test]
    fn test_tick_distance_from_length_works_for_decimal_boarder_case() {
        let unit = tick_distance_from_length(
            &Decimal {
                mantissa: 11,
                exp: -1,
            },
            0,
        );
        assert_eq!(
            unit,
            Decimal {
                mantissa: 2,
                exp: -1
            }
        );
    }

    #[test]
    fn test_ticks_iter_works() {
        let ticks = Ticks {
            min: 1.into(),
            step: Decimal::new(1, -1),
            n_ticks: 5,
            scale: 1.,
            scale_symbol: None,
        };
        let dec_values = ticks.iter().collect::<Vec<_>>();
        assert_eq!(
            &dec_values,
            &[
                1.into(),
                Decimal::new(11, -1),
                Decimal::new(12, -1),
                Decimal::new(13, -1),
                Decimal::new(14, -1)
            ]
        );
        let values = ticks.values().collect::<Vec<_>>();
        let expected_values = dec_values.iter().map(|d| f64::from(d)).collect::<Vec<_>>();
        assert_vec_almost_eq!(&values, &expected_values);
    }
    #[test]
    fn test_ticks_iter_works_for_large_step() {
        let ticks = Ticks {
            min: 1.into(),
            step: 10.into(),
            n_ticks: 3,
            scale: 1.,
            scale_symbol: None,
        };
        let dec_values = ticks.iter().collect::<Vec<_>>();
        assert_eq!(
            &dec_values,
            &[1.into(), Decimal::new(11, 0), Decimal::new(21, 0),]
        );
        let values = ticks.values().collect::<Vec<_>>();
        let expected_values = dec_values.iter().map(|d| f64::from(d)).collect::<Vec<_>>();
        assert_vec_almost_eq!(&values, &expected_values);
    }

    #[test]
    fn test_axis_new_from_interval_works() {
        let interval = Interval {
            min: 0.01,
            max: 1.25,
        };
        // l = 1.24 => log10(l/10) ~ -0.9
        let axis = Axis::new_from_interval(interval.clone());
        assert_eq!(axis.interval, interval);
        assert_eq!(
            axis.ticks,
            Ticks {
                min: Decimal {
                    mantissa: 2,
                    exp: -1
                },
                step: Decimal {
                    mantissa: 2,
                    exp: -1
                },
                n_ticks: 6,
                scale: 1.0,
                scale_symbol: None
            }
        );
    }
    #[test]
    fn test_axis_new_from_ticks_works() {
        let interval = Interval {
            min: 0.01,
            max: 1.25,
        };
        let ticks = Ticks::new_covering_interval(&interval);
        // l = 1.24 => log10(l/10) ~ -0.9
        let axis = Axis::new_from_ticks(ticks.clone());
        assert_almost_eq!(axis.interval.min, 0.);
        assert_almost_eq!(axis.interval.max, 1.4);
        assert_eq!(
            axis.ticks,
            Ticks {
                min: Decimal {
                    mantissa: 0,
                    exp: 0
                },
                step: Decimal {
                    mantissa: 2,
                    exp: -1
                },
                n_ticks: 8,
                scale: 1.0,
                scale_symbol: None
            }
        );
    }
    #[test]
    fn test_axis_new_from_radian_works_for_0_offset() {
        let axis = Axis::new_from_radian(Decimal {
            mantissa: 0,
            exp: 0,
        });
        assert_almost_eq!(axis.interval.min, 0.);
        assert_almost_eq!(axis.interval.max, 2. * std::f64::consts::PI);
        assert_eq!(axis.ticks, Ticks::RADIAN_TICKS);
    }
    #[test]
    fn test_axis_new_from_radian_works_for_neg_pi_offset() {
        let axis = Axis::new_from_radian(Decimal {
            mantissa: -1,
            exp: 0,
        });
        assert_almost_eq!(axis.interval.min, -std::f64::consts::PI);
        assert_almost_eq!(axis.interval.max, std::f64::consts::PI);
        assert_eq!(
            axis.ticks,
            Ticks {
                min: Decimal {
                    mantissa: -1,
                    exp: 0
                },
                ..Ticks::RADIAN_TICKS
            }
        );
    }
    #[test]
    fn test_axis_new_from_degree_works_for_0_offset() {
        let axis = Axis::new_from_degree(Decimal {
            mantissa: 0,
            exp: 0,
        });
        assert_almost_eq!(axis.interval.min, 0.);
        assert_almost_eq!(axis.interval.max, 360.);
        assert_eq!(axis.ticks, Ticks::DEGREE_TICKS);
    }
    #[test]
    fn test_axis_new_from_degree_works_for_neg_180_offset() {
        let axis = Axis::new_from_degree(Decimal {
            mantissa: -180,
            exp: 0,
        });
        assert_almost_eq!(axis.interval.min, -180.);
        assert_almost_eq!(axis.interval.max, 180.);
        assert_eq!(
            axis.ticks,
            Ticks {
                min: Decimal {
                    mantissa: -180,
                    exp: 0
                },
                ..Ticks::DEGREE_TICKS
            }
        );
    }

    #[test]
    fn test_interval_expand_does_not_change_interval_for_nan() {
        let interval = Interval { min: 0., max: 1. };
        let expanded_interval = interval.clone().expand(f64::NAN);
        assert_eq!(expanded_interval, interval);
    }
    #[test]
    fn test_interval_expand_changes_nan_to_finite() {
        let interval = Interval {
            min: f64::NAN,
            max: 1.,
        };
        let expanded_interval = interval.expand(0.5);
        assert_eq!(expanded_interval, Interval { min: 0.5, max: 1. });
        let interval = Interval {
            min: 0.5,
            max: f64::NAN,
        };
        let expanded_interval = interval.expand(1.0);
        assert_eq!(expanded_interval, Interval { min: 0.5, max: 1. });
        let interval = Interval {
            min: f64::NAN,
            max: f64::NAN,
        };
        let expanded_interval = interval.expand(1.0);
        assert_eq!(expanded_interval, Interval { min: 1., max: 1. });
    }
    #[test]
    fn test_interval_expand_does_not_change_interval_for_interior_point() {
        let interval = Interval { min: 0., max: 1. };
        let expanded_interval = interval.clone().expand(0.5);
        assert_eq!(expanded_interval, interval);
    }
    #[test]
    fn test_interval_expand_expands_interval_for_lower_point() {
        let interval = Interval { min: 0., max: 1. };
        let expanded_interval = interval.expand(-1.);
        assert_eq!(expanded_interval, Interval { min: -1., max: 1. });
    }
    #[test]
    fn test_interval_expand_expands_interval_for_upper_point() {
        let interval = Interval { min: 0., max: 1. };
        let expanded_interval = interval.expand(2.);
        assert_eq!(expanded_interval, Interval { min: 0., max: 2. });
    }

    #[test]
    fn test_xy_interval_expand_does_not_change_interval_for_nan() {
        let interval = Interval { min: 0., max: 1. };
        let xy_interval = XyInterval {
            x: interval.clone(),
            y: interval,
        };
        let expanded_interval = xy_interval.clone().expand([f64::NAN, f64::NAN]);
        assert_eq!(expanded_interval, xy_interval);
        let expanded_interval = xy_interval.clone().expand([f64::NAN, 10.]);
        let expected_interval = XyInterval {
            x: Interval { min: 0., max: 1. },
            y: Interval { min: 0., max: 10. },
        };
        assert_eq!(expanded_interval, expected_interval);
        let expanded_interval = xy_interval.clone().expand([10., f64::NAN]);
        let expected_interval = XyInterval {
            x: Interval { min: 0., max: 10. },
            y: Interval { min: 0., max: 1. },
        };
        assert_eq!(expanded_interval, expected_interval);
    }
    #[test]
    fn test_xy_interval_expand_does_not_change_interval_for_interior_point() {
        let interval = Interval { min: 0., max: 1. };
        let xy_interval = XyInterval {
            x: interval.clone(),
            y: interval,
        };
        let expanded_interval = xy_interval.clone().expand([0.5, 0.5]);
        assert_eq!(expanded_interval, xy_interval);
    }
    #[test]
    fn test_xy_interval_expand_expands_interval_for_exterior_points() {
        let interval = Interval { min: 0., max: 1. };
        let xy_interval = XyInterval {
            x: interval.clone(),
            y: interval.clone(),
        };
        let expanded_interval = xy_interval.clone().expand([0.5, 2.0]);
        assert_eq!(
            expanded_interval,
            XyInterval {
                x: interval.clone(),
                y: Interval { min: 0., max: 2. }
            }
        );
        let expanded_interval = xy_interval.clone().expand([-1.0, 2.0]);
        assert_eq!(
            expanded_interval,
            XyInterval {
                x: Interval { min: -1., max: 1. },
                y: Interval { min: 0., max: 2. }
            }
        );
        let expanded_interval = xy_interval.clone().expand([-1.0, 0.5]);
        assert_eq!(
            expanded_interval,
            XyInterval {
                x: Interval { min: -1., max: 1.0 },
                y: interval.clone()
            }
        );
        let expanded_interval = xy_interval.clone().expand([-1.0, -1.0]);
        assert_eq!(
            expanded_interval,
            XyInterval {
                x: Interval { min: -1., max: 1.0 },
                y: Interval { min: -1., max: 1.0 }
            }
        );
        let expanded_interval = xy_interval.clone().expand([0.5, -1.0]);
        assert_eq!(
            expanded_interval,
            XyInterval {
                x: interval.clone(),
                y: Interval { min: -1., max: 1.0 }
            }
        );
        let expanded_interval = xy_interval.clone().expand([2.0, -1.0]);
        assert_eq!(
            expanded_interval,
            XyInterval {
                x: Interval { min: 0.0, max: 2.0 },
                y: Interval { min: -1., max: 1.0 }
            }
        );
        let expanded_interval = xy_interval.clone().expand([2.0, 0.5]);
        assert_eq!(
            expanded_interval,
            XyInterval {
                x: Interval { min: 0.0, max: 2.0 },
                y: interval
            }
        );
        let expanded_interval = xy_interval.expand([2.0, 2.0]);
        assert_eq!(
            expanded_interval,
            XyInterval {
                x: Interval { min: 0.0, max: 2.0 },
                y: Interval { min: 0.0, max: 2.0 }
            }
        );
    }
    #[test]
    fn test_xy_interval_expand_changes_nan_to_finite() {
        let double_nan_interval = Interval {
            min: f64::NAN,
            max: f64::NAN,
        };
        let l_nan_interval = Interval {
            min: f64::NAN,
            max: 1.,
        };
        let r_nan_interval = Interval {
            min: f64::NAN,
            max: 1.,
        };
        let interval = Interval { min: 0., max: 1. };
        for nan_interval in [double_nan_interval, l_nan_interval, r_nan_interval] {
            let xy_interval = XyInterval {
                x: nan_interval.clone(),
                y: interval.clone(),
            };
            let expanded_interval = xy_interval.clone().expand([1.0, 1.0]);
            assert_eq!(
                expanded_interval,
                XyInterval {
                    x: Interval { min: 1., max: 1. },
                    y: interval.clone()
                }
            );
            let xy_interval = XyInterval {
                x: interval.clone(),
                y: nan_interval.clone(),
            };
            let expanded_interval = xy_interval.clone().expand([1.0, 1.0]);
            assert_eq!(
                expanded_interval,
                XyInterval {
                    x: interval.clone(),
                    y: Interval { min: 1., max: 1. }
                }
            );
        }
    }

    #[test]
    fn test_xy_interval_contains_returns_true_for_interior_point() {
        let clip_box = XyInterval {
            x: Interval { min: 0., max: 1. },
            y: Interval { min: 0., max: 1. },
        };
        assert!(clip_box.contains([0.5, 0.5]));
    }
    #[test]
    fn test_xy_interval_contains_returns_false_for_exterior_points() {
        let clip_box = XyInterval {
            x: Interval { min: 0., max: 1. },
            y: Interval { min: 0., max: 1. },
        };
        assert!(!clip_box.contains([0.5, 1.5]));
        assert!(!clip_box.contains([0.5, -0.5]));
        assert!(!clip_box.contains([-0.5, 0.5]));
        assert!(!clip_box.contains([1.5, 0.5]));
    }
    #[test]
    fn test_xy_interval_contains_returns_false_for_nan() {
        let clip_box = XyInterval {
            x: Interval { min: 0., max: 1. },
            y: Interval { min: 0., max: 1. },
        };
        assert!(!clip_box.contains([0.5, f64::NAN]));
    }

    #[test]
    fn test_xy_chart_draws_empty_chart_with_rotated_x_labels() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = XyChart::default();
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .fit_into(200., 200.);
        plot.draw_axes(&mut drawable);

        let text = |x, y, tx, size| {
            gfx::testing::UnstyledText::new(x, y, tx, size)
                .anchor(gfx::TextAnchor::End)
                .baseline(gfx::Baseline::Middle)
        };
        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![
                gfx::Rectangle {
                    x: 40.0,
                    y: 15.0,
                    width: 145.0,
                    height: 145.0,
                }
                .into(),
                [(54.5, 160.0), (54.5, 15.0)].into(),
                [(69.0, 160.0), (69.0, 15.0)].into(),
                [(83.5, 160.0), (83.5, 15.0)].into(),
                [(98.0, 160.0), (98.0, 15.0)].into(),
                [(112.5, 160.0), (112.5, 15.0)].into(),
                [(127.0, 160.0), (127.0, 15.0)].into(),
                [(141.5, 160.0), (141.5, 15.0)].into(),
                [(156.0, 160.0), (156.0, 15.0)].into(),
                [(170.5, 160.0), (170.5, 15.0)].into(),
                [(40.0, 145.5), (185.0, 145.5)].into(),
                [(40.0, 131.0), (185.0, 131.0)].into(),
                [(40.0, 116.5), (185.0, 116.5)].into(),
                [(40.0, 102.0), (185.0, 102.0)].into(),
                [(40.0, 87.5), (185.0, 87.5)].into(),
                [(40.0, 73.0), (185.0, 73.0)].into(),
                [(40.0, 58.5), (185.0, 58.5)].into(),
                [(40.0, 44.0), (185.0, 44.0)].into(),
                [(40.0, 29.5), (185.0, 29.5)].into(),
                [(40.0, 155.0), (40.0, 165.0)].into(),
                text(40.0, 170.0, "0", 10.0).rot90().into(),
                [(54.5, 155.0), (54.5, 165.0)].into(),
                text(54.5, 170.0, "0.1", 10.0).rot90().into(),
                [(69.0, 155.0), (69.0, 165.0)].into(),
                text(69.0, 170.0, "0.2", 10.0).rot90().into(),
                [(83.5, 155.0), (83.5, 165.0)].into(),
                text(83.5, 170.0, "0.3", 10.0).rot90().into(),
                [(98.0, 155.0), (98.0, 165.0)].into(),
                text(98.0, 170.0, "0.4", 10.0).rot90().into(),
                [(112.5, 155.0), (112.5, 165.0)].into(),
                text(112.5, 170.0, "0.5", 10.0).rot90().into(),
                [(127.0, 155.0), (127.0, 165.0)].into(),
                text(127.0, 170.0, "0.6", 10.0).rot90().into(),
                [(141.5, 155.0), (141.5, 165.0)].into(),
                text(141.5, 170.0, "0.7", 10.0).rot90().into(),
                [(156.0, 155.0), (156.0, 165.0)].into(),
                text(156.0, 170.0, "0.8", 10.0).rot90().into(),
                [(170.5, 155.0), (170.5, 165.0)].into(),
                text(170.5, 170.0, "0.9", 10.0).rot90().into(),
                [(185.0, 155.0), (185.0, 165.0)].into(),
                text(185.0, 170.0, "1", 10.0).rot90().into(),
                [(35.0, 160.0), (45.0, 160.0)].into(),
                text(30.0, 160.0, "0", 10.0).into(),
                [(35.0, 145.5), (45.0, 145.5)].into(),
                text(30.0, 145.5, "0.1", 10.0).into(),
                [(35.0, 131.0), (45.0, 131.0)].into(),
                text(30.0, 131.0, "0.2", 10.0).into(),
                [(35.0, 116.5), (45.0, 116.5)].into(),
                text(30.0, 116.5, "0.3", 10.0).into(),
                [(35.0, 102.0), (45.0, 102.0)].into(),
                text(30.0, 102.0, "0.4", 10.0).into(),
                [(35.0, 87.5), (45.0, 87.5)].into(),
                text(30.0, 87.5, "0.5", 10.0).into(),
                [(35.0, 72.99999999999999), (45.0, 72.99999999999999)].into(),
                text(30.0, 72.99999999999999, "0.6", 10.0).into(),
                [(35.0, 58.499999999999986), (45.0, 58.499999999999986)].into(),
                text(30.0, 58.499999999999986, "0.7", 10.0).into(),
                [(35.0, 44.0), (45.0, 44.0)].into(),
                text(30.0, 44.0, "0.8", 10.0).into(),
                [(35.0, 29.5), (45.0, 29.5)].into(),
                text(30.0, 29.5, "0.9", 10.0).into(),
                [(35.0, 15.0), (45.0, 15.0)].into(),
                text(30.0, 15.0, "1", 10.0).into(),
            ],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }

    #[test]
    fn test_xy_chart_draws_empty_chart_with_horizontal_x_labels() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = XyChart::default();
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .fit_into(390., 200.);
        plot.draw_axes(&mut drawable);

        let text_y = |x, y, tx, size| {
            gfx::testing::UnstyledText::new(x, y, tx, size)
                .anchor(gfx::TextAnchor::End)
                .baseline(gfx::Baseline::Middle)
        };
        let text_x = |x, y, tx, size| {
            gfx::testing::UnstyledText::new(x, y, tx, size)
                .anchor(gfx::TextAnchor::Middle)
                .baseline(gfx::Baseline::Hanging)
        };
        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![
                gfx::Rectangle {
                    x: 40.0,
                    y: 15.0,
                    width: 332.5,
                    height: 150.0,
                }
                .into(),
                [(73.25, 165.0), (73.25, 15.0)].into(),
                [(106.5, 165.0), (106.5, 15.0)].into(),
                [(139.75, 165.0), (139.75, 15.0)].into(),
                [(173.0, 165.0), (173.0, 15.0)].into(),
                [(206.25, 165.0), (206.25, 15.0)].into(),
                [(239.5, 165.0), (239.5, 15.0)].into(),
                [(272.75, 165.0), (272.75, 15.0)].into(),
                [(306.0, 165.0), (306.0, 15.0)].into(),
                [(339.25, 165.0), (339.25, 15.0)].into(),
                [(40.0, 150.0), (372.5, 150.0)].into(),
                [(40.0, 135.0), (372.5, 135.0)].into(),
                [(40.0, 120.0), (372.5, 120.0)].into(),
                [(40.0, 105.0), (372.5, 105.0)].into(),
                [(40.0, 90.0), (372.5, 90.0)].into(),
                [(40.0, 75.0), (372.5, 75.0)].into(),
                [(40.0, 60.0), (372.5, 60.0)].into(),
                [(40.0, 45.0), (372.5, 45.0)].into(),
                [(40.0, 30.0), (372.5, 30.0)].into(),
                [(40.0, 160.0), (40.0, 170.0)].into(),
                text_x(40.0, 175.0, "0", 10.0).into(),
                [(73.25, 160.0), (73.25, 170.0)].into(),
                text_x(73.25, 175.0, "0.1", 10.0).into(),
                [(106.5, 160.0), (106.5, 170.0)].into(),
                text_x(106.5, 175.0, "0.2", 10.0).into(),
                [(139.75, 160.0), (139.75, 170.0)].into(),
                text_x(139.75, 175.0, "0.3", 10.0).into(),
                [(173.0, 160.0), (173.0, 170.0)].into(),
                text_x(173.0, 175.0, "0.4", 10.0).into(),
                [(206.25, 160.0), (206.25, 170.0)].into(),
                text_x(206.25, 175.0, "0.5", 10.0).into(),
                [(239.5, 160.0), (239.5, 170.0)].into(),
                text_x(239.5, 175.0, "0.6", 10.0).into(),
                [(272.75, 160.0), (272.75, 170.0)].into(),
                text_x(272.75, 175.0, "0.7", 10.0).into(),
                [(306.0, 160.0), (306.0, 170.0)].into(),
                text_x(306.0, 175.0, "0.8", 10.0).into(),
                [(339.25, 160.0), (339.25, 170.0)].into(),
                text_x(339.25, 175.0, "0.9", 10.0).into(),
                [(372.5, 160.0), (372.5, 170.0)].into(),
                text_x(372.5, 175.0, "1", 10.0).into(),
                [(35.0, 165.0), (45.0, 165.0)].into(),
                text_y(30.0, 165.0, "0", 10.0).into(),
                [(35.0, 150.0), (45.0, 150.0)].into(),
                text_y(30.0, 150.0, "0.1", 10.0).into(),
                [(35.0, 135.0), (45.0, 135.0)].into(),
                text_y(30.0, 135.0, "0.2", 10.0).into(),
                [(35.0, 120.0), (45.0, 120.0)].into(),
                text_y(30.0, 120.0, "0.3", 10.0).into(),
                [(35.0, 105.0), (45.0, 105.0)].into(),
                text_y(30.0, 105.0, "0.4", 10.0).into(),
                [(35.0, 90.0), (45.0, 90.0)].into(),
                text_y(30.0, 90.0, "0.5", 10.0).into(),
                [(35.0, 75.0), (45.0, 75.0)].into(),
                text_y(30.0, 75.0, "0.6", 10.0).into(),
                [(35.0, 60.0), (45.0, 60.0)].into(),
                text_y(30.0, 60.0, "0.7", 10.0).into(),
                [(35.0, 45.0), (45.0, 45.0)].into(),
                text_y(30.0, 45.0, "0.8", 10.0).into(),
                [(35.0, 30.0), (45.0, 30.0)].into(),
                text_y(30.0, 30.0, "0.9", 10.0).into(),
                [(35.0, 15.0), (45.0, 15.0)].into(),
                text_y(30.0, 15.0, "1", 10.0).into(),
            ],
        };

        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    use crate::xy_chart::testing::{empty_axis, empty_chart, empty_chart_from_ranges};
    #[test]
    fn test_xy_chart_draws_empty_chart_without_grid_and_ticks() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = empty_chart();
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .fit_into(400., 200.);
        plot.draw_axes(&mut drawable);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![gfx::Rectangle {
                x: 10.0,
                y: 10.0,
                width: 380.,
                height: 180.,
            }
            .into()],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    #[test]
    fn test_xy_chart_draws_empty_chart_from_axes_extents_without_grid_and_ticks() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = empty_chart();
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .with_axes_extents(200., 200.);
        plot.draw_axes(&mut drawable);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![gfx::Rectangle {
                x: 10.0,
                y: 10.0,
                width: 200.,
                height: 200.,
            }
            .into()],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    #[test]
    fn test_xy_chart_draws_empty_tickless_gridless_chart_with_x_axis_align() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = empty_chart();
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .align_data_region_left_at(30.)
            .fit_into(400., 200.);

        plot.draw_axes(&mut drawable);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![gfx::Rectangle {
                x: 30.0,
                y: 10.0,
                width: 360.,
                height: 180.,
            }
            .into()],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    #[test]
    fn test_xy_chart_draws_empty_tickless_gridless_chart_with_y_axis_align() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = empty_chart();
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .align_data_region_top_at(30.)
            .fit_into(400., 200.);

        plot.draw_axes(&mut drawable);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![gfx::Rectangle {
                x: 10.0,
                y: 30.0,
                width: 380.,
                height: 160.,
            }
            .into()],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    #[test]
    fn test_xy_chart_draws_empty_tickless_gridless_chart_with_square_aspect_ratio() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = empty_chart_from_ranges(0.0..1.0, 0.0..2.0);
        let plot = chart
            .prepare_layout(AspectRatio::Square, &drawable)
            .fit_into(400., 200.);

        plot.draw_axes(&mut drawable);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![gfx::Rectangle {
                x: 10.0,
                y: 10.0,
                width: 180.,
                height: 180.,
            }
            .into()],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    #[test]
    fn test_xy_chart_draws_empty_tickless_gridless_chart_with_offset() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = empty_chart_from_ranges(0.0..1.0, 0.0..2.0);
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .fit_into(400., 200.)
            .with_offset(10., 20.);

        plot.draw_axes(&mut drawable);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![gfx::Rectangle {
                x: 20.0,
                y: 30.0,
                width: 380.,
                height: 180.,
            }
            .into()],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    #[test]
    fn test_xy_chart_draws_empty_chart_with_caption_without_grid_and_ticks() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = XyChart {
            title: Some("Chart".to_string()),
            ..empty_chart()
        };
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .fit_into(400., 200.);
        plot.draw_axes(&mut drawable);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![
                gfx::Rectangle {
                    x: 10.0,
                    y: 30.0,
                    width: 380.,
                    height: 160.,
                }
                .into(),
                gfx::testing::UnstyledText::new(200.0, 5.0, "Chart", 10.0)
                    .anchor(gfx::TextAnchor::Middle)
                    .baseline(gfx::Baseline::Hanging)
                    .into(),
            ],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    #[test]
    fn test_xy_chart_draws_empty_chart_with_captions_without_grid_and_ticks() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let axis = |label: &str| Axis {
            label: label.into(),
            ..empty_axis()
        };
        let chart = XyChart {
            title: Some("Chart".to_string()),
            grid_line_style: None,
            x_axis: axis("x axis"),
            y_axis: axis("y axis"),
            ..Default::default()
        };
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .fit_into(400., 200.);
        plot.draw_axes(&mut drawable);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![
                gfx::Rectangle {
                    x: 30.0,
                    y: 30.0,
                    width: 360.,
                    height: 140.,
                }
                .into(),
                gfx::testing::UnstyledText::new(210.0, 5.0, "Chart", 10.0)
                    .anchor(gfx::TextAnchor::Middle)
                    .baseline(gfx::Baseline::Hanging)
                    .into(),
                gfx::testing::UnstyledText::new(210.0, 185.0, "x axis", 10.0)
                    .anchor(gfx::TextAnchor::Middle)
                    .baseline(gfx::Baseline::Alphabetic)
                    .into(),
                gfx::testing::UnstyledText::new(15.0, 100.0, "y axis", 10.0)
                    .anchor(gfx::TextAnchor::Middle)
                    .baseline(gfx::Baseline::Hanging)
                    .rot90()
                    .into(),
            ],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    #[test]
    fn test_xy_chart_draws_empty_chart_with_captions_without_grid() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let axis = |label: &str| Axis {
            label: label.into(),
            ..Axis::new_from_interval_and_ticks(
                Interval { min: 0., max: 1. },
                Ticks {
                    min: 0.into(),
                    step: 1.into(),
                    n_ticks: 2,
                    scale: 1.,
                    scale_symbol: None,
                },
            )
        };
        let chart = XyChart {
            title: Some("Chart".to_string()),
            grid_line_style: None,
            x_axis: axis("x axis"),
            y_axis: axis("y axis"),
            ..Default::default()
        };
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .fit_into(400., 200.);
        plot.draw_axes(&mut drawable);
        let text = |x, y, tx, size| {
            gfx::testing::UnstyledText::new(x, y, tx, size)
                .anchor(gfx::TextAnchor::Middle)
                .baseline(gfx::Baseline::Hanging)
        };

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![
                gfx::Rectangle {
                    x: 50.0,
                    y: 35.0,
                    width: 337.5,
                    height: 110.,
                }
                .into(),
                text(218.75, 5.0, "Chart", 10.0).into(),
                [(50.0, 140.0), (50.0, 150.0)].into(),
                text(50.0, 155.0, "0", 10.0).into(),
                [(387.5, 140.0), (387.5, 150.0)].into(),
                text(387.5, 155.0, "1", 10.0).into(),
                [(45.0, 145.0), (55.0, 145.0)].into(),
                text(40.0, 145.0, "0", 10.0)
                    .baseline(gfx::Baseline::Middle)
                    .anchor(gfx::TextAnchor::End)
                    .into(),
                [(45.0, 35.0), (55.0, 35.0)].into(),
                text(40.0, 35.0, "1", 10.0)
                    .baseline(gfx::Baseline::Middle)
                    .anchor(gfx::TextAnchor::End)
                    .into(),
                text(218.75, 185.0, "x axis", 10.0)
                    .baseline(gfx::Baseline::Alphabetic)
                    .into(),
                text(15.0, 90.0, "y axis", 10.0).rot90().into(),
            ],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    #[test]
    fn test_xy_chart_draws_empty_chart_with_subgrid() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let no_font = gfx::Font {
            size: 0.,
            ..Default::default()
        };
        let tick_step = Decimal {
            mantissa: 5,
            exp: -1,
        };
        let axis = Axis {
            tick_label_font: no_font.clone(),
            sub_ticks: Ticks::new_subticks(&tick_step),
            ticks: Ticks {
                min: Decimal {
                    mantissa: 0,
                    exp: 0,
                },
                step: tick_step,
                n_ticks: 3,
                scale: 1.0,
                scale_symbol: None,
            },
            ..Default::default()
        };
        let chart = XyChart {
            sub_grid_line_style: Some(Default::default()),
            x_axis: axis.clone(),
            y_axis: axis,
            ..XyChart::default()
        };
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .fit_into(400., 200.);
        plot.draw_axes(&mut drawable);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![
                gfx::Rectangle {
                    x: 10.0,
                    y: 10.0,
                    width: 380.0,
                    height: 180.0,
                }
                .into(),
                [(29.0, 10.0), (29.0, 190.0)].into(),
                [(48.0, 10.0), (48.0, 190.0)].into(),
                [(67.0, 10.0), (67.0, 190.0)].into(),
                [(86.0, 10.0), (86.0, 190.0)].into(),
                [(105.0, 10.0), (105.0, 190.0)].into(),
                [(124.0, 10.0), (124.0, 190.0)].into(),
                [(143.0, 10.0), (143.0, 190.0)].into(),
                [(162.0, 10.0), (162.0, 190.0)].into(),
                [(181.0, 10.0), (181.0, 190.0)].into(),
                [(219.0, 10.0), (219.0, 190.0)].into(),
                [(238.0, 10.0), (238.0, 190.0)].into(),
                [(257.0, 10.0), (257.0, 190.0)].into(),
                [(276.0, 10.0), (276.0, 190.0)].into(),
                [(295.0, 10.0), (295.0, 190.0)].into(),
                [(314.0, 10.0), (314.0, 190.0)].into(),
                [(333.0, 10.0), (333.0, 190.0)].into(),
                [(352.0, 10.0), (352.0, 190.0)].into(),
                [(371.0, 10.0), (371.0, 190.0)].into(),
                [(10.0, 181.0), (390.0, 181.0)].into(),
                [(10.0, 172.0), (390.0, 172.0)].into(),
                [(10.0, 163.0), (390.0, 163.0)].into(),
                [(10.0, 154.0), (390.0, 154.0)].into(),
                [(10.0, 145.0), (390.0, 145.0)].into(),
                [(10.0, 136.0), (390.0, 136.0)].into(),
                [(10.0, 127.0), (390.0, 127.0)].into(),
                [(10.0, 118.0), (390.0, 118.0)].into(),
                [(10.0, 109.0), (390.0, 109.0)].into(),
                [(10.0, 91.0), (390.0, 91.0)].into(),
                [(10.0, 82.0), (390.0, 82.0)].into(),
                [(10.0, 73.0), (390.0, 73.0)].into(),
                [(10.0, 64.0), (390.0, 64.0)].into(),
                [(10.0, 55.0), (390.0, 55.0)].into(),
                [(10.0, 46.0), (390.0, 46.0)].into(),
                [(10.0, 37.0), (390.0, 37.0)].into(),
                [(10.0, 28.0), (390.0, 28.0)].into(),
                [(10.0, 19.0), (390.0, 19.0)].into(),
                [(200.0, 190.0), (200.0, 10.0)].into(),
                [(10.0, 100.0), (390.0, 100.0)].into(),
                [(10.0, 185.0), (10.0, 195.0)].into(),
                [(200.0, 185.0), (200.0, 195.0)].into(),
                [(390.0, 185.0), (390.0, 195.0)].into(),
                [(5.0, 190.0), (15.0, 190.0)].into(),
                [(5.0, 100.0), (15.0, 100.0)].into(),
                [(5.0, 10.0), (15.0, 10.0)].into(),
            ],
        };

        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    #[test]
    fn test_xy_chart_draws_empty_chart_with_grid_below_y_axis() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let x_interval = Interval {
            min: -1.0,
            max: 1.0,
        };
        let y_interval = Interval {
            min: -3.0,
            max: -1.0,
        };
        let no_font = gfx::Font {
            size: 0.,
            ..Default::default()
        };
        let chart = XyChart {
            x_axis: Axis {
                tick_label_font: no_font.clone(),
                ..Axis::new_from_interval(x_interval)
            },
            y_axis: Axis {
                tick_label_font: no_font.clone(),
                ..Axis::new_from_interval(y_interval)
            },
            sub_grid_line_style: None,
            ..XyChart::default()
        };
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .with_axes_extents(1000., 1000.);
        plot.draw_axes(&mut drawable);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![
                gfx::Rectangle {
                    x: 10.0,
                    y: 10.0,
                    width: 1000.0,
                    height: 1000.0,
                }
                .into(),
                [(110.0, 1010.0), (110.0, 10.0)].into(),
                [(210.0, 1010.0), (210.0, 10.0)].into(),
                [(310.0, 1010.0), (310.0, 10.0)].into(),
                [(410.0, 1010.0), (410.0, 10.0)].into(),
                [(510.0, 1010.0), (510.0, 10.0)].into(),
                [(610.0, 1010.0), (610.0, 10.0)].into(),
                [(710.0, 1010.0), (710.0, 10.0)].into(),
                [(810.0, 1010.0), (810.0, 10.0)].into(),
                [(910.0, 1010.0), (910.0, 10.0)].into(),
                [(10.0, 910.0), (1010.0, 910.0)].into(),
                [(10.0, 810.0), (1010.0, 810.0)].into(),
                [(10.0, 710.0), (1010.0, 710.0)].into(),
                [(10.0, 610.0), (1010.0, 610.0)].into(),
                [(10.0, 510.0), (1010.0, 510.0)].into(),
                [(10.0, 410.0), (1010.0, 410.0)].into(),
                [(10.0, 310.0), (1010.0, 310.0)].into(),
                [(10.0, 210.0), (1010.0, 210.0)].into(),
                [(10.0, 110.0), (1010.0, 110.0)].into(),
                [(10.0, 1005.0), (10.0, 1015.0)].into(),
                [(110.0, 1005.0), (110.0, 1015.0)].into(),
                [(210.0, 1005.0), (210.0, 1015.0)].into(),
                [(310.0, 1005.0), (310.0, 1015.0)].into(),
                [(410.0, 1005.0), (410.0, 1015.0)].into(),
                [(510.0, 1005.0), (510.0, 1015.0)].into(),
                [(610.0, 1005.0), (610.0, 1015.0)].into(),
                [(710.0, 1005.0), (710.0, 1015.0)].into(),
                [(810.0, 1005.0), (810.0, 1015.0)].into(),
                [(910.0, 1005.0), (910.0, 1015.0)].into(),
                [(1010.0, 1005.0), (1010.0, 1015.0)].into(),
                [(5.0, 1010.0), (15.0, 1010.0)].into(),
                [(5.0, 910.0), (15.0, 910.0)].into(),
                [(5.0, 810.0), (15.0, 810.0)].into(),
                [(5.0, 710.0), (15.0, 710.0)].into(),
                [(5.0, 610.0), (15.0, 610.0)].into(),
                [(5.0, 510.0), (15.0, 510.0)].into(),
                [(5.0, 410.0), (15.0, 410.0)].into(),
                [(5.0, 310.0), (15.0, 310.0)].into(),
                [(5.0, 210.0), (15.0, 210.0)].into(),
                [(5.0, 110.0), (15.0, 110.0)].into(),
                [(5.0, 10.0), (15.0, 10.0)].into(),
                [(510.0, 1010.0), (510.0, 10.0)].into(),
            ],
        };

        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
    #[test]
    fn test_xy_chart_draws_empty_chart_with_grid_above_y_axis() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let x_interval = Interval {
            min: -50.0,
            max: 50.0,
        };
        let y_interval = Interval {
            min: 44.0,
            max: 80.0,
        };
        let no_font = gfx::Font {
            size: 0.,
            ..Default::default()
        };
        let chart = XyChart {
            x_axis: Axis {
                tick_label_font: no_font.clone(),
                ..Axis::new_from_interval(x_interval)
            },
            y_axis: Axis {
                tick_label_font: no_font.clone(),
                ..Axis::new_from_interval(y_interval)
            },
            sub_grid_line_style: None,
            ..XyChart::default()
        };
        let plot = chart
            .prepare_layout(AspectRatio::Unconstrained, &drawable)
            .with_axes_extents(1000., 1000.);
        plot.draw_axes(&mut drawable);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![
                gfx::Rectangle {
                    x: 10.0,
                    y: 10.0,
                    width: 1000.0,
                    height: 1000.0,
                }
                .into(),
                [(110.0, 1010.0), (110.0, 10.0)].into(),
                [(210.0, 1010.0), (210.0, 10.0)].into(),
                [(310.0, 1010.0), (310.0, 10.0)].into(),
                [(410.0, 1010.0), (410.0, 10.0)].into(),
                [(510.0, 1010.0), (510.0, 10.0)].into(),
                [(610.0, 1010.0), (610.0, 10.0)].into(),
                [(710.0, 1010.0), (710.0, 10.0)].into(),
                [(810.0, 1010.0), (810.0, 10.0)].into(),
                [(910.0, 1010.0), (910.0, 10.0)].into(),
                [(10.0, 982.2222222222222), (1010.0, 982.2222222222222)].into(),
                [(10.0, 843.3333333333333), (1010.0, 843.3333333333333)].into(),
                [(10.0, 704.4444444444445), (1010.0, 704.4444444444445)].into(),
                [(10.0, 565.5555555555555), (1010.0, 565.5555555555555)].into(),
                [(10.0, 426.66666666666663), (1010.0, 426.66666666666663)].into(),
                [(10.0, 287.7777777777777), (1010.0, 287.7777777777777)].into(),
                [(10.0, 148.8888888888889), (1010.0, 148.8888888888889)].into(),
                [(10.0, 1005.0), (10.0, 1015.0)].into(),
                [(110.0, 1005.0), (110.0, 1015.0)].into(),
                [(210.0, 1005.0), (210.0, 1015.0)].into(),
                [(310.0, 1005.0), (310.0, 1015.0)].into(),
                [(410.0, 1005.0), (410.0, 1015.0)].into(),
                [(510.0, 1005.0), (510.0, 1015.0)].into(),
                [(610.0, 1005.0), (610.0, 1015.0)].into(),
                [(710.0, 1005.0), (710.0, 1015.0)].into(),
                [(810.0, 1005.0), (810.0, 1015.0)].into(),
                [(910.0, 1005.0), (910.0, 1015.0)].into(),
                [(1010.0, 1005.0), (1010.0, 1015.0)].into(),
                [(5.0, 982.2222222222222), (15.0, 982.2222222222222)].into(),
                [(5.0, 843.3333333333333), (15.0, 843.3333333333333)].into(),
                [(5.0, 704.4444444444445), (15.0, 704.4444444444445)].into(),
                [(5.0, 565.5555555555555), (15.0, 565.5555555555555)].into(),
                [(5.0, 426.66666666666663), (15.0, 426.66666666666663)].into(),
                [(5.0, 287.7777777777777), (15.0, 287.7777777777777)].into(),
                [(5.0, 148.8888888888889), (15.0, 148.8888888888889)].into(),
                [(5.0, 10.0), (15.0, 10.0)].into(),
                [(510.0, 1010.0), (510.0, 10.0)].into(),
            ],
        };

        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
}
