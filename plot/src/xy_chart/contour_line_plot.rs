//! Contour plots of a real valued function of 2 real variables (x, y) using [numeric::contour_lines::trace_contour_lines]
//!
//! Can be plotted via [crate::xy_chart::ExtrinsicChartLayout::plot] and included in a legend
//! by using [crate::xy_chart::legend::Legend::add_entries].

use crate::{
    gfx,
    xy_chart::{
        clipped_plot::ClippedAt,
        legend::{ExtendLegendEntriesWithTitle, LegendEntry},
        transform::Transformation,
        DataRegion, Interval, PlotTo,
    },
};
use numeric::{
    bezier::{self, CubicBezierPoint},
    contour_lines::{self, into_c1_point, into_c2_point, orientation, trace_contour_lines},
    curve::{C1Point, C2Point},
};

pub use numeric::contour_lines::Derivatives;

/// Level and style for a single contour line
pub struct ContourLineLevel {
    /// Level value
    pub value: f64,
    /// Line style for the contour line
    pub style: gfx::LineStyle,
}

/// Properties of a countour line plot
pub struct ContourLinePlot<F, DF> {
    /// Real valued function of 2 real variables (x, y)
    pub f: F,
    /// 1st and 2nd derivatives of `f` `df`: `Fn(f64, f64) -> Derivatives` (function returning [Derivatives])
    pub d_f: DF,
    /// Levels with line styles
    pub levels: Vec<ContourLineLevel>,
    /// Resolution paramater. Field lines with a diameter greater or equal to `resolution` will be detected (see [numeric::contour_lines::trace_contour_lines] for details)
    pub resolution: f64,
    /// Determines the accuracy of the field lines (see [numeric::contour_lines::trace_contour_lines])
    pub tolerance: f64,
}

impl<F, DF> ClippedAt for ContourLinePlot<F, DF> {}

impl<D, F, DF> PlotTo for ContourLinePlot<F, DF>
where
    F: Fn(f64, f64) -> f64,
    DF: Fn(f64, f64) -> D,
    D: contour_lines::AsGrad + Smoothen,
    D::Point: DrawBezierCurve,
{
    type ReturnType = ();
    fn plot_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        DataRegion {
            xy_interval,
            chart_to_canvas,
            ..
        }: &DataRegion,
    ) {
        let Interval {
            min: x_min,
            max: x_max,
        } = xy_interval.x;
        let Interval {
            min: y_min,
            max: y_max,
        } = xy_interval.y;
        let (levels, styles): (Vec<_>, Vec<_>) =
            self.levels.iter().map(|l| (l.value, &l.style)).unzip();
        let lines = trace_contour_lines(
            &self.f,
            (x_min, x_max),
            (y_min, y_max),
            &levels,
            self.resolution,
            self.tolerance,
        );
        for (line, style) in lines.into_iter().zip(styles.iter()) {
            for component in line {
                let sgn = orientation(&component.points, &self.d_f);
                let c2_curve = component
                    .points
                    .into_iter()
                    .map(|p| (self.d_f)(p[0], p[1]).smoothen(p, sgn));
                D::Point::draw_bezier_curve(
                    drawable,
                    chart_to_canvas,
                    c2_curve,
                    component.closed,
                    style,
                    self.tolerance,
                );
            }
        }
    }
}

trait DrawBezierCurve {
    fn into_closed_cubic_curve(
        curve: impl IntoIterator<Item = Self>,
        tolerance: f64,
    ) -> impl Iterator<Item = CubicBezierPoint>;
    fn into_cubic_curve(
        curve: impl IntoIterator<Item = Self>,
        tolerance: f64,
    ) -> impl Iterator<Item = CubicBezierPoint>;
    fn draw_bezier_curve(
        drawable: &mut impl gfx::Drawable,
        chart_to_canvas: &Transformation,
        curve: impl IntoIterator<Item = Self>,
        closed: bool,
        line_style: &gfx::LineStyle,
        tolerance: f64,
    ) {
        let bezier_to_canvas = |p: bezier::CubicBezierPoint| chart_to_canvas.apply(p);
        if closed {
            let bezier_curve = Self::into_closed_cubic_curve(curve, tolerance);
            drawable.stroke_polygon(bezier_curve.map(bezier_to_canvas), line_style);
        } else {
            let bezier_curve = Self::into_cubic_curve(curve, tolerance);
            drawable.stroke_path(bezier_curve.map(bezier_to_canvas), line_style);
        }
    }
}

/// Smoothen a polygonal chain
pub trait Smoothen {
    /// Type holding the original point and derivatives of a curve through that point
    type Point;
    /// For a given point on a curve, attach derivatives of the curve in that point
    fn smoothen(self, p: [f64; 2], sgn: f64) -> Self::Point;
}

impl Smoothen for Derivatives {
    type Point = C2Point;
    fn smoothen(self, p: [f64; 2], sgn: f64) -> Self::Point {
        into_c2_point(p, self, sgn)
    }
}

impl Smoothen for [f64; 2] {
    type Point = C1Point;
    fn smoothen(self, p: [f64; 2], sgn: f64) -> Self::Point {
        into_c1_point(p, self, sgn)
    }
}

impl DrawBezierCurve for C2Point {
    fn into_closed_cubic_curve(
        curve: impl IntoIterator<Item = Self>,
        tolerance: f64,
    ) -> impl Iterator<Item = CubicBezierPoint> {
        bezier::into_closed_cubic_bezier_curve_with_tolerance(curve, tolerance)
    }
    fn into_cubic_curve(
        curve: impl IntoIterator<Item = Self>,
        tolerance: f64,
    ) -> impl Iterator<Item = CubicBezierPoint> {
        bezier::into_cubic_bezier_curve_with_tolerance(curve, tolerance)
    }
}

impl DrawBezierCurve for C1Point {
    fn into_closed_cubic_curve(
        curve: impl IntoIterator<Item = Self>,
        _tolerance: f64,
    ) -> impl Iterator<Item = CubicBezierPoint> {
        bezier::into_closed_cubic_bezier_curve(curve)
    }
    fn into_cubic_curve(
        curve: impl IntoIterator<Item = Self>,
        _tolerance: f64,
    ) -> impl Iterator<Item = CubicBezierPoint> {
        bezier::into_cubic_bezier_curve(curve)
    }
}

impl<F, DF> ExtendLegendEntriesWithTitle for ContourLinePlot<F, DF> {
    fn extend_legend_entries_with_title<S: Into<String>>(
        &self,
        title: S,
        entries: &mut Vec<super::legend::LegendEntry>,
    ) {
        let title = title.into();
        entries.extend(self.levels.iter().map(|l| LegendEntry {
            title: format!("{}{}", title, l.value),
            line_style: Some(l.style.clone()),
            marker_style: None,
        }));
    }
}

#[cfg(test)]
mod test {
    use crate::gfx::testing::{self, Matcher};
    use crate::xy_chart::testing::empty_chart_from_ranges;

    use super::*;

    /// See https://web.archive.org/web/20130126163405/http://geomalgorithms.com/a03-_inclusion.html
    fn winding_number<I: IntoIterator<Item = (f64, f64)>>(
        points: I,
        (p_x, p_y): (f64, f64),
    ) -> isize {
        let mut points = points.into_iter();
        let Some(mut q) = points.next() else {
            return 0;
        };
        let first = q;
        let dn = |(r_x, r_y): (f64, f64), (q_x, q_y): (f64, f64)| -> isize {
            let s = (r_x - q_x) * (q_y - p_y) - (r_y - q_y) * (q_x - p_x);
            if q_y <= p_y && r_y > p_y && s > 0. {
                1
            } else if q_y > p_y && r_y <= p_y && s < 0. {
                -1
            } else {
                0
            }
        };
        let w: isize = points
            .map(|r| {
                let n = dn(r, q);
                q = r;
                n
            })
            .sum();
        w + dn(q, first)
    }

    #[derive(Debug)]
    pub struct Ellipsis {
        pub c: (f64, f64),
        pub a: (f64, f64),
    }

    fn assert_no_direction_change(
        path: impl IntoIterator<Item = (f64, f64)>,
        (c_x, c_y): (f64, f64),
    ) {
        let mut points = path.into_iter();
        let Some((mut p_x, mut p_y)) = points.next() else {
            return;
        };
        let mut directions_with_segments = points.map(|(q_x, q_y)| {
            let out = (
                ((p_x, p_y), (q_x, q_y)),
                (p_x - c_x) * (q_y - c_y) - (p_y - c_y) * (q_x - c_x) > 0.,
            );
            (p_x, p_y) = (q_x, q_y);
            out
        });
        let Some((first_segment, first_direction)) = directions_with_segments.next() else {
            return;
        };
        for (segment, d) in directions_with_segments {
            if d != first_direction {
                panic!("Path changes direction: first_segment={first_segment:?}, current_segment={segment:?}");
            }
        }
    }

    fn assert_is_on_ellipsis(
        path: impl AsRef<[gfx::BezierPoint]>,
        ellipsis: &Ellipsis,
        tolerance: f64,
    ) {
        for p in path.as_ref().iter() {
            let Ellipsis {
                c: (c_x, c_y),
                a: (a_x, a_y),
            } = ellipsis;
            let d = ((p.p.0 - c_x) / a_x).powi(2) + ((p.p.1 - c_y) / a_y).powi(2) - 1.;
            assert!(
                d.abs() <= tolerance,
                "{p:?} is not on ellipsis {ellipsis:?}\n(x-c_x)²/a² + (y-c_y)²/b² = 1 + d, with d = {d}"
            );
        }
        assert_no_direction_change(path.as_ref().iter().map(|p| p.p), ellipsis.c);
        // (x-c_x)²/a² + (y-c_y)²/b² = 1
        // => x'(x-c_x)/a² + y'(y-c_y)/b² = 0
        // (x', y') \perp ((x-c_x)/a², (y-c_y)/b²)
        let dot = |p: (f64, f64), q: (f64, f64)| p.0 * q.0 + p.1 * q.1;
        assert!(
            path.as_ref()
                .iter()
                .all(|gfx::BezierPoint { p, q_b, q_f }| {
                    let n = (
                        (p.0 - ellipsis.c.0) / ellipsis.a.0.powi(2),
                        (p.1 - ellipsis.c.1) / ellipsis.a.1.powi(2),
                    );
                    dot((q_b.0 - p.0, q_b.1 - p.1), n).abs() < tolerance
                        && dot((q_f.0 - p.0, q_f.1 - p.1), n).abs() < tolerance
                }),
            "Bezier point is not tangent to ellipsis."
        );
    }

    #[test]
    fn test_xy_chart_draws_contour_line_plot() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = empty_chart_from_ranges(-1.5..1.5, -1.5..1.5);
        let plot = chart
            .prepare_layout(crate::xy_chart::AspectRatio::Unconstrained, &drawable)
            .fit_into(400., 200.);
        let plot = plot.draw_axes(&mut drawable);

        let levels = [1.0, 2.0];
        let contour_line_plot = ContourLinePlot {
            f: f64::hypot,
            d_f: |x, y| {
                let r = f64::hypot(x, y);
                let rrr = r * r * r;
                Derivatives {
                    grad: [x / r, y / r],
                    hess_xx: 1. / r - x * x / rrr,
                    hess_xy: -x * y / rrr,
                    hess_yy: 1. / r - y * y / rrr,
                }
            },
            levels: levels
                .iter()
                .map(|l| ContourLineLevel {
                    value: *l,
                    style: Default::default(),
                })
                .collect::<Vec<_>>(),
            resolution: 1.0,
            tolerance: 1.0E-5,
        }
        .clipped_at_region();
        plot.plot(&mut drawable, &contour_line_plot);
        let expected_rectangle = gfx::Rectangle {
            x: 10.0,
            y: 10.0,
            width: 380.,
            height: 180.,
        };
        assert_eq!(drawable.elements.len(), 4);
        testing::assert_match!(
            &drawable.elements[0],
            testing::Approx(&gfx::testing::Element::from(expected_rectangle.clone()))
        );
        testing::assert_match!(
            &drawable.elements[1],
            testing::Approx(&gfx::testing::Element::from(vec![
                (10.0, 100.0),
                (390.0, 100.0)
            ]))
        );
        testing::assert_match!(
            &drawable.elements[2],
            testing::Approx(&gfx::testing::Element::from(vec![
                (200.0, 190.0),
                (200.0, 10.0)
            ]))
        );
        let gfx::testing::Element::ClippedRegion(gfx::testing::ClippedRegion {
            polygon,
            primitives,
        }) = &drawable.elements[3]
        else {
            panic!("drawable.elements[3] is not a clip region")
        };
        testing::assert_match!(
            polygon,
            testing::Approx(&vec![(10., 10.), (390., 10.), (390., 190.), (10., 190.)])
        );
        assert_eq!(primitives.len(), 5);
        let (kx, ky) = (
            expected_rectangle.width / chart.x_axis.interval.length(),
            expected_rectangle.height / chart.y_axis.interval.length(),
        );
        let expected_center = (200., 100.);
        let gfx::testing::UnstyledPrimitiveElement::Beziergon(gfx::testing::Beziergon(path_1)) =
            &primitives[0]
        else {
            panic!("element[0] is not a beziergon");
        };
        // First path should be a full ellipsis:
        let ellipsis = Ellipsis {
            a: (kx, ky),
            c: expected_center,
        };
        assert_is_on_ellipsis(path_1, &ellipsis, contour_line_plot.plot.tolerance);
        assert_eq!(
            winding_number(path_1.iter().map(|p| p.p), expected_center).abs(),
            1,
            "Winding number of path must be +/-1"
        );

        // All remaining paths should be parts of the second ellipsis:
        let paths = primitives[1..].iter().enumerate().map(|(n, primitive)| {
            let gfx::testing::UnstyledPrimitiveElement::BezierCurve(path) = primitive else {
                panic!("element[{n}] is not a beziergon");
            };
            path
        });
        let ellipsis = Ellipsis {
            a: (2. * kx, 2. * ky),
            c: expected_center,
        };
        for path in paths {
            assert_is_on_ellipsis(path, &ellipsis, contour_line_plot.plot.tolerance);
        }
    }
}
