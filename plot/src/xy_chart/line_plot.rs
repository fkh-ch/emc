//! Plot a chain of line segments
//!
//! Can be plotted via [crate::xy_chart::ExtrinsicChartLayout::plot] and included in a legend
//! by using [crate::xy_chart::legend::Legend::add_entries].

use crate::{
    gfx,
    xy_chart::{
        clip::{ClipBox, ClipPathForEach},
        iter::AsIterator,
        legend::{ExtendLegendEntriesWithTitle, LegendEntry},
        periodicity::Periodicity,
        DataRegion, PlotTo,
    },
};

/// Properties of a line plot
///
/// If the chart the line plot will be plotted into has
/// periodic axes, then the coordinates and segments will be periodically continued.
pub struct LinePlot<C, PX: Periodicity, PY: Periodicity> {
    /// Points which will be connected by segments
    pub coordinates: C,
    /// Line style used for the segments
    pub line_style: gfx::LineStyle,
    /// Periodicity in x and y direction.
    /// `PX` and `PY` can be set to [crate::xy_chart::periodicity::Periodic] (line will be wrapped)
    /// or [crate::xy_chart::periodicity::NotPeriodic]  (line will not be wrapped)
    pub periodicity: (PX, PY),
}

impl<PX: Periodicity, PY: Periodicity, C> PlotTo for LinePlot<C, PX, PY>
where
    for<'b> &'b C: AsIterator<[f64; 2]>,
    for<'b> &'b super::clip::ClipBox<'b, PX, PY>: ClipPathForEach,
{
    type ReturnType = ();
    fn plot_to(
        &self,
        drawable: &mut impl gfx::Drawable,
        DataRegion {
            xy_interval,
            chart_to_canvas,
            ..
        }: &DataRegion,
    ) {
        let clip_box = ClipBox::<PX, PY>::from(xy_interval);
        clip_box.clip_path_for_each(self.coordinates.as_iter(), |path_component| {
            drawable.stroke_path(
                path_component.iter().map(|p| chart_to_canvas.apply(*p)),
                &self.line_style,
            );
        });
    }
}

impl<C, PX: Periodicity, PY: Periodicity> ExtendLegendEntriesWithTitle for LinePlot<C, PX, PY> {
    fn extend_legend_entries_with_title<S: Into<String>>(
        &self,
        title: S,
        entries: &mut Vec<LegendEntry>,
    ) {
        entries.push(LegendEntry {
            title: title.into(),
            line_style: Some(self.line_style.clone()),
            marker_style: None,
        });
    }
}

#[cfg(test)]
mod test {
    use crate::gfx::testing::{self, Matcher};
    use crate::xy_chart::periodicity::NotPeriodic;
    use crate::xy_chart::testing::empty_chart;

    use super::*;

    #[test]
    fn test_xy_chart_draws_line_plot() {
        let mut drawable = gfx::testing::UnstyledFakeDrawable::new();
        let chart = empty_chart();
        let plot = chart
            .prepare_layout(crate::xy_chart::AspectRatio::Unconstrained, &drawable)
            .fit_into(400., 200.);
        let plot = plot.draw_axes(&mut drawable);

        let line_plot = LinePlot {
            coordinates: &[[0.5, 0.5], [0.75, 0.5], [0.75, 0.75]],
            line_style: Default::default(),
            periodicity: (NotPeriodic, NotPeriodic),
        };
        plot.plot(&mut drawable, &line_plot);

        let expected_plot = gfx::testing::UnstyledFakeDrawable {
            elements: vec![
                gfx::Rectangle {
                    x: 10.0,
                    y: 10.0,
                    width: 380.,
                    height: 180.,
                }
                .into(),
                [(200.0, 100.0), (295.0, 100.0), (295.0, 55.0)].into(),
            ],
        };
        testing::assert_match!(&drawable, testing::Approx(&expected_plot));
    }
}
