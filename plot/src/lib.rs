//! Abstraction layer for graphical output and chart plotting
#![warn(missing_docs)]

pub mod gfx;
pub mod xy_chart;
