//! SVG drawable (only available if built with the `"svg"` feature)

use crate::gfx;

use svg::Node;

/// SVG [crate::gfx::Drawable]
#[derive(Debug, Clone)]
pub struct SvgDrawableWithFontMetrics<M> {
    document: svg::Document,
    id_counter: usize,
    font_metrics: M,
}

impl Default for SvgDrawable {
    fn default() -> Self {
        Self::new()
    }
}

/// SVG [crate::gfx::Drawable] with [gfx::MonoSpaceFontMetrics]
pub type SvgDrawable = SvgDrawableWithFontMetrics<gfx::MonoSpaceFontMetrics>;

impl SvgDrawable {
    /// Create a new svg drawable with dimension `width`x`height`
    pub fn new() -> Self {
        Self {
            document: svg::Document::new(),
            id_counter: 0,
            font_metrics: gfx::MonoSpaceFontMetrics { aspect_ratio: 2.0 },
        }
    }
}

impl<M> SvgDrawableWithFontMetrics<M> {
    /// Create a new svg drawable with dimension `width`x`height` and a [gfx::FontMetrics]
    /// implementation
    pub fn new_with_font_metrics(font_metrics: M) -> Self {
        Self {
            document: svg::Document::new(),
            id_counter: 0,
            font_metrics,
        }
    }
    /// Write the svg document to `stream`
    pub fn write(
        self,
        stream: &mut impl std::io::Write,
        width: f64,
        height: f64,
    ) -> std::io::Result<()> {
        svg::write(
            stream,
            &self.document.set("viewBox", (0., 0., width, height)),
        )
    }
}

trait SetAttribute {
    fn set_attribute<S: Into<String>, V: Into<svg::node::Value>>(self, name: S, value: V) -> Self;
}

trait SetColor {
    fn set_color(self, color: Option<&gfx::Color>) -> Self;
}
trait SetLineStyle {
    fn set_line_style(self, line_style: Option<&gfx::LineStyle>) -> Self;
}

trait AppendNode {
    fn append_node<T: Into<Box<dyn svg::node::Node>>>(&mut self, node: T)
    where
        Self: Sized;
}
impl<N: svg::node::Node> AppendNode for N {
    fn append_node<T: Into<Box<dyn svg::node::Node>>>(&mut self, node: T)
    where
        Self: Sized,
    {
        self.append(node);
    }
}
impl<M> AppendNode for SvgDrawableWithFontMetrics<M> {
    fn append_node<T: Into<Box<dyn svg::node::Node>>>(&mut self, node: T) {
        self.document.append(node);
    }
}

fn format_color(color: &gfx::Color) -> String {
    format!(
        "rgb({},{},{})",
        color.r * 255.0,
        color.g * 255.0,
        color.b * 255.0
    )
}

impl<T: SetAttribute> SetColor for T {
    fn set_color(self, color: Option<&gfx::Color>) -> Self {
        if let Some(color) = color {
            let node = self.set_attribute("fill", format_color(color));
            if color.a != 1. {
                node.set_attribute("fill_opacity", color.a)
            } else {
                node
            }
        } else {
            self.set_attribute("fill", "none")
        }
    }
}
impl<T: SetAttribute> SetLineStyle for T {
    fn set_line_style(self, line_style: Option<&gfx::LineStyle>) -> Self {
        if let Some(line_style) = line_style {
            let node = self
                .set_attribute("stroke", format_color(&line_style.color))
                .set_attribute("stroke-width", line_style.width);
            let node = if line_style.color.a != 1. {
                node.set_attribute("stroke-opacity", line_style.color.a)
            } else {
                node
            };
            let node = if line_style.dash_lengths.is_empty() {
                node
            } else {
                node.set_attribute(
                    "stroke-dasharray",
                    line_style
                        .dash_lengths
                        .iter()
                        .map(|l| format!("{l}"))
                        .collect::<Vec<_>>()
                        .join(","),
                )
            };
            if line_style.dash_offset != 0. {
                node.set_attribute("stroke-dashoffset", line_style.dash_offset)
            } else {
                node
            }
        } else {
            self
        }
    }
}

impl SetAttribute for svg::node::element::Rectangle {
    fn set_attribute<S: Into<String>, V: Into<svg::node::Value>>(self, name: S, value: V) -> Self {
        self.set(name, value)
    }
}

impl<N: AppendNode> gfx::Draw<gfx::Rectangle> for N {
    fn draw(
        &mut self,
        element: &gfx::Rectangle,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        self.append_node(
            svg::node::element::Rectangle::new()
                .set("x", element.x)
                .set("y", element.y)
                .set("width", element.width)
                .set("height", element.height)
                .set_color(color)
                .set_line_style(line_style),
        );
    }
}

impl SetAttribute for svg::node::element::Circle {
    fn set_attribute<S: Into<String>, V: Into<svg::node::Value>>(self, name: S, value: V) -> Self {
        self.set(name, value)
    }
}

impl<N: AppendNode> gfx::Draw<gfx::Circle> for N {
    fn draw(
        &mut self,
        element: &gfx::Circle,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        self.append_node(
            svg::node::element::Circle::new()
                .set("cx", element.x)
                .set("cy", element.y)
                .set("r", element.r)
                .set_color(color)
                .set_line_style(line_style),
        );
    }
}

impl SetAttribute for svg::node::element::Path {
    fn set_attribute<S: Into<String>, V: Into<svg::node::Value>>(self, name: S, value: V) -> Self {
        self.set(name, value)
    }
}

fn create_path_data(
    p: impl IntoIterator<Item = (f64, f64)>,
) -> Option<svg::node::element::path::Data> {
    let mut points = p.into_iter();
    let first_point = points.next()?;
    Some(points.fold(
        svg::node::element::path::Data::new().move_to(first_point),
        |path, point| path.line_to(point),
    ))
}

fn create_curve_data(
    mut points: impl Iterator<Item = gfx::BezierPoint>,
) -> Option<svg::node::element::path::Data> {
    let first_point = points.next()?;
    Some(
        points
            .fold(
                (
                    first_point.q_f,
                    svg::node::element::path::Data::new().move_to(first_point.p),
                ),
                |(q_f, path), point| {
                    (
                        point.q_f,
                        path.cubic_curve_to((
                            q_f.0,
                            q_f.1,
                            point.q_b.0,
                            point.q_b.1,
                            point.p.0,
                            point.p.1,
                        )),
                    )
                },
            )
            .1,
    )
}

fn create_path(
    data: svg::node::element::path::Data,
    line_style: Option<&gfx::LineStyle>,
    color: Option<&gfx::Color>,
) -> svg::node::element::Path {
    svg::node::element::Path::new()
        .set("d", data)
        .set_color(color)
        .set_line_style(line_style)
}

impl<N: AppendNode> gfx::DrawPolygon<(f64, f64)> for N {
    fn draw_polygon(
        &mut self,
        p: impl IntoIterator<Item = (f64, f64)>,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        if let Some(data) = create_path_data(p) {
            self.append_node(create_path(data.close(), line_style, color));
        }
    }
}

impl<N: AppendNode> gfx::StrokePath<(f64, f64)> for N {
    fn stroke_path(
        &mut self,
        p: impl IntoIterator<Item = (f64, f64)>,
        line_style: &gfx::LineStyle,
    ) {
        if let Some(data) = create_path_data(p) {
            self.append_node(create_path(data, Some(line_style), None));
        }
    }
}

impl<N: AppendNode> gfx::StrokePath<gfx::BezierPoint> for N {
    fn stroke_path(
        &mut self,
        curve: impl IntoIterator<Item = gfx::BezierPoint>,
        line_style: &gfx::LineStyle,
    ) {
        if let Some(data) = create_curve_data(curve.into_iter()) {
            self.append_node(create_path(data, Some(line_style), None));
        }
    }
}

impl<N: AppendNode> gfx::DrawPolygon<gfx::BezierPoint> for N {
    fn draw_polygon(
        &mut self,
        p: impl IntoIterator<Item = gfx::BezierPoint>,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        let mut points = p.into_iter();
        let Some(start) = points.next() else {
            return;
        };
        let points = std::iter::once(start.clone())
            .chain(points)
            .chain(std::iter::once(start));
        if let Some(data) = create_curve_data(points) {
            self.append_node(create_path(data.close(), line_style, color));
        }
    }
}

impl SetAttribute for svg::node::element::Text {
    fn set_attribute<S: Into<String>, V: Into<svg::node::Value>>(self, name: S, value: V) -> Self {
        self.set(name, value)
    }
}

trait SetFont {
    fn set_font<SF: AsRef<str>>(self, font: &gfx::Font<SF>) -> Self;
}

impl SetFont for svg::node::element::Text {
    fn set_font<SF: AsRef<str>>(self, font: &gfx::Font<SF>) -> Self {
        let text_node = self
            .set("font-size", font.size)
            .set_color(Some(&font.color));
        let text_node = if let gfx::FontWeight::Bold = font.weight {
            text_node.set("font-weight", "bold")
        } else {
            text_node
        };
        let text_node = match font.slant {
            gfx::FontSlant::Italic => text_node.set("font-style", "italic"),
            gfx::FontSlant::Normal => text_node,
            gfx::FontSlant::Oblique => text_node.set("font-style", "oblique"),
        };
        let family = font.family.as_ref();
        if family.is_empty() {
            text_node
        } else {
            text_node.set("font-family", family)
        }
    }
}

impl<N: AppendNode> gfx::DrawText for N {
    fn draw_text<ST: AsRef<str>, SF: AsRef<str>>(
        &mut self,
        text: &gfx::Text<ST>,
        font: &gfx::Font<SF>,
    ) {
        let text_node = svg::node::element::Text::new()
            .add(svg::node::Text::new(text.text.as_ref()))
            .set("x", text.x)
            .set("y", text.y)
            .set(
                "dominant-baseline",
                match text.baseline {
                    gfx::Baseline::Alphabetic => "alphabetic",
                    gfx::Baseline::Hanging => "hanging",
                    gfx::Baseline::Middle => "middle",
                },
            )
            .set_font(font);
        let text_node = match text.anchor {
            gfx::TextAnchor::End => text_node.set("text-anchor", "end"),
            gfx::TextAnchor::Middle => text_node.set("text-anchor", "middle"),
            gfx::TextAnchor::Start => text_node,
        };
        let text_node = if text.angle == 0. {
            text_node
        } else {
            text_node.set(
                "transform",
                format!("rotate({},{},{})", text.angle.to_degrees(), text.x, text.y),
            )
        };
        self.append_node(text_node);
    }
}

/// Represents a svg \<g> element with a "clip-path" attribute
#[derive(Debug, Default)]
pub struct ClipGroup<M> {
    /// The \<g> element
    pub group: svg::node::element::Group,
    /// Counter used to create id attributes for elements
    pub id_counter: usize,
    /// Clip paths applied to the group
    pub clip_paths: Vec<svg::node::element::ClipPath>,
    /// The font metrics to use for the group
    pub font_metrics: M,
}
impl<M> AppendNode for ClipGroup<M> {
    fn append_node<T: Into<Box<dyn svg::node::Node>>>(&mut self, node: T) {
        self.group.append_node(node);
    }
}

/// Manage id counters for a hierarchy of clip groups
pub trait ClipPathManager {
    /// Mutable access to the id counter
    fn counter_mut(&mut self) -> &mut usize;
    /// Current value of the id counter
    fn counter(&self) -> usize;
    /// Start a new clip group using the specified clip path
    fn append_clip_path(&mut self, clip_path: svg::node::element::ClipPath);
}

impl<M> ClipPathManager for ClipGroup<M> {
    fn counter_mut(&mut self) -> &mut usize {
        &mut self.id_counter
    }
    fn counter(&self) -> usize {
        self.id_counter
    }
    fn append_clip_path(&mut self, clip_path: svg::node::element::ClipPath) {
        self.clip_paths.push(clip_path);
    }
}
impl<M> ClipPathManager for SvgDrawableWithFontMetrics<M> {
    fn counter_mut(&mut self) -> &mut usize {
        &mut self.id_counter
    }
    fn counter(&self) -> usize {
        self.id_counter
    }
    fn append_clip_path(&mut self, clip_path: svg::node::element::ClipPath) {
        self.append_node(clip_path);
    }
}

/// Trait used to propagate a font metric through a hierarchy of \<g> elements
pub trait CloneFontMetrics {
    /// Type of the font metrics
    type FontMetricsType: gfx::FontMetrics;
    /// Return a clone of a font metrics object
    fn clone_font_metrics(&self) -> Self::FontMetricsType;
}

impl<M: Clone + gfx::FontMetrics> CloneFontMetrics for SvgDrawableWithFontMetrics<M> {
    type FontMetricsType = M;
    fn clone_font_metrics(&self) -> Self::FontMetricsType {
        self.font_metrics.clone()
    }
}
impl<M: Clone + gfx::FontMetrics> CloneFontMetrics for ClipGroup<M> {
    type FontMetricsType = M;
    fn clone_font_metrics(&self) -> Self::FontMetricsType {
        self.font_metrics.clone()
    }
}
impl<M: gfx::FontMetrics> gfx::FontMetrics for ClipGroup<M> {
    fn get_text_extents<S: AsRef<str>>(&self, text: &str, font: &gfx::Font<S>) -> (f64, f64) {
        self.font_metrics.get_text_extents(text, font)
    }
}

impl<N: AppendNode + ClipPathManager + CloneFontMetrics> gfx::ClipPolygon for N
where
    N::FontMetricsType: Clone,
{
    type ClipRegion = ClipGroup<N::FontMetricsType>;
    fn clip_polygon<R, F: FnOnce(&mut Self::ClipRegion) -> R>(
        &mut self,
        polygon: impl IntoIterator<Item = (f64, f64)>,
        draw_callback: F,
    ) -> Option<R> {
        let path_data = create_path_data(polygon)?;
        let id = self.counter();
        let clip_id = format!("clip-path-{}", id);
        let clip_ref = format!("url(#{clip_id})");
        self.append_clip_path(
            svg::node::element::ClipPath::new()
                .set("id", clip_id)
                .add(svg::node::element::Path::new().set("d", path_data.close())),
        );
        let mut group = ClipGroup {
            group: svg::node::element::Group::new().set("clip-path", clip_ref),
            id_counter: id + 1,
            clip_paths: Vec::new(),
            font_metrics: self.clone_font_metrics(),
        };
        let return_value = draw_callback(&mut group);
        for clip_path in group.clip_paths.into_iter() {
            self.append_clip_path(clip_path);
        }
        self.append_node(group.group);
        *self.counter_mut() = group.id_counter;
        Some(return_value)
    }
}

impl<M: gfx::FontMetrics> gfx::FontMetrics for SvgDrawableWithFontMetrics<M> {
    fn get_text_extents<S: AsRef<str>>(&self, text: &str, font: &gfx::Font<S>) -> (f64, f64) {
        self.font_metrics.get_text_extents(text, font)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::gfx::{Draw, DrawText};

    fn dump_drawable(drawable: SvgDrawable) -> Result<String, ()> {
        let mut buf = Vec::new();
        drawable.write(&mut buf, 100., 100.).map_err(|_| ())?;
        Ok(std::str::from_utf8(buf.as_slice())
            .map_err(|_| ())?
            .to_string())
    }

    #[test]
    fn test_draw_rectangle() {
        let mut drawable = SvgDrawable::new();
        drawable.draw(
            &gfx::Rectangle {
                x: 10.,
                y: 15.,
                width: 20.,
                height: 25.,
            },
            Some(&gfx::LineStyle::default()),
            Some(&gfx::Color {
                r: 0.75,
                g: 0.125,
                b: 0.25,
                a: 1.0,
            }),
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<rect fill="rgb(191.25,31.875,63.75)" height="25" stroke="rgb(0,0,0)" stroke-width="1" width="20" x="10" y="15"/>
</svg>"#
        );
    }
    #[test]
    fn test_draw_circle() {
        let mut drawable = SvgDrawable::new();
        drawable.draw(
            &gfx::Circle {
                x: 20.,
                y: 25.,
                r: 10.,
            },
            None,
            Some(&gfx::Color {
                r: 0.125,
                g: 0.75,
                b: 0.25,
                a: 0.5,
            }),
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<circle cx="20" cy="25" fill="rgb(31.875,191.25,63.75)" fill_opacity="0.5" r="10"/>
</svg>"#
        );
    }
    #[test]
    fn test_draw_polygon() {
        use crate::gfx::DrawPolygon;
        let mut drawable = SvgDrawable::new();
        drawable.draw_polygon(
            [(10., 10.), (5., 15.), (10., 20.), (15., 15.)],
            Some(&gfx::LineStyle {
                color: gfx::Color {
                    r: 0.,
                    g: 0.5,
                    b: 0.,
                    a: 1.,
                },
                width: 3.,
                ..gfx::LineStyle::default()
            }),
            Some(&gfx::Color {
                r: 0.125,
                g: 0.75,
                b: 0.25,
                a: 1.0,
            }),
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<path d="M10,10 L5,15 L10,20 L15,15 z" fill="rgb(31.875,191.25,63.75)" stroke="rgb(0,127.5,0)" stroke-width="3"/>
</svg>"#
        );
    }
    #[test]
    fn test_stroke_polygon() {
        use crate::gfx::StrokePath;
        let mut drawable = SvgDrawable::new();
        drawable.stroke_path(
            [(0., 50.), (50., 0.), (100., 50.)],
            &gfx::LineStyle {
                color: gfx::Color {
                    r: 0.,
                    g: 0.,
                    b: 0.5,
                    a: 1.,
                },
                width: 3.,
                dash_lengths: vec![10., 5.],
                ..gfx::LineStyle::default()
            },
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<path d="M0,50 L50,0 L100,50" fill="none" stroke="rgb(0,0,127.5)" stroke-dasharray="10,5" stroke-width="3"/>
</svg>"#
        );
    }
    #[test]
    fn test_draw_beziergon() {
        use crate::gfx::DrawPolygon;
        let mut drawable = SvgDrawable::new();
        drawable.draw_polygon(
            [
                gfx::BezierPoint {
                    p: (10., 10.),
                    q_b: (10., 10.),
                    q_f: (20., 10.),
                },
                gfx::BezierPoint {
                    p: (20., 20.),
                    q_b: (20., 10.),
                    q_f: (20., 30.),
                },
                gfx::BezierPoint {
                    p: (10., 30.),
                    q_b: (20., 30.),
                    q_f: (10., 30.),
                },
            ],
            Some(&gfx::LineStyle::default()),
            None,
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<path d="M10,10 C20,10,20,10,20,20 C20,30,20,30,10,30 C10,30,10,10,10,10 z" fill="none" stroke="rgb(0,0,0)" stroke-width="1"/>
</svg>"#
        );
    }
    #[test]
    fn test_draw_bezier_curve() {
        use crate::gfx::StrokePath;
        let mut drawable = SvgDrawable::new();
        drawable.stroke_path(
            [
                gfx::BezierPoint {
                    p: (10., 10.),
                    q_b: (10., 10.),
                    q_f: (50., 10.),
                },
                gfx::BezierPoint {
                    p: (50., 45.),
                    q_b: (50., 10.),
                    q_f: (50., 90.),
                },
                gfx::BezierPoint {
                    p: (10., 90.),
                    q_b: (50., 90.),
                    q_f: (10., 90.),
                },
            ],
            &gfx::LineStyle {
                color: gfx::Color {
                    r: 0.,
                    g: 0.,
                    b: 0.5,
                    a: 1.,
                },
                width: 3.,
                dash_lengths: vec![5., 10.],
                dash_offset: 1.,
            },
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<path d="M10,10 C50,10,50,10,50,45 C50,90,50,90,10,90" fill="none" stroke="rgb(0,0,127.5)" stroke-dasharray="5,10" stroke-dashoffset="1" stroke-width="3"/>
</svg>"#
        );
    }
    #[test]
    fn test_draw_default_text() {
        let mut drawable = SvgDrawable::new();
        drawable.draw_text(
            &gfx::Text {
                x: 10.,
                y: 10.,
                text: "text",
                ..Default::default()
            },
            &gfx::Font::<&str> {
                color: gfx::Color {
                    r: 0.5,
                    g: 0.5,
                    b: 0.5,
                    a: 1.,
                },
                size: 12.,
                ..Default::default()
            },
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<text dominant-baseline="alphabetic" fill="rgb(127.5,127.5,127.5)" font-size="12" x="10" y="10">
text
</text>
</svg>"#
        );
    }
    #[test]
    fn test_draw_hanging_text() {
        let mut drawable = SvgDrawable::new();
        drawable.draw_text(
            &gfx::Text {
                x: 10.,
                y: 10.,
                text: "text",
                baseline: gfx::Baseline::Hanging,
                ..Default::default()
            },
            &gfx::Font::<&str> {
                size: 12.,
                ..Default::default()
            },
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<text dominant-baseline="hanging" fill="rgb(0,0,0)" font-size="12" x="10" y="10">
text
</text>
</svg>"#
        );
    }
    #[test]
    fn test_draw_text_middle_end() {
        let mut drawable = SvgDrawable::new();
        drawable.draw_text(
            &gfx::Text {
                x: 10.,
                y: 10.,
                text: "text",
                baseline: gfx::Baseline::Middle,
                anchor: gfx::TextAnchor::End,
                ..Default::default()
            },
            &gfx::Font::<&str> {
                size: 12.,
                ..Default::default()
            },
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<text dominant-baseline="middle" fill="rgb(0,0,0)" font-size="12" text-anchor="end" x="10" y="10">
text
</text>
</svg>"#
        );
    }
    #[test]
    fn test_draw_text_alphabetic_middle() {
        let mut drawable = SvgDrawable::new();
        drawable.draw_text(
            &gfx::Text {
                x: 10.,
                y: 10.,
                text: "text",
                baseline: gfx::Baseline::Alphabetic,
                anchor: gfx::TextAnchor::Middle,
                ..Default::default()
            },
            &gfx::Font::<&str> {
                size: 12.,
                ..Default::default()
            },
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<text dominant-baseline="alphabetic" fill="rgb(0,0,0)" font-size="12" text-anchor="middle" x="10" y="10">
text
</text>
</svg>"#
        );
    }
    #[test]
    fn test_draw_rotated_text() {
        let mut drawable = SvgDrawable::new();
        drawable.draw_text(
            &gfx::Text {
                x: 10.,
                y: 10.,
                text: "text",
                angle: std::f64::consts::PI / 4.,
                ..Default::default()
            },
            &gfx::Font::<&str> {
                size: 12.,
                ..Default::default()
            },
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<text dominant-baseline="alphabetic" fill="rgb(0,0,0)" font-size="12" transform="rotate(45,10,10)" x="10" y="10">
text
</text>
</svg>"#
        );
    }
    #[test]
    fn test_draw_bold_italic_text() {
        let mut drawable = SvgDrawable::new();
        drawable.draw_text(
            &gfx::Text {
                x: 10.,
                y: 10.,
                text: "text",
                ..Default::default()
            },
            &gfx::Font {
                size: 12.,
                slant: gfx::FontSlant::Italic,
                weight: gfx::FontWeight::Bold,
                family: "Arial",
                ..Default::default()
            },
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<text dominant-baseline="alphabetic" fill="rgb(0,0,0)" font-family="Arial" font-size="12" font-style="italic" font-weight="bold" x="10" y="10">
text
</text>
</svg>"#
        );
    }
    #[test]
    fn test_draw_oblique_text() {
        let mut drawable = SvgDrawable::new();
        drawable.draw_text(
            &gfx::Text {
                x: 10.,
                y: 10.,
                text: "text",
                ..Default::default()
            },
            &gfx::Font {
                size: 12.,
                slant: gfx::FontSlant::Oblique,
                family: "Arial",
                ..Default::default()
            },
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<text dominant-baseline="alphabetic" fill="rgb(0,0,0)" font-family="Arial" font-size="12" font-style="oblique" x="10" y="10">
text
</text>
</svg>"#
        );
    }
    #[test]
    fn test_draw_clipped_circle() {
        use gfx::ClipPolygon;
        let mut drawable = SvgDrawable::new();
        drawable.clip_polygon(
            [(20., 15.), (5., 20.), (20., 25.), (35., 20.)],
            |drawable| {
                drawable.draw(
                    &gfx::Circle {
                        x: 20.,
                        y: 20.,
                        r: 10.,
                    },
                    None,
                    Some(&gfx::Color {
                        r: 0.125,
                        g: 0.75,
                        b: 0.25,
                        a: 0.5,
                    }),
                )
            },
        );
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<clipPath id="clip-path-0">
<path d="M20,15 L5,20 L20,25 L35,20 z"/>
</clipPath>
<g clip-path="url(#clip-path-0)">
<circle cx="20" cy="20" fill="rgb(31.875,191.25,63.75)" fill_opacity="0.5" r="10"/>
</g>
</svg>"#
        );
    }
    #[test]
    fn test_nested_clip_paths() {
        use gfx::ClipPolygon;
        let mut drawable = SvgDrawable::new();
        drawable.clip_polygon([(0., 0.), (0., 20.)], |group| {
            group.clip_polygon([(0., 0.), (0., 20.)], |sub_group| {
                sub_group.clip_polygon([(0., 0.), (0., 20.)], |_| {})
            });
            group.clip_polygon([(0., 0.), (0., 20.)], |_| {});
        });
        let svg_body = dump_drawable(drawable).unwrap();
        assert_eq!(
            svg_body,
            r#"<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
<clipPath id="clip-path-0">
<path d="M0,0 L0,20 z"/>
</clipPath>
<clipPath id="clip-path-1">
<path d="M0,0 L0,20 z"/>
</clipPath>
<clipPath id="clip-path-2">
<path d="M0,0 L0,20 z"/>
</clipPath>
<clipPath id="clip-path-3">
<path d="M0,0 L0,20 z"/>
</clipPath>
<g clip-path="url(#clip-path-0)">
<g clip-path="url(#clip-path-1)">
<g clip-path="url(#clip-path-2)"/>
</g>
<g clip-path="url(#clip-path-3)"/>
</g>
</svg>"#
        );
    }
}
