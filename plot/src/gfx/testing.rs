//! Testing utilities

use crate::gfx;
pub use testing::assert_match;
pub use testing::Matcher;

/// In memory drawable.
#[derive(Debug, Clone, Default)]
pub struct GenericFakeDrawable<PE> {
    /// Elements which are be drawn to this drawable
    pub elements: Vec<Element<PE>>,
}

/// In memory drawable with style information.
pub type FakeDrawable = GenericFakeDrawable<PrimitiveElement>;
/// In memory drawable discarding style information.
pub type UnstyledFakeDrawable = GenericFakeDrawable<UnstyledPrimitiveElement>;

impl<PE> GenericFakeDrawable<PE> {
    /// Create an new empty drawable.
    pub fn new() -> Self {
        Self {
            elements: Vec::new(),
        }
    }
    /// Add a graphical element to the drawable.
    pub fn add<T: Into<Element<PE>>>(&mut self, element: T) {
        self.elements.push(element.into());
    }
}

#[cfg(feature = "svg")]
impl<PE> GenericFakeDrawable<PE>
where
    GenericFakeDrawable<PE>: DrawTo<gfx::svg::SvgDrawable> + GetExtents,
{
    /// Save the drawable to a svg document
    pub fn save_svg<P: AsRef<std::path::Path>>(&self, path: P) -> std::io::Result<()> {
        let mut svg_plot = gfx::svg::SvgDrawable::new();
        self.draw_to(&mut svg_plot);
        let mut file = std::fs::File::create(path)?;
        const DEFAULT_WIDTH: f64 = 1024.0;
        let (x_range, y_range) = self
            .get_extents()
            .unwrap_or((0. ..DEFAULT_WIDTH, 0. ..DEFAULT_WIDTH));
        let (width, height) = (x_range.end, y_range.end);
        svg_plot.write(&mut file, width, height)
    }
}

/// Graphical element carrying style information.
#[derive(Debug, Clone)]
pub struct StyledElement<T> {
    /// Line style
    pub line_style: Option<gfx::LineStyle>,
    /// Color
    pub color: Option<gfx::Color>,
    /// Actual element
    pub inner: T,
}

/// Polygon as a graphical element represented as a vector of corner points.
#[derive(Debug, Clone)]
pub struct Polygon(pub Vec<(f64, f64)>);

/// Polygonal chain (not closed) as a g raphical element represented as a vector of corner points. Carries style.
#[derive(Debug, Clone)]
pub struct PolygonalChain {
    /// Points of the polygonal chain
    pub points: Vec<(f64, f64)>,
    /// Line style
    pub line_style: gfx::LineStyle,
}

/// Closed Bezier curve as a graphical element represented as a vector of Bezier corner points.
#[derive(Debug, Clone)]
pub struct Beziergon(pub Vec<gfx::BezierPoint>);

/// Unclosed Bezier curve as a graphical element represented as a vector of Bezier corner points. Carries style.
#[derive(Debug, Clone)]
pub struct BezierCurve {
    /// Points and control points of the Bézier curve
    pub points: Vec<gfx::BezierPoint>,
    /// Line style
    pub line_style: gfx::LineStyle,
}

/// Text with font as a graphical element.
#[derive(Debug, Clone)]
pub struct Text {
    /// Actual text
    pub inner: gfx::Text<String>,
    /// Font for the text
    pub font: gfx::Font<String>,
}

/// Text without font as a graphical element.
#[derive(Debug, Clone)]
pub struct UnstyledText {
    /// Actual text
    pub inner: gfx::Text<String>,
    /// Text size
    pub size: f64,
}

impl UnstyledText {
    /// Create a new [UnstyledText] element with its anchor point
    /// (left, bottom of bounding box) at coordinates `(x, y)` with `size`.
    pub fn new(x: f64, y: f64, text: &str, size: f64) -> Self {
        Self {
            inner: gfx::Text {
                x,
                y,
                text: text.to_string(),
                baseline: gfx::Baseline::Alphabetic,
                anchor: gfx::TextAnchor::Start,
                angle: 0.,
            },
            size,
        }
    }
    /// Set the rotation angle to -90° (clockwise rotation).
    pub fn rot90(self) -> Self {
        Self {
            inner: gfx::Text {
                angle: -std::f64::consts::FRAC_PI_2,
                ..self.inner
            },
            ..self
        }
    }
    /// Set the anchor (horizontal anchor).
    pub fn anchor(self, anchor: gfx::TextAnchor) -> Self {
        Self {
            inner: gfx::Text {
                anchor,
                ..self.inner
            },
            ..self
        }
    }
    /// Set the baseline (vertical anchor).
    pub fn baseline(self, baseline: gfx::Baseline) -> Self {
        Self {
            inner: gfx::Text {
                baseline,
                ..self.inner
            },
            ..self
        }
    }
}

impl<S: AsRef<str>> From<&gfx::Font<S>> for gfx::Font<String> {
    fn from(value: &gfx::Font<S>) -> Self {
        Self {
            slant: value.slant.clone(),
            weight: value.weight.clone(),
            family: value.family.as_ref().to_string(),
            size: value.size,
            color: value.color.clone(),
        }
    }
}
impl<S: AsRef<str>> From<&gfx::Text<S>> for gfx::Text<String> {
    fn from(value: &gfx::Text<S>) -> Self {
        Self {
            x: value.x,
            y: value.y,
            text: value.text.as_ref().to_string(),
            anchor: value.anchor,
            baseline: value.baseline,
            angle: value.angle,
        }
    }
}

/// Primitive graphical element.
/// Only primitive elements can be put into a [ClippedRegion].
#[derive(Debug, Clone)]
pub enum PrimitiveElement {
    /// Rectangle
    Rectangle(StyledElement<gfx::Rectangle>),
    /// Circle
    Circle(StyledElement<gfx::Circle>),
    /// Polygonal chain
    PolygonalChain(PolygonalChain),
    /// Polygon (closed shape)
    Polygon(StyledElement<Polygon>),
    /// Closed Bézier curve
    Beziergon(StyledElement<Beziergon>),
    /// Bézier curve
    BezierCurve(BezierCurve),
    /// Text
    Text(Text),
}

/// Primitive graphical element without style information.
/// Only primitive elements can be put into a [ClippedRegion].
#[derive(Debug, Clone)]
pub enum UnstyledPrimitiveElement {
    /// Rectangle
    Rectangle(gfx::Rectangle),
    /// Circle
    Circle(gfx::Circle),
    /// Polygonal chain
    PolygonalChain(Vec<(f64, f64)>),
    /// Polygon (closed shape)
    Polygon(Polygon),
    /// Closed Bézier curve
    Beziergon(Beziergon),
    /// Bézier curve
    BezierCurve(Vec<gfx::BezierPoint>),
    /// Text
    Text(UnstyledText),
}

/// Clipped region containing primitive graphical elements.
#[derive(Debug, Clone)]
pub struct ClippedRegion<PE> {
    /// Clip path
    pub polygon: Vec<(f64, f64)>,
    /// Elements to be clipped
    pub primitives: Vec<PE>,
}

/// Graphical element (either primitve element or [ClippedRegion]).
#[derive(Debug, Clone)]
pub enum Element<PE> {
    /// Unclipped element
    Primitive(PE),
    /// Clipped region, clipping contained elements
    ClippedRegion(ClippedRegion<PE>),
}
impl From<StyledElement<gfx::Rectangle>> for Element<PrimitiveElement> {
    fn from(value: StyledElement<gfx::Rectangle>) -> Self {
        Self::Primitive(PrimitiveElement::Rectangle(value))
    }
}
impl From<gfx::Rectangle> for Element<UnstyledPrimitiveElement> {
    fn from(value: gfx::Rectangle) -> Self {
        Self::Primitive(UnstyledPrimitiveElement::Rectangle(value))
    }
}
impl From<StyledElement<gfx::Circle>> for Element<PrimitiveElement> {
    fn from(value: StyledElement<gfx::Circle>) -> Self {
        Self::Primitive(PrimitiveElement::Circle(value))
    }
}
impl From<gfx::Circle> for Element<UnstyledPrimitiveElement> {
    fn from(value: gfx::Circle) -> Self {
        Self::Primitive(UnstyledPrimitiveElement::Circle(value))
    }
}
impl From<PolygonalChain> for Element<PrimitiveElement> {
    fn from(value: PolygonalChain) -> Self {
        Self::Primitive(PrimitiveElement::PolygonalChain(value))
    }
}
impl From<Vec<(f64, f64)>> for Element<UnstyledPrimitiveElement> {
    fn from(value: Vec<(f64, f64)>) -> Self {
        Self::Primitive(UnstyledPrimitiveElement::PolygonalChain(value))
    }
}
impl<const N: usize> From<[(f64, f64); N]> for Element<UnstyledPrimitiveElement> {
    fn from(value: [(f64, f64); N]) -> Self {
        Self::Primitive(UnstyledPrimitiveElement::PolygonalChain(value.to_vec()))
    }
}
impl From<StyledElement<Polygon>> for Element<PrimitiveElement> {
    fn from(value: StyledElement<Polygon>) -> Self {
        Self::Primitive(PrimitiveElement::Polygon(value))
    }
}
impl From<Polygon> for Element<UnstyledPrimitiveElement> {
    fn from(value: Polygon) -> Self {
        Self::Primitive(UnstyledPrimitiveElement::Polygon(value))
    }
}
impl From<StyledElement<Beziergon>> for Element<PrimitiveElement> {
    fn from(value: StyledElement<Beziergon>) -> Self {
        Self::Primitive(PrimitiveElement::Beziergon(value))
    }
}
impl From<Beziergon> for Element<UnstyledPrimitiveElement> {
    fn from(value: Beziergon) -> Self {
        Self::Primitive(UnstyledPrimitiveElement::Beziergon(value))
    }
}
impl From<BezierCurve> for Element<PrimitiveElement> {
    fn from(value: BezierCurve) -> Self {
        Self::Primitive(PrimitiveElement::BezierCurve(value))
    }
}
impl From<Vec<gfx::BezierPoint>> for Element<UnstyledPrimitiveElement> {
    fn from(value: Vec<gfx::BezierPoint>) -> Self {
        Self::Primitive(UnstyledPrimitiveElement::BezierCurve(value))
    }
}
impl From<Text> for Element<PrimitiveElement> {
    fn from(value: Text) -> Self {
        Self::Primitive(PrimitiveElement::Text(value))
    }
}
impl From<UnstyledText> for Element<UnstyledPrimitiveElement> {
    fn from(value: UnstyledText) -> Self {
        Self::Primitive(UnstyledPrimitiveElement::Text(value))
    }
}
impl<PE> From<ClippedRegion<PE>> for Element<PE> {
    fn from(value: ClippedRegion<PE>) -> Self {
        Self::ClippedRegion(value)
    }
}

impl gfx::Draw<gfx::Rectangle> for GenericFakeDrawable<PrimitiveElement> {
    fn draw(
        &mut self,
        element: &gfx::Rectangle,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        self.add(StyledElement {
            inner: element.clone(),
            line_style: line_style.cloned(),
            color: color.cloned(),
        });
    }
}
impl gfx::Draw<gfx::Rectangle> for GenericFakeDrawable<UnstyledPrimitiveElement> {
    fn draw(
        &mut self,
        element: &gfx::Rectangle,
        _line_style: Option<&gfx::LineStyle>,
        _color: Option<&gfx::Color>,
    ) {
        self.add(element.clone());
    }
}

impl gfx::Draw<gfx::Circle> for GenericFakeDrawable<PrimitiveElement> {
    fn draw(
        &mut self,
        element: &gfx::Circle,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        self.add(StyledElement {
            inner: element.clone(),
            line_style: line_style.cloned(),
            color: color.cloned(),
        });
    }
}
impl gfx::Draw<gfx::Circle> for GenericFakeDrawable<UnstyledPrimitiveElement> {
    fn draw(
        &mut self,
        element: &gfx::Circle,
        _line_style: Option<&gfx::LineStyle>,
        _color: Option<&gfx::Color>,
    ) {
        self.add(element.clone());
    }
}

impl gfx::DrawPolygon<(f64, f64)> for GenericFakeDrawable<PrimitiveElement> {
    fn draw_polygon(
        &mut self,
        p: impl IntoIterator<Item = (f64, f64)>,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        self.add(StyledElement {
            inner: Polygon(p.into_iter().collect()),
            line_style: line_style.cloned(),
            color: color.cloned(),
        });
    }
}
impl gfx::DrawPolygon<(f64, f64)> for GenericFakeDrawable<UnstyledPrimitiveElement> {
    fn draw_polygon(
        &mut self,
        p: impl IntoIterator<Item = (f64, f64)>,
        _line_style: Option<&gfx::LineStyle>,
        _color: Option<&gfx::Color>,
    ) {
        self.add(Polygon(p.into_iter().collect()));
    }
}

impl gfx::StrokePath<(f64, f64)> for GenericFakeDrawable<PrimitiveElement> {
    fn stroke_path(
        &mut self,
        p: impl IntoIterator<Item = (f64, f64)>,
        line_style: &gfx::LineStyle,
    ) {
        self.add(PolygonalChain {
            points: p.into_iter().collect(),
            line_style: line_style.clone(),
        });
    }
}
impl gfx::StrokePath<(f64, f64)> for GenericFakeDrawable<UnstyledPrimitiveElement> {
    fn stroke_path(
        &mut self,
        p: impl IntoIterator<Item = (f64, f64)>,
        _line_style: &gfx::LineStyle,
    ) {
        self.add(p.into_iter().collect::<Vec<_>>());
    }
}

impl gfx::StrokePath<gfx::BezierPoint> for GenericFakeDrawable<PrimitiveElement> {
    fn stroke_path(
        &mut self,
        curve: impl IntoIterator<Item = gfx::BezierPoint>,
        line_style: &gfx::LineStyle,
    ) {
        self.add(BezierCurve {
            points: curve.into_iter().collect(),
            line_style: line_style.clone(),
        });
    }
}
impl gfx::StrokePath<gfx::BezierPoint> for GenericFakeDrawable<UnstyledPrimitiveElement> {
    fn stroke_path(
        &mut self,
        curve: impl IntoIterator<Item = gfx::BezierPoint>,
        _line_style: &gfx::LineStyle,
    ) {
        self.add(curve.into_iter().collect::<Vec<_>>());
    }
}

impl gfx::DrawPolygon<gfx::BezierPoint> for GenericFakeDrawable<PrimitiveElement> {
    fn draw_polygon(
        &mut self,
        p: impl IntoIterator<Item = gfx::BezierPoint>,
        line_style: Option<&gfx::LineStyle>,
        color: Option<&gfx::Color>,
    ) {
        self.add(StyledElement {
            inner: Beziergon(p.into_iter().collect()),
            line_style: line_style.cloned(),
            color: color.cloned(),
        });
    }
}
impl gfx::DrawPolygon<gfx::BezierPoint> for GenericFakeDrawable<UnstyledPrimitiveElement> {
    fn draw_polygon(
        &mut self,
        p: impl IntoIterator<Item = gfx::BezierPoint>,
        _line_style: Option<&gfx::LineStyle>,
        _color: Option<&gfx::Color>,
    ) {
        self.add(Beziergon(p.into_iter().collect()));
    }
}

impl gfx::DrawText for GenericFakeDrawable<PrimitiveElement> {
    fn draw_text<ST: AsRef<str>, SF: AsRef<str>>(
        &mut self,
        text: &gfx::Text<ST>,
        font: &gfx::Font<SF>,
    ) {
        self.add(Text {
            inner: text.into(),
            font: font.into(),
        });
    }
}
impl gfx::DrawText for GenericFakeDrawable<UnstyledPrimitiveElement> {
    fn draw_text<ST: AsRef<str>, SF: AsRef<str>>(
        &mut self,
        text: &gfx::Text<ST>,
        font: &gfx::Font<SF>,
    ) {
        self.add(UnstyledText {
            inner: text.into(),
            size: font.size,
        });
    }
}

fn clip_polygon<PE, R, F: FnOnce(&mut GenericFakeDrawable<PE>) -> R>(
    drawable: &mut GenericFakeDrawable<PE>,
    polygon: impl IntoIterator<Item = (f64, f64)>,
    draw_callback: F,
) -> Option<R> {
    let mut sub_drawable = GenericFakeDrawable::<PE>::new();
    let return_value = draw_callback(&mut sub_drawable);
    drawable.add(ClippedRegion {
        polygon: polygon.into_iter().collect(),
        primitives: sub_drawable
            .elements
            .into_iter()
            .map(|e| match e {
                Element::Primitive(p) => p,
                _ => {
                    unimplemented!("Nested clipped regions are forbidden");
                }
            })
            .collect(),
    });
    Some(return_value)
}

impl gfx::ClipPolygon for GenericFakeDrawable<UnstyledPrimitiveElement> {
    type ClipRegion = GenericFakeDrawable<UnstyledPrimitiveElement>;
    fn clip_polygon<R, F: FnOnce(&mut Self::ClipRegion) -> R>(
        &mut self,
        polygon: impl IntoIterator<Item = (f64, f64)>,
        draw_callback: F,
    ) -> Option<R> {
        clip_polygon(self, polygon, draw_callback)
    }
}

impl gfx::ClipPolygon for GenericFakeDrawable<PrimitiveElement> {
    type ClipRegion = GenericFakeDrawable<PrimitiveElement>;
    fn clip_polygon<R, F: FnOnce(&mut Self::ClipRegion) -> R>(
        &mut self,
        polygon: impl IntoIterator<Item = (f64, f64)>,
        draw_callback: F,
    ) -> Option<R> {
        clip_polygon(self, polygon, draw_callback)
    }
}

const MONOSPACE_FONT_METRICS: gfx::MonoSpaceFontMetrics =
    gfx::MonoSpaceFontMetrics { aspect_ratio: 2.0 };

impl<PE> gfx::FontMetrics for GenericFakeDrawable<PE> {
    fn get_text_extents<S: AsRef<str>>(&self, text: &str, font: &gfx::Font<S>) -> (f64, f64) {
        MONOSPACE_FONT_METRICS.get_text_extents(text, font)
    }
}

/// Tolerance value when comparing graphical coordinates (1/4 pixel).
const PX_TOLERANCE: f64 = 0.25;

/// Comparison container. [Approx<T>] matches another object of type `T` if all involved
/// graphical coordinates do not differ more than [PX_TOLERANCE].
/// For all graphical components `T`, [Approx<T>] will implement [testing::Matcher].
#[derive(Debug)]
pub struct Approx<T>(pub T);

/// Defines a distance for graphical elements by measurung their internal graphical coordinates.
trait Distance {
    fn distance(self, other: Self) -> f64;
}

impl Distance for &gfx::Rectangle {
    fn distance(self, other: Self) -> f64 {
        [
            (self.x - other.x).abs(),
            (self.y - other.y).abs(),
            (self.width - other.width).abs(),
            (self.height - other.height).abs(),
        ]
        .into_iter()
        .reduce(f64::max)
        .unwrap()
    }
}

impl Distance for &gfx::Circle {
    fn distance(self, other: Self) -> f64 {
        [
            (self.x - other.x).abs(),
            (self.y - other.y).abs(),
            (self.r - other.r).abs(),
        ]
        .into_iter()
        .reduce(f64::max)
        .unwrap()
    }
}

impl Distance for &(f64, f64) {
    fn distance(self, other: Self) -> f64 {
        f64::max((self.0 - other.0).abs(), (self.1 - other.1).abs())
    }
}

impl Distance for &gfx::BezierPoint {
    fn distance(self, other: Self) -> f64 {
        [
            (self.p.0 - other.p.0).abs(),
            (self.p.1 - other.p.1).abs(),
            (self.q_f.0 - other.q_f.0).abs(),
            (self.q_f.1 - other.q_f.1).abs(),
            (self.q_b.0 - other.q_b.0).abs(),
            (self.q_b.1 - other.q_b.1).abs(),
        ]
        .into_iter()
        .reduce(f64::max)
        .unwrap()
    }
}

impl<T: Distance + std::fmt::Debug + Copy> Matcher<T> for Approx<T> {
    fn matches(&self, obj: T) -> Result<(), String> {
        let d = self.0.distance(obj);
        if d < PX_TOLERANCE {
            Ok(())
        } else {
            Err(format!("dist({obj:?}, {self:?}) = {d} > {PX_TOLERANCE}"))
        }
    }
}

impl<T: std::fmt::Debug> Matcher<&Vec<T>> for Approx<&Vec<T>>
where
    for<'a> Approx<&'a T>: Matcher<&'a T>,
{
    fn matches(&self, obj: &Vec<T>) -> Result<(), String> {
        testing::containers_match(obj, self.0, |x: &T, y: &T| Approx(y).matches(x))
    }
}

impl Matcher<&Polygon> for Approx<&Polygon> {
    fn matches(&self, obj: &Polygon) -> Result<(), String> {
        Approx(&self.0 .0).matches(&obj.0)
    }
}
impl Matcher<&Beziergon> for Approx<&Beziergon> {
    fn matches(&self, obj: &Beziergon) -> Result<(), String> {
        Approx(&self.0 .0).matches(&obj.0)
    }
}

impl<S: AsRef<str>> Matcher<&gfx::Text<S>> for Approx<&gfx::Text<S>> {
    fn matches(&self, obj: &gfx::Text<S>) -> Result<(), String> {
        let d = (self.0.x, self.0.y).distance(&(obj.x, obj.y));
        if d > PX_TOLERANCE {
            return Err(format!(
                "dist({:?}, {:?}) = {d} > {PX_TOLERANCE}",
                (self.0.x, self.0.y),
                (obj.x, obj.y)
            ));
        }
        let d = (self.0.angle - obj.angle).abs();
        if d > PX_TOLERANCE {
            return Err(format!(
                "|{} - {}| = {d} > {PX_TOLERANCE}",
                self.0.angle, obj.angle
            ));
        }
        if self.0.baseline != obj.baseline {
            return Err(format!("{:?} != {:?}", self.0.baseline, obj.baseline));
        }
        if self.0.anchor != obj.anchor {
            return Err(format!("{:?} != {:?}", self.0.anchor, obj.anchor));
        }
        if self.0.text.as_ref() != obj.text.as_ref() {
            return Err(format!("{} != {}", self.0.text.as_ref(), obj.text.as_ref()));
        }
        Ok(())
    }
}

impl Matcher<&UnstyledText> for Approx<&UnstyledText> {
    fn matches(&self, obj: &UnstyledText) -> Result<(), String> {
        Approx(&self.0.inner).matches(&obj.inner)?;
        if (self.0.size - obj.size).abs() < PX_TOLERANCE {
            Ok(())
        } else {
            Err(format!("|{} - {}| > {PX_TOLERANCE}", self.0.size, obj.size))
        }
    }
}

impl Matcher<&UnstyledPrimitiveElement> for Approx<&UnstyledPrimitiveElement> {
    fn matches(&self, obj: &UnstyledPrimitiveElement) -> Result<(), String> {
        type PE = UnstyledPrimitiveElement;
        match (self.0, obj) {
            (PE::Rectangle(obj), PE::Rectangle(obj_other)) => Approx(obj).matches(obj_other),
            (PE::Circle(obj), PE::Circle(obj_other)) => Approx(obj).matches(obj_other),
            (PE::Polygon(obj), PE::Polygon(obj_other)) => Approx(obj).matches(obj_other),
            (PE::PolygonalChain(obj), PE::PolygonalChain(obj_other)) => {
                Approx(obj).matches(obj_other)
            }
            (PE::Beziergon(obj), PE::Beziergon(obj_other)) => Approx(obj).matches(obj_other),
            (PE::BezierCurve(obj), PE::BezierCurve(obj_other)) => Approx(obj).matches(obj_other),
            (PE::Text(obj), PE::Text(obj_other)) => Approx(obj).matches(obj_other),
            _ => Err(format!("Object type does not match: {:?}, {obj:?}", self.0)),
        }
    }
}

impl<PE: std::fmt::Debug> Matcher<&GenericFakeDrawable<PE>> for Approx<&GenericFakeDrawable<PE>>
where
    for<'a> Approx<&'a PE>: Matcher<&'a PE>,
    for<'a> Approx<&'a Element<PE>>: Matcher<&'a Element<PE>>,
{
    fn matches(&self, obj: &GenericFakeDrawable<PE>) -> Result<(), String> {
        Approx(&self.0.elements).matches(&obj.elements)
    }
}

impl<PE: std::fmt::Debug> Matcher<&ClippedRegion<PE>> for Approx<&ClippedRegion<PE>>
where
    for<'a> Approx<&'a PE>: Matcher<&'a PE>,
{
    fn matches(&self, obj: &ClippedRegion<PE>) -> Result<(), String> {
        Approx(&self.0.polygon)
            .matches(&obj.polygon)
            .and_then(|_| Approx(&self.0.primitives).matches(&obj.primitives))
    }
}

impl<PE: std::fmt::Debug> Matcher<&Element<PE>> for Approx<&Element<PE>>
where
    for<'a> Approx<&'a PE>: Matcher<&'a PE>,
    for<'a> Approx<&'a ClippedRegion<PE>>: Matcher<&'a ClippedRegion<PE>>,
{
    fn matches(&self, obj: &Element<PE>) -> Result<(), String> {
        match (&self.0, obj) {
            (Element::ClippedRegion(obj), Element::ClippedRegion(obj_other)) => {
                Approx(obj).matches(obj_other)
            }
            (Element::Primitive(obj), Element::Primitive(obj_other)) => {
                Approx(obj).matches(obj_other)
            }
            _ => Err(format!("Object type does not match: {:?}, {obj:?}", self.0)),
        }
    }
}

/// Draw from a drawable to an other drawable
pub trait DrawTo<D> {
    /// Draw to an other drawable
    fn draw_to(&self, drawable: &mut D);
}

impl<D: gfx::Draw<gfx::Rectangle>> DrawTo<D> for StyledElement<gfx::Rectangle> {
    fn draw_to(&self, drawable: &mut D) {
        drawable.draw(&self.inner, self.line_style.as_ref(), self.color.as_ref());
    }
}
impl<D: gfx::Draw<gfx::Rectangle>> DrawTo<D> for gfx::Rectangle {
    fn draw_to(&self, drawable: &mut D) {
        drawable.draw(self, Some(&Default::default()), None);
    }
}
impl<D: gfx::Draw<gfx::Circle>> DrawTo<D> for StyledElement<gfx::Circle> {
    fn draw_to(&self, drawable: &mut D) {
        drawable.draw(&self.inner, self.line_style.as_ref(), self.color.as_ref());
    }
}
impl<D: gfx::Draw<gfx::Circle>> DrawTo<D> for gfx::Circle {
    fn draw_to(&self, drawable: &mut D) {
        drawable.draw(self, Some(&Default::default()), None);
    }
}

impl<D: gfx::DrawPolygon<(f64, f64)>> DrawTo<D> for StyledElement<Polygon> {
    fn draw_to(&self, drawable: &mut D) {
        drawable.draw_polygon(
            self.inner.0.iter().cloned(),
            self.line_style.as_ref(),
            self.color.as_ref(),
        );
    }
}
impl<D: gfx::DrawPolygon<(f64, f64)>> DrawTo<D> for Polygon {
    fn draw_to(&self, drawable: &mut D) {
        drawable.draw_polygon(self.0.iter().cloned(), Some(&Default::default()), None);
    }
}
impl<D: gfx::DrawPolygon<gfx::BezierPoint>> DrawTo<D> for StyledElement<Beziergon> {
    fn draw_to(&self, drawable: &mut D) {
        drawable.draw_polygon(
            self.inner.0.iter().cloned(),
            self.line_style.as_ref(),
            self.color.as_ref(),
        );
    }
}
impl<D: gfx::DrawPolygon<gfx::BezierPoint>> DrawTo<D> for Beziergon {
    fn draw_to(&self, drawable: &mut D) {
        drawable.draw_polygon(self.0.iter().cloned(), Some(&Default::default()), None);
    }
}
impl<D: gfx::StrokePath<(f64, f64)>> DrawTo<D> for PolygonalChain {
    fn draw_to(&self, drawable: &mut D) {
        drawable.stroke_path(self.points.iter().cloned(), &self.line_style);
    }
}
impl<D: gfx::StrokePath<(f64, f64)>> DrawTo<D> for Vec<(f64, f64)> {
    fn draw_to(&self, drawable: &mut D) {
        drawable.stroke_path(self.iter().cloned(), &Default::default());
    }
}
impl<D: gfx::StrokePath<gfx::BezierPoint>> DrawTo<D> for BezierCurve {
    fn draw_to(&self, drawable: &mut D) {
        drawable.stroke_path(self.points.iter().cloned(), &self.line_style);
    }
}
impl<D: gfx::StrokePath<gfx::BezierPoint>> DrawTo<D> for Vec<gfx::BezierPoint> {
    fn draw_to(&self, drawable: &mut D) {
        drawable.stroke_path(self.iter().cloned(), &Default::default());
    }
}
impl<D: gfx::DrawText> DrawTo<D> for Text {
    fn draw_to(&self, drawable: &mut D) {
        drawable.draw_text(&self.inner, &self.font);
    }
}
impl<D: gfx::DrawText> DrawTo<D> for UnstyledText {
    fn draw_to(&self, drawable: &mut D) {
        drawable.draw_text(
            &self.inner,
            &gfx::Font {
                family: "",
                size: self.size,
                ..Default::default()
            },
        );
    }
}
impl<D: gfx::PrimitiveDrawable> DrawTo<D> for PrimitiveElement {
    fn draw_to(&self, drawable: &mut D) {
        match self {
            Self::Rectangle(obj) => obj.draw_to(drawable),
            Self::Circle(obj) => obj.draw_to(drawable),
            Self::Polygon(obj) => obj.draw_to(drawable),
            Self::PolygonalChain(obj) => obj.draw_to(drawable),
            Self::Beziergon(obj) => obj.draw_to(drawable),
            Self::BezierCurve(obj) => obj.draw_to(drawable),
            Self::Text(obj) => obj.draw_to(drawable),
        }
    }
}
impl<D: gfx::PrimitiveDrawable> DrawTo<D> for UnstyledPrimitiveElement {
    fn draw_to(&self, drawable: &mut D) {
        match self {
            Self::Rectangle(obj) => obj.draw_to(drawable),
            Self::Circle(obj) => obj.draw_to(drawable),
            Self::Polygon(obj) => obj.draw_to(drawable),
            Self::PolygonalChain(obj) => obj.draw_to(drawable),
            Self::Beziergon(obj) => obj.draw_to(drawable),
            Self::BezierCurve(obj) => obj.draw_to(drawable),
            Self::Text(obj) => obj.draw_to(drawable),
        }
    }
}
impl<D: gfx::Drawable> DrawTo<D> for ClippedRegion<PrimitiveElement> {
    fn draw_to(&self, drawable: &mut D) {
        drawable.clip_polygon(self.polygon.iter().cloned(), |drawable| {
            for p in &self.primitives {
                p.draw_to(drawable)
            }
        });
    }
}
impl<D: gfx::Drawable> DrawTo<D> for ClippedRegion<UnstyledPrimitiveElement> {
    fn draw_to(&self, drawable: &mut D) {
        drawable.clip_polygon(self.polygon.iter().cloned(), |drawable| {
            for p in &self.primitives {
                p.draw_to(drawable)
            }
        });
    }
}
impl<D: gfx::Drawable> DrawTo<D> for Element<PrimitiveElement> {
    fn draw_to(&self, drawable: &mut D) {
        match self {
            Self::Primitive(obj) => obj.draw_to(drawable),
            Self::ClippedRegion(obj) => obj.draw_to(drawable),
        }
    }
}
impl<D: gfx::Drawable> DrawTo<D> for Element<UnstyledPrimitiveElement> {
    fn draw_to(&self, drawable: &mut D) {
        match self {
            Self::Primitive(obj) => obj.draw_to(drawable),
            Self::ClippedRegion(obj) => obj.draw_to(drawable),
        }
    }
}
impl<D: gfx::Drawable> DrawTo<D> for FakeDrawable {
    fn draw_to(&self, drawable: &mut D) {
        for p in &self.elements {
            p.draw_to(drawable)
        }
    }
}
impl<D: gfx::Drawable> DrawTo<D> for UnstyledFakeDrawable {
    fn draw_to(&self, drawable: &mut D) {
        for p in &self.elements {
            p.draw_to(drawable)
        }
    }
}

/// Query extends of an object
pub trait GetExtents {
    /// Determine the extents of an object. Return `None` for empty objects.
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)>;
}

impl GetExtents for StyledElement<gfx::Rectangle> {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        self.inner.get_extents()
    }
}
impl GetExtents for gfx::Rectangle {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        Some((self.x..self.x + self.width, self.y..self.y + self.height))
    }
}
impl GetExtents for StyledElement<gfx::Circle> {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        self.inner.get_extents()
    }
}
impl GetExtents for gfx::Circle {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        Some((
            self.x - self.r..self.x + self.r,
            self.y - self.r..self.y + self.r,
        ))
    }
}

impl GetExtents for StyledElement<Polygon> {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        self.inner.get_extents()
    }
}
impl GetExtents for Polygon {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        get_extents(&self.0)
    }
}
fn get_extents(chain: &[(f64, f64)]) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
    let x_min = chain.iter().map(|(x, _)| *x).reduce(f64::min);
    let x_max = chain.iter().map(|(x, _)| *x).reduce(f64::max);
    let y_min = chain.iter().map(|(_, y)| *y).reduce(f64::min);
    let y_max = chain.iter().map(|(_, y)| *y).reduce(f64::max);
    if let (Some(x_min), Some(x_max), Some(y_min), Some(y_max)) = (x_min, x_max, y_min, y_max) {
        Some((x_min..x_max, y_min..y_max))
    } else {
        None
    }
}
impl GetExtents for StyledElement<Beziergon> {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        self.inner.get_extents()
    }
}
impl GetExtents for Beziergon {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        let chain = self.0.iter().map(|b| b.p).collect::<Vec<_>>();
        get_extents(&chain)
    }
}
impl GetExtents for PolygonalChain {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        get_extents(&self.points)
    }
}
impl GetExtents for Vec<(f64, f64)> {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        get_extents(&self)
    }
}
impl GetExtents for BezierCurve {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        self.points.get_extents()
    }
}
impl GetExtents for Vec<gfx::BezierPoint> {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        let chain = self.iter().map(|b| b.p).collect::<Vec<_>>();
        get_extents(&chain)
    }
}
impl GetExtents for Text {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        Some(get_text_extents(&self.inner, &self.font))
    }
}
fn get_text_extents(
    text: &gfx::Text<String>,
    font: &gfx::Font<String>,
) -> (std::ops::Range<f64>, std::ops::Range<f64>) {
    use gfx::FontMetrics;
    let (w, h) = MONOSPACE_FONT_METRICS.get_text_extents(&text.text, font);
    let x = text.x
        - match text.anchor {
            gfx::TextAnchor::Start => 0.,
            gfx::TextAnchor::Middle => w / 2.,
            gfx::TextAnchor::End => w,
        };
    let y = text.y
        - match text.baseline {
            gfx::Baseline::Alphabetic => 0.,
            gfx::Baseline::Middle => h / 2.,
            gfx::Baseline::Hanging => h,
        };
    (x..x + w, y..y + h)
}
impl GetExtents for UnstyledText {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        Some(get_text_extents(
            &self.inner,
            &gfx::Font {
                size: self.size,
                ..Default::default()
            },
        ))
    }
}
impl GetExtents for PrimitiveElement {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        match self {
            Self::Rectangle(obj) => obj.get_extents(),
            Self::Circle(obj) => obj.get_extents(),
            Self::Polygon(obj) => obj.get_extents(),
            Self::PolygonalChain(obj) => obj.get_extents(),
            Self::Beziergon(obj) => obj.get_extents(),
            Self::BezierCurve(obj) => obj.get_extents(),
            Self::Text(obj) => obj.get_extents(),
        }
    }
}
impl GetExtents for UnstyledPrimitiveElement {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        match self {
            Self::Rectangle(obj) => obj.get_extents(),
            Self::Circle(obj) => obj.get_extents(),
            Self::Polygon(obj) => obj.get_extents(),
            Self::PolygonalChain(obj) => obj.get_extents(),
            Self::Beziergon(obj) => obj.get_extents(),
            Self::BezierCurve(obj) => obj.get_extents(),
            Self::Text(obj) => obj.get_extents(),
        }
    }
}
impl<PE> GetExtents for ClippedRegion<PE> {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        self.polygon.get_extents()
    }
}
impl<PE> GetExtents for Element<PE>
where
    PE: GetExtents,
{
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        match self {
            Self::Primitive(obj) => obj.get_extents(),
            Self::ClippedRegion(obj) => obj.get_extents(),
        }
    }
}
impl<PE: GetExtents> GetExtents for GenericFakeDrawable<PE> {
    fn get_extents(&self) -> Option<(std::ops::Range<f64>, std::ops::Range<f64>)> {
        self.elements.iter().flat_map(|e| e.get_extents()).reduce(
            |(x_range, y_range), (x_range_2, y_range_2)| {
                (
                    f64::min(x_range.start, x_range_2.start)..f64::max(x_range.end, x_range_2.end),
                    f64::min(y_range.start, y_range_2.start)..f64::max(y_range.end, y_range_2.end),
                )
            },
        )
    }
}
