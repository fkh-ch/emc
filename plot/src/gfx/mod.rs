//! 2d graphics abstraction layer - a collection of traits a graphics library has to implement
//! so that xy_chart can draw plots.
//!
//! Both vector graphic libraries and pixel graphic libraries are targeted as implementors.
//! This library already provides an svg implementation (requires the `"svg"` feature):
//! [svg].

#[cfg(feature = "svg")]
pub mod svg;

#[cfg(test)]
pub mod testing;

/// Corresponds to the svg attribute
/// [font-style](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/font-style).
#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub enum FontSlant {
    /// Normal
    #[default]
    Normal,
    /// Italic (slants to the right possibly using different glyphs)
    Italic,
    /// Oblique (slants slightly to the right using the same glyphs)
    Oblique,
}

/// Corresponds to the svg attribute
/// [font-weight](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/font-weight).
/// Not all possible svg values are available here.
#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub enum FontWeight {
    /// Normal font weight
    #[default]
    Normal,
    /// Bold font weight
    Bold,
}

/// Corresponds to the svg attribute
/// [text-anchor](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/text-anchor).
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub enum TextAnchor {
    /// Horizontal alignment at the start
    #[default]
    Start,
    /// Horizontal alignment in the middle
    Middle,
    /// Horizontal alignment at the end
    End,
}

/// Corresponds to the svg attribute
/// [dominant-baseline](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/dominant-baseline).
/// Not all possible svg values are available here.
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub enum Baseline {
    /// Vertical alignment at the bottom
    #[default]
    Alphabetic,
    /// Vertical alignment at the middle
    Middle,
    /// Vertical alignment at the top
    Hanging,
}

/// Font properties including color.
#[derive(Debug, Clone)]
pub struct Font<S> {
    /// Normal / Italic / Oblique
    pub slant: FontSlant,
    /// Normal / Bold
    pub weight: FontWeight,
    /// Font family
    pub family: S,
    /// Font size
    pub size: f64,
    /// Font color
    pub color: Color,
}

impl<S: Default> Default for Font<S> {
    fn default() -> Self {
        Self {
            slant: Default::default(),
            weight: Default::default(),
            family: Default::default(),
            size: 10.,
            color: Color::BLACK,
        }
    }
}

impl Font<&'static str> {
    /// Create a new font with given size
    pub const fn new(size: f64) -> Self {
        Self {
            slant: FontSlant::Normal,
            weight: FontWeight::Normal,
            family: "",
            size,
            color: Color::BLACK,
        }
    }
}
impl Font<String> {
    /// Create a new font with given size
    pub const fn new(size: f64) -> Self {
        Self {
            slant: FontSlant::Normal,
            weight: FontWeight::Normal,
            family: String::new(),
            size,
            color: Color::BLACK,
        }
    }
}

/// RGB color with transparency.
#[derive(Debug, Clone)]
pub struct Color {
    /// Red value between 0 and 1
    pub r: f64,
    /// Green value between 0 and 1
    pub g: f64,
    /// Blue value between 0 and 1
    pub b: f64,
    /// Alpha value between 0 and 1
    pub a: f64,
}

impl Default for Color {
    fn default() -> Self {
        Self::BLACK
    }
}

impl Color {
    /// Create a new [Color] with r, g, b and alpha channel values
    pub const fn rgba(r: f64, g: f64, b: f64, a: f64) -> Self {
        Self { r, g, b, a }
    }
    /// Create a new [Color] with r, g, b, set alpha channel value to 1
    pub const fn rgb(r: f64, g: f64, b: f64) -> Self {
        Self::rgba(r, g, b, 1.0)
    }
    /// White color constant
    pub const WHITE: Self = Self::rgb(1., 1., 1.);
    /// Grey color constant
    pub const GREY: Self = Self::rgb(0.5, 0.5, 0.5);
    /// Black color constant
    pub const BLACK: Self = Self::rgb(0., 0., 0.);
    /// Red color constant
    pub const RED: Self = Self::rgb(1., 0., 0.);
    /// Green color constant
    pub const GREEN: Self = Self::rgb(0., 1., 0.);
    /// Blue color constant
    pub const BLUE: Self = Self::rgb(0., 0., 1.);
    /// Yellow color constant
    pub const YELLOW: Self = Self::rgb(1., 1., 0.);
    /// Fuchsia color constant
    pub const FUCHSIA: Self = Self::rgb(1., 0., 1.);
    /// Aqua color constant
    pub const AQUA: Self = Self::rgb(0., 1., 1.);
}

/// Properties of lines / contours.
#[derive(Clone, Debug)]
pub struct LineStyle {
    /// Line width
    pub width: f64,
    /// Line color
    pub color: Color,
    /// A sequence of arc lengths that specify the lengths of alternating dashes and gaps.
    /// Will be repeated. An empty sequence corresponds to a solid line.
    /// If an odd number of values is provided, then the list of values is repeated to yield
    /// an even number of values. Thus, 5,3,2 is equivalent to 5,3,2,5,3,2.
    /// Corresponds to the
    /// [stroke-dasharray](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/stroke-dasharray)
    /// svg attribute.
    pub dash_lengths: Vec<f64>,
    /// Moves the start and end positions of dashes by arc length.
    pub dash_offset: f64,
}

impl Default for LineStyle {
    fn default() -> Self {
        Self::new(1.0)
    }
}

impl LineStyle {
    /// Create a new [LineStyle] with given width
    pub const fn new(width: f64) -> Self {
        Self::new_with_color(width, Color::BLACK)
    }

    /// Create a new [LineStyle] with given width and color
    pub const fn new_with_color(width: f64, color: Color) -> Self {
        Self {
            width,
            color,
            dash_lengths: Vec::new(),
            dash_offset: 0.,
        }
    }

    /// Change the color of the line style
    pub fn color(self, color: Color) -> Self {
        Self { color, ..self }
    }
}

/// Rectangle - geometry without styling.
#[derive(Debug, Clone)]
pub struct Rectangle {
    /// x coordinate of the left border
    pub x: f64,
    /// y coordinate of the top border
    pub y: f64,
    /// distance between left and right border
    pub width: f64,
    /// distance between top and bottom border
    pub height: f64,
}

/// Circle - geometry without styling.
#[derive(Debug, Clone)]
pub struct Circle {
    /// Center, x coordinate.
    pub x: f64,
    /// Center, y coordinate.
    pub y: f64,
    /// Radius, x coordinate.
    pub r: f64,
}

/// Text - geometry without styling (font size is considered style).
#[derive(Debug, Clone, Default)]
pub struct Text<S: AsRef<str>> {
    /// Anchor point, x coordinate.
    pub x: f64,
    /// Anchor point, y coordinate.
    pub y: f64,
    /// Actual Text
    pub text: S,
    /// Horizontal position of the anchor point within the bounding box.
    pub anchor: TextAnchor,
    /// Vertical position of the anchor point within the bounding box.
    pub baseline: Baseline,
    /// Counter clockwise rotation angle around anchor point.
    pub angle: f64,
}

/// Draw an object to a drawable.
pub trait Draw<T> {
    /// Draw fill and border
    fn draw(&mut self, element: &T, line_style: Option<&LineStyle>, color: Option<&Color>);
    /// Fill the shape
    fn fill(&mut self, element: &T, color: &Color) {
        self.draw(element, None, Some(color));
    }
    /// Draw the border of the shape
    fn stroke(&mut self, element: &T, line_style: &LineStyle) {
        self.draw(element, Some(line_style), None);
    }
}

/// Draw a closed curve. `I` is either a point or a bezier point (point with 2 control points).
pub trait DrawPolygon<I> {
    /// Draw fill and border
    fn draw_polygon(
        &mut self,
        p: impl IntoIterator<Item = I>,
        line_style: Option<&LineStyle>,
        color: Option<&Color>,
    );
    /// Draw the border of the shape
    fn stroke_polygon(&mut self, p: impl IntoIterator<Item = I>, line_style: &LineStyle) {
        self.draw_polygon(p, Some(line_style), None);
    }
    /// Fill the shape
    fn fill_polygon(&mut self, p: impl IntoIterator<Item = I>, color: &Color) {
        self.draw_polygon(p, None, Some(color));
    }
}

/// Draw an unclosed curve. `I` is either a point or a bezier point (point with 2 control points).
pub trait StrokePath<I> {
    /// Draw the curve
    fn stroke_path(&mut self, p: impl IntoIterator<Item = I>, line_style: &LineStyle);
}

/// Collection trait to draw without text
pub trait PrimitiveDrawable:
    Draw<Rectangle>
    + Draw<Circle>
    + DrawPolygon<(f64, f64)>
    + StrokePath<(f64, f64)>
    + DrawPolygon<BezierPoint>
    + StrokePath<BezierPoint>
    + DrawText
    + ClipPolygon
{
}

impl<
        T: Draw<Rectangle>
            + Draw<Circle>
            + DrawPolygon<(f64, f64)>
            + StrokePath<(f64, f64)>
            + DrawPolygon<BezierPoint>
            + StrokePath<BezierPoint>
            + DrawText
            + ClipPolygon,
    > PrimitiveDrawable for T
{
}

/// Collection trait for an object which can be drawn on
pub trait Drawable: PrimitiveDrawable + FontMetrics {}

impl<T: PrimitiveDrawable + FontMetrics> Drawable for T {}

/// Cubic Bezier Point.
/// A Bezier segment $B(t)$ between 2 adjacent Bezier points `start` and `end` is defined by:
/// $B(t) = (1-t)^3 p_{start} + 3 (1-t)^2 t q_{f,start} + 3 (1-t)t^2 q_{b,end} + t^3 p_{end}$.
#[derive(Debug, Clone)]
pub struct BezierPoint {
    /// Point.
    pub p: (f64, f64),
    /// Forward control point.
    pub q_f: (f64, f64),
    /// Backward control point.
    pub q_b: (f64, f64),
}

/// Draw text.
pub trait DrawText {
    /// Draw text using the specified font
    fn draw_text<ST: AsRef<str>, SF: AsRef<str>>(&mut self, text: &Text<ST>, font: &Font<SF>);
}

/// Set a polygonal clip path for a group of draw operations.
pub trait ClipPolygon {
    /// Drawable handling the clipping
    type ClipRegion: Drawable;
    /// Set `polygon` as a clip path for everything which gets drawn by `draw_callback`.
    ///
    /// Implementation may or may not draw for clip polygons with less than 3 points.
    /// If nothing is drawn, implementations shall return `None`. Otherwise they shall
    /// return the return value of the callback.
    fn clip_polygon<R, F: FnOnce(&mut Self::ClipRegion) -> R>(
        &mut self,
        polygon: impl IntoIterator<Item = (f64, f64)>,
        draw_callback: F,
    ) -> Option<R>;
}

/// Predict extents of a text for a given font without drawing.
pub trait FontMetrics {
    /// Determine with and height of `text` if it would be drawn using `font`.
    fn get_text_extents<S: AsRef<str>>(&self, text: &str, font: &Font<S>) -> (f64, f64);
}

/// Simple [FontMetrics] implementation which computes the text extent of
/// a text of n characters with font size `font_size` as `(n*font_size/aspect_ratio) x font_size`
#[derive(Debug, Clone)]
pub struct MonoSpaceFontMetrics {
    /// width : height of a character
    pub aspect_ratio: f64,
}

impl FontMetrics for MonoSpaceFontMetrics {
    fn get_text_extents<S: AsRef<str>>(&self, text: &str, font: &Font<S>) -> (f64, f64) {
        (text.len() as f64 * font.size / self.aspect_ratio, font.size)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_mono_space_font_metrics_returns_correct_extents() {
        let font_metrics = MonoSpaceFontMetrics { aspect_ratio: 2.0 };
        let text_extents = font_metrics.get_text_extents(
            "text",
            &Font {
                family: "",
                size: 20.,
                ..Default::default()
            },
        );
        let expected_text_extents = (40., 20.);
        assert_eq!(text_extents, expected_text_extents);
    }
}
