use clap::Parser;

/// EBL
#[derive(clap::Parser, Debug)]
#[clap(about)]
struct Cli {
    path: std::path::PathBuf,
}

fn main() -> Result<(), String> {
    let opt = Cli::parse();
    let project = ebl::Project::from_json(
        &std::fs::File::open(&opt.path)
            .map_err(|e| format!("Error reading from {}: {e}", opt.path.display()))?,
    )
    .map_err(|e| format!("{e}"))?;
    let base = opt.path.with_extension("");
    let errors = ebl::process(&base, &project).map_err(|e| e.to_string())?;
    for err in errors.into_iter() {
        eprintln!("{err}");
    }
    Ok(())
}
