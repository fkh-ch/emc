//! EBL library
#![warn(missing_docs)]
#![allow(non_snake_case)]
use std::f64::consts::{PI, TAU as TWO_PI};
use std::fmt;

use libemc::EPS_0;
use libemc::{GuardWire, PhasePositions, PhaseSystem};
use numeric::complex::Complex;

pub mod output;
pub mod project;
pub mod sound;
#[cfg(test)]
pub mod test;

pub use project::Project;
use project::TransverseProfile;

/// Error occurring during computations
#[derive(Debug)]
pub enum Error {
    /// 2 conductors are at the same position or at the
    /// position of the mirror charge
    ConductorCollision,
    /// Bundle radius is 0
    ZeroBundleRadius(sound::ZeroBundleRadius),
    /// I/O error
    IO(std::io::Error),
    /// A project contains a system with a number of phases different from 1,2,3
    UnsupportedNumPhases(libemc::UnsupportedNumPhasesError),
    /// A value is out of its domain
    ValueError(&'static str),
}

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::ConductorCollision => write!(
                f,
                "2 conductors are at the same position or at the position of the mirror charge"
            ),
            Self::ZeroBundleRadius(err) => err.fmt(f),
            Self::IO(err) => err.fmt(f),
            Self::UnsupportedNumPhases(err) => err.fmt(f),
            Self::ValueError(msg) => write!(f, "Value out of domain: {msg}"),
        }
    }
}

impl From<sound::ZeroBundleRadius> for Error {
    fn from(value: sound::ZeroBundleRadius) -> Self {
        Self::ZeroBundleRadius(value)
    }
}
impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        Self::IO(value)
    }
}
impl From<libemc::UnsupportedNumPhasesError> for Error {
    fn from(value: libemc::UnsupportedNumPhasesError) -> Self {
        Self::UnsupportedNumPhases(value)
    }
}

/// Multi index identifying a conductor within a project
///
/// Used in the csv output
#[derive(Debug, PartialEq, Clone, Default)]
pub struct ConductorIndex {
    /// Index
    pub system_index: usize,
    /// Index of phase
    pub phase_index: usize,
    /// Index of conductor
    pub conductor_index: usize,
}

/// Single conductor
#[derive(Debug, PartialEq, Clone, Default)]
pub struct Conductor {
    /// Kind of conductor
    pub kind: ConductorType,
    /// Index of system, phase, this conductor is part of
    pub index: ConductorIndex,
    /// Position \[m]
    pub c: [f64; 2],
    /// Mirrored position \[m]
    pub c_m: [f64; 2],
    /// Radius \[m]
    pub r: f64,
    /// Voltage \[V]
    pub U: Complex<f64>,
    /// Complex Current \[A]
    pub I: Complex<f64>,
    /// Complex Charge [C/(2 π ε_0)]
    pub Q: Complex<f64>,
}

/// Kind of a conductor
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub enum ConductorType {
    /// Ordinary conductor
    #[default]
    Conductor,
    /// Grounded conductor
    GuardWire,
    /// Grounded conductor, shielding an ordinary conductor
    Shield,
}

/// Group of conductors (either a phase bundle or a grounded conductor)
#[derive(Debug, Clone, Default, PartialEq)]
pub struct ConductorGroup {
    /// Kind of line
    pub kind: ConductorType,
    /// Index of system and phase of this line
    pub index: ConductorIndex,
    /// Position \[m]
    pub position: [f64; 2],
    /// Radius of a partial conductor of the bundle \[m]
    pub r_cnd: f64,
    /// Radius of the bundle \[m]
    pub r_bundle: f64,
    /// Complex Current \[A]
    pub I: Complex<f64>,
    /// Used to obtain a slice of the vec of all conductors in [ConductorGeometry]
    /// containing the partial conductors of this group
    pub partial_conductors: std::ops::Range<usize>,
}

impl ConductorGroup {
    fn new_guard_wire(position: [f64; 2], r: f64, slot: usize) -> Self {
        Self {
            position,
            index: ConductorIndex {
                system_index: 0,
                phase_index: 0,
                conductor_index: 0,
            },
            kind: ConductorType::GuardWire,
            r_cnd: r,
            r_bundle: 1.,
            I: Default::default(),
            partial_conductors: slot..slot + 1,
        }
    }
}

impl AsRef<ConductorGroup> for ConductorGroup {
    fn as_ref(&self) -> &ConductorGroup {
        self
    }
}

impl Conductor {
    fn new(
        index: ConductorIndex,
        c: [f64; 2],
        c_m: [f64; 2],
        r: f64,
        U: Complex<f64>,
        I: Complex<f64>,
    ) -> Self {
        Self {
            index,
            kind: ConductorType::Conductor,
            c,
            c_m,
            r,
            U,
            I,
            Q: Complex::default(),
        }
    }

    fn new_guard_wire(c: [f64; 2], c_m: [f64; 2], r: f64) -> Self {
        Self {
            index: ConductorIndex {
                system_index: 0,
                phase_index: 0,
                conductor_index: 0,
            },
            kind: ConductorType::GuardWire,
            c,
            c_m,
            r,
            U: Complex::from(0.),
            I: Complex::from(0.),
            Q: Complex::default(),
        }
    }

    fn new_shield(index: ConductorIndex, conductor: &Conductor, r: f64) -> Self {
        Self {
            index,
            kind: ConductorType::Shield,
            c: conductor.c,
            c_m: conductor.c_m,
            r,
            U: Complex::from(0.),
            I: Complex::from(0.),
            Q: Complex::default(),
        }
    }

    fn as_guardwire(&self, R: f64) -> GuardWire {
        GuardWire {
            x: self.c[0],
            y: self.c[1],
            r: self.r,
            R_i: R,
        }
    }
}

/// All conductors / bundles of a project
#[derive(Debug, Default, PartialEq)]
pub struct ConductorGeometry {
    /// Single conductors (with voltage or ground)
    pub conductors: Vec<Conductor>,
    /// Groups of either phase bundles or single guard wires / shields
    pub conductor_groups: Vec<ConductorGroup>,
    /// Resistivity of ground \[Ωm]
    rho: f64,
    /// Frequency for AC conductors \[Hz]
    f: f64,
    /// End indices separating a system within `conductor_groups`
    system_dividers: Vec<usize>,
    /// Influenced conductor parameters needed to compute the current (partially redundant)
    influenced_conductors: Vec<GuardWire>,
    /// Range for the guard wires within the `conducors` vector
    rng_guard_wires: std::ops::Range<usize>,
    /// Range for the shields within the `conducors` vector
    rng_shields: std::ops::Range<usize>,
    /// Range for shields and guard wires within the `conducors` vector
    rng_neutral_conductors: std::ops::Range<usize>,
}

/// Representation of the slope of ground
struct Slope(f64, f64);
impl Slope {
    fn new(angle: f64) -> Self {
        let (dy, dx) = f64::sin_cos(2. * angle);
        Self(dx, dy)
    }
    fn reflect(&self, position: [f64; 2]) -> [f64; 2] {
        let z = Complex::new(position[0], -position[1]) * Complex::new(self.0, self.1);
        [z.re, z.im]
    }
}

impl From<&Project> for ConductorGeometry {
    fn from(e: &Project) -> Self {
        let alpha = Slope::new(e.alpha.as_radians());

        let mut conductors = vec![];
        let mut conductor_groups = vec![];
        let mut shields = vec![];
        let mut influenced_conductors = Vec::new();
        let mut system_dividers = Vec::with_capacity(e.systems.len());
        for (system_index, sys) in (1..).zip(&e.systems) {
            Self::add_system(
                &mut conductors,
                &mut shields,
                &mut conductor_groups,
                sys,
                &alpha,
                e.y_ref,
                system_index,
            );
            system_dividers.push(conductor_groups.len());
        }
        let rng_guard_wires = conductors.len()..conductors.len() + e.guardwires.len();
        for guardwire in &e.guardwires {
            let position = [guardwire.x, guardwire.y + e.y_ref];
            let conductor =
                Conductor::new_guard_wire(position, alpha.reflect(position), guardwire.d / 2.);
            conductor_groups.push(ConductorGroup::new_guard_wire(
                position,
                guardwire.d / 2.,
                conductors.len(),
            ));
            conductors.push(conductor);
            influenced_conductors.push(GuardWire {
                x: position[0],
                y: position[1],
                r: guardwire.d / 2.,
                R_i: guardwire.R_i,
            });
        }
        let rng_shields = conductors.len()..conductors.len() + shields.len();
        for (s, R) in shields.into_iter() {
            influenced_conductors.push(s.as_guardwire(R));
            conductors.push(s);
        }
        let rng_neutral_conductors = rng_guard_wires.start..conductors.len();
        ConductorGeometry {
            rho: e.rho,
            f: e.f_grid,
            influenced_conductors,
            conductors,
            conductor_groups,
            system_dividers,
            rng_guard_wires,
            rng_shields,
            rng_neutral_conductors,
        }
    }
}

impl ConductorGeometry {
    /// View of the guard wires of the conductor geometry
    pub fn guard_wires(&self) -> &[Conductor] {
        &self.conductors[self.rng_guard_wires.clone()]
    }

    /// Compute Q for all conductors
    fn compute_Q(mut self) -> Result<Self, Error> {
        compute_Q(&mut self.conductors)?;
        Ok(self)
    }

    /// Compute I for all conductors
    fn compute_I(mut self) -> Result<Self, Error> {
        if self.system_dividers.is_empty() || self.influenced_conductors.is_empty() {
            return Ok(self);
        }
        if self.rho <= 0. {
            return Err(Error::ValueError("ρ > 0 violated"));
        }
        if self.f <= 0. {
            return Err(Error::ValueError("f > 0 violated"));
        }
        let phase_systems: Vec<_> = divide_range(&self.system_dividers)
            .map(|rng| &self.conductor_groups[rng])
            .map(|system| {
                let positions = PhasePositions::try_collect(system.iter().map(|g| g.position))?;
                let I = system[0].I;
                Ok(PhaseSystem { positions, I })
            })
            .collect::<Result<_, Error>>()?;
        let v_I = libemc::compute_i(
            phase_systems.as_slice(),
            self.influenced_conductors.as_slice(),
            self.rho,
            self.f,
        )
        .map_err(|_| Error::ConductorCollision)?;
        self.conductors[self.rng_neutral_conductors.clone()]
            .iter_mut()
            .zip(v_I)
            .for_each(|(c, I)| c.I = I);
        Ok(self)
    }

    fn add_system(
        conductors: &mut Vec<Conductor>,
        shields: &mut Vec<(Conductor, f64)>,
        conductor_groups: &mut Vec<ConductorGroup>,
        sys: &project::System,
        alpha: &Slope,
        y_E: f64,
        system_index: usize,
    ) {
        let d_alpha = TWO_PI / f64::from(sys.n_t);
        let alpha_0 = sys.beta_s.as_radians();
        let zeta_UI = Complex::from_polar(1., TWO_PI / (sys.phases.len() as f64));
        let mut U = Complex::from_polar(sys.U, sys.phi_U.as_radians())
            / (2. * f64::sin(PI / (sys.phases.len() as f64)));
        let mut I0 = Complex::from_polar(sys.I, sys.phi_I.as_radians());
        let mut I = I0 / f64::from(sys.n_t);
        for (phase_index, phase) in (1..).zip(&sys.phases) {
            let (x0, y0) = (phase.x, phase.y + y_E);
            // D/2 / r = sin(π / n_t)
            let r_bundle = sys.a / 2. / f64::sin(PI / sys.n_t as f64);
            conductor_groups.push(ConductorGroup {
                kind: ConductorType::Conductor,
                index: ConductorIndex {
                    system_index,
                    phase_index,
                    conductor_index: 0,
                },
                position: [x0, y0],
                r_cnd: sys.d / 2.,
                r_bundle,
                partial_conductors: conductors.len()..conductors.len() + sys.n_t as usize,
                I: I0,
            });
            for i in 0..sys.n_t as usize {
                let (e_y, e_x) = f64::sin_cos(alpha_0 + i as f64 * d_alpha);
                let cnd_index = ConductorIndex {
                    system_index,
                    phase_index,
                    conductor_index: i + 1,
                };
                let pos = [x0 + r_bundle * e_x, y0 + r_bundle * e_y];
                let conductor =
                    Conductor::new(cnd_index.clone(), pos, alpha.reflect(pos), sys.d / 2., U, I);
                if sys.shield.enabled {
                    shields.push((
                        Conductor::new_shield(cnd_index, &conductor, sys.shield.d / 2.),
                        sys.shield.R_i,
                    ));
                }
                conductors.push(conductor);
            }
            U *= zeta_UI;
            I *= zeta_UI;
            I0 *= zeta_UI;
        }
    }
}

struct DividerRanges<'a> {
    i_start: usize,
    i_end: std::slice::Iter<'a, usize>,
}

impl Iterator for DividerRanges<'_> {
    type Item = std::ops::Range<usize>;
    fn next(&mut self) -> Option<Self::Item> {
        let i_end = *self.i_end.next()?;
        let i_start = self.i_start;
        self.i_start = i_end;
        Some(i_start..i_end)
    }
}

fn divide_range(dividers: &[usize]) -> DividerRanges<'_> {
    DividerRanges {
        i_start: 0,
        i_end: dividers.iter(),
    }
}

/// Solve U_i = 1/(2π ε_0) Σ Q_j [ln(|c_i - c_j|) - ln(|c_i - M c_j|)]
/// for Q_j/(2π ε_0)
fn compute_Q(conductors: &mut [Conductor]) -> Result<(), Error> {
    if conductors.is_empty() {
        return Ok(());
    }
    let c_len = conductors.len();
    let mut Q: Vec<Complex<f64>> = conductors.iter().map(|c| c.U).collect();
    let mut C: Vec<f64> = vec![0.; c_len * c_len];

    for i in 0..c_len {
        for j in 0..=i {
            let dx = conductors[j].c[0] - conductors[i].c[0];
            let dy = conductors[j].c[1] - conductors[i].c[1];
            let sx = conductors[j].c[0] - conductors[i].c_m[0];
            let sy = conductors[j].c[1] - conductors[i].c_m[1];
            let c_ij = 0.5
                * f64::ln(
                    (sx * sx + sy * sy) / (dx * dx + dy * dy + conductors[j].r * conductors[j].r),
                );
            if c_ij.is_nan() {
                return Err(Error::ConductorCollision);
            }
            C[i * c_len + j] = c_ij;
            C[j * c_len + i] = c_ij;
        }
    }
    // Solve system of equations
    numeric::lu::solve(C.as_mut_slice(), Q.as_mut_slice());
    for i in 0..c_len {
        conductors[i].Q = Q[i];
    }

    Ok(())
}

fn sampling_points(profile: &TransverseProfile, alpha: f64) -> Vec<[f64; 2]> {
    let TransverseProfile {
        m, x_min, x_max, y, ..
    } = *profile;
    let tg = f64::tan(alpha);
    let fx = (x_max - x_min) / (f64::from(m) - 1.);
    (0..m)
        .map(|i| {
            let x = x_min + fx * f64::from(i);
            [x, y + tg * x]
        })
        .collect()
}

/// Complex valued E field
fn compute_E(x: f64, y: f64, conductors: &[Conductor]) -> [Complex<f64>; 2] {
    let mut Ex = Complex::from(0.);
    let mut Ey = Complex::from(0.);

    for cnd in conductors {
        for ([cx, cy], sgn) in &[(cnd.c, 1.), (cnd.c_m, -1.)] {
            let dx = x - cx;
            let dy = y - cy;
            let e0 = sgn / (dx * dx + dy * dy);
            Ex += e0 * dx * cnd.Q;
            Ey += e0 * dy * cnd.Q;
        }
    }
    [Ex, Ey]
}

/// Gradient and Hessian of |E|²
pub fn compute_d_abs_2_e(
    x: f64,
    y: f64,
    conductors: &[Conductor],
) -> numeric::contour_lines::Derivatives {
    // f = \sum q_l (p-c_l) / \|p-c_l\|^2
    // \nabla \|f\|^2 = \nabla <f, f> = 2 Re (f_k \nabla f_k*)
    // (Hess <f, f>)_ij = 2 Re (f_k_ij f_k* + f_k_i f_k_j*)
    // 2Re (fx fx_x* + fy fy_x*)_x = 2Re (fx_x fx_x* + fy_x fy_x* + fx fx_xx* + fy fy_xx*)
    // f_k_i = \delta_ik \sum q_l / \|p-c_l\|^2 - 2 \sum q_l (p_k-c_l_k) (p_i-c_l_i) / \|p-c_l\|^4
    // f_k_ij = - 2 \delta_ik \sum q_l (p_j-c_l_j) / \|p-c_l\|^4
    //          - 2 \delta_jk \sum q_l (p_i-c_l_i) / \|p-c_l\|^4
    //          - 2 \delta_ij \sum q_l (p_k-c_l_k) / \|p-c_l\|^4
    //          + 8 \sum q_l (p_k-c_l_k) (p_i-c_l_i) (p_j-c_l_j) / \|p-c_l\|^6

    // \nabla <f,f> = 2 Re (fx \nabla fx* + fy \nabla fy*)
    // fx_x = \sum q_l / \|p-c_l\|^2 - 2 \sum q_l (x-c_l_x)^2 / \|p-c_l\|^4
    // fx_y = fy_x = - 2 \sum q_l (x-c_l_x)(y-c_l_y) / \|p-c_l\|^4
    // \partial_x <f, f> = 2 Re(fx fx_x* + fy fy_x*)
    // \partial_y <f, f> = 2 Re(fx fx_y* + fy fy_y*)
    //
    // f_x_xx = -6 \sum q_l (x-c_l_x) / \|p-c_l\|^4 + 8 \sum q_l (x-c_l_x)^3 / \|p-c_l\|^6
    // f_x_xy = f_y_xx = -2 \sum q_l (y-c_l_y) / \|p-c_l\|^4 + 8 \sum q_l (x-c_l_x)^2(y-c_l_y) / \|p-c_l\|^6
    // f_x_yy = f_y_xy = -2 \sum q_l (x-c_l_x) / \|p-c_l\|^4 + 8 \sum q_l (x-c_l_x)(y-c_l_y)^2 / \|p-c_l\|^6
    // f_y_yy = -6 \sum q_l (y-c_l_y) / \|p-c_l\|^4 + 8 \sum q_l (y-c_l_y)^3 / \|p-c_l\|^6

    let [fx, fy] = compute_E(x, y, conductors);
    let mut s = Complex::from(0.);
    let mut qx = Complex::from(0.);
    let mut qy = Complex::from(0.);
    let mut qxy = Complex::from(0.);
    let mut qxx = Complex::from(0.);
    let mut qyy = Complex::from(0.);
    let mut qxxx = Complex::from(0.);
    let mut qxxy = Complex::from(0.);
    let mut qxyy = Complex::from(0.);
    let mut qyyy = Complex::from(0.);
    for cnd in conductors {
        for ([cx, cy], sgn) in &[(cnd.c, 1.), (cnd.c_m, -1.)] {
            let dx = x - cx;
            let dy = y - cy;
            let q = sgn * cnd.Q;
            let e0 = 1. / (dx * dx + dy * dy);
            let ee0 = e0 * e0;
            let eee0 = ee0 * e0;
            s += e0 * q;
            qxx += dx * dx * ee0 * q;
            qyy += dy * dy * ee0 * q;
            qxy += dx * dy * ee0 * q;
            qx += dx * ee0 * q;
            qy += dy * ee0 * q;
            qxxx += dx * dx * dx * eee0 * q;
            qxxy += dx * dx * dy * eee0 * q;
            qxyy += dx * dy * dy * eee0 * q;
            qyyy += dy * dy * dy * eee0 * q;
        }
    }
    let fx_x = s - 2. * qxx;
    let fx_y = -2. * qxy;
    let fy_x = fx_y;
    let fy_y = s - 2. * qyy;
    let fx_xx = -6. * qx + 8. * qxxx;
    let fx_xy = -2. * qy + 8. * qxxy;
    let fy_xx = fx_xy;
    let fx_yy = -2. * qx + 8. * qxyy;
    let fy_xy = fx_yy;
    let fy_yy = -6. * qy + 8. * qyyy;

    numeric::contour_lines::Derivatives {
        grad: [
            2. * (fx * fx_x.conj() + fy * fy_x.conj()).re,
            2. * (fx * fx_y.conj() + fy * fy_y.conj()).re,
        ],
        hess_xx: 2.0
            * (fx_xx * fx.conj() + fy_xx * fy.conj() + fx_x * fx_x.conj() + fy_x * fy_x.conj()).re,
        hess_xy: 2.0
            * (fx_xy * fx.conj() + fy_xy * fy.conj() + fx_x * fx_y.conj() + fy_x * fy_y.conj()).re,
        hess_yy: 2.0
            * (fx_yy * fx.conj() + fy_yy * fy.conj() + fx_y * fx_y.conj() + fy_y * fy_y.conj()).re,
    }
}

/// ∇|E|²
fn grad_abs_2_e(x: f64, y: f64, conductors: &[Conductor]) -> [f64; 2] {
    let [fx, fy] = compute_E(x, y, conductors);
    let mut s = Complex::from(0.);
    let mut qxy = Complex::from(0.);
    let mut qxx = Complex::from(0.);
    let mut qyy = Complex::from(0.);
    for cnd in conductors {
        for ([cx, cy], sgn) in &[(cnd.c, 1.), (cnd.c_m, -1.)] {
            let dx = x - cx;
            let dy = y - cy;
            let q = sgn * cnd.Q;
            let e0 = 1. / (dx * dx + dy * dy);
            let ee0 = e0 * e0;
            s += e0 * q;
            qxx += dx * dx * ee0 * q;
            qyy += dy * dy * ee0 * q;
            qxy += dx * dy * ee0 * q;
        }
    }
    let fx_x = s - 2. * qxx;
    let fx_y = -2. * qxy;
    let fy_x = fx_y;
    let fy_y = s - 2. * qyy;

    [
        2. * (fx * fx_x.conj() + fy * fy_x.conj()).re,
        2. * (fx * fx_y.conj() + fy * fy_y.conj()).re,
    ]
}

/// Electric potential (real part)
fn compute_re_u(x: f64, y: f64, conductors: &[Conductor]) -> f64 {
    let mut u = 0.;

    for cnd in conductors {
        let d = f64::hypot(x - cnd.c[0], y - cnd.c[1]);
        let d_m = f64::hypot(x - cnd.c_m[0], y - cnd.c_m[1]);
        u += cnd.Q.re * f64::ln(d / d_m);
    }
    u
}

/// Gradient and Hessian of the electric potential (real part)
pub fn compute_d_re_u(
    x: f64,
    y: f64,
    conductors: &[Conductor],
) -> numeric::contour_lines::Derivatives {
    // U = c \sum q_l ln(|p-c_l|)
    // \nabla U = E = c \sum q_l (p-c_l)/|p-c_l|^2
    // U_xx = c \sum q_l (1/|p-c_l|^2-2(x-x_l)^2/|p-c_l|^4)
    // U_xy = c \sum q_l -2(x-x_l)(y-y_l)/|p-c_l|^4
    // U_yy = c \sum q_l (1/|p-c_l|^2-2(y-y_l)^2/|p-c_l|^4)

    let mut grad = [0.; 2];
    let (mut hess_xx, mut hess_xy, mut hess_yy) = (0., 0., 0.);
    for cnd in conductors {
        let (dx, dy) = (x - cnd.c[0], y - cnd.c[1]);
        let (dx_m, dy_m) = (x - cnd.c_m[0], y - cnd.c_m[1]);
        let (dd, dd_m) = (dx * dx + dy * dy, dx_m * dx_m + dy_m * dy_m);
        grad[0] += cnd.Q.re * (dx / dd - dx_m / dd_m);
        grad[1] += cnd.Q.re * (dy / dd - dy_m / dd_m);
        hess_xx += cnd.Q.re
            * (1. / dd - 2. * dx * dx / (dd * dd) - 1. / dd_m + 2. * dx_m * dx_m / (dd_m * dd_m));
        hess_xy += cnd.Q.re * (-2. * dx * dy / (dd * dd) + 2. * dx_m * dy_m / (dd_m * dd_m));
        hess_yy += cnd.Q.re
            * (1. / dd - 2. * dy * dy / (dd * dd) - 1. / dd_m + 2. * dy_m * dy_m / (dd_m * dd_m));
    }
    numeric::contour_lines::Derivatives {
        grad,
        hess_xx,
        hess_xy,
        hess_yy,
    }
}

/// ∇Re(U)
fn grad_re_u(x: f64, y: f64, conductors: &[Conductor]) -> [f64; 2] {
    let [mut grad_x, mut grad_y] = [0.; 2];
    for cnd in conductors {
        let (dx, dy) = (x - cnd.c[0], y - cnd.c[1]);
        let (dx_m, dy_m) = (x - cnd.c_m[0], y - cnd.c_m[1]);
        let (dd, dd_m) = (dx * dx + dy * dy, dx_m * dx_m + dy_m * dy_m);
        grad_x += cnd.Q.re * (dx / dd - dx_m / dd_m);
        grad_y += cnd.Q.re * (dy / dd - dy_m / dd_m);
    }
    [grad_x, grad_y]
}

/// Electric potential
fn compute_u(x: f64, y: f64, conductors: &[Conductor]) -> Complex<f64> {
    let mut u = Complex::from(0.);

    for cnd in conductors {
        let d = f64::hypot(x - cnd.c[0], y - cnd.c[1]);
        let d_m = f64::hypot(x - cnd.c_m[0], y - cnd.c_m[1]);
        u += cnd.Q * f64::ln(d / d_m);
    }
    u
}

/// Electric potential (squared absolute value)
fn compute_abs_2_u(x: f64, y: f64, conductors: &[Conductor]) -> f64 {
    let u = compute_u(x, y, conductors);
    u.re * u.re + u.im * u.im
}

/// Gradient and Hessian of the electric potential (squared absolute value)
pub fn compute_d_abs_2_u(
    x: f64,
    y: f64,
    conductors: &[Conductor],
) -> numeric::contour_lines::Derivatives {
    // U = c \sum q_l ln(|p-c_l|)
    // \nabla |U|^2 = 2 Re(E \bar{U})
    // |U|^2_xx = 2 Re(Ex_x \bar{U} + |Ex|^2)
    // |U|^2_xy = 2 Re(Ex_y \bar{U} + Ex \bar{Ey})
    // |U|^2_yy = 2 Re(Ey_y \bar{U} + |Ey|^2)

    let u_bar = compute_u(x, y, conductors).conj();
    let mut Ex = Complex::from(0.);
    let mut Ey = Complex::from(0.);
    let mut Ex_x = Complex::from(0.);
    let mut Ex_y = Complex::from(0.);
    let mut Ey_y = Complex::from(0.);
    for cnd in conductors {
        let (dx, dy) = (x - cnd.c[0], y - cnd.c[1]);
        let (dx_m, dy_m) = (x - cnd.c_m[0], y - cnd.c_m[1]);
        let (dd, dd_m) = (dx * dx + dy * dy, dx_m * dx_m + dy_m * dy_m);
        let (e_dx, e_dy) = (dx / dd, dy / dd);
        let (e_dx_m, e_dy_m) = (dx_m / dd_m, dy_m / dd_m);
        Ex += cnd.Q * (e_dx - e_dx_m);
        Ey += cnd.Q * (e_dy - e_dy_m);
        Ex_x += cnd.Q * (1. / dd - 2. * e_dx * e_dx - 1. / dd_m + 2. * e_dx_m * e_dx_m);
        Ex_y += cnd.Q * (-2. * e_dx * e_dy + 2. * e_dx_m * e_dy_m);
        Ey_y += cnd.Q * (1. / dd - 2. * e_dy * e_dy - 1. / dd_m + 2. * e_dy_m * e_dy_m);
    }

    numeric::contour_lines::Derivatives {
        grad: [2.0 * (Ex * u_bar).re, 2.0 * (Ey * u_bar).re],
        hess_xx: 2.0 * ((Ex_x * u_bar).re + (Ex.re * Ex.re + Ex.im * Ex.im)),
        hess_xy: 2.0 * ((Ex_y * u_bar).re + (Ex * Ey.conj()).re),
        hess_yy: 2.0 * ((Ey_y * u_bar).re + (Ey.re * Ey.re + Ey.im * Ey.im)),
    }
}

/// ∇|U|²
fn grad_abs_2_u(x: f64, y: f64, conductors: &[Conductor]) -> [f64; 2] {
    // U = c \sum q_l ln(|p-c_l|)
    // \nabla |U|^2 = 2 Re(E \bar{U})
    // |U|^2_xx = 2 Re(Ex_x \bar{U} + |Ex|^2)
    // |U|^2_xy = 2 Re(Ex_y \bar{U} + Ex \bar{Ey})
    // |U|^2_yy = 2 Re(Ey_y \bar{U} + |Ey|^2)

    let u_bar = compute_u(x, y, conductors).conj();
    let mut Ex = Complex::from(0.);
    let mut Ey = Complex::from(0.);
    for cnd in conductors {
        let (dx, dy) = (x - cnd.c[0], y - cnd.c[1]);
        let (dx_m, dy_m) = (x - cnd.c_m[0], y - cnd.c_m[1]);
        let (dd, dd_m) = (dx * dx + dy * dy, dx_m * dx_m + dy_m * dy_m);
        let (e_dx, e_dy) = (dx / dd, dy / dd);
        let (e_dx_m, e_dy_m) = (dx_m / dd_m, dy_m / dd_m);
        Ex += cnd.Q * (e_dx - e_dx_m);
        Ey += cnd.Q * (e_dy - e_dy_m);
    }

    [2.0 * (Ex * u_bar).re, 2.0 * (Ey * u_bar).re]
}

/// Complex valued B field
fn compute_B(x: f64, y: f64, conductors: &[Conductor]) -> [Complex<f64>; 2] {
    let mut Bx = Complex::from(0.);
    let mut By = Complex::from(0.);
    for cnd in conductors {
        let dx = x - cnd.c[0];
        let dy = y - cnd.c[1];
        let h = 1. / f64::max(dx * dx + dy * dy, cnd.r * cnd.r);
        Bx += -h * dy * cnd.I;
        By += h * dx * cnd.I;
    }

    [0.2 * Bx, 0.2 * By]
}

/// Gradient and Hessian of |B|²
pub fn compute_d_abs_2_b(
    x: f64,
    y: f64,
    conductors: &[Conductor],
) -> numeric::contour_lines::Derivatives {
    // fx = \sum q_l -dy_l / max(\|p-c_l\|^2, r^2)
    // fy = \sum q_l dx_l / max(\|p-c_l\|^2, r^2)
    // fx_x = 2 \sum q_l dx_l dy_l / \|p-c_l\|^4 \chi
    // fx_y = -\sum q_l / max(\|p-c_l\|^2, r^2) + 2 \sum q_l dy_l^2 / \|p-c_l\|^4 \chi
    // fy_x = \sum q_l / max(\|p-c_l\|^2, r^2) - 2 \sum q_l dx_l^2 / \|p-c_l\|^4 \chi
    // fy_y = -2 \sum q_l dx_l dy_l / \|p-c_l\|^4 \chi
    // fy_y = -fx_x
    // fx_xx = 2 \sum q_l dy_l / \|p-c_l\|^4 \chi - 8 \sum q_l dx_l^2 dy_l / \|p-c_l\|^6 \chi
    // fx_xy = 2 \sum q_l dx_l / \|p-c_l\|^4 \chi - 8 \sum q_l dx_l dy_l^2 / \|p-c_l\|^6 \chi
    // fx_yy = 6 \sum q_l dy_l / \|p-c_l\|^4 \chi - 8 \sum q_l dy_l^3 / \|p-c_l\|^6 \chi
    // fy_xx = -6 \sum q_l dx_l / \|p-c_l\|^4 \chi + 8 \sum q_l dx_l^3 / \|p-c_l\|^6 \chi
    // fy_xy = -fx_xx
    // fy_yy = -fx_xy
    let c = 0.2;
    let [fx, fy] = compute_B(x, y, conductors);
    let mut s = Complex::from(0.);
    let mut qx = Complex::from(0.);
    let mut qy = Complex::from(0.);
    let mut qxx = Complex::from(0.);
    let mut qyy = Complex::from(0.);
    let mut qxy = Complex::from(0.);
    let mut qxxx = Complex::from(0.);
    let mut qxxy = Complex::from(0.);
    let mut qxyy = Complex::from(0.);
    let mut qyyy = Complex::from(0.);
    for cnd in conductors {
        let dx = x - cnd.c[0];
        let dy = y - cnd.c[1];
        let q = cnd.I;
        let dd = dx * dx + dy * dy;
        let e0 = 1. / f64::max(dd, cnd.r * cnd.r);
        let ee0 = e0 * e0;
        let eee0 = ee0 * e0;
        s += e0 * q;
        if dd > cnd.r * cnd.r {
            qx += dx * ee0 * q;
            qy += dy * ee0 * q;
            qxx += dx * dx * ee0 * q;
            qyy += dy * dy * ee0 * q;
            qxy += dx * dy * ee0 * q;
            qxxx += dx * dx * dx * eee0 * q;
            qxxy += dx * dx * dy * eee0 * q;
            qxyy += dx * dy * dy * eee0 * q;
            qyyy += dy * dy * dy * eee0 * q;
        }
    }
    let fx_x = c * 2. * qxy;
    let fx_y = c * (-s + 2.0 * qyy);
    let fy_x = c * (s - 2.0 * qxx);
    let fy_y = -fx_x;
    let fx_xx = c * (2. * qy - 8. * qxxy);
    let fx_xy = c * (2. * qx - 8. * qxyy);
    let fx_yy = c * (6. * qy - 8. * qyyy);
    let fy_xx = c * (-6. * qx + 8. * qxxx);
    let fy_xy = -fx_xx;
    let fy_yy = -fx_xy;

    numeric::contour_lines::Derivatives {
        grad: [
            2. * (fx * fx_x.conj() + fy * fy_x.conj()).re,
            2. * (fx * fx_y.conj() + fy * fy_y.conj()).re,
        ],
        hess_xx: 2.0
            * (fx_xx * fx.conj() + fy_xx * fy.conj() + fx_x * fx_x.conj() + fy_x * fy_x.conj()).re,
        hess_xy: 2.0
            * (fx_xy * fx.conj() + fy_xy * fy.conj() + fx_x * fx_y.conj() + fy_x * fy_y.conj()).re,
        hess_yy: 2.0
            * (fx_yy * fx.conj() + fy_yy * fy.conj() + fx_y * fx_y.conj() + fy_y * fy_y.conj()).re,
    }
}

/// ∇|B|²
fn grad_abs_2_b(x: f64, y: f64, conductors: &[Conductor]) -> [f64; 2] {
    let c = 0.2;
    let [fx, fy] = compute_B(x, y, conductors);
    let mut s = Complex::from(0.);
    let mut qxx = Complex::from(0.);
    let mut qyy = Complex::from(0.);
    let mut qxy = Complex::from(0.);
    for cnd in conductors {
        let dx = x - cnd.c[0];
        let dy = y - cnd.c[1];
        let q = cnd.I;
        let dd = dx * dx + dy * dy;
        let e0 = 1. / f64::max(dd, cnd.r * cnd.r);
        let ee0 = e0 * e0;
        s += e0 * q;
        if dd > cnd.r * cnd.r {
            qxx += dx * dx * ee0 * q;
            qyy += dy * dy * ee0 * q;
            qxy += dx * dy * ee0 * q;
        }
    }
    let fx_x = c * 2. * qxy;
    let fx_y = c * (-s + 2.0 * qyy);
    let fy_x = c * (s - 2.0 * qxx);
    let fy_y = -fx_x;

    [
        2. * (fx * fx_x.conj() + fy * fy_x.conj()).re,
        2. * (fx * fx_y.conj() + fy * fy_y.conj()).re,
    ]
}

/// Magnetic vector potential (real part)
fn compute_re_a(x: f64, y: f64, conductors: &[Conductor]) -> f64 {
    let mut a = 0.;
    for cnd in conductors {
        a -= cnd.I.re * f64::ln(f64::hypot(x - cnd.c[0], y - cnd.c[1]));
    }
    0.2 * a
}

/// Gradient and Hessian of the magnetic vector (real part)
pub fn compute_d_re_a(
    x: f64,
    y: f64,
    conductors: &[Conductor],
) -> numeric::contour_lines::Derivatives {
    let c = -0.2;
    let mut grad = [0.; 2];
    let (mut hess_xx, mut hess_xy, mut hess_yy) = (0., 0., 0.);
    for cnd in conductors {
        let (dx, dy) = (x - cnd.c[0], y - cnd.c[1]);
        let dd = dx * dx + dy * dy;
        let dddd = dd * dd;
        grad[0] += cnd.I.re * dx / dd;
        grad[1] += cnd.I.re * dy / dd;
        hess_xx += cnd.I.re * (1. / dd - 2. * dx * dx / dddd);
        hess_xy += cnd.I.re * -2. * dx * dy / dddd;
        hess_yy += cnd.I.re * (1. / dd - 2. * dy * dy / dddd);
    }
    grad[0] *= c;
    grad[1] *= c;
    hess_xx *= c;
    hess_xy *= c;
    hess_yy *= c;

    numeric::contour_lines::Derivatives {
        grad,
        hess_xx,
        hess_xy,
        hess_yy,
    }
}

/// ∇Re(A)
fn grad_re_a(x: f64, y: f64, conductors: &[Conductor]) -> [f64; 2] {
    let c = -0.2;
    let (mut grad_x, mut grad_y) = (0., 0.);
    for cnd in conductors {
        let (dx, dy) = (x - cnd.c[0], y - cnd.c[1]);
        let dd = dx * dx + dy * dy;
        grad_x += cnd.I.re * dx / dd;
        grad_y += cnd.I.re * dy / dd;
    }
    [c * grad_x, c * grad_y]
}

/// Magnetic vector potential
fn compute_a(x: f64, y: f64, conductors: &[Conductor]) -> Complex<f64> {
    let mut a = Complex::from(0.);
    for cnd in conductors {
        a -= cnd.I * f64::ln(f64::hypot(x - cnd.c[0], y - cnd.c[1]));
    }
    0.2 * a
}

/// Magnetic vector potential (squared absolute value)
fn compute_abs_2_a(x: f64, y: f64, conductors: &[Conductor]) -> f64 {
    let a = compute_a(x, y, conductors);
    a.re * a.re + a.im * a.im
}

/// Gradient and Hessian of the magnetic vector potential (squared absolute value)
pub fn compute_d_abs_2_a(
    x: f64,
    y: f64,
    conductors: &[Conductor],
) -> numeric::contour_lines::Derivatives {
    // a = c \sum I_l ln(|p-c_l|)
    // \nabla |a|^2 = 2 Re(B \bar{a})
    // |a|^2_xx = 2 Re(Bx_x \bar{a} + |Bx|^2)
    // |a|^2_xy = 2 Re(Bx_y \bar{a} + Bx \bar{By})
    // |a|^2_yy = 2 Re(By_y \bar{a} + |By|^2)

    let a_bar = compute_a(x, y, conductors).conj();
    let mut Bx = Complex::from(0.);
    let mut By = Complex::from(0.);
    let mut Bx_x = Complex::from(0.);
    let mut Bx_y = Complex::from(0.);
    let mut By_y = Complex::from(0.);
    let c = -0.2;
    for cnd in conductors {
        let (dx, dy) = (x - cnd.c[0], y - cnd.c[1]);
        let dd = dx * dx + dy * dy;
        let (e_dx, e_dy) = (dx / dd, dy / dd);
        Bx += cnd.I * e_dx;
        By += cnd.I * e_dy;
        Bx_x += cnd.I * (1. / dd - 2. * e_dx * e_dx);
        Bx_y += cnd.I * (-2. * e_dx * e_dy);
        By_y += cnd.I * (1. / dd - 2. * e_dy * e_dy);
    }
    Bx *= c;
    By *= c;
    Bx_x *= c;
    Bx_y *= c;
    By_y *= c;

    numeric::contour_lines::Derivatives {
        grad: [2.0 * (Bx * a_bar).re, 2.0 * (By * a_bar).re],
        hess_xx: 2.0 * ((Bx_x * a_bar).re + (Bx.re * Bx.re + Bx.im * Bx.im)),
        hess_xy: 2.0 * ((Bx_y * a_bar).re + (Bx * By.conj()).re),
        hess_yy: 2.0 * ((By_y * a_bar).re + (By.re * By.re + By.im * By.im)),
    }
}

/// ∇|A|²
fn grad_abs_2_a(x: f64, y: f64, conductors: &[Conductor]) -> [f64; 2] {
    let a_bar = compute_a(x, y, conductors).conj();
    let mut Bx = Complex::from(0.);
    let mut By = Complex::from(0.);
    let c = -0.2;
    for cnd in conductors {
        let (dx, dy) = (x - cnd.c[0], y - cnd.c[1]);
        let dd = dx * dx + dy * dy;
        let (e_dx, e_dy) = (dx / dd, dy / dd);
        Bx += cnd.I * e_dx;
        By += cnd.I * e_dy;
    }
    Bx *= c;
    By *= c;

    [2.0 * (Bx * a_bar).re, 2.0 * (By * a_bar).re]
}

fn compute_E_profile(positions: &[[f64; 2]], conductors: &[Conductor]) -> Vec<[Complex<f64>; 2]> {
    positions
        .iter()
        .map(|[x, y]| compute_E(*x, *y, conductors))
        .collect()
}

fn compute_B_profile(positions: &[[f64; 2]], conductors: &[Conductor]) -> Vec<[Complex<f64>; 2]> {
    positions
        .iter()
        .map(|[x, y]| compute_B(*x, *y, conductors))
        .collect()
}

fn compute_transverse_profile(
    geometry: &ConductorGeometry,
    positions: &[[f64; 2]],
    with_e: bool,
    with_b: bool,
) -> Result<output::result_values::TransverseProfileResult, Error> {
    let v_E = if with_e {
        compute_E_profile(positions, &geometry.conductors)
    } else {
        vec![]
    };
    let v_B = if with_b {
        compute_B_profile(positions, &geometry.conductors)
    } else {
        vec![]
    };
    Ok(output::result_values::TransverseProfileResult {
        s: positions.iter().map(|[x, _y]| *x).collect(),
        E: v_E,
        B: v_B,
        L: Default::default(),
    })
}

impl Project {
    /// Compute a project: field strengths, corona sound
    pub fn compute(
        &self,
    ) -> Result<(ConductorGeometry, output::result_values::ResultValues), Error> {
        let geometry = ConductorGeometry::from(self).compute_I()?.compute_Q()?;
        let positions = sampling_points(&self.transverse_profile, self.alpha.as_radians());
        let (conductor_groups, L) = if self.corona_sound.enabled {
            let Umax = self.systems.iter().map(|s| s.U).fold(0., f64::max);
            sound::compute_corona_sound(&geometry, &positions, Umax, &self.corona_sound)?
        } else {
            (
                output::result_values::ConductorGroupResults {
                    group_values: geometry
                        .conductor_groups
                        .iter()
                        .map(|group| output::result_values::ConductorGroupValues {
                            group: group.clone(),
                            E_max: f64::NAN,
                            n: group.partial_conductors.len(),
                            d: 200. * group.r_cnd,
                            A_wc: f64::NAN,
                        })
                        .collect(),
                    ..Default::default()
                },
                Default::default(),
            )
        };
        let results = output::result_values::ResultValues {
            transverse_profile: output::result_values::TransverseProfileResult {
                L,
                ..compute_transverse_profile(
                    &geometry,
                    &positions,
                    self.transverse_profile.columns.with_e_components
                        || self.transverse_profile.columns.with_e_abs
                        || self.transverse_profile.plots.with_e_profile,
                    self.transverse_profile.columns.with_b_components
                        || self.transverse_profile.columns.with_b_abs
                        || self.transverse_profile.plots.with_b_profile,
                )?
            },
            conductor_groups,
            conductors: geometry
                .conductors
                .iter()
                .enumerate()
                .map(|(index, c)| output::result_values::ConductorValues {
                    E_m: sound::e_max(c),
                    conductor: c.clone(),
                    index,
                    // for output, convert to Coulomb, by multiplying with 2 π ε_0
                    Q: c.Q * (TWO_PI * EPS_0),
                })
                .collect(),
        };
        Ok((geometry, results))
    }
}

/// Compute and save output
pub fn process(
    file_base: &std::path::Path,
    project: &Project,
) -> Result<output::plot::PlotErrors, Error> {
    let (geometry, results) = project.compute()?;
    output::csv::write_csv_files(file_base, &results, project)?;
    output::plot::save_2d_figures(file_base, &geometry, project)?;
    output::plot::save_graphs(
        file_base,
        &results.transverse_profile,
        &project.transverse_profile.plots,
    )?;
    if project.transverse_profile.plots.with_conductor_picture {
        let path = output::path::with_suffix(file_base, "_geometry.svg");
        output::plot::plot_conductor_picture(&path, &geometry, project.alpha.as_radians())?;
    }
    Ok(output::plot::PlotErrors::new())
}

/// Absolute value of a ℂ² vector
pub fn c2_abs(z: &[Complex<f64>; 2]) -> f64 {
    let a = c2_abs_2(z).sqrt();
    if a.is_nan() {
        f64::INFINITY
    } else {
        a
    }
}

/// Absolute value squared of a ℂ² vector
pub fn c2_abs_2(z: &[Complex<f64>; 2]) -> f64 {
    let a = z[0].re.powi(2) + z[0].im.powi(2) + z[1].re.powi(2) + z[1].im.powi(2);
    if a.is_nan() {
        f64::INFINITY
    } else {
        a
    }
}

/// Real part of a ℂ² vector
pub fn c2_re(z: &[Complex<f64>; 2]) -> [f64; 2] {
    [z[0].re, z[1].re]
}
