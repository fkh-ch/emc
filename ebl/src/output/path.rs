//! Path manipulation

/// Append a suffix to a path
pub fn with_suffix(base: &std::path::Path, suffix: &str) -> std::path::PathBuf {
    let mut stem = base.file_name().unwrap_or_default().to_os_string();
    stem.push(suffix);
    base.with_file_name(stem)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_with_suffix() {
        let base = std::path::PathBuf::from("/a/b/c");
        let expected_path = std::path::PathBuf::from("/a/b/c_suffix.ext");
        assert_eq!(with_suffix(&base, "_suffix.ext"), expected_path);
    }
}
