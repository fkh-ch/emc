//! svg export
use numeric::complex::Complex;
use plot::{gfx, xy_chart};

use crate::output::{path::with_suffix, result_values::TransverseProfileResult};
use crate::project::{self, CombinedPlot, Figure, Project, WebColor};
use crate::{self as core, c2_abs, c2_abs_2, c2_re, Conductor, ConductorGeometry};

type Font = gfx::Font<&'static str>;

const NOT_PERIODIC: (
    xy_chart::periodicity::NotPeriodic,
    xy_chart::periodicity::NotPeriodic,
) = (
    xy_chart::periodicity::NotPeriodic,
    xy_chart::periodicity::NotPeriodic,
);

type Singularities = (Vec<(f64, f64)>, Vec<(f64, f64)>);

/// Templates for one field (E/B) for contour lines and field lines
#[derive(Debug)]
struct FigureTemplate {
    name: &'static str,
    contour_lines: &'static ContourLinePlotTemplate,
    field_lines: &'static FieldLinePlotTemplate,
}

/// Template for one field (E/B) for contour lines
#[derive(Debug)]
struct ContourLinePlotTemplate {
    name: &'static str,
    /// Unit of the quantity
    unit: &'static str,
    /// Quantity
    f: fn(f64, f64, &[Conductor]) -> f64,
    /// Compute gradient and Hessian of the quantity
    d_f: fn(f64, f64, &[Conductor]) -> [f64; 2],
    /// Determine the clip polygon for given region in pixel space
    /// (region for B field / region above ground line for E field)
    make_clip: fn(&xy_chart::DataRegion, f64) -> Vec<(f64, f64)>,
    map_level: fn(f64) -> f64,
}

/// Template for one field (E/B) for field lines
#[derive(Debug)]
struct FieldLinePlotTemplate {
    unit: &'static str,
    /// Evaluate ℂ² valued vector field for given conductors
    f: fn(f64, f64, &[Conductor]) -> [Complex<f64>; 2],
    singularity_collector: fn(&[Conductor]) -> Singularities,
    /// Determine the clip polygon for given region in pixel space
    /// (region for B field / region above ground line for E field)
    make_clip: fn(&xy_chart::DataRegion, f64) -> Vec<(f64, f64)>,
}

/// Figure template namespace
mod fig {
    use super::*;

    pub mod field_lines {
        use super::*;
        pub const E: FieldLinePlotTemplate = FieldLinePlotTemplate {
            unit: "V/m",
            f: core::compute_E,
            singularity_collector: collect_singularities_e,
            make_clip: make_clip_above_ground_line,
        };

        pub const B: FieldLinePlotTemplate = FieldLinePlotTemplate {
            unit: "µT",
            f: core::compute_B,
            singularity_collector: collect_singularities_b,
            make_clip: |region, _| xy_chart::clipped_plot::to_polygon(region).to_vec(),
        };
    }

    pub mod contour_lines {
        use super::*;
        pub const E_ABS: ContourLinePlotTemplate = ContourLinePlotTemplate {
            name: "E",
            unit: "V/m",
            f: |x, y, conductors| c2_abs_2(&core::compute_E(x, y, conductors)),
            d_f: core::grad_abs_2_e,
            make_clip: make_clip_above_ground_line,
            map_level: |l| l * l,
        };

        pub const U_RE: ContourLinePlotTemplate = ContourLinePlotTemplate {
            name: "RE_U",
            unit: "V",
            f: core::compute_re_u,
            d_f: core::grad_re_u,
            make_clip: make_clip_above_ground_line,
            map_level: |l| l,
        };

        pub const U_ABS: ContourLinePlotTemplate = ContourLinePlotTemplate {
            name: "U",
            unit: "V",
            f: core::compute_abs_2_u,
            d_f: core::grad_abs_2_u,
            make_clip: make_clip_above_ground_line,
            map_level: |l| l * l,
        };

        pub const B_ABS: ContourLinePlotTemplate = ContourLinePlotTemplate {
            name: "B",
            unit: "µT",
            f: |x, y, conductors| c2_abs_2(&core::compute_B(x, y, conductors)),
            d_f: core::grad_abs_2_b,
            make_clip: |region, _| xy_chart::clipped_plot::to_polygon(region).to_vec(),
            map_level: |l| l * l,
        };

        pub const A_RE: ContourLinePlotTemplate = ContourLinePlotTemplate {
            name: "RE_A",
            unit: "µTm",
            f: core::compute_re_a,
            d_f: core::grad_re_a,
            make_clip: |region, _| xy_chart::clipped_plot::to_polygon(region).to_vec(),
            map_level: |l| l,
        };

        pub const A_ABS: ContourLinePlotTemplate = ContourLinePlotTemplate {
            name: "A",
            unit: "µTm",
            f: |x, y, conductors| core::compute_abs_2_a(x, y, conductors),
            d_f: core::grad_abs_2_a,
            make_clip: |region, _| xy_chart::clipped_plot::to_polygon(region).to_vec(),
            map_level: |l| l * l,
        };
    }

    /// Templates for Re(U)
    pub const U_RE: FigureTemplate = FigureTemplate {
        name: contour_lines::U_RE.name,
        contour_lines: &contour_lines::U_RE,
        field_lines: &field_lines::E,
    };
    /// Templates for Re(A)
    pub const A_RE: FigureTemplate = FigureTemplate {
        name: contour_lines::A_RE.name,
        contour_lines: &contour_lines::A_RE,
        field_lines: &field_lines::B,
    };
}

/// Assemble multiple plots to a figure
trait AssembleFigure {
    /// Single or dual plot
    type PlotParameters;
    /// Either () or a `Result`
    type ReturnType;

    /// Assemble the core figure from either one or 2 plots
    fn assemble_figure(
        &self,
        parameters: &Self::PlotParameters,
        geometry: &ConductorGeometry,
        alpha: f64,
        line_width: f64,
    ) -> impl xy_chart::PlotTo<ReturnType = Self::ReturnType> + xy_chart::legend::ExtendLegendEntries;

    fn save(
        &self,
        parameters: &Figure<Self::PlotParameters>,
        path: &std::path::Path,
        geometry: &ConductorGeometry,
        alpha: f64,
    ) -> std::io::Result<Self::ReturnType> {
        let line_width = parameters.width * LINE_WIDTH_RATIO;
        let xy_interval =
            xy_chart::XyInterval::new((&parameters.x_range).into(), (&parameters.y_range).into());
        let height = parameters.width / xy_interval.x.length() * xy_interval.y.length();
        let font_size = height * FONT_RATIO;
        let label_font_size = font_size * 1.25;
        let chart = xy_chart::XyChart {
            x_axis: xy_chart::Axis::new_from_interval(xy_interval.x)
                .with_label("x [m]")
                .with_font(Font::new(label_font_size), Font::new(font_size)),
            y_axis: xy_chart::Axis::new_from_interval(xy_interval.y)
                .with_label("y [m]")
                .with_font(Font::new(label_font_size), Font::new(font_size)),
            grid_line_style: Some(gfx::LineStyle::new(line_width)),
            sub_grid_line_style: parameters
                .with_subgrid
                .then(|| gfx::LineStyle::new_with_color(0.5 * line_width, gfx::Color::GREY)),
            line_width,
            ..Default::default()
        };
        let plots = xy_chart::plot_stack::PlotStack(
            self.assemble_figure(&parameters.plots, geometry, alpha, line_width),
            xy_chart::plot_stack::PlotStack(
                prepare_ground_line_plot(&chart.x_axis.interval, &chart.y_axis.interval, alpha),
                prepare_conductor_plot(|| geometry.conductors.iter().map(|c| c.c), line_width),
            ),
        );
        let legend =
            xy_chart::legend::Legend::empty_with_font(Font::new(font_size)).add_entries(&plots);

        let mut canvas = gfx::svg::SvgDrawable::new();
        let plot = chart
            .prepare_layout(xy_chart::AspectRatio::Unconstrained, &canvas)
            .with_axes_extents(parameters.width, height)
            .draw_axes(&mut canvas);
        let result = plot.try_plot(&mut canvas, &plots);
        let plot = plot.draw(&mut canvas, &legend.prepare_one_column_layout().top_right());
        let mut file = std::fs::File::create(path)?;
        canvas.write(&mut file, plot.width(), plot.height())?;
        Ok(result)
    }
}

/// Collection of errors happening during plotting
pub type PlotErrors = Vec<numeric::odeint::Error>;

impl AssembleFigure for FigureTemplate {
    type PlotParameters = CombinedPlot;
    type ReturnType = Result<(), PlotErrors>;

    fn assemble_figure(
        &self,
        parameters: &Self::PlotParameters,
        geometry: &ConductorGeometry,
        alpha: f64,
        line_width: f64,
    ) -> impl xy_chart::PlotTo<ReturnType = Self::ReturnType> + xy_chart::legend::ExtendLegendEntries
    {
        let conductors = &geometry.conductors;
        let field_line_plot = parameters.field_lines.enabled.then(|| {
            prepare_field_line_plot(
                self.field_lines,
                parameters.field_lines.n_lines_at_singularity,
                parameters.field_lines.r_singularity,
                geometry,
                move |_t, [x, y]: [f64; 2]| c2_re(&(self.field_lines.f)(x, y, conductors)),
                move |region: &xy_chart::DataRegion| (self.field_lines.make_clip)(region, alpha),
                line_width,
            )
        });
        let contour_line_plot = parameters.contour_lines.enabled.then(|| {
            let template = self.contour_lines;
            let levels: Vec<_> = parameters.contour_lines.levels().collect();
            prepare_contour_line_plot(
                template,
                &levels,
                parameters.contour_lines.resolution,
                move |x, y| (template.f)(x, y, conductors),
                move |x, y| (template.d_f)(x, y, conductors),
                move |region: &xy_chart::DataRegion| (template.make_clip)(region, alpha),
                line_width,
            )
        });
        xy_chart::plot_stack::PlotStack(field_line_plot, contour_line_plot)
    }
}

impl AssembleFigure for ContourLinePlotTemplate {
    type PlotParameters = project::SinglePlot;
    type ReturnType = ();
    fn assemble_figure(
        &self,
        parameters: &Self::PlotParameters,
        geometry: &ConductorGeometry,
        alpha: f64,
        line_width: f64,
    ) -> impl xy_chart::PlotTo<ReturnType = Self::ReturnType> + xy_chart::legend::ExtendLegendEntries
    {
        let conductors = &geometry.conductors;
        let parameters = &parameters.contour_lines;

        parameters.enabled.then(|| {
            let levels: Vec<_> = parameters.levels().collect();
            prepare_contour_line_plot(
                self,
                &levels,
                parameters.resolution,
                move |x, y| (self.f)(x, y, conductors),
                move |x, y| (self.d_f)(x, y, conductors),
                move |region: &xy_chart::DataRegion| (self.make_clip)(region, alpha),
                line_width,
            )
        })
    }
}

/// Save the E and B graphs as defined in a [Project]
pub fn save_graphs(
    file_base: &std::path::Path,
    profile: &TransverseProfileResult,
    options: &crate::project::TransverseProfilePlots,
) -> std::io::Result<()> {
    if options.with_e_profile && !profile.E.is_empty() {
        let path = with_suffix(file_base, "_E_profile.svg");
        plot_graph(&path, &profile.s, &profile.E, "E [V/m]")?;
    }
    if options.with_b_profile && !profile.B.is_empty() {
        let path = with_suffix(file_base, "_B_profile.svg");
        plot_graph(&path, &profile.s, &profile.B, "B [µT]")?;
    }
    Ok(())
}

/// Save contour line plots / field line plots
pub fn save_2d_figures(
    file_base: &std::path::Path,
    geometry: &ConductorGeometry,
    project: &Project,
) -> std::io::Result<PlotErrors> {
    // Contour lines only:
    let figures = [
        (&fig::contour_lines::E_ABS, &project.figures.e),
        (&fig::contour_lines::B_ABS, &project.figures.b),
        (&fig::contour_lines::U_ABS, &project.figures.u),
        (&fig::contour_lines::A_ABS, &project.figures.a),
    ];
    for (template, figure) in figures.into_iter() {
        if figure.plots.contour_lines.enabled {
            template.save(
                figure,
                &with_suffix(file_base, &format!("_{}.svg", template.name)),
                geometry,
                project.alpha.as_radians(),
            )?;
        }
    }
    // Contour lines and field lines:
    let figures = [
        (&fig::U_RE, &project.figures.u_re),
        (&fig::A_RE, &project.figures.a_re),
    ];
    let mut errors = Vec::new();
    for (template, figure) in figures.into_iter() {
        if figure.plots.contour_lines.enabled || figure.plots.field_lines.enabled {
            if let Err(fig_errors) = template.save(
                figure,
                &with_suffix(file_base, &format!("_{}.svg", template.name)),
                geometry,
                project.alpha.as_radians(),
            )? {
                errors.extend(fig_errors);
            };
        }
    }
    Ok(errors)
}

fn graph_line_style(width: f64) -> gfx::LineStyle {
    gfx::LineStyle::new_with_color(width, gfx::Color::rgb(0., 0.75, 0.))
}

fn marker_style(line_width: f64) -> xy_chart::marker::Marker {
    xy_chart::marker::Marker {
        shape: xy_chart::marker::MarkerShape::OTIMES,
        size: 10.,
        color: None,
        line_style: Some(gfx::LineStyle::new_with_color(
            0.5 * line_width,
            gfx::Color::BLACK,
        )),
    }
}

fn marker_style_conductor(line_width: f64) -> xy_chart::marker::Marker {
    xy_chart::marker::Marker {
        shape: xy_chart::marker::MarkerShape::TIMES,
        size: 10.,
        color: None,
        line_style: Some(gfx::LineStyle::new_with_color(
            2.0 * line_width,
            gfx::Color::RED,
        )),
    }
}

fn marker_style_guard_wire(line_width: f64) -> xy_chart::marker::Marker {
    xy_chart::marker::Marker {
        shape: xy_chart::marker::MarkerShape::DIAMOND,
        size: 12.,
        color: Some(gfx::Color::BLACK),
        line_style: Some(gfx::LineStyle::new_with_color(
            2.0 * line_width,
            gfx::Color::WHITE,
        )),
    }
}

fn marker_style_shield(line_width: f64) -> xy_chart::marker::Marker {
    xy_chart::marker::Marker {
        shape: xy_chart::marker::MarkerShape::CIRCLE,
        size: 12.,
        color: None,
        line_style: Some(gfx::LineStyle::new_with_color(
            2.0 * line_width,
            gfx::Color::GREEN,
        )),
    }
}

fn format_complex(z: &Complex<f64>) -> String {
    format!(
        "{abs:.1} A / {arg:.2} °",
        abs = z.norm(),
        arg = z.arg().to_degrees()
    )
}

/// Scatter plot of conductors positions
pub fn plot_conductor_picture(
    path: &std::path::Path,
    geometry: &ConductorGeometry,
    alpha: f64,
) -> std::io::Result<()> {
    let conductors = |kind| {
        geometry
            .conductors
            .iter()
            .filter_map(move |cnd| (cnd.kind == kind).then_some(cnd.c))
    };
    let bundles = || {
        geometry
            .conductor_groups
            .iter()
            .filter(|g| g.kind == crate::ConductorType::Conductor)
            .map(|bundle| bundle.position)
    };
    let bounding_box = xy_chart::XyInterval::new_fitting_points(
        geometry.conductors.iter().map(|cnd| cnd.c).chain(bundles()),
    );
    let conductor_labels = |kind| {
        geometry
            .conductors
            .iter()
            .filter_map(move |cnd| (cnd.kind == kind).then_some(Some(format_complex(&cnd.I))))
    };
    let (x_axis, y_axis) =
        xy_chart::Axis::new_pair_covering_interval_with_equal_units(&bounding_box);
    const DEFAULT_WIDTH: f64 = 1024.0;
    let font_size = DEFAULT_WIDTH * FONT_RATIO;
    let line_width = DEFAULT_WIDTH * LINE_WIDTH_RATIO;
    let mut canvas = gfx::svg::SvgDrawable::new();
    let chart = xy_chart::XyChart {
        x_axis: x_axis
            .with_label("x [m]")
            .with_font(Font::new(1.25 * font_size), Font::new(font_size)),
        y_axis: y_axis
            .with_label("y [m]")
            .with_font(Font::new(1.25 * font_size), Font::new(font_size)),
        line_width,
        grid_line_style: Some(gfx::LineStyle::new(line_width)),
        ..Default::default()
    };
    let plot = chart.prepare_layout(xy_chart::AspectRatio::AnglePreserving, &canvas);
    let height = plot.natural_height_for_width(DEFAULT_WIDTH);
    plot.fit_into(DEFAULT_WIDTH, height)
        .draw_axes(&mut canvas)
        .plot(
            &mut canvas,
            &xy_chart::scatter_plot::ScatterPlot {
                coordinates: || conductors(crate::ConductorType::Conductor),
                marker_style: Some(marker_style_conductor(line_width)),
                labels: xy_chart::scatter_plot::NoLabels,
                periodicity: NOT_PERIODIC,
            },
        )
        .plot(
            &mut canvas,
            &xy_chart::scatter_plot::ScatterPlot {
                coordinates: || conductors(crate::ConductorType::GuardWire),
                marker_style: Some(marker_style_guard_wire(line_width)),
                periodicity: NOT_PERIODIC,
                labels: xy_chart::scatter_plot::ScatterPlotLabels::<_, &'static str> {
                    labels: || conductor_labels(crate::ConductorType::GuardWire),
                    font: Font::new(font_size),
                    offset_angle: std::f64::consts::FRAC_PI_4,
                },
            },
        )
        .plot(
            &mut canvas,
            &xy_chart::scatter_plot::ScatterPlot {
                coordinates: || conductors(crate::ConductorType::Shield),
                marker_style: Some(marker_style_shield(line_width)),
                periodicity: NOT_PERIODIC,
                labels: xy_chart::scatter_plot::ScatterPlotLabels::<_, &'static str> {
                    labels: || conductor_labels(crate::ConductorType::Shield),
                    font: Font::new(font_size),
                    offset_angle: std::f64::consts::FRAC_PI_4,
                },
            },
        )
        .plot(
            &mut canvas,
            &xy_chart::scatter_plot::ScatterPlot {
                coordinates: bundles,
                marker_style: None,
                periodicity: NOT_PERIODIC,
                labels: xy_chart::scatter_plot::ScatterPlotLabels::<_, &'static str> {
                    labels: || {
                        geometry
                            .conductor_groups
                            .iter()
                            .map(move |bundle| Some(format_complex(&bundle.I)))
                    },
                    font: Font::new(font_size),
                    offset_angle: std::f64::consts::FRAC_PI_4,
                },
            },
        )
        .plot(
            &mut canvas,
            &prepare_ground_line_plot(&chart.x_axis.interval, &chart.y_axis.interval, alpha),
        );
    let mut file = std::fs::File::create(path)?;
    canvas.write(&mut file, DEFAULT_WIDTH, height)
}

/// Font size : image size
const FONT_RATIO: f64 = 1. / 50.;
/// Line width : image size
const LINE_WIDTH_RATIO: f64 = 1. / 500.;

impl<'a> From<&'a WebColor> for gfx::Color {
    fn from(value: &'a WebColor) -> Self {
        Self::rgb(
            value.r as f64 / 255.,
            value.g as f64 / 255.,
            value.b as f64 / 255.,
        )
    }
}

fn intersect_ground_line_with_box(
    alpha: f64,
    x_interval: &xy_chart::Interval,
    y_interval: &xy_chart::Interval,
) -> Option<[[f64; 2]; 2]> {
    let tan_alpha = f64::tan(alpha);
    // y = tan_alpha * x
    let y_0 = tan_alpha * x_interval.min;
    let y_1 = tan_alpha * x_interval.max;
    if (y_0 < y_interval.min && y_1 < y_interval.min)
        || (y_0 > y_interval.max && y_1 > y_interval.max)
    {
        return None;
    }
    let p_0 = if (y_interval.min..y_interval.max).contains(&y_0) {
        [x_interval.min, y_0]
    } else if y_0 < y_interval.min {
        [y_interval.min / tan_alpha, y_interval.min]
    } else {
        [y_interval.max / tan_alpha, y_interval.max]
    };
    let p_1 = if (y_interval.min..y_interval.max).contains(&y_1) {
        [x_interval.max, y_1]
    } else if y_1 < y_interval.min {
        [y_interval.min / tan_alpha, y_interval.min]
    } else {
        [y_interval.max / tan_alpha, y_interval.max]
    };
    Some([p_0, p_1])
}

fn make_clip_above_ground_line(data_region: &xy_chart::DataRegion, alpha: f64) -> Vec<(f64, f64)> {
    let polygon = intersect_box_with_line(
        alpha,
        &data_region.xy_interval.x,
        &data_region.xy_interval.y,
    );
    let t = &data_region.chart_to_canvas;
    polygon.into_iter().map(|p| t.apply(p)).collect()
}

fn intersect_box_with_line(
    alpha: f64,
    x_interval: &xy_chart::Interval,
    y_interval: &xy_chart::Interval,
) -> Vec<[f64; 2]> {
    let bounding_box_polygon = vec![
        [x_interval.max, y_interval.min],
        [x_interval.max, y_interval.max],
        [x_interval.min, y_interval.max],
        [x_interval.min, y_interval.min],
    ];
    let Some(line) = intersect_ground_line_with_box(alpha, x_interval, y_interval) else {
        return bounding_box_polygon;
    };
    let [n_x, n_y] = [line[0][1] - line[1][1], -(line[0][0] - line[1][0])];
    let [x0, y0] = line[0];
    line.into_iter()
        .chain(
            bounding_box_polygon
                .into_iter()
                .filter(|[x, y]| (x - x0) * n_x + (y - y0) * n_y > 0.),
        )
        .collect()
}

type ContourLinePlot<F, DF, C> = xy_chart::legend::WithLegendEntries<
    Vec<xy_chart::legend::LegendEntry>,
    xy_chart::clipped_plot::ClippedPlot<xy_chart::contour_line_plot::ContourLinePlot<F, DF>, C>,
>;
fn prepare_contour_line_plot<F, DF, C>(
    template: &ContourLinePlotTemplate,
    levels: &[(f64, WebColor)],
    resolution: f64,
    f: F,
    d_f: DF,
    make_clip: C,
    line_width: f64,
) -> ContourLinePlot<F, DF, C> {
    const EPS: f64 = 1.0e-5;
    use plot::xy_chart::clipped_plot::ClippedAt;
    xy_chart::legend::WithLegendEntries(
        levels
            .iter()
            .map(|(value, color)| xy_chart::legend::LegendEntry {
                title: format!("{} {}", value, template.unit),
                line_style: Some(gfx::LineStyle::new_with_color(line_width, color.into())),
                marker_style: None,
            })
            .collect::<Vec<_>>(),
        xy_chart::contour_line_plot::ContourLinePlot {
            f,
            d_f,
            levels: levels
                .iter()
                .map(
                    |(value, color)| xy_chart::contour_line_plot::ContourLineLevel {
                        value: (template.map_level)(*value),
                        style: gfx::LineStyle::new_with_color(line_width, color.into()),
                    },
                )
                .collect(),
            tolerance: EPS,
            resolution,
        }
        .clipped_at(make_clip),
    )
}

type FieldLinePlot<F, C> = xy_chart::legend::WithLegendEntriesFromPlotWithTitle<
    &'static str,
    xy_chart::clipped_plot::ClippedPlot<xy_chart::field_line_plot::FieldLinePlot<F>, C>,
>;
fn prepare_field_line_plot<F, C>(
    template: &'static FieldLinePlotTemplate,
    n_lines_at_singularity: usize,
    r_singularity: f64,
    geometry: &ConductorGeometry,
    f: F,
    make_clip: C,
    line_width: f64,
) -> FieldLinePlot<F, C> {
    let (source_singularities, sink_singularities) =
        (template.singularity_collector)(&geometry.conductors);
    use plot::xy_chart::clipped_plot::ClippedAt;
    xy_chart::legend::WithLegendEntriesFromPlotWithTitle(
        template.unit,
        xy_chart::field_line_plot::FieldLinePlot {
            f,
            source_singularities,
            sink_singularities,
            n_lines_at_singularity,
            r_singularity,
            line_style: gfx::LineStyle::new(line_width),
        }
        .clipped_at(make_clip),
    )
}

type GroundLinePlot = xy_chart::line_plot::LinePlot<
    [[f64; 2]; 2],
    xy_chart::periodicity::NotPeriodic,
    xy_chart::periodicity::NotPeriodic,
>;

fn prepare_ground_line_plot(
    x_interval: &xy_chart::Interval,
    y_interval: &xy_chart::Interval,
    alpha: f64,
) -> Option<xy_chart::legend::NoLegendEntry<GroundLinePlot>> {
    intersect_ground_line_with_box(alpha, x_interval, y_interval).map(|coordinates| {
        xy_chart::legend::NoLegendEntry(xy_chart::line_plot::LinePlot {
            coordinates,
            line_style: Default::default(),
            periodicity: NOT_PERIODIC,
        })
    })
}

fn prepare_conductor_plot<I>(
    conductors: I,
    line_width: f64,
) -> xy_chart::legend::NoLegendEntry<
    xy_chart::scatter_plot::ScatterPlot<
        I,
        xy_chart::scatter_plot::NoLabels,
        xy_chart::periodicity::NotPeriodic,
        xy_chart::periodicity::NotPeriodic,
    >,
> {
    xy_chart::legend::NoLegendEntry(xy_chart::scatter_plot::ScatterPlot {
        coordinates: conductors,
        marker_style: Some(marker_style(line_width)),
        labels: xy_chart::scatter_plot::NoLabels,
        periodicity: NOT_PERIODIC,
    })
}

fn collect_singularities_e(conductors: &[Conductor]) -> Singularities {
    let mut source_singularities = Vec::new();
    let mut sink_singularities = Vec::new();
    for conductor in conductors {
        if conductor.Q.re > 0. {
            source_singularities.push(conductor.c.into());
            sink_singularities.push(conductor.c_m.into());
        } else {
            sink_singularities.push(conductor.c.into());
            source_singularities.push(conductor.c_m.into());
        }
    }
    (source_singularities, sink_singularities)
}

fn collect_singularities_b(conductors: &[Conductor]) -> Singularities {
    let mut source_singularities = Vec::new();
    let mut sink_singularities = Vec::new();
    for conductor in conductors {
        if conductor.I.re > 0. {
            source_singularities.push(conductor.c.into());
        } else {
            sink_singularities.push(conductor.c.into());
        }
    }
    (source_singularities, sink_singularities)
}

fn plot_graph(
    path: &std::path::Path,
    x: &[f64],
    f: &[[Complex<f64>; 2]],
    y_label: &str,
) -> std::io::Result<()> {
    let graph = x
        .iter()
        .zip(f.iter().map(c2_abs))
        .map(|(x, y)| [*x, y])
        .collect::<Vec<_>>();
    let xy_chart::XyInterval {
        x: x_range,
        y: y_range,
    } = xy_chart::XyInterval::new_fitting_points(graph.iter().cloned());
    const DEFAULT_WIDTH: f64 = 400.0;
    const DEFAULT_HEIGHT: f64 = 400.0;
    let (label_font_size, font_size) = (
        DEFAULT_HEIGHT * FONT_RATIO * 1.25,
        DEFAULT_HEIGHT * FONT_RATIO,
    );
    let mut canvas = gfx::svg::SvgDrawable::new();
    let line_width = DEFAULT_WIDTH * LINE_WIDTH_RATIO;
    let chart = xy_chart::XyChart {
        x_axis: xy_chart::Axis::new_from_ticks(xy_chart::Ticks::new_covering_interval(&x_range))
            .with_label("s [m]")
            .with_font(Font::new(label_font_size), Font::new(font_size)),
        y_axis: xy_chart::Axis::new_from_ticks(xy_chart::Ticks::new_covering_interval(&y_range))
            .with_label(y_label)
            .with_font(Font::new(label_font_size), Font::new(font_size)),
        grid_line_style: Some(gfx::LineStyle::new(line_width)),
        line_width,
        ..Default::default()
    };
    let plot = chart.prepare_layout(xy_chart::AspectRatio::Unconstrained, &canvas);
    plot.fit_into(DEFAULT_WIDTH, DEFAULT_HEIGHT)
        .draw_axes(&mut canvas)
        .plot(
            &mut canvas,
            &xy_chart::line_plot::LinePlot {
                coordinates: &graph,
                line_style: graph_line_style(line_width),
                periodicity: NOT_PERIODIC,
            },
        );
    let mut file = std::fs::File::create(path)?;
    canvas.write(&mut file, DEFAULT_WIDTH, DEFAULT_HEIGHT)
}

#[cfg(test)]
mod test {
    use numeric::assert_vec_almost_eq;

    use super::*;

    #[test]
    fn test_intersect_box_with_line_works_for_horizontal_line() {
        let bounding_box = xy_chart::XyInterval {
            x: xy_chart::Interval {
                min: -50.0,
                max: 50.0,
            },
            y: xy_chart::Interval {
                min: -20.0,
                max: 80.0,
            },
        };
        let alpha = 0.;
        let intersection = intersect_box_with_line(alpha, &bounding_box.x, &bounding_box.y);
        assert_vec_almost_eq!(
            intersection,
            vec!([-50., 0.], [50.0, 0.0], [50.0, 80.0], [-50.0, 80.0])
        );
    }
    #[test]
    fn test_intersect_box_with_line_works_for_line_isolating_lower_right_corner() {
        let bounding_box = xy_chart::XyInterval {
            x: xy_chart::Interval {
                min: -50.0,
                max: 50.0,
            },
            y: xy_chart::Interval {
                min: -20.0,
                max: 80.0,
            },
        };
        let alpha = std::f64::consts::FRAC_PI_4;
        let intersection = intersect_box_with_line(alpha, &bounding_box.x, &bounding_box.y);
        assert_vec_almost_eq!(
            intersection,
            vec!(
                [-20., -20.],
                [50.0, 50.0],
                [50.0, 80.0],
                [-50.0, 80.0],
                [-50., -20.]
            )
        );
    }
    #[test]
    fn test_intersect_box_with_line_works_works_for_line_isolating_lower_left_corner() {
        let bounding_box = xy_chart::XyInterval {
            x: xy_chart::Interval {
                min: -50.0,
                max: 50.0,
            },
            y: xy_chart::Interval {
                min: -20.0,
                max: 80.0,
            },
        };
        let alpha = -std::f64::consts::FRAC_PI_4;
        let intersection = intersect_box_with_line(alpha, &bounding_box.x, &bounding_box.y);
        assert_vec_almost_eq!(
            intersection,
            vec!(
                [-50., 50.],
                [20.0, -20.0],
                [50.0, -20.0],
                [50.0, 80.0],
                [-50.0, 80.0],
            )
        );
    }
    #[test]
    fn test_intersect_box_with_line_works_for_line_isolating_upper_left_corner() {
        let bounding_box = xy_chart::XyInterval {
            x: xy_chart::Interval {
                min: -50.0,
                max: 50.0,
            },
            y: xy_chart::Interval {
                min: -80.0,
                max: 20.0,
            },
        };
        let alpha = std::f64::consts::FRAC_PI_4;
        let intersection = intersect_box_with_line(alpha, &bounding_box.x, &bounding_box.y);
        assert_vec_almost_eq!(
            intersection,
            vec!([-50., -50.], [20.0, 20.0], [-50.0, 20.0])
        );
    }
    #[test]
    fn test_intersect_box_with_line_works_for_line_isolating_upper_right_corner() {
        let bounding_box = xy_chart::XyInterval {
            x: xy_chart::Interval {
                min: -50.0,
                max: 50.0,
            },
            y: xy_chart::Interval {
                min: -80.0,
                max: 20.0,
            },
        };
        let alpha = -std::f64::consts::FRAC_PI_4;
        let intersection = intersect_box_with_line(alpha, &bounding_box.x, &bounding_box.y);
        assert_vec_almost_eq!(intersection, vec!([-20., 20.], [50.0, -50.0], [50.0, 20.0]));
    }
}
