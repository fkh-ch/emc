//! csv export
use crate::c2_abs;
use crate::output::{
    path::with_suffix,
    result_values::{
        ConductorGroupResults, ConductorGroupValues, ConductorValues, ResultValues,
        TransverseProfileResult,
    },
};
use crate::project::{Project, TransverseProfileColumns};
use crate::sound::FeasibleAValue;
use numeric::complex::Complex;
use tabularize::Tabularizer;

/// Write all csv plots enabled in a project
pub fn write_csv_files(
    file_base: &std::path::Path,
    results: &ResultValues,
    project: &Project,
) -> std::io::Result<()> {
    const SEP: char = ';';
    let profile = &results.transverse_profile;
    let have_l = !profile.L.header.is_empty();
    if have_l || !profile.E.is_empty() || !profile.B.is_empty() {
        let path = with_suffix(file_base, "_profile.csv");
        let mut stream = std::fs::File::create(path)?;
        TransverseProfileResult::write_csv(
            &mut stream,
            SEP,
            &results.transverse_profile,
            &project.transverse_profile.columns,
        )?;
    }
    if project.corona_sound.enabled {
        let path = with_suffix(file_base, "_bundle.csv");
        let mut stream = std::fs::File::create(path)?;
        ConductorGroupResults::write_csv(&mut stream, SEP, &results.conductor_groups)?;

        let path = with_suffix(file_base, "_conductor.csv");
        let mut stream = std::fs::File::create(path)?;
        ConductorValues::write_csv(&mut stream, SEP, &results.conductors, true)?;
    }
    Ok(())
}

type Col<D, T> = tabularize::SchemaColumn<'static, fn(&D) -> T>;

impl ConductorValues {
    /// Write a slice of `ConductorValues` as csv to `stream`. Include the column
    /// "E_m" only if `with_l` is set to `true`.
    pub fn write_csv(
        stream: &mut impl std::io::Write,
        separator: char,
        values: &[Self],
        with_l: bool,
    ) -> std::io::Result<()> {
        type CI = Col<ConductorValues, usize>;
        const INDEX_COLUMNS: (CI, CI, CI, CI) = (
            CI::new("Index", |row| row.index),
            CI::new("Index (System)", |row| row.conductor.index.system_index),
            CI::new("Index (Phase)", |row| row.conductor.index.phase_index),
            CI::new("Index (Conductor)", |row| {
                row.conductor.index.conductor_index
            }),
        );

        type CF = Col<ConductorValues, f64>;
        const Q_COLUMNS: (CF, CF, CF) = (
            CF::new("|Q| [C/m]", |row| row.Q.norm()),
            CF::new("re(Q) [C/m]", |row| row.Q.re),
            CF::new("im(Q) [C/m]", |row| row.Q.im),
        );

        const F_COLUMN: CF = CF::new("E_m [V/m]", |row| row.E_m);
        let mut csv_writer = tabularize::CsvWriter::new(stream).with_separator(separator);
        if with_l {
            csv_writer.tabularize(tabularize::RecordTable {
                header: (INDEX_COLUMNS, F_COLUMN, Q_COLUMNS),
                body: values,
            })
        } else {
            csv_writer.tabularize(tabularize::RecordTable {
                header: (INDEX_COLUMNS, Q_COLUMNS),
                body: values,
            })
        }
    }
}

impl tabularize::TabularizeRow for &FeasibleAValue {
    fn tabularize_row<S>(self, tabularizer: S) -> Result<(), S::Error>
    where
        S: tabularize::RowTabularizer,
    {
        tabularizer.tabularize_split(self.value, self.feasible)
    }
}

#[derive(Debug, Clone, Copy)]
struct ALabel<'a>(&'a str);
impl std::fmt::Display for ALabel<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "G-{} [dB(A),1pW/m]", self.0)
    }
}

#[derive(Debug, Clone, Copy)]
struct AFeasibleLabel<'a>(&'a str);
impl std::fmt::Display for AFeasibleLabel<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}: feasible", self.0)
    }
}

impl ConductorGroupResults {
    /// Write [ConductorGroupResults] as csv to `stream`
    pub fn write_csv(
        stream: &mut impl std::io::Write,
        separator: char,
        data: &Self,
    ) -> std::io::Result<()> {
        type CI = Col<ConductorGroupValues, usize>;
        type CF = Col<ConductorGroupValues, f64>;
        const BASE_COLUMNS: ((CI, CI, CI), (CF, CF)) = (
            (
                CI::new("Index (System)", |row| row.group.index.system_index),
                CI::new("Index (Phase)", |row| row.group.index.phase_index),
                CI::new("n", |row| row.n),
            ),
            (
                CF::new("E_max (bundle) [kV/cm]", |row| 1.0E-5 * row.E_max),
                CF::new("d [cm]", |row| row.d),
            ),
        );
        const L_COLUMN: CF = CF::new("EPRI, A_wc [dB(A)]", |row| row.A_wc);
        let body = data.group_values.as_slice();
        let mut csv_writer = tabularize::CsvWriter::new(stream).with_separator(separator);
        let index_col = tabularize::Column::new("Index", 1..);
        use tabularize::Tabularize;
        if !data.A.values.is_empty() {
            let tab = index_col.join(tabularize::RecordTable {
                header: (BASE_COLUMNS, L_COLUMN),
                body,
            });
            let header = data
                .A
                .header
                .iter()
                .map(|label| tabularize::Concat(ALabel(label), AFeasibleLabel(label)))
                .collect::<Vec<_>>();
            let l_tab = tabularize::ArrayTable {
                header: header.as_slice(),
                body: data.A.values.as_slice(),
            };
            csv_writer.tabularize(tab.join(l_tab))
        } else {
            let tab = index_col.join(tabularize::RecordTable {
                header: BASE_COLUMNS,
                body,
            });
            csv_writer.tabularize(tab)
        }
    }
}

macro_rules! complex_field_columns {
    ($label:expr, $unit:expr) => {
        (
            (
                tabularize::SchemaColumn::new(concat!("re(", $label, "_x) [", $unit, "]"), |f| {
                    f[0].re
                }),
                tabularize::SchemaColumn::new(concat!("im(", $label, "_x) [", $unit, "]"), |f| {
                    f[0].im
                }),
                tabularize::SchemaColumn::new(concat!("re(", $label, "_y) [", $unit, "]"), |f| {
                    f[1].re
                }),
                tabularize::SchemaColumn::new(concat!("im(", $label, "_y) [", $unit, "]"), |f| {
                    f[1].im
                }),
            ),
            tabularize::SchemaColumn::new(concat!("|", $label, "| [", $unit, "]"), c2_abs),
        )
    };
}

impl TransverseProfileResult {
    /// Write `TransverseProfileResult` as csv to `stream`. The complex fields E and B
    /// will be represented by columns as specified in `options`.
    pub fn write_csv(
        stream: &mut impl std::io::Write,
        separator: char,
        data: &Self,
        options: &TransverseProfileColumns,
    ) -> std::io::Result<()> {
        type C = Col<[Complex<f64>; 2], f64>;
        use tabularize::{Column, Either, RecordTable as RecTab, Tabularize as _};

        struct LLabel(&'static str);
        impl std::fmt::Display for LLabel {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, "L-{} [dB(A),20µPa]", self.0)
            }
        }

        let l_header = if data.L.values.is_empty() {
            Vec::new()
        } else {
            data.L.header.iter().map(|l| LLabel(l)).collect::<Vec<_>>()
        };

        type EMTabFull<'a> = RecTab<((C, C, C, C), C), &'a [[Complex<f64>; 2]]>;
        type EMTabComponents<'a> = RecTab<(C, C, C, C), &'a [[Complex<f64>; 2]]>;
        type EMTabAbs<'a> = RecTab<C, &'a [[Complex<f64>; 2]]>;
        fn em_field_tab(
            columns: ((C, C, C, C), C),
            data: &[[Complex<f64>; 2]],
            with_abs: bool,
            with_components: bool,
        ) -> Option<Either<EMTabFull<'_>, Either<EMTabComponents<'_>, EMTabAbs<'_>>>> {
            match (
                with_abs && !data.is_empty(),
                with_components && !data.is_empty(),
            ) {
                (false, false) => None,
                (true, false) => Some(Either::Or(Either::Or(RecTab::new(columns.1, data)))),
                (false, true) => Some(Either::Or(Either::Either(RecTab::new(columns.0, data)))),
                (true, true) => Some(Either::Either(RecTab::new(columns, data))),
            }
        }
        let mut csv_writer = tabularize::CsvWriter::new(stream).with_separator(separator);
        let tab = Column::new("s [m]", data.s.as_slice())
            .join_variant(em_field_tab(
                complex_field_columns!("E", "V/m"),
                &data.E,
                options.with_e_abs,
                options.with_e_components,
            ))
            .join_variant(em_field_tab(
                complex_field_columns!("B", "µT"),
                &data.B,
                options.with_b_abs,
                options.with_b_components,
            ))
            .join_variant(
                (!data.L.values.is_empty()).then_some(tabularize::ArrayTable {
                    header: l_header.as_slice(),
                    body: data.L.values.as_slice(),
                }),
            );

        csv_writer.tabularize(tab)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::sound::LTable;
    use crate::{Conductor, ConductorIndex};

    #[test]
    fn test_write_csv_works_for_conductor_values() {
        let data = [
            ConductorValues {
                conductor: Conductor {
                    index: ConductorIndex {
                        system_index: 1,
                        phase_index: 1,
                        conductor_index: 1,
                    },
                    ..Default::default()
                },
                index: 1,
                E_m: 5.,
                Q: Complex::from(3.0),
            },
            ConductorValues {
                conductor: Conductor {
                    index: ConductorIndex {
                        system_index: 1,
                        phase_index: 1,
                        conductor_index: 2,
                    },
                    ..Default::default()
                },
                index: 2,
                E_m: 4.,
                Q: Complex::from(2.0),
            },
        ];
        let mut buf = Vec::new();
        ConductorValues::write_csv(&mut buf, ';', &data, true).unwrap();
        assert_eq!(
            std::str::from_utf8(buf.as_slice()).unwrap().to_string(),
            r#"Index;Index (System);Index (Phase);Index (Conductor);E_m [V/m];|Q| [C/m];re(Q) [C/m];im(Q) [C/m]
1;1;1;1;5;3;3;0
2;1;1;2;4;2;2;0
"#
        );
        let mut buf = Vec::new();
        ConductorValues::write_csv(&mut buf, ';', &data, false).unwrap();
        assert_eq!(
            std::str::from_utf8(buf.as_slice()).unwrap().to_string(),
            r#"Index;Index (System);Index (Phase);Index (Conductor);|Q| [C/m];re(Q) [C/m];im(Q) [C/m]
1;1;1;1;3;3;0
2;1;1;2;2;2;0
"#
        );
    }

    fn group_with_index(
        index: usize,
        system_index: usize,
        phase_index: usize,
    ) -> crate::ConductorGroup {
        crate::ConductorGroup {
            index: crate::ConductorIndex {
                conductor_index: index,
                system_index,
                phase_index,
            },
            ..Default::default()
        }
    }

    #[test]
    fn test_write_csv_works_for_conductor_group_results_without_l() {
        let results = ConductorGroupResults {
            group_values: vec![
                ConductorGroupValues {
                    group: group_with_index(1, 1, 1),
                    E_max: 1000.,
                    n: 8,
                    d: 0.125,
                    A_wc: 5.,
                },
                ConductorGroupValues {
                    group: group_with_index(2, 1, 2),
                    E_max: 2000.,
                    n: 12,
                    d: 0.25,
                    A_wc: 6.,
                },
            ],
            A: LTable::new(),
        };
        let mut buf = Vec::new();
        ConductorGroupResults::write_csv(&mut buf, ';', &results).unwrap();
        assert_eq!(
            std::str::from_utf8(buf.as_slice()).unwrap().to_string(),
            r#"Index;Index (System);Index (Phase);n;E_max (bundle) [kV/cm];d [cm]
1;1;1;8;0.01;0.125
2;1;2;12;0.02;0.25
"#
        );
    }
    fn g_val(value: f64, feasible: bool) -> FeasibleAValue {
        FeasibleAValue { value, feasible }
    }

    #[test]
    fn test_write_csv_works_for_conductor_group_results_with_l() {
        let results = ConductorGroupResults {
            group_values: vec![
                ConductorGroupValues {
                    group: group_with_index(1, 1, 1),
                    E_max: 1000.,
                    n: 8,
                    d: 0.125,
                    A_wc: 5.,
                },
                ConductorGroupValues {
                    group: group_with_index(2, 1, 2),
                    E_max: 2000.,
                    n: 12,
                    d: 0.25,
                    A_wc: 6.,
                },
            ],
            A: LTable {
                values: vec![
                    g_val(1., true),
                    g_val(2., true),
                    g_val(3., false),
                    g_val(4., false),
                    g_val(5., true),
                    g_val(6., false),
                ],
                header: &[
                    crate::sound::CIGRE.name,
                    crate::sound::EPRI.name,
                    crate::sound::EDF.name,
                ],
            },
        };
        let mut buf = Vec::new();
        ConductorGroupResults::write_csv(&mut buf, ';', &results).unwrap();
        assert_eq!(
            std::str::from_utf8(buf.as_slice()).unwrap().to_string(),
            r#"Index;Index (System);Index (Phase);n;E_max (bundle) [kV/cm];d [cm];EPRI, A_wc [dB(A)];G-CIGRE [dB(A),1pW/m];CIGRE: feasible;G-EPRI [dB(A),1pW/m];EPRI: feasible;G-EDF [dB(A),1pW/m];EDF: feasible
1;1;1;8;0.01;0.125;5;1;true;2;true;3;false
2;1;2;12;0.02;0.25;6;4;false;5;true;6;false
"#
        );
    }
    #[test]
    fn test_write_csv_works_for_transverse_profile_result_without_b() {
        let results = TransverseProfileResult {
            s: vec![0., 1., 3.],
            E: vec![
                [Complex::from(3.), Complex { re: 0., im: 4. }],
                [Complex::from(4.), Complex { re: 0., im: -3. }],
                [Complex::from(-3.), Complex { re: 0., im: -4. }],
            ],
            B: Vec::new(),
            L: LTable {
                values: vec![-1., -1.5, -2., -2.5, -3., -3.5],
                header: &[crate::sound::CIGRE.name, crate::sound::EPRI.name],
            },
        };
        let options = TransverseProfileColumns {
            with_e_abs: true,
            with_e_components: true,
            with_b_abs: true,
            with_b_components: true,
        };
        let mut buf = Vec::new();
        TransverseProfileResult::write_csv(&mut buf, ';', &results, &options).unwrap();
        assert_eq!(
            std::str::from_utf8(buf.as_slice()).unwrap().to_string(),
            r#"s [m];re(E_x) [V/m];im(E_x) [V/m];re(E_y) [V/m];im(E_y) [V/m];|E| [V/m];L-CIGRE [dB(A),20µPa];L-EPRI [dB(A),20µPa]
0;3;0;0;4;5;-1;-1.5
1;4;0;0;-3;5;-2;-2.5
3;-3;0;0;-4;5;-3;-3.5
"#
        );
    }
    #[test]
    fn test_write_csv_works_for_transverse_profile_result_without_l() {
        let results = TransverseProfileResult {
            s: vec![0., 1., 3.],
            E: vec![
                [Complex::from(10.), Complex::from(0.)],
                [Complex::from(11.), Complex::from(0.)],
                [Complex::from(12.), Complex::from(0.)],
            ],
            B: vec![
                [Complex::from(20.), Complex::from(0.)],
                [Complex::from(21.), Complex::from(0.)],
                [Complex::from(22.), Complex::from(0.)],
            ],
            L: LTable::new(),
        };
        let options = TransverseProfileColumns {
            with_e_abs: true,
            with_e_components: false,
            with_b_abs: true,
            with_b_components: false,
        };
        let mut buf = Vec::new();
        TransverseProfileResult::write_csv(&mut buf, ';', &results, &options).unwrap();
        assert_eq!(
            std::str::from_utf8(buf.as_slice()).unwrap().to_string(),
            r#"s [m];|E| [V/m];|B| [µT]
0;10;20
1;11;21
3;12;22
"#
        );
    }
}
