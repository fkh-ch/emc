//! Output computed results to svg / csv

pub mod csv;
pub mod path;
pub mod plot;
pub mod result_values;
