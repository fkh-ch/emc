//! EBL Output data structures

use crate::sound;
use numeric::complex::Complex;

use crate::Conductor;

#[derive(Debug)]
/// Computed values for a system
pub struct ResultValues {
    /// Results along a transverse profile
    pub transverse_profile: TransverseProfileResult,
    /// Results per conductor
    pub conductors: Vec<ConductorValues>,
    /// Results per conductor group
    pub conductor_groups: ConductorGroupResults,
}

#[allow(non_snake_case)]
#[derive(Debug)]
/// Results along a transverse profile
pub struct TransverseProfileResult {
    /// Distance from start \[m]
    pub s: Vec<f64>,
    /// E field [V/m]
    pub E: Vec<[Complex<f64>; 2]>,
    /// B field \[µT]
    pub B: Vec<[Complex<f64>; 2]>,
    /// Sound levels [dB(A),20µPa]
    pub L: sound::LTable<f64>,
}

#[allow(non_snake_case)]
#[derive(Debug)]
/// Results of one conductor
pub struct ConductorValues {
    /// Conductor parameters
    pub conductor: Conductor,
    /// Index
    pub index: usize,
    /// Average field strength of the conductor [V/m]
    pub E_m: f64,
    /// Surface charge [C/m]
    pub Q: Complex<f64>,
}

#[allow(non_snake_case)]
#[derive(Debug)]
/// Results of a phase bundle or a single guard wire / shield
pub struct ConductorGroupValues {
    /// Original conductor group
    pub group: crate::ConductorGroup,
    /// E_max (phase) [V/m]
    pub E_max: f64,
    /// n
    pub n: usize,
    /// d \[cm]
    pub d: f64,
    /// A_wc
    pub A_wc: f64,
}

impl AsRef<crate::ConductorGroup> for ConductorGroupValues {
    fn as_ref(&self) -> &crate::ConductorGroup {
        &self.group
    }
}

#[allow(non_snake_case)]
#[derive(Debug, Default)]
/// Results per conductor group
pub struct ConductorGroupResults {
    /// All values from [ConductorGroupValues]
    pub group_values: Vec<ConductorGroupValues>,
    /// Length-related sound power levels [dB(A),1pW/m]
    pub A: sound::LTable<sound::FeasibleAValue>,
}
