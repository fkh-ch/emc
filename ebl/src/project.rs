//! EBL Project data

use crate::sound;

/// EBL Project data containing conductor geometry, environment properties and output parameters
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Deserialize, serde::Serialize)]
pub struct Project {
    /// Systems
    pub systems: Vec<System>,
    /// Grounded conductors
    pub guardwires: Vec<GuardWire>,

    /// Parameters for the transverse profile
    pub transverse_profile: TransverseProfile,
    /// Corona sound configuration
    #[serde(default)]
    pub corona_sound: CoronaSound,

    /// Output figures
    pub figures: Figures,

    /// Reference level for conductor configuration \[m]
    pub y_ref: f64,
    /// Slope of ground in the direction of the transverse profile
    pub alpha: Angle,
    /// Resistivity of ground \[Ωm]
    pub rho: f64,
    /// AC frequency used for computing induced currents \[Hz]
    pub f_grid: f64,
}

impl Default for Project {
    fn default() -> Self {
        Self {
            systems: vec![System {
                phases: vec![
                    Phase { x: -8., y: 0. },
                    Phase { x: -10., y: 10. },
                    Phase { x: -7., y: 20. },
                ],
                ..Default::default()
            }],
            guardwires: Default::default(),
            transverse_profile: Default::default(),
            corona_sound: Default::default(),
            figures: Default::default(),
            y_ref: 20.0,
            alpha: Angle(0.0),
            rho: 100.0,
            f_grid: 50.0,
        }
    }
}

/// Transverse profile properties
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Deserialize, serde::Serialize)]
pub struct TransverseProfile {
    /// Hight of profile above ground \[m]
    pub y: f64,
    /// x coordinate start point of transverse profile \[m]
    pub x_min: f64,
    /// x coordinate end point of transverse profile \[m]
    pub x_max: f64,
    /// Number of sampling points for transverse profile
    pub m: u32,
    /// Options which columns to include for the complex vector fields E and B
    pub columns: TransverseProfileColumns,
    /// Options which plots to include for the transverse profile
    pub plots: TransverseProfilePlots,
}

impl Default for TransverseProfile {
    fn default() -> Self {
        Self {
            y: 1.0,
            x_min: -100.0,
            x_max: 100.0,
            m: 401,
            columns: Default::default(),
            plots: Default::default(),
        }
    }
}

/// Options which columns to include for the complex vector fields E and B
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Deserialize, serde::Serialize)]
pub struct TransverseProfileColumns {
    /// Include |E| column
    pub with_e_abs: bool,
    /// Include columns for re(E_x), im(E_x), re(E_y), im(E_y)
    pub with_e_components: bool,
    /// Include |B| column
    pub with_b_abs: bool,
    /// Include columns for re(B_x), im(B_x), re(B_y), im(B_y)
    pub with_b_components: bool,
}

impl Default for TransverseProfileColumns {
    fn default() -> Self {
        Self {
            with_e_abs: true,
            with_e_components: false,
            with_b_abs: true,
            with_b_components: false,
        }
    }
}

/// Options which plots to include for the transverse profile
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Deserialize, serde::Serialize)]
pub struct TransverseProfilePlots {
    /// Render a graph of the E field
    pub with_e_profile: bool,
    /// Render a graph of the B field
    pub with_b_profile: bool,
    /// Render a conductor cross-section diagram
    pub with_conductor_picture: bool,
}

impl Default for TransverseProfilePlots {
    fn default() -> Self {
        Self {
            with_e_profile: true,
            with_b_profile: true,
            with_conductor_picture: true,
        }
    }
}

/// Angle
/// Internally represented as degree
#[derive(Debug, Default, Clone, Copy, PartialEq, serde::Deserialize, serde::Serialize)]
pub struct Angle(pub f64);

impl Angle {
    /// Create an [Angle] instance from degrees
    pub const fn from_degrees(value: f64) -> Self {
        Self(value)
    }
    /// Create an [Angle] instance from radians
    pub fn from_radians(value: f64) -> Self {
        Self(value.to_degrees())
    }
    /// Numerical values in degrees
    pub const fn as_degrees(&self) -> f64 {
        self.0
    }
    /// Numerical values in radians
    pub fn as_radians(&self) -> f64 {
        self.0.to_radians()
    }
}

impl std::fmt::Display for Angle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl std::str::FromStr for Angle {
    type Err = std::num::ParseFloatError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse::<f64>().map(Self)
    }
}

/// System consisting of phases and guard wires
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Deserialize, serde::Serialize)]
pub struct System {
    /// # partial conductors per phase in system
    pub n_t: u32,
    /// Starting angle for 1st partial conductor
    pub beta_s: Angle,
    /// Distance between 2 partial conductors \[m]
    pub a: f64,
    /// ⌀ of partial conductors \[m]
    pub d: f64,
    /// Voltage \[V]
    pub U: f64,
    /// Phase shift of voltage \[rad]
    pub phi_U: Angle,
    /// Current \[A]
    pub I: f64,
    /// Phase shift of current \[rad]
    pub phi_I: Angle,

    /// Coordinates for each bundle
    pub phases: Vec<Phase>,
    /// When activated, create a shield for each conductor with given properties
    #[serde(default, skip_serializing_if = "IsDefault::is_default")]
    pub shield: ShieldProperties,
}

impl Default for System {
    fn default() -> Self {
        Self {
            n_t: 2,
            beta_s: Angle(0.0),
            a: 0.4,
            d: 0.03,
            U: 400_000.,
            phi_U: Angle(0.),
            I: 1000.,
            phi_I: Angle(0.),
            phases: vec![Phase { x: 0., y: 0. }; 3],
            shield: Default::default(),
        }
    }
}

/// Guard wire
///
/// In Contrast to [libemc::GuardWire], this struct uses
/// the conductor diameter instead of the radius
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Serialize, serde::Deserialize)]
#[allow(non_snake_case)]
pub struct GuardWire {
    /// x coordinate \[m]
    pub x: f64,
    /// y coordinate \[m]
    pub y: f64,
    /// Conductor ⌀ \[m]
    pub d: f64,
    /// Conductor resistance per length \[Ω/m]
    pub R_i: f64,
}

impl Default for GuardWire {
    fn default() -> Self {
        Self {
            x: 0.0,
            y: 30.0,
            d: 0.02,
            R_i: 0.0005,
        }
    }
}

/// x- and y- coordinate of a phase
#[derive(Debug, Default, Clone, PartialEq, serde::Deserialize, serde::Serialize)]
pub struct Phase {
    /// x coordinate
    pub x: f64,
    /// y coordinate
    pub y: f64,
}

/// Properties of a shield
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Deserialize, serde::Serialize)]
pub struct ShieldProperties {
    /// Add a shield for every line in the system
    pub enabled: bool,
    /// ⌀ of shields \[m]
    pub d: f64,
    /// Resistance per length of shields [Ω/m]
    pub R_i: f64,
}

impl Default for ShieldProperties {
    fn default() -> Self {
        Self {
            d: 0.1,
            R_i: 0.0005,
            enabled: false,
        }
    }
}

trait IsDefault {
    fn is_default(&self) -> bool;
}

impl<T> IsDefault for T
where
    T: Default,
    for<'a> &'a T: PartialEq,
{
    fn is_default(&self) -> bool {
        self == &Default::default()
    }
}

/// Corona sound parameters
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Deserialize, serde::Serialize)]
pub struct CoronaSound {
    /// Enable computation of corona sound
    pub enabled: bool,
    /// Altitude above sea level \[m]
    #[serde(default)]
    pub h_0: f64,
    /// Rain rate \[mm/h]
    #[serde(default)]
    pub rain_rate: f64,
    /// Take the atmospheric air absorption into account
    #[serde(default)]
    pub enable_atmospheric_air_absorption: bool,
    /// Annual assessment parameters
    #[serde(default)]
    pub annual_assessment: sound::AnnualAssessment,
}

impl Default for CoronaSound {
    fn default() -> Self {
        Self {
            h_0: 300.,
            rain_rate: 1.0,
            enable_atmospheric_air_absorption: true,
            annual_assessment: Default::default(),
            enabled: true,
        }
    }
}

impl Project {
    /// Load a project from a json stream
    pub fn from_json<R: std::io::Read>(reader: R) -> Result<Self, serde_json::error::Error> {
        serde_json::from_reader(reader)
    }
}

/// Representation of a color as RGB values
#[derive(Debug, Clone, Default, PartialEq)]
pub struct WebColor {
    /// Red
    pub r: u8,
    /// Green
    pub g: u8,
    /// Blue
    pub b: u8,
}

impl WebColor {
    /// Color constant for black
    pub const BLACK: Self = Self::new(0, 0, 0);

    /// Create a new color from RGB values
    pub const fn new(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b }
    }

    /// Create a new color from a hex number in the form `0x__rrggbb`,
    /// where the bits in `__` are ignored, and `rr`, `gg`, `bb` are used
    /// for the RGB values.
    pub const fn from_hex(hex: u32) -> Self {
        let hex = hex & 0x00ffffff;
        Self {
            r: ((hex & 0x00ff0000) >> 16) as u8,
            g: ((hex & 0x0000ff00) >> 8) as u8,
            b: (hex & 0x000000ff) as u8,
        }
    }

    /// h ∈ [0,360), s, v ∈ [0, 255]
    pub const fn from_hsv(h: u16, s: u8, v: u8) -> Self {
        let h = h % 360;
        // chroma: X = S V, (S, V ∈ [0, 1))
        let c = ((s as u16 * v as u16) / 255) as u8;
        // H' = H / 60
        let h6 = h / 60;
        // X = C (1 - |H' mod 2 - 1|)
        let h_mod = if c == 0 {
            0
        } else {
            (c as u32 * h as u32 / 60) as u16 % (2 * c as u16)
        };
        let h_abs = if h_mod > c as u16 {
            h_mod - c as u16
        } else {
            c as u16 - h_mod
        } as u8;
        let x = c - h_abs;
        let m = v - c;
        let mc = c + m;
        let mx = x + m;
        match h6 {
            0 => WebColor { r: mc, g: mx, b: m },
            1 => WebColor { r: mx, g: mc, b: m },
            2 => WebColor { r: m, g: mc, b: mx },
            3 => WebColor { r: m, g: mx, b: mc },
            4 => WebColor { r: mx, g: m, b: mc },
            5 => WebColor { r: mc, g: m, b: mx },
            _ => WebColor { r: 0, g: 0, b: 0 },
        }
    }
}

/// Error occurring when parsing an invalid color string
#[derive(Debug)]
pub struct ParseWebColorError(pub String);

impl From<std::num::ParseIntError> for ParseWebColorError {
    fn from(value: std::num::ParseIntError) -> Self {
        Self(format!("{value}"))
    }
}

impl std::fmt::Display for ParseWebColorError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl std::error::Error for ParseWebColorError {}

impl std::str::FromStr for WebColor {
    type Err = ParseWebColorError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.len() != 7 {
            return Err(ParseWebColorError(format!(
                "Expected a string of length 7, got: {s} (length {})",
                s.len()
            )));
        }
        let s = s.strip_prefix('#').ok_or(ParseWebColorError(
            "Webcolor does not start with #".to_string(),
        ))?;
        Ok(Self {
            r: u8::from_str_radix(&s[..2], 16)?,
            g: u8::from_str_radix(&s[2..4], 16)?,

            b: u8::from_str_radix(&s[4..], 16)?,
        })
    }
}

impl std::fmt::Display for WebColor {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "#{:02x}{:02x}{:02x}", self.r, self.g, self.b)
    }
}

impl<'de> serde::Deserialize<'de> for WebColor {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let buf = String::deserialize(deserializer)?;
        <Self as std::str::FromStr>::from_str(&buf).map_err(serde::de::Error::custom)
    }
}

impl serde::Serialize for WebColor {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.to_string().serialize(serializer)
    }
}

/// xy diagram containing one or two plots
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Deserialize, serde::Serialize)]
pub struct Figure<P> {
    /// x range of the plot
    pub x_range: std::ops::Range<f64>,
    /// y range of the plot
    pub y_range: std::ops::Range<f64>,
    /// Plot subgrid
    pub with_subgrid: bool,
    /// Total width of the resulting plot \[px]
    pub width: f64,
    /// One or two of contourline plot, field plot
    pub plots: P,
}

impl<P: Default> Default for Figure<P> {
    fn default() -> Self {
        Self {
            x_range: -50.0..50.,
            y_range: -10.0..60.,
            with_subgrid: false,
            width: 1024.,
            plots: Default::default(),
        }
    }
}

/// All available 2D figures
#[derive(Debug, Clone, PartialEq, serde::Deserialize, serde::Serialize)]
pub struct Figures {
    /// E contour lines / field lines
    #[serde(default, skip_serializing_if = "IsDefault::is_default")]
    pub e: Figure<SinglePlot>,
    /// B contour lines / field lines
    #[serde(default, skip_serializing_if = "IsDefault::is_default")]
    pub b: Figure<SinglePlot>,
    /// |U| contour lines / gradient lines
    #[serde(default, skip_serializing_if = "IsDefault::is_default")]
    pub u: Figure<SinglePlot>,
    /// Re(U) contour lines / gradient lines
    #[serde(default, skip_serializing_if = "IsDefault::is_default")]
    pub u_re: Figure<CombinedPlot>,
    /// |A| contour lines / gradient lines
    #[serde(default, skip_serializing_if = "IsDefault::is_default")]
    pub a: Figure<SinglePlot>,
    /// Re(A) contour lines / gradient lines
    #[serde(default, skip_serializing_if = "IsDefault::is_default")]
    pub a_re: Figure<CombinedPlot>,
}

impl Figure<SinglePlot> {
    fn new_with_contour_lines(levels: &[f64]) -> Self {
        Self {
            plots: SinglePlot {
                contour_lines: ContourLinePlot {
                    levels: levels
                        .iter()
                        .cloned()
                        .map(|value| ContourLineLevel { value, color: None })
                        .collect(),
                    ..Default::default()
                },
            },
            ..Default::default()
        }
    }
}

const E_LEVELS: &[f64] = &[100.0, 200.0, 500.0, 1000.0, 2000.0, 5000.0, 10000.0];
const B_LEVELS: &[f64] = &[
    0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 200.0, 500.0,
];

impl Default for Figures {
    fn default() -> Self {
        Self {
            e: Figure::new_with_contour_lines(E_LEVELS),
            b: Figure::new_with_contour_lines(B_LEVELS),
            u: Default::default(),
            a: Default::default(),
            u_re: Default::default(),
            a_re: Default::default(),
        }
    }
}

/// Contour line plot of an E/M field
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Deserialize, serde::Serialize)]
pub struct ContourLinePlot {
    /// Include this plot in the figure
    pub enabled: bool,
    /// Resolution \[m]
    ///
    /// Contour lines with a diameter greater than resolution are resolvable
    pub resolution: f64,
    /// Level values and colors of the contour lines
    pub levels: Vec<ContourLineLevel>,
    /// If not empty, override the default color sequence used for contour lines
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub level_colors: Vec<WebColor>,
}

impl ContourLinePlot {
    /// Lookup the color for the contour line with index `level_index`
    pub fn level_color(&self, level_index: usize) -> &WebColor {
        let level_colors: &[WebColor] = if !self.level_colors.is_empty() {
            &self.level_colors
        } else {
            DEFAULT_COLORS
        };
        self.levels
            .get(level_index)
            .and_then(|level| level.color.as_ref())
            .unwrap_or_else(|| &level_colors[level_index % level_colors.len()])
    }
    /// Sequence for value and color for contour lines
    pub fn levels(&self) -> impl Iterator<Item = (f64, WebColor)> + '_ {
        self.levels
            .iter()
            .enumerate()
            .map(|(n, level)| (level.value, self.level_color(n).clone()))
    }
}

/// Default contour line color sequence
pub const DEFAULT_COLORS: &[WebColor] = &[
    WebColor::from_hex(0x370083),
    WebColor::from_hex(0x6b00b3),
    WebColor::from_hex(0xa444e0),
    WebColor::from_hex(0xa185ff),
    WebColor::from_hex(0x4635f8),
    WebColor::from_hex(0x002ead),
    WebColor::from_hex(0x00858b),
    WebColor::from_hex(0x01a7a7),
    WebColor::from_hex(0x006b89),
    WebColor::from_hex(0x01ef35),
    WebColor::from_hex(0x28ad00),
    WebColor::from_hex(0x207f05),
    WebColor::from_hex(0x96d100),
    WebColor::from_hex(0xd6c400),
    WebColor::from_hex(0xfacd52),
    WebColor::from_hex(0xed9d12),
    WebColor::from_hex(0xff700a),
    WebColor::from_hex(0xe30202),
    WebColor::from_hex(0x8f0000),
    WebColor::from_hex(0x000000),
];

impl Default for ContourLinePlot {
    fn default() -> Self {
        Self {
            enabled: false,
            resolution: 1.0,
            levels: Default::default(),
            level_colors: Default::default(),
        }
    }
}

/// Level for a contour line consisting of value and color
#[derive(Debug, Clone, Default, PartialEq, serde::Deserialize, serde::Serialize)]
pub struct ContourLineLevel {
    /// Level value of the contour line
    pub value: f64,
    /// Color of the contour line
    /// If set to None, pick a color from [ContourLinePlot::level_colors]
    #[serde(default, skip_serializing_if = "IsDefault::is_default")]
    pub color: Option<WebColor>,
}

/// Field line plot for an E/M field
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Deserialize, serde::Serialize)]
pub struct FieldLinePlot {
    /// Include this plot in the figure
    pub enabled: bool,
    /// Radius around singularities where field lines should start / stop
    pub r_singularity: f64,
    /// Number of field lines around source singularities
    pub n_lines_at_singularity: usize,
}

impl Default for FieldLinePlot {
    fn default() -> Self {
        Self {
            enabled: false,
            r_singularity: 1.0,
            n_lines_at_singularity: 8,
        }
    }
}

/// One or two plots for an EM field [Figure]
#[derive(Debug, Clone, Default, PartialEq, serde::Deserialize, serde::Serialize)]
pub struct CombinedPlot {
    /// Contour line plot of the absoulte value of the field
    #[serde(default)]
    pub contour_lines: ContourLinePlot,
    /// Field lines of the field
    #[serde(default)]
    pub field_lines: FieldLinePlot,
}

/// Contour line plot only [Figure]
#[derive(
    Debug, Clone, Default, metastruct::MetaStruct, PartialEq, serde::Deserialize, serde::Serialize,
)]
pub struct SinglePlot {
    /// Contour line plot of the absoulte value of the field
    #[serde(default)]
    pub contour_lines: ContourLinePlot,
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_load_from_json() {
        let json = include_bytes!("../tests/default.json");
        let ed = Project::from_json(json.as_slice()).unwrap();
        let expected = Project::default();
        assert_eq!(ed, expected);
    }

    #[test]
    fn test_webcolor_from_str() {
        use std::str::FromStr;
        assert_eq!(
            WebColor::from_str("#080808").unwrap(),
            WebColor::new(0x08, 0x08, 0x08)
        );
        assert_eq!(
            WebColor::from_str("#808081").unwrap(),
            WebColor::new(0x80, 0x80, 0x81)
        );
    }

    #[test]
    fn test_webcolor_to_string() {
        let s = WebColor::new(0xff, 0xff, 0xff).to_string();
        assert_eq!(s, "#ffffff".to_string());
        let s = WebColor::new(0xf1, 0xf2, 0xf3).to_string();
        assert_eq!(s, "#f1f2f3".to_string());
        let s = WebColor::new(0x08, 0x08, 0x08).to_string();
        assert_eq!(s, "#080808".to_string());
    }

    #[test]
    fn test_webcolor_from_hsv_works_for_red() {
        let c = WebColor::from_hsv(0, 255, 255);
        assert_eq!(c, WebColor::new(255, 0, 0));
    }
    #[test]
    fn test_webcolor_from_hsv_works_for_green() {
        let c = WebColor::from_hsv(120, 255, 255);
        assert_eq!(c, WebColor::new(0, 255, 0));
    }
    #[test]
    fn test_webcolor_from_hsv_works_for_blue() {
        let c = WebColor::from_hsv(240, 255, 255);
        assert_eq!(c, WebColor::new(0, 0, 255));
    }
    #[test]
    fn test_webcolor_from_hsv_works_for_yellow() {
        let c = WebColor::from_hsv(60, 255, 255);
        assert_eq!(c, WebColor::new(255, 255, 0));
    }
    #[test]
    fn test_webcolor_from_hsv_works_for_cyan() {
        let c = WebColor::from_hsv(180, 255, 255);
        assert_eq!(c, WebColor::new(0, 255, 255));
    }
    #[test]
    fn test_webcolor_from_hsv_works_for_magenta() {
        let c = WebColor::from_hsv(300, 255, 255);
        assert_eq!(c, WebColor::new(255, 0, 255));
    }
    #[test]
    fn test_webcolor_from_hsv_works_for_white() {
        let c = WebColor::from_hsv(0, 0, 255);
        assert_eq!(c, WebColor::new(255, 255, 255));
    }
    #[test]
    fn test_webcolor_from_hsv_works_for_grey() {
        let c = WebColor::from_hsv(0, 0, 128);
        assert_eq!(c, WebColor::new(128, 128, 128));
    }
    #[test]
    fn test_webcolor_from_hsv_works_for_black() {
        let c = WebColor::from_hsv(0, 0, 0);
        assert_eq!(c, WebColor::new(0, 0, 0));
    }
    #[test]
    fn test_webcolor_from_hsv_works_for_black_2() {
        let c = WebColor::from_hsv(0, 255, 0);
        assert_eq!(c, WebColor::new(0, 0, 0));
    }

    #[test]
    fn test_webcolor_from_hex_works_for_black() {
        let c = WebColor::from_hex(0);
        assert_eq!(c, WebColor::new(0, 0, 0));
    }
    #[test]
    fn test_webcolor_from_hex_works_for_r() {
        let c = WebColor::from_hex(0xff0000);
        assert_eq!(c, WebColor::new(0xff, 0, 0));
    }
    #[test]
    fn test_webcolor_from_hex_works_for_g() {
        let c = WebColor::from_hex(0x00ff00);
        assert_eq!(c, WebColor::new(0, 0xff, 0));
    }
    #[test]
    fn test_webcolor_from_hex_works_for_b() {
        let c = WebColor::from_hex(0x0000ff);
        assert_eq!(c, WebColor::new(0, 0, 0xff));
    }
}
