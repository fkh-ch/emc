//! Tests
#![cfg(test)]

pub mod conductors;
pub mod fields;
pub mod projects;

use super::*;
use numeric::{assert_near, assert_vec_almost_eq, assert_vec_near};
use pretty_assertions::assert_eq;

#[test]
fn test_reflect() {
    let slope = Slope(4.0, 5.0);
    let p = slope.reflect([1.0, 2.0]);
    let expected_p = [14.0, -3.0];
    assert_eq!(expected_p, p);
}

#[test]
fn test_compute_transverse_profile() {
    let json = include_bytes!("../../tests/M3.json");
    let project = Project::from_json(json.as_slice()).unwrap();
    assert_eq!(project.alpha, project::Angle::from_degrees(0.0));
    let default_index = ConductorIndex {
        system_index: 0,
        phase_index: 0,
        conductor_index: 0,
    };
    let i = numeric::complex::Complex::<f64>::new(0., 1.);
    let mut expected_cfg = ConductorGeometry {
        rho: 200.,
        f: 50.,
        conductors: conductors::CONDUCTORS.to_vec(),
        system_dividers: vec![3, 6],
        conductor_groups: vec![
            ConductorGroup {
                kind: ConductorType::Conductor,
                index: default_index.clone(),
                position: [-7.5, 17.0],
                r_bundle: 0.2,
                I: 1920.0 + 0.0 * i,
                partial_conductors: 0..2,
                r_cnd: 0.01595,
            },
            ConductorGroup {
                kind: ConductorType::Conductor,
                index: default_index.clone(),
                position: [-9.5, 26.0],
                r_bundle: 0.2,
                I: -959.999_999_999_999_5 + 1662.7687752661222 * i,
                partial_conductors: 2..4,
                r_cnd: 0.01595,
            },
            ConductorGroup {
                kind: ConductorType::Conductor,
                index: default_index.clone(),
                position: [-6.0, 35.5],
                r_bundle: 0.2,
                I: -960.000_000_000_000_7 - 1662.7687752661216 * i,
                partial_conductors: 4..6,
                r_cnd: 0.01595,
            },
            ConductorGroup {
                kind: ConductorType::Conductor,
                index: default_index.clone(),
                position: [6.0, 35.5],
                r_bundle: 0.2,
                I: 1920.0 + 0.0 * i,
                partial_conductors: 6..8,
                r_cnd: 0.01595,
            },
            ConductorGroup {
                kind: ConductorType::Conductor,
                index: default_index.clone(),
                position: [9.5, 26.0],
                r_bundle: 0.2,
                I: -959.999_999_999_999_5 + 1662.7687752661222 * i,
                partial_conductors: 8..10,
                r_cnd: 0.01595,
            },
            ConductorGroup {
                kind: ConductorType::Conductor,
                index: default_index.clone(),
                position: [7.5, 17.0],
                r_bundle: 0.2,
                I: -960.000_000_000_000_7 - 1662.7687752661216 * i,
                partial_conductors: 10..12,
                r_cnd: 0.01595,
            },
            ConductorGroup {
                kind: ConductorType::GuardWire,
                index: default_index.clone(),
                position: [0.0, 46.0],
                r_bundle: 1.0,
                I: c(0.0),
                partial_conductors: 12..13,
                r_cnd: 0.013,
            },
        ],
        influenced_conductors: vec![GuardWire {
            x: 0.0,
            y: 46.0,
            r: 0.013,
            R_i: 0.002,
        }],
        rng_guard_wires: conductors::CONDUCTORS.len() - 1..conductors::CONDUCTORS.len(),
        rng_shields: conductors::CONDUCTORS.len()..conductors::CONDUCTORS.len(),
        rng_neutral_conductors: conductors::CONDUCTORS.len() - 1..conductors::CONDUCTORS.len(),
    };
    compute_Q(expected_cfg.conductors.as_mut_slice()).unwrap();
    // -- do compute_Q test separately
    let geometry = ConductorGeometry::from(&project)
        .compute_I()
        .unwrap()
        .compute_Q()
        .unwrap();

    assert_vec_almost_eq!(&geometry.conductors, &expected_cfg.conductors);
    assert_vec_almost_eq!(&geometry.conductor_groups, &expected_cfg.conductor_groups);
    assert_eq!(
        geometry.guard_wires().len(),
        expected_cfg.guard_wires().len()
    );
    assert_eq!(geometry.rng_shields, expected_cfg.rng_shields);
    // -- verifying sampling_points
    let positions = sampling_points(&project.transverse_profile, project.alpha.as_radians());
    assert_eq!(
        positions,
        (0..=400)
            .map(|i| [-100. + (i as f64) * 0.5, 1.0])
            .collect::<Vec<_>>()
    );
    let geometry = ConductorGeometry::from(&project)
        .compute_I()
        .unwrap()
        .compute_Q()
        .unwrap();
    let positions = sampling_points(&project.transverse_profile, project.alpha.as_radians());
    let result = compute_transverse_profile(&geometry, &positions, false, false).unwrap();
    assert!(result.E.is_empty());
    assert!(result.B.is_empty());
}

const fn c(re: f64) -> Complex<f64> {
    Complex { re, im: 0.0 }
}

const fn default_index() -> ConductorIndex {
    ConductorIndex {
        system_index: 0,
        phase_index: 0,
        conductor_index: 0,
    }
}

#[test]
fn test_compute_Q() {
    let mut conductors = conductors::CONDUCTORS.to_vec();
    compute_Q(conductors.as_mut_slice()).unwrap();
    let q: Vec<_> = conductors.iter().map(|c| c.Q * (TWO_PI * EPS_0)).collect();
    let i = numeric::complex::Complex::<f64>::new(0., 1.);
    let expected_q = &[
        1.417_596_406_202_952_4e-6 - 8.503_260_060_226_904e-8 * i,
        1.413_132_865_227_707_6e-6 - 9.898_534_844_842_37e-8 * i,
        -7.377_301_733_659_586e-7 + 1.238_171_175_477_002_6e-6 * i,
        -7.343_957_624_787_717e-7 + 1.229_658_670_710_152_3e-6 * i,
        -6.851_579_328_686_599e-7 - 1.248_193_717_657_131_5e-6 * i,
        -6.630_004_717_661_677e-7 - 1.251_762_163_947_413_6e-6 * i,
        1.415_558_069_357_724_1e-6 - 5.170_583_070_313_951e-8 * i,
        1.423_546_434_769_548_4e-6 - 3.073_268_335_987_324e-8 * i,
        -6.977_177_655_794_099e-7 + 1.250_834_722_093_335_3e-6 * i,
        -7.034_226_055_137_438e-7 + 1.25797865901172e-06 * i,
        -6.208_426_062_550_659e-7 - 1.273_301_634_434_099e-6 * i,
        -6.351_578_108_300_549e-7 - 1.270_190_800_386_414_1e-6 * i,
        -6.963_867_334_769_441e-8 + 1.206_177_204_098_992_7e-7 * i,
    ];

    let tolerance = 1e-15;
    assert_vec_near!(q, expected_q, tolerance);
}

#[test]
fn test_conductor_geometry_from_project() {
    let json = include_bytes!("../../tests/M3.json");
    let project = Project::from_json(json.as_slice()).unwrap();
    let geometry = ConductorGeometry::from(&project).compute_I().unwrap();
    let epsilon = 1e-9;
    geometry
        .conductors
        .iter()
        .zip(conductors::CONDUCTORS.iter())
        .for_each(|(c, expected)| {
            assert_near!(c.U.re, expected.U.re, epsilon);
            assert_near!(c.U.im, expected.U.im, epsilon);
            assert_near!(c.I.re, expected.I.re, epsilon);
            assert_near!(c.I.im, expected.I.im, epsilon);
        });
    assert_eq!(geometry.conductor_groups.len(), 7);
}

#[test]
fn test_v_e_b_profile() {
    let json = include_bytes!("../../tests/M3_v_e_b.json");
    let project = Project::from_json(json.as_slice()).unwrap();
    let geometry = ConductorGeometry::from(&project)
        .compute_I()
        .unwrap()
        .compute_Q()
        .unwrap();
    let positions = sampling_points(&project.transverse_profile, project.alpha.as_radians());
    let expected_first_pos = [-100., 1.];
    let expected_last_pos = [100., 1.];
    assert_eq!(positions[0], expected_first_pos);
    assert_eq!(positions[400], expected_last_pos);
    let profile_e = compute_E_profile(positions.as_slice(), &geometry.conductors);
    // from GDB dump
    let i = numeric::complex::Complex::<f64>::new(0., 1.);
    let expected_profile_e = vec![[
        0.452261737518868 - 0.1086920584610169 * i,
        17.759924640277166 + 6.615_468_439_908_29 * i,
    ]];
    assert_vec_almost_eq!(&profile_e[..1], expected_profile_e);
    let profile_b = compute_B_profile(positions.as_slice(), &geometry.conductors);
    let expected_profile_b = vec![[
        -0.07432609794988343 - 0.002216445421840717 * i,
        -0.13924053613519072 - 0.13499925082004252 * i,
    ]];
    assert_vec_almost_eq!(&profile_b[..1], expected_profile_b);
}

#[test]
fn test_shield() {
    use conductors::SHIELDED_CONDUCTORS;
    let json = include_bytes!("../../tests/M3_shield.json");
    let project = Project::from_json(json.as_slice()).unwrap();
    let geometry = ConductorGeometry::from(&project).compute_I().unwrap();
    assert_eq!(geometry.rng_shields.len(), 12);
    let epsilon = 1e-9; // f64::EPSILON fails
    let i = geometry.conductors.iter().map(|c| c.I).collect::<Vec<_>>();
    let expected_i = SHIELDED_CONDUCTORS.iter().map(|c| c.I).collect::<Vec<_>>();
    assert_vec_near!(i, expected_i, epsilon);
    let u = geometry.conductors.iter().map(|c| c.U).collect::<Vec<_>>();
    let expected_u = SHIELDED_CONDUCTORS.iter().map(|c| c.U).collect::<Vec<_>>();
    assert_vec_near!(u, expected_u, epsilon);
}

#[test]
fn project_to_conductor_geometry_works_for_shielded_conductors() {
    let geometry = ConductorGeometry::from(&projects::shields())
        .compute_I()
        .unwrap();
    let i = numeric::complex::Complex::<f64>::new(0., 1.);
    let c_idx = |i, j, k| ConductorIndex {
        system_index: i,
        phase_index: j,
        conductor_index: k,
    };
    let expected_geometry = ConductorGeometry {
        rho: 100.,
        f: 50.,
        system_dividers: vec![3, 6],
        conductors: vec![
            Conductor {
                kind: ConductorType::Conductor,
                index: c_idx(0, 0, 0),
                c: [0.22, -0.85],
                c_m: [0.22, 0.85],
                r: 0.005,
                U: 28867.513459481292.into(),
                I: 300.0.into(),
                Q: 0.0.into(),
            },
            Conductor {
                kind: ConductorType::Conductor,
                index: c_idx(0, 1, 0),
                c: [0.28, -0.85],
                c_m: [0.28, 0.85],
                r: 0.005,
                U: -14433.756729740639 + 25000.0 * i,
                I: -150.0 + 259.8076211353316 * i,
                Q: 0.0.into(),
            },
            Conductor {
                kind: ConductorType::Conductor,
                index: c_idx(0, 2, 0),
                c: [0.25, -0.815],
                c_m: [0.25, 0.815],
                r: 0.005,
                U: -14433.756729740655 + -25000.0 * i,
                I: -150.0 + -259.80762113533149 * i,
                Q: 0.0.into(),
            },
            Conductor {
                kind: ConductorType::Conductor,
                index: c_idx(1, 0, 0),
                c: [-0.205, -0.98],
                c_m: [-0.205, 0.98],
                r: 0.005,
                U: 9237.6043070340129.into(),
                I: 500.0.into(),
                Q: 0.0.into(),
            },
            Conductor {
                kind: ConductorType::Conductor,
                index: c_idx(1, 1, 0),
                c: [-0.135, -0.98],
                c_m: [-0.135, 0.98],
                r: 0.005,
                U: -4618.8021535170046 + 8000.0 * i,
                I: -249.99999999999989 + 433.01270189221935 * i,
                Q: 0.0 + 0.0 * i,
            },
            Conductor {
                kind: ConductorType::Conductor,
                index: c_idx(1, 2, 0),
                c: [-0.17, -0.954],
                c_m: [-0.17, 0.954],
                r: 0.005,
                U: -4618.8021535170092 + -8000.0 * i,
                I: -250.0 + -433.01270189221918 * i,
                Q: 0.0 + 0.0 * i,
            },
            Conductor {
                kind: ConductorType::GuardWire,
                index: c_idx(0, 0, 0),
                c: [0.0, -0.85],
                c_m: [0.0, 0.85],
                r: 0.01,
                U: 0.0 + 0.0 * i,
                I: -0.46096512422394698 + 0.5462889776014872 * i,
                Q: 0.0 + 0.0 * i,
            },
            Conductor {
                kind: ConductorType::Shield,
                index: c_idx(0, 0, 0),
                c: [0.22, -0.85],
                c_m: [0.22, 0.85],
                r: 0.01,
                U: 0.0 + 0.0 * i,
                I: -1.1027049061153675 + -32.054738708074034 * i,
                Q: 0.0 + 0.0 * i,
            },
            Conductor {
                kind: ConductorType::Shield,
                index: c_idx(0, 1, 0),
                c: [0.28, -0.85],
                c_m: [0.28, 0.85],
                r: 0.01,
                U: 0.0 + 0.0 * i,
                I: -24.163938154841215 + 23.867353190433125 * i,
                Q: 0.0 + 0.0 * i,
            },
            Conductor {
                kind: ConductorType::Shield,
                index: c_idx(0, 2, 0),
                c: [0.25, -0.815],
                c_m: [0.25, 0.815],
                r: 0.01,
                U: 0.0 + 0.0 * i,
                I: 26.610580232985072 + 12.792559125774961 * i,
                Q: 0.0 + 0.0 * i,
            },
            Conductor {
                kind: ConductorType::Shield,
                index: c_idx(1, 0, 0),
                c: [-0.205, -0.98],
                c_m: [-0.205, 0.98],
                r: 0.01,
                U: 0.0 + 0.0 * i,
                I: 4.1362611161803748 + -30.920089646547218 * i,
                Q: 0.0 + 0.0 * i,
            },
            Conductor {
                kind: ConductorType::Shield,
                index: c_idx(1, 1, 0),
                c: [-0.135, -0.98],
                c_m: [-0.135, 0.98],
                r: 0.01,
                U: 0.0 + 0.0 * i,
                I: -19.559344647815941 + 17.592316719114169 * i,
                Q: 0.0 + 0.0 * i,
            },
            Conductor {
                kind: ConductorType::Shield,
                index: c_idx(1, 2, 0),
                c: [-0.17, -0.954],
                c_m: [-0.17, 0.954],
                r: 0.01,
                U: 0.0 + 0.0 * i,
                I: 20.542452962907557 + 7.754073755327382 * i,
                Q: 0.0 + 0.0 * i,
            },
        ],
        conductor_groups: vec![
            ConductorGroup {
                kind: ConductorType::Conductor,
                index: c_idx(0, 1, 0),
                position: [0.22, -0.85],
                r_bundle: 0.0,
                I: 300.0 + 0.0 * i,
                partial_conductors: 0..1,
                r_cnd: 0.005,
            },
            ConductorGroup {
                kind: ConductorType::Conductor,
                index: c_idx(0, 2, 0),
                position: [0.28, -0.85],
                r_bundle: 0.0,
                I: -150.0 + 259.8076211353316 * i,
                partial_conductors: 1..2,
                r_cnd: 0.005,
            },
            ConductorGroup {
                kind: ConductorType::Conductor,
                index: c_idx(0, 3, 0),
                position: [0.25, -0.815],
                r_bundle: 0.0,
                I: -150.0 + -259.80762113533149 * i,
                partial_conductors: 2..3,
                r_cnd: 0.005,
            },
            ConductorGroup {
                kind: ConductorType::Conductor,
                index: c_idx(1, 0, 0),
                position: [-0.205, -0.98],
                r_bundle: 0.0,
                I: 500.0 + 0.0 * i,
                partial_conductors: 3..4,
                r_cnd: 0.005,
            },
            ConductorGroup {
                kind: ConductorType::Conductor,
                index: c_idx(1, 1, 0),
                position: [-0.135, -0.98],
                r_bundle: 0.0,
                I: -250.0 + 433.01270189221935 * i,
                partial_conductors: 4..5,
                r_cnd: 0.005,
            },
            ConductorGroup {
                kind: ConductorType::Conductor,
                index: c_idx(1, 2, 0),
                position: [-0.17, -0.954],
                r_bundle: 0.0,
                I: -250.0 + -433.01270189221918 * i,
                partial_conductors: 5..6,
                r_cnd: 0.005,
            },
            ConductorGroup {
                kind: ConductorType::GuardWire,
                index: c_idx(0, 0, 0),
                position: [0.0, -0.85],
                r_bundle: 1.0,
                partial_conductors: 6..7,
                r_cnd: 0.01,
                I: 0.0 * i,
            },
        ],
        influenced_conductors: vec![
            GuardWire {
                x: 0.0,
                y: -0.85,
                r: 0.01,
                R_i: 0.002,
            },
            GuardWire {
                x: 0.22,
                y: -0.85,
                r: 0.01,
                R_i: 0.001,
            },
            GuardWire {
                x: 0.28,
                y: -0.85,
                r: 0.01,
                R_i: 0.001,
            },
            GuardWire {
                x: 0.25,
                y: -0.815,
                r: 0.01,
                R_i: 0.001,
            },
            GuardWire {
                x: -0.205,
                y: -0.98,
                r: 0.01,
                R_i: 0.002,
            },
            GuardWire {
                x: -0.135,
                y: -0.98,
                r: 0.01,
                R_i: 0.002,
            },
            GuardWire {
                x: -0.17,
                y: -0.954,
                r: 0.01,
                R_i: 0.002,
            },
        ],
        rng_guard_wires: 6..7,
        rng_shields: 7..13,
        rng_neutral_conductors: 6..13,
    };
    use numeric::assert_vec_almost_eq;
    assert_vec_almost_eq!(&geometry.conductors, &expected_geometry.conductors);
    assert_vec_almost_eq!(
        &geometry.conductor_groups,
        &expected_geometry.conductor_groups
    );
}

#[test]
fn test_compute_project() {
    let project = projects::ac_380_kv();
    let (_, results) = project.compute().unwrap();
    const N_GROUPS: usize = 7;
    assert_eq!(results.conductor_groups.group_values.len(), N_GROUPS);
    assert_eq!(
        results.conductor_groups.A.values.len(),
        results.conductor_groups.A.header.len() * N_GROUPS
    );
    assert_eq!(results.conductors.len(), 13);
    const N_SAMPLES: usize = 401;
    assert_eq!(results.transverse_profile.s.len(), N_SAMPLES);
    assert_eq!(
        results.transverse_profile.L.values.len(),
        N_SAMPLES * results.transverse_profile.L.header.len()
    );
}

#[test]
fn test_compute_u_is_near_0_near_guard_wire() {
    let project = projects::ac_380_kv();
    let geometry = ConductorGeometry::from(&project).compute_Q().unwrap();
    let (x, y) = (
        project.guardwires[0].x + project.guardwires[0].d / 2.,
        project.guardwires[0].y,
    );
    let u = compute_abs_2_u(x, y, &geometry.conductors).sqrt();
    assert!(u < 50.);
}

impl numeric::testing::NanDistance for &Conductor {
    fn nan_distance(self, other: Self) -> f64 {
        let all = [
            self.c[0].nan_distance(other.c[0]),
            self.c[1].nan_distance(other.c[1]),
            self.c_m[0].nan_distance(other.c_m[0]),
            self.c_m[1].nan_distance(other.c_m[1]),
            self.r.nan_distance(other.r),
            self.U.nan_distance(other.U),
            self.I.nan_distance(other.I),
            self.Q.nan_distance(other.Q),
        ];
        all.iter().copied().max_by(|a, b| a.total_cmp(b)).unwrap()
    }
}

impl numeric::testing::NanDistance for &ConductorGroup {
    fn nan_distance(self, other: Self) -> f64 {
        let all = [
            self.position[0].nan_distance(other.position[0]),
            self.position[1].nan_distance(other.position[1]),
            self.r_cnd.nan_distance(other.r_cnd),
            self.r_bundle.nan_distance(other.r_bundle),
            i16::from(self.partial_conductors != other.partial_conductors) as f64,
        ];
        all.iter().copied().max_by(|a, b| a.total_cmp(b)).unwrap()
    }
}
