//! Example project for tests
use crate::project::{
    Angle, GuardWire, Phase, Project, ShieldProperties, System, TransverseProfile,
};

/// Project with shielded conductors
pub fn shields() -> Project {
    Project {
        systems: vec![
            System {
                n_t: 1,
                beta_s: Angle(0.),
                a: 0.0,
                d: 0.01,
                U: 50000.,
                phi_U: Angle(0.),
                I: 300.,
                phi_I: Angle(0.),
                shield: ShieldProperties {
                    d: 0.02,
                    R_i: 0.001,
                    enabled: true,
                },
                phases: vec![
                    Phase { x: 0.22, y: 0.25 },
                    Phase { x: 0.28, y: 0.25 },
                    Phase { x: 0.25, y: 0.285 },
                ],
            },
            System {
                n_t: 1,
                beta_s: Angle(0.),
                a: 0.0,
                d: 0.01,
                U: 16000.,
                phi_U: Angle(0.),
                I: 500.,
                phi_I: Angle(0.),
                shield: ShieldProperties {
                    d: 0.02,
                    R_i: 0.002,
                    enabled: true,
                },
                phases: vec![
                    Phase { x: -0.205, y: 0.12 },
                    Phase { x: -0.135, y: 0.12 },
                    Phase { x: -0.17, y: 0.146 },
                ],
            },
        ],
        guardwires: vec![GuardWire {
            x: 0.,
            y: 0.25,
            d: 0.02,
            R_i: 0.001,
        }],
        rho: 100.,
        f_grid: 50.,
        alpha: Angle(0.),
        y_ref: -1.1,
        transverse_profile: TransverseProfile {
            y: 0.2,
            ..Default::default()
        },
        ..Default::default()
    }
}

/// Project with AC systems at 380 kV
pub fn ac_380_kv() -> Project {
    Project {
        systems: vec![
            System {
                n_t: 2,
                beta_s: Angle(0.),
                a: 0.4,
                d: 0.03,
                U: 380000.,
                phi_U: Angle(0.),
                I: 1000.,
                phi_I: Angle(0.),
                shield: Default::default(),
                phases: vec![
                    Phase { x: -8.0, y: 40.0 },
                    Phase { x: -11.0, y: 30.0 },
                    Phase { x: -9.0, y: 20.0 },
                ],
            },
            System {
                n_t: 2,
                beta_s: Angle(0.),
                a: 0.4,
                d: 0.03,
                U: 380000.,
                phi_U: Angle(0.),
                I: 1000.,
                phi_I: Angle(0.),
                shield: Default::default(),
                phases: vec![
                    Phase { x: 9.0, y: 20.0 },
                    Phase { x: 11.0, y: 30.0 },
                    Phase { x: 8.0, y: 40.0 },
                ],
            },
        ],
        guardwires: vec![GuardWire {
            x: 0.0,
            y: 50.0,
            d: 0.02,
            R_i: 0.001,
        }],
        rho: 100.,
        f_grid: 50.,
        alpha: Angle(0.),
        y_ref: 0.0,
        transverse_profile: TransverseProfile {
            m: 401,
            y: 1.0,
            x_min: -100.0,
            x_max: 100.0,
            columns: crate::project::TransverseProfileColumns {
                with_e_abs: true,
                with_e_components: true,
                with_b_abs: true,
                with_b_components: true,
            },
            ..Default::default()
        },
        corona_sound: crate::project::CoronaSound {
            rain_rate: 0.75,
            ..Default::default()
        },
        ..Default::default()
    }
}

/// Project with AC systems at 420 kV
pub fn ac_420_kv() -> Project {
    Project {
        systems: vec![
            System {
                n_t: 2,
                beta_s: Angle(0.),
                a: 0.4,
                d: 0.0319,
                U: 420000.,
                phi_U: Angle(0.),
                I: 2530.,
                phi_I: Angle(0.),
                shield: Default::default(),
                phases: vec![
                    Phase { x: 8.0, y: 15.0 },
                    Phase { x: 10.0, y: 24.3 },
                    Phase { x: 7.0, y: 35.3 },
                ],
            },
            System {
                n_t: 2,
                beta_s: Angle(0.),
                a: 0.4,
                d: 0.0261,
                U: 245000.,
                phi_U: Angle(0.),
                I: 1950.,
                phi_I: Angle(0.),
                shield: Default::default(),
                phases: vec![
                    Phase { x: -7.0, y: 37.1 },
                    Phase { x: -10.0, y: 26.1 },
                    Phase { x: -8.0, y: 16.8 },
                ],
            },
        ],
        guardwires: vec![GuardWire {
            x: 0.0,
            y: 52.6,
            d: 0.03,
            R_i: 0.001,
        }],
        rho: 100.,
        f_grid: 50.,
        alpha: Angle(0.),
        y_ref: 0.0,
        transverse_profile: TransverseProfile {
            y: 0.0,
            ..Default::default()
        },
        ..Default::default()
    }
}
