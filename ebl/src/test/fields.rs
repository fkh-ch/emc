//! Test computation of field strengths
use super::*;
use numeric::{
    numdiff::{lim_0_err, numdiff_err},
    testing::NanDistance,
};

fn check_derivatives(
    f: impl Fn(f64, f64) -> f64,
    df: impl Fn(f64, f64) -> numeric::contour_lines::Derivatives,
) {
    const N: usize = 10;
    const M: usize = 10;
    const D: f64 = 0.25;
    const PAD: f64 = 5.0;
    const REL_TOL: f64 = 1e-4;
    #[derive(Debug, Clone, Copy)]
    struct Mismatch<T> {
        x: f64,
        y: f64,
        tol: f64,
        value: T,
        approximated: T,
    }
    impl<T: std::fmt::Debug> std::fmt::Display for Mismatch<T>
    where
        for<'a> &'a T: IntoIterator<Item = &'a f64>,
    {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            let Mismatch {
                x,
                y,
                tol,
                value,
                approximated,
            } = self;
            let err = value
                .into_iter()
                .zip(approximated)
                .map(|(a, b)| f64::abs(*a - *b))
                .reduce(f64::max)
                .unwrap_or(0.);
            write!(
                f,
                r#"({x:.2}, {y:.2}):
 value={value:?}
 approx={approximated:?}
 δ={err}
 tol={tol}
"#
            )
        }
    }
    let is_rel_near = |a: f64, b: f64, tol: f64| {
        let d = a.nan_distance(b);
        if d.is_infinite() {
            return false;
        }
        d <= tol * f64::max(a.nan_distance(0.0), b.nan_distance(0.0))
    };
    let is_error = |values: &[f64], reference: &[f64], abs: &[f64]| {
        values
            .iter()
            .zip(reference)
            .zip(abs)
            .any(|((a, b), tol)| a.nan_distance(b) > PAD * tol && !is_rel_near(*a, *b, REL_TOL))
    };
    let mut grad_errors = [[None; N]; M];
    let mut hess_errors = [[None; N]; M];
    for (i, x) in linspace(-10., 10., N).enumerate() {
        for (j, y) in linspace(-10., 10., M).enumerate() {
            let (dx, dy) = (D, D);
            let f0 = f(x, y);
            let (fx_num, fx_err) = numdiff_err(|t| f(t, y), x, dx);
            let (fy_num, fy_err) = numdiff_err(|t| f(x, t), y, dy);
            let df0 = df(x, y);
            grad_errors[j][i] = is_error(&df0.grad, &[fx_num, fy_num], &[fx_err, fy_err])
                .then_some(Mismatch {
                    x,
                    y,
                    tol: f64::min(fx_err, fy_err),
                    value: df0.grad,
                    approximated: [fx_num, fy_num],
                });
            // f''(x) = (f(x+dx) + f(x-dx) -2f(x))/dx² + O(dx^3)
            let (fxx_num, fxx_err) =
                lim_0_err(|h| (f(x + h, y) + f(x - h, y) - 2. * f0) / (h * h), dx);
            let (fyy_num, fyy_err) =
                lim_0_err(|h| (f(x, y + h) + f(x, y - h) - 2. * f0) / (h * h), dy);
            let (fxy_num, fxy_err) = lim_0_err(
                |h| {
                    (f(x + h, y + h) - f(x - h, y + h) - f(x + h, y - h) + f(x - h, y - h))
                        / (4. * h * h)
                },
                D,
            );
            hess_errors[j][i] = is_error(
                &[df0.hess_xx, df0.hess_xy, df0.hess_yy],
                &[fxx_num, fxy_num, fyy_num],
                &[fxx_err, fxy_err, fyy_err],
            )
            .then_some(Mismatch {
                x,
                y,
                tol: f64::min(f64::min(fxx_err, fxy_err), fyy_err),
                value: [df0.hess_xx, df0.hess_xy, df0.hess_yy],
                approximated: [fxx_num, fxy_num, fyy_num],
            });
        }
    }
    fn format_err<const SIZE: usize>(errors: &[[Option<Mismatch<[f64; SIZE]>>; N]]) -> String {
        let mut s = String::new();
        for row in errors {
            for col in row {
                s += if col.is_some() { "■" } else { "□" };
            }
            s += "\n";
        }
        for row in errors {
            for col in row {
                if let Some(mismatch) = col {
                    s += &format!("{mismatch}");
                }
            }
        }
        s
    }
    assert!(
        !grad_errors
            .iter()
            .any(|row| row.iter().any(|e| e.is_some())),
        "grad violation: \n{}",
        format_err(&grad_errors)
    );
    assert!(
        !hess_errors
            .iter()
            .any(|row| row.iter().any(|e| e.is_some())),
        "hess violation: \n{}",
        format_err(&hess_errors)
    );
}

fn linspace(x_min: f64, x_max: f64, m: usize) -> impl Iterator<Item = f64> {
    assert!(m > 1);
    assert!((m - 1) as f64 as usize == (m - 1));
    let dx = (x_max - x_min) / ((m - 1) as f64);
    (0..m).map(move |i| x_min + (i as f64) * dx)
}

fn e_conductors() -> [Conductor; 2] {
    [
        Conductor {
            kind: ConductorType::Conductor,
            index: Default::default(),
            c: [0., 1.],
            c_m: [0., -1.],
            Q: Complex::from(1.0e-8),
            ..Default::default()
        },
        Conductor {
            kind: ConductorType::Conductor,
            index: Default::default(),
            c: [1., 2.],
            c_m: [1., -2.],
            Q: Complex {
                re: 0.0,
                im: 2.0e-8,
            },
            ..Default::default()
        },
    ]
}

#[test]
fn test_compute_d_abs_2_e() {
    let conductors = e_conductors();
    let f = |x, y| c2_abs_2(&compute_E(x, y, &conductors));
    let df = |x, y| compute_d_abs_2_e(x, y, &conductors);
    check_derivatives(f, df);
}

#[test]
fn test_compute_d_re_u() {
    let conductors = e_conductors();
    let f = |x, y| compute_re_u(x, y, &conductors);
    let df = |x, y| compute_d_re_u(x, y, &conductors);
    check_derivatives(f, df);
}

#[test]
fn test_compute_d_abs_2_u() {
    let conductors = e_conductors();
    let f = |x, y| compute_abs_2_u(x, y, &conductors);
    let df = |x, y| compute_d_abs_2_u(x, y, &conductors);
    check_derivatives(f, df);
}

fn b_conductors() -> [Conductor; 2] {
    [
        Conductor {
            kind: ConductorType::Conductor,
            index: Default::default(),
            c: [0., 1.],
            r: 1.5,
            I: Complex::from(1.0e4),
            ..Default::default()
        },
        Conductor {
            kind: ConductorType::Conductor,
            index: Default::default(),
            c: [1., 2.],
            r: 0.0,
            I: Complex { re: 0.0, im: 2.0e4 },
            ..Default::default()
        },
    ]
}

#[test]
fn test_compute_d_abs_2_b() {
    let conductors = b_conductors();
    let f = |x, y| c2_abs_2(&compute_B(x, y, &conductors));
    let df = |x, y| compute_d_abs_2_b(x, y, &conductors);
    check_derivatives(f, df);
}

#[test]
fn test_compute_d_re_a() {
    let conductors = b_conductors();
    let f = |x, y| compute_re_a(x, y, &conductors);
    let df = |x, y| compute_d_re_a(x, y, &conductors);
    check_derivatives(f, df);
}

#[test]
fn test_compute_d_abs_2_a() {
    let conductors = b_conductors();
    let f = |x, y| compute_abs_2_a(x, y, &conductors);
    let df = |x, y| compute_d_abs_2_a(x, y, &conductors);
    check_derivatives(f, df);
}
