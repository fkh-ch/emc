//! Corona sound routines

pub use crate::{project::CoronaSound, Conductor};

use crate::{
    output::result_values::{ConductorGroupResults, ConductorGroupValues},
    ConductorGeometry,
};

/// A value paired with the info if the parameters were feasible
#[derive(Debug, Clone, Copy, Default)]
pub struct FeasibleAValue {
    /// Actual value
    pub value: f64,
    /// True if the parameters used to compute the value were in the feasible region of the routine
    pub feasible: bool,
}

#[allow(non_snake_case)]
#[derive(Debug, Clone)]
/// Sound level routine
pub struct SoundLevelRoutine {
    /// Name of the routine
    pub name: &'static str,
    /// Reference rain rate [mm/h]
    pub reference_rain_rate: f64,
    /// Sound absorption of atmospheric air []
    pub D_A_0: f64,
    /// Offset per logarithm of distance for base sound level when using atmospheric air absorption
    pub D_A: f64,
    /// Compute the sound level value [dB(A)] as a function of
    /// `E_max` [V/m],
    /// `r`: conductor radius \[m],
    /// `R`: bundle radius \[m],
    /// `n`: the number of of conductors per bundle
    pub L: fn(E_max: f64, r: f64, R: f64, n: usize) -> f64,
    /// Predicate defining for which values of `U_max`, `r`, `R`, `n_t_s`
    /// the routine is feasible
    pub is_in_scope: fn(f64, f64, f64, usize) -> bool,
}

/// Parameters for the annual assessment, based on BPA MOD at a rain rate of 1 mm/h
#[allow(non_snake_case)]
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Deserialize, serde::Serialize)]
pub struct AnnualAssessment {
    /// Enable computation of the annual assessment
    pub enabled: bool,
    /// Relative rain duration t_i/t_0 at night with r≥0.2 mm/h \[h]
    pub ti_t0: f64,
    /// Correction for audible tonality content [dB(A)]
    pub c: f64,
}

impl Default for AnnualAssessment {
    fn default() -> Self {
        Self {
            enabled: false,
            ti_t0: 0.08,
            c: 0.0,
        }
    }
}

/// Table for the results of the different corona sound computation methods (routines)
///
/// Each column contains values for one routine
#[derive(Debug, Clone, Default)]
pub struct LTable<T> {
    /// Flat array containing sound level values.
    /// Memory layout: row major, where columns correspond to sound level
    /// routines present in [L_ROUTINES] and rows correspond to conductor groups
    pub values: Vec<T>,
    /// Column headers
    pub header: &'static [&'static str],
}

impl<T> LTable<T> {
    /// Iterate over rows containing values for each selected sound routine
    pub fn rows(&self) -> std::slice::ChunksExact<'_, T> {
        self.values.chunks_exact(self.header.len())
    }

    /// Create an empty table
    pub fn new() -> Self {
        LTable {
            values: Vec::new(),
            header: &[],
        }
    }
}

/// Field strength weighted rain rate correction relative to a reference rain rate of `r_wc = 0.75mm/h` according to EPRI.
fn rain_correction(rain_rate: f64, reference_rain_rate: f64, A_wc: f64) -> f64 {
    const R_HR: f64 = 6.5; // Heavy rain rate [mm/h]
    const R_WC: f64 = 0.75; // Light rain (wet conductors) rate [mm/h]
    -A_wc * f64::log10(rain_rate / reference_rain_rate) / f64::log10(R_HR / R_WC)
}

/// Expression used in the rain correction term
fn compute_A_wc(Emax: f64, r: f64, R: f64, n_t_s: usize) -> f64 {
    let d = 200. * r; // r [m] -> ⌀ [cm]
    let A = -346.5e5 / (Emax * d.powf(0.24)); // V/m -> kV/cm
    match n_t_s {
        0..=2 => A + 8.2,
        3..=8 => A + 10.4 + 8. * (n_t_s - 1) as f64 * r / R,
        _ => A,
    }
}

/// Correction term for the altitude
///
/// h: Altitude over sea level [m]
fn Delta_h(h: f64) -> f64 {
    h / 300. - 1.
}

/// Difference to transform noise power to noise pressure (in 1m distance)
const DELTA_L: f64 = 5.7;

/// All available sound routines
pub const L_ROUTINES: &[&SoundLevelRoutine] = &[&EPRI, &BPA, &EDF, &CIGRE, &BPAMOD];

/// Header for csv export (including annual assessment level)
pub const L_ROUTINE_NAMES: [&str; L_ROUTINES.len() + 1] = const {
    let mut names = [""; L_ROUTINES.len() + 1];
    let mut i = 0;
    while i < L_ROUTINES.len() {
        names[i] = L_ROUTINES[i].name;
        i += 1;
    }
    names[names.len() - 1] = "Annual Assessment Level";
    names
};

/// Empirical formula defined by EPRI for AC lines
pub const EPRI: SoundLevelRoutine = SoundLevelRoutine {
    name: "EPRI",
    reference_rain_rate: 6.5,
    D_A: 10.0,
    D_A_0: 0.02,
    L: |Emax, r, R, n| {
        let d = 200. * r; // r [m] -> ⌀ [cm]
        let Emax = Emax * 1e-5; // V/m -> kV/cm
        let kn = match n {
            1 => 7.5,
            2 => 2.6,
            _ => 0.0,
        };
        match n {
            0..=2 => 20. * f64::log10(n as f64) + 44. * f64::log10(d) - 665.0 / Emax + kn + 80.9,
            3..=8 => {
                20. * f64::log10(n as f64) + 44. * f64::log10(d) - 665.0 / Emax
                    + 22.9 * (n - 1) as f64 * r / R
                    + 73.6
            }
            _ => 0.,
        }
    },
    is_in_scope: |Umax, r, _R, n| {
        (220.0e3..=1.5e6).contains(&Umax) && n <= 8 && (0.01..=0.03).contains(&r)
    },
};

/// Empirical formula defined by BPA for AC lines
pub const BPA: SoundLevelRoutine = SoundLevelRoutine {
    name: "BPA",
    reference_rain_rate: 1.0,
    D_A: 11.4,
    D_A_0: 0.0,
    L: |Emax, r, _R, n| {
        let Emax = Emax * 1e-5; // V/m -> kV/cm
        let d = 200. * r; // r [m] -> ⌀ [cm]
        let (k1, k2) = match n {
            0..=2 => (-109.7, 0.),
            _ => (-122.7, 26.4),
        };
        k1 + 120. * f64::log10(Emax) + k2 * f64::log10(n as f64) + 55. * f64::log10(d)
    },
    is_in_scope: |Umax, r, _R, n| {
        (120.0e3..=1.5e6).contains(&Umax) && n <= 16 && (0.01..=0.0325).contains(&r)
    },
};

/// Empirical formula defined by BPA for AC lines (modified version)
pub const BPAMOD: SoundLevelRoutine = SoundLevelRoutine {
    name: "BPA MOD",
    reference_rain_rate: 1.0,
    D_A: 10.0,
    D_A_0: 0.02,
    L: |Emax, r, _R, n| {
        const DELTA_E_MIN: f64 = 1.0e-100;
        let d = 200. * r; // r [m] -> ⌀ [cm]
        let Emax = Emax * 1e-5; // V/m -> kV/cm
        -67.0
            + 83. * f64::log10(f64::max(DELTA_E_MIN, Emax - 5.8))
            + 55. * f64::log10(d)
            + 7.1 * f64::log10((n as f64).powf(4.3) + 193.0)
    },
    is_in_scope: |Umax, r, _R, n| {
        (120.0e3..=1.5e6).contains(&Umax) && n <= 16 && (0.01..=0.0325).contains(&r)
    },
};

#[allow(non_snake_case)]
/// Validity predicate which can be used in [SoundLevelRoutine] for routines
/// with full scope
fn always(_Umax: f64, _r: f64, _R: f64, _n: usize) -> bool {
    true
}

/// Empirical formula defined by EDF for AC lines
pub const EDF: SoundLevelRoutine = SoundLevelRoutine {
    name: "EDF",
    reference_rain_rate: 6.0,
    D_A_0: 0.02,
    D_A: 10.,
    L: |Emax, r, _R, n| {
        const K_L: f64 = 44.024; // [dB]
        const E_0: f64 = 6.991E5; // [V/m]
        const E_1: f64 = 9.883E4; // [V/m]
        if Emax <= E_0 {
            return -100.;
        }
        K_L * f64::log10((Emax - E_0) / E_1) + 900. * r + 15. * f64::log10(n as f64) + DELTA_L
    },
    is_in_scope: always,
};

/// L-CIGRE
pub const CIGRE: SoundLevelRoutine = SoundLevelRoutine {
    name: "CIGRE",
    reference_rain_rate: 6.5,
    D_A: 10.,
    D_A_0: 0.02,
    L: |Emax, r, _R, n| {
        let d = 200. * r; // r [m] -> ⌀ [cm]
        85. - 65.0e6 / Emax + 40. * f64::log10(d) + 15. * f64::log10(n as f64)
    },
    is_in_scope: always,
};

/// |Q| / (2π ε_0 r)
pub fn e_max(conductor: &Conductor) -> f64 {
    conductor.Q.norm() / conductor.r
}

/// Prepare empty conductor group result values prefilled with surface field strength
fn prepare_conductor_group_values(
    geometry: &ConductorGeometry,
) -> Result<Vec<ConductorGroupValues>, ZeroBundleRadius> {
    geometry
        .conductor_groups
        .iter()
        .map(|b| with_e(b, geometry))
        .collect()
}

/// Transpose contiguous row major values from shape (m, n) to
/// shape (n, m)
fn transpose<T: Clone>(tab: &[T], m: usize, n: usize) -> Vec<T> {
    let mut transposed = Vec::with_capacity(tab.len());
    for j in 0..n {
        for i in 0..m {
            transposed.push(tab[i * n + j].clone());
        }
    }
    transposed
}

/// Compute corona sound levels along sampling points
pub fn compute_corona_sound(
    g: &ConductorGeometry,
    sampling_points: &[[f64; 2]],
    Umax: f64,
    corona_sound: &CoronaSound,
) -> Result<(ConductorGroupResults, LTable<f64>), ZeroBundleRadius> {
    let group_values = prepare_conductor_group_values(g)?;
    let n_routines =
        L_ROUTINES.len() + usize::from(u8::from(corona_sound.annual_assessment.enabled));
    let mut A = Vec::with_capacity(n_routines * group_values.len());
    let mut l_values = Vec::<f64>::with_capacity(n_routines * sampling_points.len());
    for routine in L_ROUTINES {
        let a_values = routine.compute_conductor_groups(&group_values, corona_sound);
        let a_feasability = routine.check_feasability(&group_values, Umax);
        let mut l = routine.compute_cross_section(
            sampling_points,
            &g.conductor_groups,
            &a_values,
            corona_sound.enable_atmospheric_air_absorption,
        );
        for (value, feasible) in a_values.into_iter().zip(a_feasability) {
            A.push(FeasibleAValue { value, feasible });
        }
        l_values.append(&mut l);
    }
    if corona_sound.annual_assessment.enabled {
        let (mut a, mut l) = compute_annual_assessment(
            &group_values,
            sampling_points,
            Umax,
            &corona_sound.annual_assessment,
        );
        l_values.append(&mut l);
        A.append(&mut a);
    }
    let header = &L_ROUTINE_NAMES[..n_routines];
    Ok((
        ConductorGroupResults {
            group_values,
            A: LTable {
                header,
                values: transpose(&A, n_routines, g.conductor_groups.len()),
            },
        },
        LTable {
            header,
            values: transpose(&l_values, n_routines, sampling_points.len()),
        },
    ))
}

impl SoundLevelRoutine {
    fn compute_conductor_group(
        &self,
        conductor_group_values: &ConductorGroupValues,
        corona_sound: &CoronaSound,
    ) -> f64 {
        let group = &conductor_group_values.group;
        (self.L)(
            conductor_group_values.E_max,
            group.r_cnd,
            group.r_bundle,
            conductor_group_values.n,
        ) + rain_correction(
            corona_sound.rain_rate,
            self.reference_rain_rate,
            conductor_group_values.A_wc,
        ) + Delta_h(corona_sound.h_0)
    }
    fn compute_conductor_groups(
        &self,
        conductor_group_values: &[ConductorGroupValues],
        corona_sound: &CoronaSound,
    ) -> Vec<f64> {
        conductor_group_values
            .iter()
            .map(|group| self.compute_conductor_group(group, corona_sound))
            .collect()
    }
    fn check_feasability(
        &self,
        conductor_group_values: &[ConductorGroupValues],
        Umax: f64,
    ) -> Vec<bool> {
        conductor_group_values
            .iter()
            .map(|group_values| {
                let group = &group_values.group;
                (self.is_in_scope)(Umax, group.r_cnd, group.r_bundle, group_values.n)
            })
            .collect()
    }
    fn compute_cross_section<T: AsRef<crate::ConductorGroup>>(
        &self,
        sampling_points: &[[f64; 2]],
        groups: &[T],
        l_values: &[f64],
        enable_atmospheric_air_absorption: bool,
    ) -> Vec<f64> {
        let dL = if enable_atmospheric_air_absorption {
            self.D_A_0
        } else {
            0.
        };
        let sum_L0: f64 = l_values.iter().sum();
        if sum_L0 == 0. {
            return vec![0.; sampling_points.len()];
        }
        let mut L = Vec::with_capacity(sampling_points.len());

        for p in sampling_points {
            let mut l = 0.;
            for (group, a) in groups.iter().zip(l_values) {
                let c = &group.as_ref().position;
                let d = f64::hypot(p[0] - c[0], p[1] - c[1]);
                if *a != 0. {
                    l += 10.0f64.powf(0.1 * (a - DELTA_L - dL * d - self.D_A * f64::log10(d)));
                }
            }
            L.push(10. * f64::log10(l));
        }
        L
    }
}

/// Annual assessment level based on BPA MOD at a rain rate of 1 mm/h
pub fn compute_annual_assessment(
    conductor_group_values: &[ConductorGroupValues],
    sampling_points: &[[f64; 2]],
    Umax: f64,
    parameters: &AnnualAssessment,
) -> (Vec<FeasibleAValue>, Vec<f64>) {
    let env = CoronaSound {
        h_0: 300.,
        rain_rate: 1.0,
        enable_atmospheric_air_absorption: true,
        ..Default::default()
    };
    let a_values = BPAMOD.compute_conductor_groups(conductor_group_values, &env);
    let a_feasability = BPAMOD.check_feasability(conductor_group_values, Umax);
    let mut L = BPAMOD.compute_cross_section(
        sampling_points,
        conductor_group_values,
        &a_values,
        env.enable_atmospheric_air_absorption,
    );
    // Correction for industrial and commercial noise (fixed):
    let K_1 = 5.0; // dB(A)
    let correction = K_1 + parameters.c + 10.0 * f64::log10(parameters.ti_t0);
    for l in &mut L {
        *l += correction;
    }
    (
        a_values
            .into_iter()
            .zip(a_feasability)
            .map(|(value, feasible)| FeasibleAValue { value, feasible })
            .collect(),
        L,
    )
}

/// Error occurring when the radius of a bundle is zero
#[derive(Debug)]
pub struct ZeroBundleRadius {
    /// Index of the system containing the 0 radius bundle
    pub system_index: usize,
    /// Index of the phase with 0 bundle radius
    pub phase_index: usize,
}

impl std::fmt::Display for ZeroBundleRadius {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "system {}, phase {}: radius bundle is 0",
            self.system_index, self.phase_index
        )
    }
}

impl core::error::Error for ZeroBundleRadius {}

fn with_e(
    group: &crate::ConductorGroup,
    g: &ConductorGeometry,
) -> Result<ConductorGroupValues, ZeroBundleRadius> {
    let partial_conductors = &g.conductors[group.partial_conductors.clone()];
    if partial_conductors.len() > 1 && group.r_bundle == 0. {
        return Err(ZeroBundleRadius {
            system_index: group.index.system_index,
            phase_index: group.index.phase_index,
        });
    }
    let n_t = group.partial_conductors.len();
    let r_s = group.r_cnd;
    let v_k_max_s = if partial_conductors.len() <= 1 {
        1.
    } else {
        1. + (n_t - 1) as f64 * r_s / group.r_bundle
    };
    let E_max = partial_conductors.iter().map(e_max).sum::<f64>() * v_k_max_s / n_t as f64;
    let A_wc = compute_A_wc(E_max, r_s, group.r_bundle, n_t);
    Ok(ConductorGroupValues {
        group: group.clone(),
        E_max,
        n: group.partial_conductors.len(),
        d: 200. * group.r_cnd,
        A_wc,
    })
}

#[cfg(test)]
mod test {
    use crate::test;

    use super::*;

    #[test]
    fn test_transpose() {
        let a = [1., 2., 3., 4., 5., 6.];
        let a_t = transpose(&a, 2, 3);
        assert_eq!(&a_t, &[1., 4., 2., 5., 3., 6.]);
    }

    const CORONA_SOUND: CoronaSound = CoronaSound {
        enabled: true,
        h_0: 0.,
        rain_rate: 0.75,
        enable_atmospheric_air_absorption: true,
        annual_assessment: AnnualAssessment {
            enabled: false,
            ti_t0: 0.0,
            c: 4.0,
        },
    };
    fn sampling_points() -> Vec<[f64; 2]> {
        (-6..=6).map(|i| [10. * i as f64, 0.]).collect()
    }

    const E_MAX_AC_420_KV: &[f64] = &[
        17.06566133,
        17.18384275,
        16.29527149,
        12.08539507,
        11.87845342,
        11.97188224,
        1.33279722,
    ];

    const E_MAX_AC_380_KV: &[f64] = &[15.98, 16.42, 16.0, 16.0, 16.42, 15.98, 2.19];

    struct Tower {
        geometry: ConductorGeometry,
        groups: Vec<ConductorGroupValues>,
    }

    impl Tower {
        fn new(project: crate::project::Project) -> Self {
            let mut geometry: ConductorGeometry = (&project).try_into().unwrap();
            crate::compute_Q(&mut geometry.conductors).unwrap();
            let groups = prepare_conductor_group_values(&geometry).unwrap();
            Self { geometry, groups }
        }

        fn with_E_max(self, E_max_values: &[f64]) -> Self {
            Self {
                groups: self
                    .groups
                    .into_iter()
                    .zip(E_max_values)
                    .map(|(g, e)| ConductorGroupValues {
                        E_max: e * 1e5,
                        ..g
                    })
                    .collect(),
                ..self
            }
        }
    }

    #[test]
    fn test_ac_epri() {
        let tower = Tower::new(test::projects::ac_420_kv()).with_E_max(E_MAX_AC_420_KV);
        let l_w50 = EPRI.compute_conductor_groups(&tower.groups, &CORONA_SOUND);
        let v_c = sampling_points();
        let L = EPRI.compute_cross_section(
            &v_c,
            &tower.geometry.conductor_groups,
            &l_w50,
            CORONA_SOUND.enable_atmospheric_air_absorption,
        );
        let expected_l_w50 = [
            -55.448738,
            -55.075042,
            -58.017581,
            -82.745488,
            -84.100858,
            -83.483141,
            -710.166885,
        ]
        .into_iter()
        .map(|v| v + 120.)
        .collect::<Vec<_>>();
        let expected_L = [
            43.01703571812674,  // -60
            43.80706260899289,  // -50
            44.67522348195591,  // -40
            45.63865509037108,  // -30
            46.71379238659792,  // -20
            47.89516689279349,  // -10
            49.035587110179904, // 0
            49.46342478874003,  // 10
            48.713381606903326, // 20
            47.54835349240712,  // 30
            46.403361794135776, // 40
            45.362704635051,    // 50
            44.42714518940892,  // 60
        ];
        numeric::assert_vec_near!(l_w50, expected_l_w50, 0.05);
        numeric::assert_vec_near!(L, expected_L, 0.05);
    }

    #[test]
    fn test_ac_bpa() {
        let tower = Tower::new(test::projects::ac_420_kv()).with_E_max(E_MAX_AC_420_KV);
        let l_w50 = BPA.compute_conductor_groups(&tower.groups, &CORONA_SOUND);
        let v_c = sampling_points();
        let L = BPA.compute_cross_section(
            &v_c,
            &tower.geometry.conductor_groups,
            &l_w50,
            CORONA_SOUND.enable_atmospheric_air_absorption,
        );
        let expected_l_w50 = [
            -56.091776,
            -55.718035,
            -58.595954,
            -79.854879,
            -80.807846,
            -80.375454,
            -216.07822408786407,
        ]
        .into_iter()
        .map(|v| v + 120.)
        .collect::<Vec<_>>();
        let expected_L = [
            41.247801, // -60
            41.934452, // -50
            42.714654, // -40
            43.610289, // -30
            44.644312, // -20
            45.819147, // -10
            46.989837, // 0
            47.436132, // 10
            46.6484,   // 20
            45.461329, // 30
            44.333482, // 40
            43.34281,  // 50
            42.482038, // 60
        ];
        numeric::assert_vec_near!(l_w50, expected_l_w50, 0.05);
        numeric::assert_vec_near!(L, expected_L, 0.05);
    }

    #[test]
    fn test_ac_edf() {
        let tower = Tower::new(test::projects::ac_420_kv()).with_E_max(E_MAX_AC_420_KV);
        let corona_sound = CoronaSound {
            rain_rate: 0.8,
            ..CORONA_SOUND
        };
        let l_w50 = EDF.compute_conductor_groups(&tower.groups, &corona_sound);
        let v_c = sampling_points();
        let L = EDF.compute_cross_section(
            &v_c,
            &tower.geometry.conductor_groups,
            &l_w50,
            CORONA_SOUND.enable_atmospheric_air_absorption,
        );
        let expected_l_w50 = [
            -58.727329,
            -58.40573,
            -60.926225,
            -81.282965,
            -82.446019,
            -81.915269,
            -407.24539028492313,
        ]
        .into_iter()
        .map(|v| v + 120.)
        .collect::<Vec<_>>();
        let expected_L = [
            39.81458,  // -60
            40.603631, // -50
            41.47017,  // -40
            42.430807, // -30
            43.500901, // -20
            44.673008, // -10
            45.800588, // 0
            46.221238, // 10
            45.47373,  // 20
            44.314832, // 30
            43.17559,  // 40
            42.139382, // 50
            41.207099, // 60
        ];
        numeric::assert_vec_near!(l_w50, expected_l_w50, 0.05);
        numeric::assert_vec_near!(L, expected_L, 0.05);
    }

    #[test]
    fn test_ac_bpa_mod() {
        let tower = Tower::new(test::projects::ac_380_kv()).with_E_max(E_MAX_AC_380_KV);

        let corona_sound = CoronaSound {
            rain_rate: 1.0,
            h_0: 300.,
            ..CORONA_SOUND
        };
        let l_w50 = BPAMOD.compute_conductor_groups(&tower.groups, &corona_sound);
        let v_c = sampling_points();
        let L = BPAMOD.compute_cross_section(
            &v_c,
            &tower.geometry.conductor_groups,
            &l_w50,
            CORONA_SOUND.enable_atmospheric_air_absorption,
        );
        let expected_l_w50 = [
            59.411859,
            60.937129,
            59.482608,
            59.482608,
            60.937129,
            59.411859,
            -8334.199957955978,
        ];
        let expected_L = [
            42.552327, // -60
            43.353041, // -50
            44.208034, // -40
            45.096025, // -30
            45.939072, // -20
            46.547744, // -10
            46.747666, // 0
            46.547744, // 10
            45.939072, // 20
            45.096025, // 30
            44.208034, // 40
            43.353041, // 50
            42.552327, // 60
        ];
        numeric::assert_vec_near!(l_w50, expected_l_w50, 0.05);
        numeric::assert_vec_near!(L, expected_L, 0.05);
    }

    #[test]
    fn test_annual_assessment() {
        let tower = Tower::new(test::projects::ac_380_kv()).with_E_max(E_MAX_AC_380_KV);
        let parameters = AnnualAssessment {
            enabled: true,
            ti_t0: 0.11,
            c: 4.0,
        };
        let v_c = sampling_points();
        let Umax = 0.0;
        let (l_w50, L) = compute_annual_assessment(&tower.groups, &v_c, Umax, &parameters);
        let l_w50 = l_w50.into_iter().map(|val| val.value).collect::<Vec<_>>();
        let expected_l_w50 = [
            59.411859,
            60.937129,
            59.482608,
            59.482608,
            60.937129,
            59.411859,
            -8334.199957955978,
        ];
        let expected_L = [
            41.966254, // -60
            42.766968, // -50
            43.621961, // -40
            44.509952, // -30
            45.352999, // -20
            45.961671, // -10
            46.161593, // 0
            45.961671, // 10
            45.352999, // 20
            44.509952, // 30
            43.621961, // 40
            42.766968, // 50
            41.966254, // 60
        ];
        numeric::assert_vec_near!(l_w50, expected_l_w50, 0.05);
        numeric::assert_vec_near!(L, expected_L, 0.05);
    }
}
