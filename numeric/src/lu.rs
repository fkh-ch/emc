//! LU decomposition

use crate::field::{Abs, Field, Real};

/// Reusable LU decomposition of a square matrix
pub struct LUDecomposition<K> {
    lu: Vec<K>,
    row_permutation: Vec<usize>,
    sign: Option<i8>,
}

/// Transform the `n×n` matrix `a` into its LU decomposition
pub fn lu<K>(mut a: Vec<K>, n: usize) -> LUDecomposition<K>
where
    K: Field,
    <K as Abs>::Output: Real,
{
    let mut index = vec![0; n];
    // Solve equation
    let sign = lu_decompose(&mut a, &mut index);
    LUDecomposition {
        lu: a,
        row_permutation: index,
        sign,
    }
}

impl<K> LUDecomposition<K> {
    /// Solve the linear system of equations `A x = b` for `x`, where
    /// this LU decomposition is made of `A`
    pub fn solve<R: Real, K2>(&self, mut b: Vec<K2>) -> Vec<K2>
    where
        K: std::ops::Mul<K2, Output = K2> + Field + Abs<Output = R>,
        K2: std::ops::Div<K, Output = K2> + Field + Abs<Output = R>,
    {
        if self.sign.is_some() {
            lu_solve(&self.lu, &self.row_permutation, &mut b);
        } else {
            b.fill(K2::NAN);
        }
        b
    }
}

/// LU decomposition of matrix `a` (Crout method).
/// `a` will be replaced and will contain (L - id) + U, such that $P * A = L * U$,
/// where P is a permutaion matrix determined by `row_permutation` by
/// $P = \tau_{n-1 p_{n-1}} \circ \ldots \circ \tau_{0 p_0}$, where
/// `p_i := row_permutation[i]` and `\tau_{ij}` denotes the transposition swaping `i` with `j`.
/// Returns ±1 depending on whether the number of row interchanges was even
/// or odd, respectively or None if the matrix is singular.
pub fn lu_decompose<R: Real, K: Field + Abs<Output = R>>(
    a: &mut [K],
    row_permutation: &mut [usize],
) -> Option<i8> {
    let n = row_permutation.len();
    assert!(n * n == a.len());
    let mut row_max = vec![R::from(0); n];
    let mut sign = 1;
    for (i, row) in a.chunks_exact(n).enumerate() {
        let max_val = vec_max_abs(row);
        if max_val == 0.into() {
            return None;
        }
        row_max[i] = R::from(1) / max_val;
    }
    let mut _a = MatrixViewMut { data: a, n };
    for j in 0..n {
        for i in 0..j {
            let mut a_ij = _a[(i, j)];
            for k in 0..i {
                a_ij -= _a[(i, k)] * _a[(k, j)];
            }
            _a[(i, j)] = a_ij;
        }
        let mut col_max = R::from(0);
        let mut i_max = j;
        for i in j..n {
            let mut a_ij = _a[(i, j)];
            for k in 0..j {
                a_ij -= _a[(i, k)] * _a[(k, j)];
            }
            _a[(i, j)] = a_ij;
            let abs_ij = row_max[i] * K::abs(a_ij);
            if abs_ij > col_max {
                col_max = abs_ij;
                i_max = i;
            }
        }
        if j != i_max {
            // Swap row i_max with row j:
            for k in 0..n {
                let temp = _a[(i_max, k)];
                _a[(i_max, k)] = _a[(j, k)];
                _a[(j, k)] = temp;
            }
            sign = -sign;
            row_max[i_max] = row_max[j];
        }
        row_permutation[j] = i_max;
        if _a[(j, j)] == K::ZERO {
            return None;
        }
        if j != n - 1 {
            let diag = _a[(j, j)];
            for i in j + 1..n {
                _a[(i, j)] /= diag;
            }
        }
    }
    Some(sign)
}

/// Solves `A x = b` for `x`, for given LU decomposition matrix `lu` of a `A` and vector `b`.
/// Input: `lu`, `row_permutation` filled in `lu_decompose`, `b`: rhs vector
/// Output: `x`: solution vector of A*x = b, written back into `b`.
pub fn lu_solve<R: Real, K1, K2>(lu: &[K1], row_permutation: &[usize], b: &mut [K2])
where
    K1: std::ops::Mul<K2, Output = K2> + Field + Abs<Output = R>,
    K2: std::ops::Div<K1, Output = K2> + Field + Abs<Output = R>,
{
    let n = b.len();
    assert!(n * n == lu.len());
    let _lu = MatrixView { data: lu, n };
    for i in 0..n {
        let i_p = row_permutation[i];
        let mut b_i = b[i_p];
        b[i_p] = b[i];
        for j in 0..i {
            b_i -= _lu[(i, j)] * b[j];
        }
        b[i] = b_i;
    }
    for i in (0..n).rev() {
        let mut b_i = b[i];
        for j in i + 1..n {
            b_i -= _lu[(i, j)] * b[j];
        }
        b[i] = b_i / _lu[(i, i)];
    }
}

/// Solves `A x = b` for `x`, for given matrix coefficients `a` of `A` and vector `b`.
/// `x` is written back to `b`.
pub fn solve<R: Real, K1, K2>(a: &mut [K1], b: &mut [K2])
where
    K1: std::ops::Mul<K2, Output = K2> + Field + Abs<Output = R>,
    K2: std::ops::Div<K1, Output = K2> + Field + Abs<Output = R>,
{
    let n = b.len();
    let mut index = vec![0; n];
    // Solve equation
    if lu_decompose(a, &mut index).is_some() {
        lu_solve(a, &index, b);
    } else {
        b.fill(K2::NAN);
    }
}

/// View of contiguous data which allows indexing by row and column indices
struct MatrixView<'a, T> {
    data: &'a [T],
    n: usize,
}
impl<T> std::ops::Index<(usize, usize)> for MatrixView<'_, T> {
    type Output = T;

    fn index(&self, (i, j): (usize, usize)) -> &Self::Output {
        &self.data[i * self.n + j]
    }
}
/// View of contiguous data which allows mutable indexing by row and column indices
pub struct MatrixViewMut<'a, T> {
    /// Flat data array
    pub data: &'a mut [T],
    /// Row length
    pub n: usize,
}
impl<T> std::ops::Index<(usize, usize)> for MatrixViewMut<'_, T> {
    type Output = T;

    fn index(&self, (i, j): (usize, usize)) -> &Self::Output {
        &self.data[i * self.n + j]
    }
}
impl<T> std::ops::IndexMut<(usize, usize)> for MatrixViewMut<'_, T> {
    fn index_mut(&mut self, (i, j): (usize, usize)) -> &mut Self::Output {
        &mut self.data[i * self.n + j]
    }
}

fn vec_max_abs<R: Real, K: Field + Abs<Output = R>>(v: &[K]) -> R {
    let mut max_val = R::from(0);
    for x in v {
        let x_abs = K::abs(*x);
        if x_abs > max_val {
            max_val = x_abs;
        }
    }
    max_val
}

#[cfg(test)]
mod test {
    use crate::assert_almost_eq;

    #[test]
    fn test_solve_returns_nan_for_singular_matrix_f64() {
        let mut a = [0., 0., 0., 0.];
        let mut b = [0., 0.];
        super::solve(&mut a, &mut b);
        assert!(f64::is_nan(b[0]) || f64::is_nan(b[1]));
    }
    #[test]
    fn test_solve_works_for_regular_2x2_matrix_f64() {
        let mut a = [0., 2., 3., 0.];
        let mut b = [1., 0.];
        super::solve(&mut a, &mut b);
        assert_almost_eq!(b[0], 0.);
        assert_almost_eq!(b[1], 0.5);
    }
    #[test]
    fn test_solve_works_for_regular_3x3_matrix_f64() {
        let mut a = [1., 0., 1., 1., 1., 0., 1., -1., 1.];
        let mut b = [1., 0., 0.];
        super::solve(&mut a, &mut b);
        assert_almost_eq!(b[0], -1.);
        assert_almost_eq!(b[1], 1.);
        assert_almost_eq!(b[2], 2.);
    }
    /*
        #[test]
        fn test_solve_returns_nan_for_singular_matrix_complex_f64() {
            let mut a = [0., 0., 0., 0.];
            let mut b = [0., 0.];
            super::solve(&mut a, &mut b);
            assert!(b[0].is_nan() || b[1].is_nan());
        }
        #[test]
        fn test_solve_works_for_regular_2x2_matrix_complex_f64() {
            let mut a = [0., 2. * I, 3. * I, 0.];
            let mut b = [1., 0.];
            super::solve(&mut a, &mut b);
            assert_almost_eq!(b[0], 0.);
            assert_almost_eq!(b[1], 0.5 * I);
        }
        #[test]
        fn test_solve_works_for_regular_3x3_matrix_complex_f64() {
            let mut a = [I, 0., I, 1., 1., 0., 1., -1., 1.];
            let mut b = [1., 0., 0];
            super::solve(&mut a, &mut b);
            assert_almost_eq!(b[0], I);
            assert_almost_eq!(b[1], -I);
            assert_almost_eq!(b[2], -2. * I);
        }
        #[test]
        fn test_solve_works_for_regular_real_2x2_matrix_complex_f64() {
            let mut a = [0., 2., 3., 0.];
            let mut b = [I, 0.];
            super::solve(&mut a, &mut b);
            assert_almost_eq!(b[0], 0.);
            assert_almost_eq!(b[1], 0.5 * I);
        }
        #[test]
        fn test_solve_works_for_regular_real_3x3_matrix_complex_f64() {
            let mut a = [1., 0., 1., 1., 1., 0., 1., -1., 1.];
            let mut b = [I, 0., 0];
            super::solve(&mut a, &mut b);
            assert_almost_eq!(b[0], -I);
            assert_almost_eq!(b[1], I);
            assert_almost_eq!(b[2], 2. * I);
    }
        */
}
