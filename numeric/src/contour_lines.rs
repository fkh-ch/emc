//! Compute contour lines of scalar fields

use crate::curve::{C1Point, C2Point};
use crate::root;

/// One connected component of a contour line.
/// The member `points` stores a polygonal chain,
/// representing the contour line.
/// If `closed`, first and last point in the chain are not
/// repeated.
#[derive(Debug)]
pub struct ContourLineComponent {
    /// Coordinates of points along the contour lines
    pub points: Vec<[f64; 2]>,
    /// True if the last point is connected to the first point
    pub closed: bool,
}

/// Contour line, composed of one or multiple connected components.
type ContourLine = Vec<ContourLineComponent>;

/// Determine contour lines to given `levels` within
/// the area `[x_min, x_max] x [y_min, y_max]`.
/// The algorithm evaluates the function on a square grid,
/// whose nearest neighbours are choosen such that their distance is less than `resolution`.
/// `tolerance` controls the maximal distance a point is allowed to have from the contour line.
pub fn trace_contour_lines(
    f: impl Fn(f64, f64) -> f64,
    (x_min, x_max): (f64, f64),
    (y_min, y_max): (f64, f64),
    levels: &[f64],
    resolution: f64,
    tolerance: f64,
) -> Vec<ContourLine> {
    let n_x: usize = f64::ceil((x_max - x_min) / resolution) as usize;
    let n_y: usize = f64::ceil((y_max - y_min) / resolution) as usize;
    let x_range = linspace(x_min, x_max, n_x + 1);
    let y_range = linspace(y_min, y_max, n_y + 1);
    let z_grid = y_range
        .iter()
        .flat_map(|y| x_range.iter().map(|x| f(*x, *y)))
        .collect::<Vec<_>>();
    let grid = Grid {
        x_range,
        y_range,
        z_grid,
    };

    levels
        .iter()
        .cloned()
        .map(|level| trace_contour_line(&f, &grid, level, tolerance))
        .collect()
}

fn trace_contour_line(
    f: impl Fn(f64, f64) -> f64,
    grid: &Grid,
    level: f64,
    tolerance: f64,
) -> ContourLine {
    let mut line = FragmentedPath::new();
    for j in 0..grid.y_range.len() - 1 {
        for i in 0..grid.x_range.len() - 1 {
            let above_level_00 = grid[(i, j)] >= level;
            let above_level_10 = grid[(i + 1, j)] >= level;
            let above_level_01 = grid[(i, j + 1)] >= level;
            let above_level_11 = grid[(i + 1, j + 1)] >= level;
            const EDGES: [((usize, usize), Direction); 4] = [
                ((0, 0), Direction::Right), // bottom
                ((0, 0), Direction::Up),    // left
                ((0, 1), Direction::Right), // top
                ((1, 0), Direction::Up),    // right
            ];
            let intersections = &[
                above_level_00 ^ above_level_10,
                above_level_00 ^ above_level_01,
                above_level_01 ^ above_level_11,
                above_level_10 ^ above_level_11,
            ];
            if intersections[0] && intersections[1] && intersections[2] && intersections[3] {
                // 4 intersecting edges.
                // Have to decide which pairs to connect, by inspecting
                // the z value in the middle.
                let above_level_m = f(
                    (grid.x_range[i] + grid.x_range[i + 1]) / 2.,
                    (grid.y_range[j] + grid.y_range[j + 1]) / 2.,
                ) >= level;
                if above_level_m == above_level_00 {
                    // both lower left and middle at same side of level
                    line.add_segment(
                        grid.edge_id((i, j), Direction::Right),
                        grid.edge_id((i + 1, j), Direction::Up),
                    );
                    line.add_segment(
                        grid.edge_id((i, j), Direction::Up),
                        grid.edge_id((i, j + 1), Direction::Right),
                    );
                } else {
                    line.add_segment(
                        grid.edge_id((i, j), Direction::Right),
                        grid.edge_id((i, j), Direction::Up),
                    );
                    line.add_segment(
                        grid.edge_id((i + 1, j), Direction::Up),
                        grid.edge_id((i, j + 1), Direction::Right),
                    );
                }
                continue;
            }
            // 0 or 2 intersecting edges
            let mut intersecting_edges = EDGES
                .iter()
                .zip(intersections)
                .filter_map(|(e, e_intersects)| (*e_intersects).then_some(e));
            if let (Some(a), Some(b)) = (intersecting_edges.next(), intersecting_edges.next()) {
                // 2 intersecting edges
                line.add_segment(
                    grid.edge_id((i + a.0 .0, j + a.0 .1), a.1),
                    grid.edge_id((i + b.0 .0, j + b.0 .1), b.1),
                );
            }
        }
    }
    line.into_components()
        .map(|component| ContourLineComponent {
            closed: component.closed,
            points: component
                .points
                .into_iter()
                .map(|e_id| {
                    let ((a_i, a_j), (b_i, b_j)) = grid.edge_from_id(e_id);
                    if a_i == b_i {
                        [
                            grid.x_range[a_i],
                            root::find_bracketed_zero(
                                |y| f(grid.x_range[a_i], y) - level,
                                (grid.y_range[a_j], grid.y_range[b_j]),
                                tolerance,
                            )
                            .unwrap(), /*
                                       grid.y_range[a_j]
                                           + (grid.y_range[b_j] - grid.y_range[a_j])
                                               / (grid[(a_i, b_j)] - grid[(a_i, a_j)])
                                        * (level - grid[(a_i, a_j)]),
                                           */
                        ]
                    } else {
                        [
                            root::find_bracketed_zero(
                                |x| f(x, grid.y_range[a_j]) - level,
                                (grid.x_range[a_i], grid.x_range[b_i]),
                                tolerance,
                            )
                            .unwrap(), /*
                                       grid.x_range[a_i]
                                           + (grid.x_range[b_i] - grid.x_range[a_i])
                                               / (grid[(b_i, a_j)] - grid[(a_i, a_j)])
                                               * (level - grid[(a_i, a_j)])*/
                            grid.y_range[a_j],
                        ]
                    }
                })
                .collect(),
        })
        .collect()
}

/// 1st and 2nd derivatives of a function `f: R² -> R`
pub struct Derivatives {
    /// ∇f
    pub grad: [f64; 2],
    /// Hessian of f, xx component: ∂²f/∂x²
    pub hess_xx: f64,
    /// Hessian of f, xy component: ∂²f/(∂x∂y)
    pub hess_xy: f64,
    /// Hessian of f, yy component: ∂²f/∂y²
    pub hess_yy: f64,
}

impl std::ops::Add<Derivatives> for Derivatives {
    type Output = Self;
    fn add(self, rhs: Derivatives) -> Self::Output {
        Self::Output {
            grad: [self.grad[0] + rhs.grad[0], self.grad[1] + rhs.grad[1]],
            hess_xx: self.hess_xx + rhs.hess_xx,
            hess_xy: self.hess_xy + rhs.hess_xy,
            hess_yy: self.hess_yy + rhs.hess_yy,
        }
    }
}

/// For a given point on a countour line, defined by
/// $f(x,y) = c$, compute the unit tangent of the
/// contour line at this point.
///
/// If `orientation > 0`, then the tangent will have the direction of
/// `grad f(x, y)` rotated 90° counter clockwise.
pub fn into_c1_point(pos: [f64; 2], grad_f: [f64; 2], orientation: f64) -> C1Point {
    let [n_x, n_y] = grad_f;
    let n = f64::hypot(n_x, n_y).copysign(orientation);
    let tau = [-n_y / n, n_x / n];
    // => n_x t_y - n_y t_x = (n_x^2 + n_y^2)/n > 0
    C1Point { pos, tau }
}

/// For a given point on a countour line, defined by
/// $f(x,y) = c$, compute the unit tangent and curvature of the
/// contour line at this point.
///
/// Let $\gamma(t)$ be a parametrisation of the contour line such that
/// $\| \dot{\gamma}(t) \| = 1$ and
/// $(grad f)(\gamma(t)) \times \dot{\gamma}(t) > 0$ ($a \times b := a_x b_y - a_y b_x$).
///
/// $$
/// f(\gamma(t)) = c
/// => 0 = \langle \dot{\gamma}(t), (grad f)(\gamma(t)) \rangle.
/// => 0 = \langle \ddot{\gamma}(t), (grad f)(\gamma(t)) \rangle + \langle \dot{\gamma}(t), Hess[f](\gamma(t)) \dot{\gamma}(t) \rangle.
/// => k(t) = \langle \dot{\gamma}(t), Hess[f](\gamma(t)) \dot{\gamma}(t) \rangle / \| (grad f)(x,y) \|.
/// $$
pub fn into_c2_point(pos: [f64; 2], d_f: Derivatives, orientation: f64) -> C2Point {
    let Derivatives {
        grad: [n_x, n_y],
        hess_xx,
        hess_xy,
        hess_yy,
    } = d_f;
    let n = f64::hypot(n_x, n_y).copysign(orientation);
    let (t_x, t_y) = (-n_y / n, n_x / n);
    // => n_x t_y - n_y t_x = (n_x^2 + n_y^2)/n > 0
    let k = hess_xx * t_x * t_x + 2. * hess_xy * t_x * t_y + hess_yy * t_y * t_y;
    C2Point {
        pos,
        tau: [t_x, t_y],
        k: k / n,
    }
}

/// Determine the orientation of `path`.
/// The orientation is +1 if `n_x * t_y - n_y * t_x > 0` and
/// -1 otherwise.
/// `(n_x, n_y)` is determined by `d_f(x, y).grad`, `(t_x, t_y)` is
/// the tangent to the path.
pub fn orientation<D: AsGrad>(path: &[[f64; 2]], d_f: impl Fn(f64, f64) -> D) -> f64 {
    if path.len() < 2 {
        return 1.;
    }
    let [x, y] = path[1];
    let d = d_f(x, y);
    orientation_for_normal(path, d.as_grad())
}

/// Restrict a collection of derivatives of different order to the gradient
pub trait AsGrad {
    /// Restrict to the gradient
    fn as_grad(&self) -> &[f64; 2];
}

impl AsGrad for [f64; 2] {
    fn as_grad(&self) -> &[f64; 2] {
        self
    }
}

impl AsGrad for Derivatives {
    fn as_grad(&self) -> &[f64; 2] {
        &self.grad
    }
}

// Determine the orientation of `path`.
/// The orientation is +1 if `n_x * t_y - n_y * t_x > 0` and
/// -1 otherwise.
fn orientation_for_normal(path: &[[f64; 2]], n: &[f64; 2]) -> f64 {
    if path.len() < 2 {
        return 1.;
    }
    let [n_x, n_y] = n;
    let (t_x, t_y) = if path.len() > 2 {
        (path[2][0] - path[0][0], path[2][1] - path[0][1])
    } else {
        (path[1][0] - path[0][0], path[1][1] - path[0][1])
    };
    (n_x * t_y - n_y * t_x).signum()
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::{assert_almost_eq, assert_vec_almost_eq};

    #[test]
    fn test_fragmented_path_joins_segments() {
        let mut fragments = FragmentedPath {
            components: Vec::new(),
            fronts: std::collections::HashMap::new(),
            backs: std::collections::HashMap::new(),
        };
        for (from, to) in [
            (0, 1),
            (2, 3),
            (5, 4),
            (8, 9),
            (6, 7),
            (1, 2), // join back - front
            (3, 4), // join back - back
            (8, 7), // join front - back
            (0, 6), // join front - front
        ] {
            fragments.add_segment(from, to);
        }
        let mut components = fragments.into_components();
        let PathFragment { points, closed } = components.next().unwrap();
        assert!(components.next().is_none());
        assert_eq!(closed, false);
        assert_eq!(
            points,
            [9, 8, 7, 6, 0, 1, 2, 3, 4, 5]
                .into_iter()
                .collect::<std::collections::VecDeque::<usize>>()
        );
    }

    /// Standard example. The grid is choosen as in the following schema:
    /// ^-^-^-^-^
    /// | | | | |
    /// ^- - - -^
    /// | | | | |
    /// ^- - - -^  
    /// | | | | |
    /// ^- - - -^
    /// | | | | |
    /// ^-^-^-^-^
    ///
    /// "^" denote points above level f(x,y)=1, while " " denote points below level.
    #[test]
    fn test_contourlines_for_circle() {
        let f = |x, y| x * x + y * y;
        let d = 1.0;
        let levels = [1.];
        let resolution = 0.5;
        let tolerance = 1.0e-3;
        let lines = trace_contour_lines(f, (-d, d), (-d, d), &levels, resolution, tolerance);
        // One level:
        assert_eq!(lines.len(), 1);
        let line = &lines[0];
        // One line component:
        assert_eq!(line.len(), 1);
        let component = &line[0];
        assert!(component.closed);
        assert_eq!(component.points.len(), 12);
        // Check that all points are outside of the square [-0.5, 0.5]x[-0.5, 0.5]
        for [x, y] in &component.points {
            assert!(f64::abs(*x) >= 0.5 || f64::abs(*y) >= 0.5);
        }
        // Check that edges have a max length of 0.5:
        let (x, y): (Vec<_>, Vec<_>) = component.points.iter().map(|[x, y]| (x, y)).unzip();
        let dist_max = |values: &[f64]| {
            let mut d = values[values.len() - 1] - values[0];
            for i in 0..values.len() - 1 {
                let d_candidate = f64::abs(values[i + 1] - values[i]);
                if d_candidate > d {
                    d = d_candidate;
                }
            }
            d
        };
        let (dx_max, dy_max) = (dist_max(&x), dist_max(&y));
        assert!(dx_max <= 0.5);
        assert!(dy_max <= 0.5);
        // Check that the path passes covers a full circle:
        assert!(x.iter().cloned().reduce(f64::min).unwrap() <= -0.5);
        assert!(x.iter().cloned().reduce(f64::max).unwrap() >= 0.5);
        assert!(y.iter().cloned().reduce(f64::min).unwrap() <= -0.5);
        assert!(y.iter().cloned().reduce(f64::max).unwrap() >= 0.5);
    }

    /// All edges intersect with contour lines.
    /// Here, the lines are separated by y = -x.
    ///  -^
    /// | |
    /// ^-
    #[test]
    fn test_contourlines_for_hyperbola() {
        let f = |x, y| x * y;

        let grid = Grid {
            x_range: vec![-1.0, 1.0],
            y_range: vec![-1.0, 1.0],
            z_grid: vec![1.0, -1.0, -1.0, 1.0],
        };
        let level = 0.5;
        const EPS: f64 = 1.0e-3;
        let line = trace_contour_line(f, &grid, level, EPS);
        // One line component:
        assert_eq!(line.len(), 2);
        assert!(!line[0].closed);
        assert!(!line[1].closed);
        assert_eq!(line[0].points.len(), 2);
        assert_eq!(line[1].points.len(), 2);
        // Check that components are separated by y = -x:
        assert!(
            (line[0].points.iter().all(|[x, y]| x + y > 0.)
                && line[1].points.iter().all(|[x, y]| x + y < 0.))
                || (line[0].points.iter().all(|[x, y]| x + y < 0.)
                    && line[1].points.iter().all(|[x, y]| x + y > 0.))
        );
        // Check that components intersect boundary:
        for component in &line {
            assert!(
                ((f64::abs(f64::abs(component.points[0][0]) - 1.0) < EPS)
                    && (f64::abs(f64::abs(component.points[1][1]) - 1.0) < EPS))
                    || ((f64::abs(f64::abs(component.points[0][1]) - 1.0) < EPS)
                        && (f64::abs(f64::abs(component.points[1][0]) - 1.0) < EPS))
            );
        }
    }

    /// Again, all edges intersect with contour lines.
    /// In contrast to the last example, the lines are
    /// now separated by the other diagonal y = x.
    ///  -^
    /// | |
    /// ^-
    #[test]
    fn test_contourlines_for_diagonal_barrier() {
        let f = |x, y| 1. - (x - y) * (x - y);

        let grid = Grid {
            x_range: vec![-1.0, 1.0],
            y_range: vec![-1.0, 1.0],
            z_grid: vec![1.0, -3.0, -3.0, 1.0],
        };
        let level = 0.5;
        const EPS: f64 = 1.0e-3;
        let line = trace_contour_line(f, &grid, level, EPS);
        // One line component:
        assert_eq!(line.len(), 2);
        assert!(!line[0].closed);
        assert!(!line[1].closed);
        assert_eq!(line[0].points.len(), 2);
        assert_eq!(line[1].points.len(), 2);
        // Check that components are separated by y = x:
        assert!(
            (line[0].points.iter().all(|[x, y]| y - x > 0.)
                && line[1].points.iter().all(|[x, y]| y - x < 0.))
                || (line[0].points.iter().all(|[x, y]| y - x < 0.)
                    && line[1].points.iter().all(|[x, y]| y - x > 0.))
        );
        // Check that components intersect boundary:
        for component in &line {
            assert!(
                ((f64::abs(f64::abs(component.points[0][0]) - 1.0) < EPS)
                    && (f64::abs(f64::abs(component.points[1][1]) - 1.0) < EPS))
                    || ((f64::abs(f64::abs(component.points[0][1]) - 1.0) < EPS)
                        && (f64::abs(f64::abs(component.points[1][0]) - 1.0) < EPS))
            );
        }
    }

    #[test]
    fn test_into_c2_point_works_for_circle_with_radius_1() {
        // f(x,y) = x * x + y * y
        let d_f = |x, y| Derivatives {
            grad: [2. * x, 2. * y],
            hess_xx: 2.,
            hess_xy: 0.,
            hess_yy: 2.,
        };
        let orientation = 1.;
        let c2_point = into_c2_point([1., 0.], d_f(1., 0.), orientation);
        assert_vec_almost_eq!(c2_point.pos, [1., 0.]);
        assert_vec_almost_eq!(c2_point.tau, [0., 1.]);
        assert_almost_eq!(c2_point.k, 1.);
    }
    #[test]
    fn test_into_c2_point_works_for_circle_with_radius_2() {
        // f(x,y) = x * x + y * y
        let d_f = |x, y| Derivatives {
            grad: [2. * x, 2. * y],
            hess_xx: 2.,
            hess_xy: 0.,
            hess_yy: 2.,
        };
        let orientation = 1.;
        let c2_point = into_c2_point([2., 0.], d_f(2., 0.), orientation);
        assert_vec_almost_eq!(c2_point.pos, [2., 0.]);
        assert_vec_almost_eq!(c2_point.tau, [0., 1.]);
        assert_almost_eq!(c2_point.k, 0.5);
    }
    #[test]
    fn test_get_mut_pair() {
        let mut v = vec![0; 3];
        let (x, y) = v.get_mut_pair(1, 2);
        *x = 1;
        *y = 2;
        assert_eq!(v, vec![0, 1, 2]);
        let (x, y) = v.get_mut_pair(2, 1);
        *x = 1;
        *y = 2;
        assert_eq!(v, vec![0, 2, 1]);
    }
}

type EdgeId = usize;

/// Work in progress variant of [ContourLineComponent],
/// using [std::collections::VecDeque] instead of [std::vec::Vec] to support
/// insertion at both ends.
struct PathFragment {
    pub points: std::collections::VecDeque<EdgeId>,
    pub closed: bool,
}

/// Work in progress variant of [ContourLine],
/// providing fast access to the current open endings (`fronts`, `backs`).
struct FragmentedPath {
    pub components: Vec<PathFragment>,
    pub fronts: std::collections::HashMap<EdgeId, usize>,
    pub backs: std::collections::HashMap<EdgeId, usize>,
}
impl FragmentedPath {
    fn new() -> Self {
        FragmentedPath {
            components: Vec::new(),
            fronts: std::collections::HashMap::new(),
            backs: std::collections::HashMap::new(),
        }
    }
    fn into_components(self) -> impl Iterator<Item = PathFragment> {
        self.components.into_iter().filter(|f| !f.points.is_empty())
    }
    fn add_segment(&mut self, edge_id_from: EdgeId, edge_id_to: EdgeId) {
        enum End {
            Front,
            Back,
        }
        let connection_from = self
            .backs
            .remove(&edge_id_from)
            .map(|idx| (idx, End::Back))
            .or_else(|| {
                self.fronts
                    .remove(&edge_id_from)
                    .map(|idx| (idx, End::Front))
            });
        let connection_to = self
            .backs
            .remove(&edge_id_to)
            .map(|idx| (idx, End::Back))
            .or_else(|| self.fronts.remove(&edge_id_to).map(|idx| (idx, End::Front)));
        let mut push_id = |component_idx: usize, edge_id, end| match end {
            End::Back => {
                self.components[component_idx].points.push_back(edge_id);
                self.backs.insert(edge_id, component_idx);
            }
            End::Front => {
                self.components[component_idx].points.push_front(edge_id);
                self.fronts.insert(edge_id, component_idx);
            }
        };
        match (connection_from, connection_to) {
            (None, None) => {
                self.components.push(PathFragment {
                    points: std::collections::VecDeque::from([edge_id_from, edge_id_to]),
                    closed: false,
                });
                self.backs.insert(edge_id_to, self.components.len() - 1);
                self.fronts.insert(edge_id_from, self.components.len() - 1);
            }
            (Some((component_idx, end)), None) => {
                push_id(component_idx, edge_id_to, end);
            }
            (None, Some((component_idx, end))) => {
                push_id(component_idx, edge_id_from, end);
            }
            (Some((component_idx_a, end_a)), Some((component_idx_b, end_b))) => {
                if component_idx_a == component_idx_b {
                    self.components[component_idx_a].closed = true;
                } else {
                    let (f1, f2) = self
                        .components
                        .get_mut_pair(component_idx_a, component_idx_b);
                    // This will leave empty vecs in the component vec, so we do
                    // not have to renumber fronts and backs:
                    match (end_a, end_b) {
                        (End::Back, End::Front) => {
                            if let Some(e_id) = f2.points.back() {
                                if let Some(pos) = self.backs.get_mut(e_id) {
                                    *pos = component_idx_a;
                                }
                            }
                            join_component_pair(f1, f2);
                        }
                        (End::Front, End::Back) => {
                            if let Some(e_id) = f1.points.back() {
                                if let Some(pos) = self.backs.get_mut(e_id) {
                                    *pos = component_idx_b;
                                }
                            }
                            join_component_pair(f2, f1);
                        }
                        (End::Back, End::Back) => {
                            // Set front of f2 as back of merged component
                            if let Some(e_id) = f2.points.front() {
                                self.fronts.remove(e_id);
                                self.backs.insert(*e_id, component_idx_a);
                            };
                            join_component_pair_back(f1, f2);
                        }
                        (End::Front, End::Front) => {
                            // Set back of f2 as front of merged component
                            if let Some(e_id) = f2.points.back() {
                                self.backs.remove(e_id);
                                self.fronts.insert(*e_id, component_idx_a);
                            };
                            join_component_pair_front(f1, f2);
                        }
                    }
                }
            }
        }
    }
}

/// Trait allowing mutable access to 2 indices at once
pub trait GetMutPair<'a, T> {
    /// Access a pair of indices
    fn get_mut_pair(self, i: usize, j: usize) -> (&'a mut T, &'a mut T);
}
impl<'a, T> GetMutPair<'a, T> for &'a mut [T] {
    fn get_mut_pair(self, i: usize, j: usize) -> (&'a mut T, &'a mut T) {
        let k = usize::min(i, j);
        let (a, b) = self.split_at_mut(k + 1);
        if i < j {
            (&mut a[i], &mut b[j - i - 1])
        } else {
            (&mut b[i - j - 1], &mut a[j])
        }
    }
}

struct Grid {
    pub x_range: Vec<f64>,
    pub y_range: Vec<f64>,
    z_grid: Vec<f64>,
}

impl std::ops::Index<(usize, usize)> for Grid {
    type Output = f64;

    fn index(&self, (i, j): (usize, usize)) -> &Self::Output {
        &self.z_grid[j * self.x_range.len() + i]
    }
}

impl Grid {
    fn edge_id(&self, (i, j): (usize, usize), direction: Direction) -> EdgeId {
        let n_edges = self.x_range.len();
        2 * (j * n_edges + i)
            + match direction {
                Direction::Right => 0,
                Direction::Up => 1,
            }
    }

    fn edge_from_id(&self, edge_id: EdgeId) -> ((usize, usize), (usize, usize)) {
        let n_edges = self.x_range.len();
        let (delta, vertex_id) = (edge_id % 2, edge_id / 2);
        let (i, j) = (vertex_id % n_edges, vertex_id / n_edges);
        if delta == 0 {
            ((i, j), (i + 1, j))
        } else {
            ((i, j), (i, j + 1))
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum Direction {
    Right,
    Up,
}

fn linspace(x_min: f64, x_max: f64, m: usize) -> Vec<f64> {
    assert!(m > 1);
    assert!((m - 1) as f64 as usize == (m - 1));
    let dx = (x_max - x_min) / ((m - 1) as f64);
    (0..m).map(|i| x_min + (i as f64) * dx).collect()
}

fn join_component_pair(a: &mut PathFragment, b: &mut PathFragment) {
    b.closed = true;
    a.points.append(&mut b.points);
    a.closed = a.points.back() == a.points.front();
}
fn join_component_pair_front(a: &mut PathFragment, b: &mut PathFragment) {
    b.closed = true;
    for p in b.points.drain(..) {
        a.points.push_front(p);
    }
    a.closed = a.points.back() == a.points.front();
}
fn join_component_pair_back(a: &mut PathFragment, b: &mut PathFragment) {
    b.closed = true;
    while let Some(p) = b.points.pop_back() {
        a.points.push_back(p);
    }
    a.closed = a.points.back() == a.points.front();
}
