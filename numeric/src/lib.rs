//! Numerical routines
#![warn(missing_docs)]

pub mod bezier;
pub mod complex;
pub mod consts;
pub mod contour_lines;
pub mod curve;
pub mod field;
pub mod field_lines;
pub mod lu;
pub mod numdiff;
pub mod odeint;
pub mod root;

pub mod testing;
