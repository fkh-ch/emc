//! Bezier curves

use crate::{
    curve::{C1Point, C2Point},
    root,
};

/// A node in a bezier curve. Contains the point, the first control point of the following segment
/// and the last control point of the preceeding segment.
#[derive(Debug)]
pub struct CubicBezierPoint {
    /// Coordinates of the point on the curve
    pub point: [f64; 2],
    /// Coordinates of the control point in forward direction
    pub forward_control_point: [f64; 2],
    /// Coordinates of the control point in backward direction
    pub backward_control_point: [f64; 2],
}

/// Turns an iterator of C1Point (point and tangent) into a cubic bezier curve, using
/// SegmentControlPoints::from_c1_segment.
/// The first and last segment are set to the first and last point respectively.
pub fn into_cubic_bezier_curve(
    curve: impl IntoIterator<Item = C1Point>,
) -> impl Iterator<Item = CubicBezierPoint> {
    into_cubic_bezier_curve_by_segment(curve, SegmentControlPoints::from_c1_segment)
}

fn into_cubic_bezier_curve_by_segment<P, F>(
    curve: impl IntoIterator<Item = P>,
    to_control_points: F,
) -> impl Iterator<Item = CubicBezierPoint>
where
    for<'a> &'a P: Into<[f64; 2]>,
    for<'a, 'b> F: Fn(&'a P, &'b P) -> SegmentControlPoints,
{
    let mut points = curve.into_iter();
    let (q0, p0) = if let Some(p0) = points.next() {
        ((&p0).into(), Some(p0))
    } else {
        ([f64::NAN; 2], None)
    };
    BezierPointIter {
        q0,
        q_final: None,
        p0,
        points,
        to_control_points,
    }
}

fn into_closed_cubic_bezier_curve_by_segment<P, F>(
    curve: impl IntoIterator<Item = P>,
    to_control_points: F,
) -> impl Iterator<Item = CubicBezierPoint>
where
    for<'a> &'a P: Into<[f64; 2]>,
    for<'a, 'b> F: Fn(&'a P, &'b P) -> SegmentControlPoints,
{
    let mut points = curve.into_iter();
    let (q0, p0, q_final, points_queue) = if let Some(p_initial) = points.next() {
        if let Some(p_second) = points.next() {
            let SegmentControlPoints { q_start, q_stop } = to_control_points(&p_initial, &p_second);
            (q_stop, Some(p_second), Some(q_start), Some(p_initial))
        } else {
            let q0 = (&p_initial).into();
            (q0, Some(p_initial), Some(q0), None)
        }
    } else {
        ([f64::NAN; 2], None, None, None)
    };

    BezierPointIter {
        q0,
        q_final,
        p0,
        points: points.chain(points_queue),
        to_control_points,
    }
}

/// Turns an iterator of C1Point (point and tangent) into a closed cubic bezier curve,
/// using SegmentControlPoints::from_c1_segment.
pub fn into_closed_cubic_bezier_curve(
    curve: impl IntoIterator<Item = C1Point>,
) -> impl Iterator<Item = CubicBezierPoint> {
    into_closed_cubic_bezier_curve_by_segment(curve, SegmentControlPoints::from_c1_segment)
}

/// Turns an iterator of C2Point (point, tangent and curvature) into a cubic bezier curve,
/// using SegmentControlPoints::from_c2_segment_with_tolerance.
/// The first and last segment are set to the first and last point respectively.
pub fn into_cubic_bezier_curve_with_tolerance(
    curve: impl IntoIterator<Item = C2Point>,
    tolerance: f64,
) -> impl Iterator<Item = CubicBezierPoint> {
    into_cubic_bezier_curve_by_segment(curve, SegmentControlPoints::from_c2_segment(tolerance))
}

/// Turns an iterator of C2Point (point, tangent and curvature) into a closed cubic bezier curve,
/// using SegmentControlPoints::from_c2_segment_with_tolerance.
pub fn into_closed_cubic_bezier_curve_with_tolerance(
    curve: impl IntoIterator<Item = C2Point>,
    tolerance: f64,
) -> impl Iterator<Item = CubicBezierPoint> {
    into_closed_cubic_bezier_curve_by_segment(
        curve,
        SegmentControlPoints::from_c2_segment(tolerance),
    )
}

struct BezierPointIter<I, P, F> {
    p0: Option<P>,
    q0: [f64; 2],
    q_final: Option<[f64; 2]>,
    points: I,
    to_control_points: F,
}
impl<I: Iterator<Item = P>, P, F> Iterator for BezierPointIter<I, P, F>
where
    for<'a> &'a P: Into<[f64; 2]>,
    for<'a, 'b> F: Fn(&'a P, &'b P) -> SegmentControlPoints,
{
    type Item = CubicBezierPoint;
    fn next(&mut self) -> Option<Self::Item> {
        let p0 = self.p0.as_ref()?;
        let Some(p1) = self.points.next() else {
            let point = p0.into();
            self.p0 = None;
            return Some(CubicBezierPoint {
                point,
                forward_control_point: self.q_final.unwrap_or(point),
                backward_control_point: self.q0,
            });
        };
        let SegmentControlPoints { q_start, q_stop } = (self.to_control_points)(p0, &p1);
        let out = CubicBezierPoint {
            point: p0.into(),
            forward_control_point: q_start,
            backward_control_point: self.q0,
        };
        self.q0 = q_stop;
        self.p0 = Some(p1);
        Some(out)
    }
}

/// Holds 2 control points q_start, q_stop in a Bezier segment
/// B(t) = (1-t)^3 p_start + 3 (1-t)^2 t q_start + 3 (1-t)t^2 q_stop + t^3 p_stop
struct SegmentControlPoints {
    pub q_start: [f64; 2],
    pub q_stop: [f64; 2],
}

impl SegmentControlPoints {
    /// For given points p0, p1 with corresponding tangent vectors t0 and t1,
    /// compute the control points q0, q1 such that
    /// B(t) = (1-t)^3 p0 + 3 (1-t)^2 t q0 + 3 (1-t)t^2 q1 + t^3 p1
    /// satisfies B'(0) = v0, B'(1) = v1, where
    /// v0, v1 have directions t0, t1 respectively and both having the norm |p1 - p0|.
    fn from_c1_segment(p0: &C1Point, p1: &C1Point) -> Self {
        // B'(0) = 3 (q0 - p0)
        // => q0 = p0 + v0 / 3
        let dp = f64::hypot(p1.pos[0] - p0.pos[0], p1.pos[1] - p0.pos[1]) / 3.;
        let d = dp / (f64::hypot(p0.tau[0], p0.tau[1]));
        let q_start = [p0.pos[0] + d * p0.tau[0], p0.pos[1] + d * p0.tau[1]];

        // B'(1) = 3 (p1 - q1)
        // => q1 = p1 - v1 / 3
        let d = dp / (f64::hypot(p1.tau[0], p1.tau[1]));
        let q_stop = [p1.pos[0] - d * p1.tau[0], p1.pos[1] - d * p1.tau[1]];
        SegmentControlPoints { q_start, q_stop }
    }

    fn from_c2_segment(tolerance: f64) -> impl Fn(&C2Point, &C2Point) -> Self {
        move |p0: &C2Point, p1: &C2Point| {
            SegmentControlPoints::from_c2_segment_with_tolerance(p0, p1, tolerance).unwrap_or_else(
                || SegmentControlPoints {
                    q_start: p0.into(),
                    q_stop: p1.into(),
                },
            )
        }
    }

    /// Bezier curve:
    ///
    /// ```txt
    /// B(t) = (1-t^3)P_0 + 3(1-t)^2t Q_0 + 3 (1-t)t^2 Q_1 + t^3 P_1
    /// B'(t) = 3(1-t)^2(Q_0-P_0) + 6(1-t)t(Q_1-Q_0) + 3t^2(P_1-Q_1)
    /// B''(t) = 6 (1-t)(Q_1-2Q_0+P_0) + 6t(P_1 - 2Q_1 + Q_0)
    /// \tau(t) = d/dt (B'(t)/\|B'(t)\|)|
    ///  = \frac{B''(t)}{\|B'(t)\|}
    ///    - \frac{B'(t) <B''(t), B'(t)>}{\|B'(t)\|^3}
    ///  =   \frac{Q_1-2Q_0+P_0}{\|Q_0-P_0\|}
    ///    - \frac{3(Q_0-P_0)<3(Q_0-P_0), 6(Q_1-2Q_0+P_0)>}{27\|Q_0-P_0\|^3}
    ///  <n, B'(t)> = 0
    ///  =>
    /// Curvature:
    /// k(t) = \frac{\gamma'(t) \times \gamma''(t)}{\|\gamma'(t)\|^3}
    /// k(t) = \frac{<n, B''(t)>}{\|B'(t)\|}
    ///      = \frac{B'(t) \times B''(t)}{\|B'(t)\|^3}
    /// k(0) = \frac{2}{3}\frac{(Q_0-P_0) \times (Q_1-2Q_0+P_0)}{\|Q_0-P_0\|^3}
    /// k(1) = \frac{2}{3}\frac{(P_1-Q_1) \times (P_1-2Q_1+Q_0)}{\|P_1-Q_1\|^3}
    /// p \times q = p_0 q_1 - p_1 q_0
    /// Q_0-P_0 = s_0 \gamma'(0), s_0 > 0
    /// P_1-Q_1 = s_1 \gamma'(1), s_1 > 0
    ///
    /// k(0) = \frac{2}{3}\frac{\gamma'(0) \times (P_1 - P_0 - s_1 \gamma'(1) - 2 s_0 \gamma'(0))}{s_0^2}
    ///      = \frac{2}{3}\frac{\gamma'(0) \times (P_1 - P_0 - s_1 \gamma'(1))}{s_0^2}
    /// k(1) = \frac{2}{3}\frac{\gamma'(1) \times (-P_1 + P_0 + s_0 \gamma'(0) + 2 s_1 \gamma'(1))}{s_1^2}
    ///      = \frac{2}{3}\frac{\gamma'(1) \times (-P_1 + P_0 + s_0 \gamma'(0))}{s_1^2}
    /// 3/2 k(0) s_0^2 = a_0 - b s_1 => s_1 = (-3/2 k(0) s_0^2 + a_0)/b
    /// 3/2 k(1) s_1^2 = a_1 - b s_0 => s_0 = (-3/2 k(1) s_1^2 + a_1)/b
    ///
    /// One solution (s_0, s_1 > 0) if sign(k(0)) = sign(a_0) and sign(k(1)) = sign(a_1)
    /// ```
    fn from_c2_segment_with_tolerance(
        c0: &C2Point,
        c1: &C2Point,
        tolerance: f64,
    ) -> Option<SegmentControlPoints> {
        let C2Point {
            pos: p0,
            tau: t0,
            k: k0,
        } = *c0;
        let C2Point {
            pos: p1,
            tau: t1,
            k: k1,
        } = *c1;
        let delta_p = [p1[0] - p0[0], p1[1] - p0[1]];
        let a_0 = cross(t0, delta_p); // \gamma'(0) \times (P_1 - P_0)
        let a_1 = -cross(t1, delta_p); // \gamma'(1) \times (-P_1 + P_0)
        let b = cross(t0, t1); // \gamma'(0) x \gamma'(1)

        // Solve:
        // b * s_1 = -3/2 k(0) s_0^2 + a_0
        // b * s_0 = -3/2 k(1) s_1^2 + a_1

        // (=> b <(k(0), k(1)), s> = -3/2 k(0) k(1) s^2 + k(1) a_0 + k(0) a_1)
        let (s_0, s_1) = if f64::abs(b) < tolerance {
            (
                // If we have 0 curvature and b = 0, the solution is a straight line.
                // Using s_0 = s_1 = 1/3 |delta_p|, the cubic spline becomes a quadratic spline
                if f64::abs(k0) < tolerance {
                    1. / 3. * f64::hypot(delta_p[0], delta_p[1])
                } else {
                    let q = a_0 / k0;
                    if q < 0. {
                        return None;
                    }
                    f64::sqrt(2. / 3. * q)
                },
                if f64::abs(k1) < tolerance {
                    1. / 3. * f64::hypot(delta_p[0], delta_p[1])
                } else {
                    let q = a_1 / k1;
                    if q < 0. {
                        return None;
                    }
                    f64::sqrt(2. / 3. * q)
                },
            )
        } else {
            if f64::signum(a_0) != f64::signum(k0) || f64::signum(a_1) != f64::signum(k1) {
                return None;
            }
            let s_0_0 = f64::sqrt(2. / 3. * a_0 / k0);
            let s_1_0 = f64::sqrt(2. / 3. * a_1 / k1);
            // Upper guess: Intersections of tangents of the 2 parabolas:
            // g: s_1 * b = -3 k(0) (s_0 - s_0_lower_guess)
            // g2: s_0 * b = -3 k(1) (s_1 - s_1_lower_guess)
            // => s_0 * b = -3 k(1) (-3 k(0)/b (s_0 - s_0_lower_guess) - s_1_lower_guess)
            // => s_0 (b - 9k(0)k(1)/b) = 3k(1)(-3k(0)/b*s_0_lower_guess + s_1_lower_guess)
            let s_0_interval = if a_0 * b < 0. {
                (
                    s_0_0,
                    3. * k1 * (-3. * k0 / b * s_0_0 + s_1_0) / (b - 9. * k0 * k1 / b),
                )
            } else {
                (0., s_0_0)
            };
            let f = |s_0| {
                let s_1 = (-3. / 2. * k0 * s_0 * s_0 + a_0) / b;
                let ds_1 = -3. * k0 * s_0 / b;
                (
                    -3. / 2. * k1 * s_1 * s_1 + a_1 - b * s_0,
                    -3. * k1 * s_1 * ds_1 - b,
                )
            };
            let s_0 = root::newtons_method(f, s_0_interval, tolerance)?;
            let s_1 = (-3. / 2. * k0 * s_0 * s_0 + a_0) / b;
            (s_0, s_1)
        };
        //  Q_0-P_0 = s_0 \gamma'(0), s_0 > 0
        //  P_1-Q_1 = s_1 \gamma'(1), s_1 > 0
        let q_start = [p0[0] + s_0 * t0[0], p0[1] + s_0 * t0[1]];
        let q_stop = [p1[0] - s_1 * t1[0], p1[1] - s_1 * t1[1]];
        Some(SegmentControlPoints { q_start, q_stop })
    }
}

fn cross(x: [f64; 2], y: [f64; 2]) -> f64 {
    x[0] * y[1] - x[1] * y[0]
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::{assert_near, assert_vec_almost_eq, assert_vec_near};

    /// k(0) = \frac{2}{3}\frac{(Q_0-P_0) \times (Q_1-2Q_0+P_0)}{\|Q_0-P_0\|^3}
    fn k_bezier_0(p0: [f64; 2], q0: [f64; 2], q1: [f64; 2]) -> f64 {
        let pq = [q0[0] - p0[0], q0[1] - p0[1]];
        let qqp = [q1[0] - 2. * q0[0] + p0[0], q1[1] - 2. * q0[1] + p0[1]];
        let d = f64::hypot(pq[0], pq[1]);
        2. / 3. * cross(pq, qqp) / (d * d * d)
    }
    /// k(1) = \frac{2}{3}\frac{(P_1-Q_1) \times (P_1-2Q_1+Q_0)}{\|P_1-Q_1\|^3}
    fn k_bezier_1(p1: [f64; 2], q0: [f64; 2], q1: [f64; 2]) -> f64 {
        let qp = [p1[0] - q1[0], p1[1] - q1[1]];
        let qqp = [p1[0] - 2. * q1[0] + q0[0], p1[1] - 2. * q1[1] + q0[1]];
        let d = f64::hypot(qp[0], qp[1]);
        2. / 3. * cross(qp, qqp) / (d * d * d)
    }

    /// Example: unit circle
    /// \gamma(0) = (0, -1), \gamma'(0) = (1, 0), k(0) = 1
    /// \gamma(1) = (1,  0), \gamma'(1) = (0, 1), k(0) = 1
    /// 3/2 s_0^2 - 1 = -s_1
    /// 3/2 s_1^2 - 1 = -s_0
    /// Symmetry => s_0 = s_1
    /// (-1 +- sqrt(1+4*3/2))/(2*3/2) = (-1 +- sqrt(7))/3
    #[test]
    fn test_segment_control_points_from_c2_segment_for_circle() {
        const EPS: f64 = 1.0e-5;
        let p0 = [0., -1.];
        let t0 = [1., 0.];
        let k0 = 1.;

        let p1 = [1., 0.];
        let t1 = [0., 1.];
        let k1 = 1.;

        let SegmentControlPoints {
            q_start: q0,
            q_stop: q1,
        } = SegmentControlPoints::from_c2_segment_with_tolerance(
            &C2Point {
                pos: p0,
                tau: t0,
                k: k0,
            },
            &C2Point {
                pos: p1,
                tau: t1,
                k: k1,
            },
            EPS,
        )
        .unwrap();
        // Tangents:
        let actual_pq_0 = [q0[0] - p0[0], q0[1] - p0[1]];
        assert_vec_near!(actual_pq_0, [(f64::sqrt(7.) - 1.) / 3., 0.], EPS);
        let actual_pq_1 = [q1[0] - p1[0], q1[1] - p1[1]];
        assert_vec_near!(actual_pq_1, [0., -(f64::sqrt(7.) - 1.) / 3.], EPS);

        // Curvatures:
        let actual_k0 = k_bezier_0(p0, q0, q1);
        assert_near!(actual_k0, k0, EPS);
        let actual_k1 = k_bezier_1(p1, q0, q1);
        assert_near!(actual_k1, k1, EPS);
    }

    /// Example where there is no solution
    #[test]
    fn test_segment_control_points_from_c2_segment_is_none() {
        const EPS: f64 = 1.0e-5;
        let p1 = C2Point {
            pos: [1.0, 0.0008],
            tau: [-0.9569, -0.29],
            k: 0.0006,
        };
        let p2 = C2Point {
            pos: [0.99, 0.0],
            tau: [-0.9568, -0.29],
            k: 0.0006,
        };

        let q = SegmentControlPoints::from_c2_segment_with_tolerance(&p1, &p2, EPS);
        assert!(q.is_none());
    }

    /// Example: Straight line
    /// \gamma(0) = (0, 0), \gamma'(0) = (1, 0), k(0) = 0
    /// \gamma(1) = (1, 0), \gamma'(1) = (1, 0), k(0) = 0
    #[test]
    fn test_segment_control_points_from_c2_segment_for_straight_line() {
        const EPS: f64 = 1.0e-5;
        let p0 = [0., 0.];
        let t0 = [1., 0.];
        let k0 = 0.;

        let p1 = [1., 0.];
        let t1 = [1., 0.];
        let k1 = 0.;

        let SegmentControlPoints {
            q_start: q0,
            q_stop: q1,
        } = SegmentControlPoints::from_c2_segment_with_tolerance(
            &C2Point {
                pos: p0,
                tau: t0,
                k: k0,
            },
            &C2Point {
                pos: p1,
                tau: t1,
                k: k1,
            },
            EPS,
        )
        .unwrap();
        // Tangents:
        let actual_pq_0 = [q0[0] - p0[0], q0[1] - p0[1]];
        assert_vec_near!(actual_pq_0, [1. / 3., 0.], EPS);
        let actual_pq_1 = [q1[0] - p1[0], q1[1] - p1[1]];
        assert_vec_near!(actual_pq_1, [-1. / 3., 0.], EPS);
    }
    /// Example: Parallel tangents
    /// \gamma(0) = (0, 0), \gamma'(0) = (1, 0), k(0) = 1
    /// \gamma(1) = (1, 1), \gamma'(1) = (1, 0), k(0) = -1
    #[test]
    fn test_segment_control_points_from_c2_segment_for_parallel_tangents() {
        const EPS: f64 = 1.0e-5;
        let p0 = [0., 0.];
        let t0 = [1., 0.];
        let k0 = 1.;

        let p1 = [1., 1.];
        let t1 = [1., 0.];
        let k1 = -1.;

        let SegmentControlPoints {
            q_start: q0,
            q_stop: q1,
        } = SegmentControlPoints::from_c2_segment_with_tolerance(
            &C2Point {
                pos: p0,
                tau: t0,
                k: k0,
            },
            &C2Point {
                pos: p1,
                tau: t1,
                k: k1,
            },
            EPS,
        )
        .unwrap();
        // Tangents:
        let actual_pq_0 = [q0[0] - p0[0], q0[1] - p0[1]];
        assert_vec_near!(actual_pq_0, [f64::sqrt(2. / 3.), 0.], EPS);
        let actual_pq_1 = [q1[0] - p1[0], q1[1] - p1[1]];
        assert_vec_near!(actual_pq_1, [-f64::sqrt(2. / 3.), 0.], EPS);

        // Curvatures:
        let actual_k0 = k_bezier_0(p0, q0, q1);
        assert_near!(actual_k0, k0, EPS);
        let actual_k1 = k_bezier_1(p1, q0, q1);
        assert_near!(actual_k1, k1, EPS);
    }

    /// Example:
    /// B(0) = (0, -1), B'(0) = (1, 0)
    /// B(1) = (1,  0), B'(1) = (0, 2)
    #[test]
    fn test_segment_control_points_from_c1_segment_for_circle() {
        let p0 = [0., -1.];
        let v0 = [1., 0.];

        let p1 = [1., 0.];
        let v1 = [0., 2.];

        let SegmentControlPoints { q_start, q_stop } = SegmentControlPoints::from_c1_segment(
            &C1Point { pos: p0, tau: v0 },
            &C1Point { pos: p1, tau: v1 },
        );
        // B'(0) = -3 p0 + 3 q0
        let actual_db = [3. * (-p0[0] + q_start[0]), 3. * (-p0[1] + q_start[1])];
        assert_vec_almost_eq!(actual_db, [std::f64::consts::SQRT_2, 0.]);
        // B'(1) = -3 q1 + 3 p1
        let actual_db = [3. * (p1[0] - q_stop[0]), 3. * (p1[1] - q_stop[1])];
        assert_vec_almost_eq!(actual_db, [0., std::f64::consts::SQRT_2]);
    }

    #[test]
    fn test_into_cubic_bezier_curve_returns_empty_bezier_curve_for_empty_path() {
        let gamma: [C1Point; 0] = [];
        let mut bezier_curve = into_cubic_bezier_curve(gamma);
        assert!(bezier_curve.next().is_none());
    }

    #[test]
    fn test_into_cubic_bezier_curve_returns_one_bezier_point_for_path_with_one_point() {
        let gamma = [C1Point {
            pos: [0., 0.],
            tau: [1., 1.],
        }];
        let bezier_curve = into_cubic_bezier_curve(gamma.clone()).collect::<Vec<_>>();
        assert_eq!(bezier_curve.len(), 1);
        assert_vec_almost_eq!(bezier_curve[0].point, gamma[0].pos);
    }

    #[test]
    fn test_into_cubic_bezier_curve_returns_bezier_curve_with_3_points_for_path_with_3_points() {
        let gamma = [
            C1Point {
                pos: [0., 0.],
                tau: [1., 0.],
            },
            C1Point {
                pos: [1., 1.],
                tau: [0., 1.],
            },
            C1Point {
                pos: [0., 2.],
                tau: [-1., 0.],
            },
        ];
        let bezier_curve = into_cubic_bezier_curve(gamma.clone()).collect::<Vec<_>>();
        assert_eq!(bezier_curve.len(), 3);
        assert_vec_almost_eq!(bezier_curve[0].point, gamma[0].pos);
        assert_vec_almost_eq!(bezier_curve[1].point, gamma[1].pos);
        assert_vec_almost_eq!(bezier_curve[2].point, gamma[2].pos);
        use std::f64::consts::SQRT_2;
        let v_forward =
            |CubicBezierPoint {
                 point: p,
                 forward_control_point: q,
                 ..
             }: &CubicBezierPoint| { [3. * (-p[0] + q[0]), 3. * (-p[1] + q[1])] };
        assert_vec_almost_eq!(v_forward(&bezier_curve[0]), [SQRT_2, 0.]);
        assert_vec_almost_eq!(v_forward(&bezier_curve[1]), [0., SQRT_2]);
        let v_backward =
            |CubicBezierPoint {
                 point: p,
                 backward_control_point: q,
                 ..
             }: &CubicBezierPoint| { [3. * (p[0] - q[0]), 3. * (p[1] - q[1])] };
        assert_vec_almost_eq!(v_backward(&bezier_curve[1]), [0., SQRT_2]);
        assert_vec_almost_eq!(v_backward(&bezier_curve[2]), [-SQRT_2, 0.]);
    }

    #[test]
    fn test_into_closed_cubic_bezier_curve_returns_empty_bezier_curve_for_empty_path() {
        let gamma: [C1Point; 0] = [];
        let mut bezier_curve = into_closed_cubic_bezier_curve(gamma);
        assert!(bezier_curve.next().is_none());
    }

    #[test]
    fn test_into_closed_cubic_bezier_curve_returns_one_bezier_point_for_path_with_one_point() {
        let gamma = [C1Point {
            pos: [0., 0.],
            tau: [1., 1.],
        }];
        let bezier_curve = into_closed_cubic_bezier_curve(gamma.clone()).collect::<Vec<_>>();
        assert_eq!(bezier_curve.len(), 1);
        assert_vec_almost_eq!(bezier_curve[0].point, gamma[0].pos);
    }

    fn v_forward(
        CubicBezierPoint {
            point: p,
            forward_control_point: q,
            ..
        }: &CubicBezierPoint,
    ) -> [f64; 2] {
        [3. * (-p[0] + q[0]), 3. * (-p[1] + q[1])]
    }
    fn v_backward(
        CubicBezierPoint {
            point: p,
            backward_control_point: q,
            ..
        }: &CubicBezierPoint,
    ) -> [f64; 2] {
        [3. * (p[0] - q[0]), 3. * (p[1] - q[1])]
    }

    #[test]
    fn test_into_closed_cubic_bezier_curve_returns_one_bezier_point_for_path_with_two_points() {
        let gamma = [
            C1Point {
                pos: [0., 0.],
                tau: [1., 0.],
            },
            C1Point {
                pos: [0., 1.],
                tau: [-1., 0.],
            },
        ];
        let bezier_curve = into_closed_cubic_bezier_curve(gamma.clone()).collect::<Vec<_>>();

        assert_eq!(bezier_curve.len(), 2);
        assert_vec_almost_eq!(bezier_curve[0].point, gamma[1].pos);
        assert_vec_almost_eq!(bezier_curve[1].point, gamma[0].pos);

        assert_vec_almost_eq!(v_forward(&bezier_curve[1]), [1., 0.]);
        assert_vec_almost_eq!(v_forward(&bezier_curve[0]), [-1., 0.]);
        assert_vec_almost_eq!(v_backward(&bezier_curve[0]), [-1., 0.]);
        assert_vec_almost_eq!(v_backward(&bezier_curve[1]), [1., 0.]);
    }

    #[test]
    fn test_into_closed_cubic_bezier_curve_returns_bezier_curve_with_3_points_for_path_with_3_points(
    ) {
        let gamma = [
            C1Point {
                pos: [0., 0.],
                tau: [1., 0.],
            },
            C1Point {
                pos: [1., 1.],
                tau: [0., 1.],
            },
            C1Point {
                pos: [0., 2.],
                tau: [-1., 0.],
            },
        ];
        let bezier_curve = into_closed_cubic_bezier_curve(gamma.clone()).collect::<Vec<_>>();
        assert_eq!(bezier_curve.len(), 3);
        assert_vec_almost_eq!(bezier_curve[0].point, gamma[1].pos);
        assert_vec_almost_eq!(bezier_curve[1].point, gamma[2].pos);
        assert_vec_almost_eq!(bezier_curve[2].point, gamma[0].pos);
        use std::f64::consts::SQRT_2;
        assert_vec_almost_eq!(v_forward(&bezier_curve[2]), [SQRT_2, 0.]);
        assert_vec_almost_eq!(v_forward(&bezier_curve[0]), [0., SQRT_2]);
        assert_vec_almost_eq!(v_backward(&bezier_curve[0]), [0., SQRT_2]);
        assert_vec_almost_eq!(v_backward(&bezier_curve[1]), [-SQRT_2, 0.]);

        // First and last control points:
        assert_vec_almost_eq!(v_backward(&bezier_curve[2]), [2., 0.]);
        assert_vec_almost_eq!(v_forward(&bezier_curve[1]), [-2., 0.]);
    }
}
