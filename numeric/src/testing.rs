//! Test utilities

/// Default tolerance for `assert_*_almost_*` macros.
pub const EPS_EQ: f64 = 0.5E-9;

/// Defines a f64 valued distance of two objects which might possibly consist of
/// NAN values.
pub trait NanDistance {
    /// Distance to `other`, returning infinity if `self != other` and
    /// `self` or `other` is nan.
    fn nan_distance(self, other: Self) -> f64;
}

impl NanDistance for f64 {
    fn nan_distance(self, other: Self) -> f64 {
        match (f64::is_nan(self), f64::is_nan(other)) {
            (true, true) => 0.,
            (false, false) => (self - other).abs(),
            _ => f64::INFINITY,
        }
    }
}
impl NanDistance for &f64 {
    fn nan_distance(self, other: Self) -> f64 {
        (*self).nan_distance(*other)
    }
}

impl NanDistance for &(f64, f64) {
    fn nan_distance(self, other: Self) -> f64 {
        (*self).nan_distance(*other)
    }
}
impl NanDistance for (f64, f64) {
    fn nan_distance(self, other: Self) -> f64 {
        f64::max(self.0.nan_distance(other.0), self.1.nan_distance(other.1))
    }
}
impl NanDistance for &[f64; 2] {
    fn nan_distance(self, other: Self) -> f64 {
        (*self).nan_distance(*other)
    }
}
impl NanDistance for [f64; 2] {
    fn nan_distance(self, other: Self) -> f64 {
        f64::max(
            self[0].nan_distance(other[0]),
            self[1].nan_distance(other[1]),
        )
    }
}

impl NanDistance for crate::complex::Complex<f64> {
    fn nan_distance(self, other: Self) -> f64 {
        (self.re, self.im).nan_distance((other.re, other.im))
    }
}

impl NanDistance for &crate::complex::Complex<f64> {
    fn nan_distance(self, other: Self) -> f64 {
        (*self).nan_distance(*other)
    }
}

impl NanDistance for &[crate::complex::Complex<f64>; 2] {
    fn nan_distance(self, other: Self) -> f64 {
        f64::max(
            self[0].nan_distance(other[0]),
            self[1].nan_distance(other[1]),
        )
    }
}

/// Represents a violation of `|left - right| < tolerance`.
pub struct ToleranceViolation<V, T> {
    /// Value of left
    pub left: V,
    /// Value of right
    pub right: V,
    /// Tolerance for which the test failed
    pub tolerance: T,
    /// Actual absolute difference
    pub delta: T,
}

impl<V: std::fmt::Debug, T: std::fmt::Debug> std::fmt::Debug for ToleranceViolation<V, T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            r#"assertion failed: `(|left - right| < tolerance)`
        left: `{:?}`,
       right: `{:?}`,
   tolerance: `{:?}`,
|left-right|: `{:?}`"#,
            self.left, self.right, self.tolerance, self.delta
        )
    }
}

/// Panic on a violation
#[track_caller]
pub fn raise_violation(violation: impl std::fmt::Debug) -> ! {
    panic!("{violation:?}")
}

/// Asserts that `NanDistance::nan_distance(left, right) < tolerance` with respect to [NanDistance].
#[macro_export]
macro_rules! assert_near {
    ($left:expr, $right:expr, $d: expr) => {
        match (&$left, &$right) {
            (left_val, right_val) => {
                use $crate::testing::NanDistance;
                let delta = (*left_val).nan_distance(*right_val);
                if delta > $d {
                    $crate::testing::raise_violation($crate::testing::ToleranceViolation {
                        left: *left_val,
                        right: *right_val,
                        tolerance: $d,
                        delta,
                    });
                }
            }
        }
    };
}

/// Convenience version of [assert_near!] with `tolerance` set to [EPS_EQ].
#[macro_export]
macro_rules! assert_almost_eq {
    ($x:expr, $y:expr) => {
        $crate::assert_near!($x, $y, $crate::testing::EPS_EQ);
    };
}

/// Represents a size mismatch of two slices.
pub struct SizeMismatch {
    /// Size of left
    pub left: usize,
    /// Size of right
    pub right: usize,
}

impl std::fmt::Debug for SizeMismatch {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            r#"assertion failed: `left.len() != right.len()`
        left.len(): `{:?}`
       right.len(): `{:?}`"#,
            self.left, self.right
        )
    }
}

/// Represents a violation of `|left[index] - right[index]| < tolerance`.
pub struct ItemToleranceViolation<V, T> {
    /// Index for which the test failed
    pub index: usize,
    /// Value of `left[index]`
    pub left: V,
    /// Value of `right[index]`
    pub right: V,
    /// Tolerance for which the test failed
    pub tolerance: T,
    /// Absolute difference
    pub delta: T,
}

impl<V: std::fmt::Debug, T: std::fmt::Debug> std::fmt::Debug for ItemToleranceViolation<V, T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            r#"assertion failed: `(|left[{index}] - right[{index}]| < tolerance)`
        left[{index}]: `{l:?}`
       right[{index}]: `{r:?}`,
   tolerance: `{tolerance:?}`,
|left[{index}]-right[{index}]|: `{delta:?}`"#,
            index = self.index,
            l = self.left,
            r = self.right,
            tolerance = self.tolerance,
            delta = self.delta
        )
    }
}

/// Asserts that `NanDistance::nan_distance(left[i], right[i]) < tolerance` with respect to [NanDistance] and that the sizes of `left` and `right` match.
#[macro_export]
macro_rules! assert_vec_near {
    ($left: expr, $right:expr, $d: expr) => {
        match (&$left, &$right) {
            (left_val, right_val) => {
                if left_val.len() != right_val.len() {
                    $crate::testing::raise_violation($crate::testing::SizeMismatch {
                        left: left_val.len(),
                        right: right_val.len(),
                    });
                }
                for (index, (left, right)) in left_val.iter().zip(right_val.iter()).enumerate() {
                    use $crate::testing::NanDistance;
                    let delta = left.nan_distance(right);

                    if delta > $d {
                        $crate::testing::raise_violation($crate::testing::ItemToleranceViolation {
                            index,
                            tolerance: $d,
                            left,
                            right,
                            delta,
                        });
                    }
                }
            }
        }
    };
}

/// Convenience version of [assert_vec_near!] with `tolerance` set to [EPS_EQ].
#[macro_export]
macro_rules! assert_vec_almost_eq {
    ($x:expr, $y:expr) => {
        $crate::assert_vec_near!($x, $y, $crate::testing::EPS_EQ);
    };
}
