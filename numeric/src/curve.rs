//! Representation of continuously differentiable curves by sequences of
//! points with tangent and signed curvature

/// 2D point with tangent.
#[derive(Debug, Clone)]
pub struct C1Point {
    /// Coordinates of the point
    pub pos: [f64; 2],
    /// Unit tangent
    pub tau: [f64; 2],
}

impl From<&C1Point> for [f64; 2] {
    fn from(p: &C1Point) -> Self {
        p.pos
    }
}

/// 2D point with unit tangent and signed curvature (left curve is positive).
#[derive(Debug, Clone)]
pub struct C2Point {
    /// Coordinates of the point
    pub pos: [f64; 2],
    /// Unit tangent
    pub tau: [f64; 2],
    /// Scalar curvature
    pub k: f64,
}

impl From<&C2Point> for [f64; 2] {
    fn from(p: &C2Point) -> Self {
        p.pos
    }
}

impl From<&C2Point> for C1Point {
    fn from(p: &C2Point) -> Self {
        Self {
            pos: p.pos,
            tau: p.tau,
        }
    }
}
