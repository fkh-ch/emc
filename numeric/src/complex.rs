//! Complex numbers

use super::field::{Abs, ConstNAN, ConstZero, Field, IsNan};
pub use num_complex::Complex;

impl IsNan for Complex<f64> {
    fn is_nan(self) -> bool {
        self.re.is_nan() || self.im.is_nan()
    }
}
impl Abs for Complex<f64> {
    type Output = f64;
    fn abs(self) -> Self::Output {
        <Complex<f64> as num_complex::ComplexFloat>::abs(self)
    }
}

impl ConstZero for Complex<f64> {
    const ZERO: Self = Complex { re: 0., im: 0. };
}

impl Field for Complex<f64> {}

impl ConstNAN for Complex<f64> {
    const NAN: Self = Complex {
        re: f64::NAN,
        im: f64::NAN,
    };
}
