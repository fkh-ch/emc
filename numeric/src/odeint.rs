//! ODE solving

#![allow(clippy::needless_range_loop)]

use super::field::Real;

/// Maximum size of sequence which gets extrapolated to 0.
const K_MAX: usize = 8;
/// Minimum size of sequence which gets extrapolated to 0.
const K_MIN: usize = 3;
/// Step size monitor according to
/// \[1] Deuflhard, P. Order and stepsize control in extrapolation methods. Numer. Math. 41, 399–422 (1983). <https://doi.org/10.1007/BF01418332>
pub struct StepSizeMonitor<T: Real> {
    /// Maximal step count until which work / stepsize can increase
    pub k_max: usize,
    /// Predicted optimal index (at this index, the extrapolation is expected to converge within tolerance).
    pub q_opt: usize,
    /// Work coefficients dependent on eps_tolerance
    pub alpha: [[T; K_MAX + 1]; K_MAX + 1],
    /// Tolerance
    pub eps_tolerance: T,
    /// True if initialization phase has been completed
    pub initialized: bool,
    /// Residual history
    pub e: [T; K_MAX],
}

/// Number of subdivisions n_k in the k iteration of the basic step H into
/// successively smaller sub steps h_k = H/n_k.
/// Even sequence: n_k = 2, 4, 6, ...
const fn n_subdivisions_per_step(step: usize) -> usize {
    2 * (step + 1)
}

/// Measures amount of work required to produce the ith approximation of the target value
/// of y(x+H). See [1], (2.11).
/// a_work[i] corresponds to A_{i+1}.
const fn a_work(i: usize) -> u16 {
    if i == 0 {
        n_subdivisions_per_step(0) as u16 + 1
    } else {
        a_work(i - 1) + n_subdivisions_per_step(i) as u16
    }
}

/// const version of std::array::from_fn
macro_rules! arr_from_fn {
    [$type:ty; $size:expr; $f:expr] => {
        {
            let mut arr: [$type; $size] = [0; $size];
            let mut k: usize = 0;
            while k < $size {
                arr[k] = $f(k);
                k += 1;
            }
            arr
        }
    }
}

/// Precompute a_work:
const A_WORK: [u16; K_MAX + 2] = arr_from_fn![u16; K_MAX + 2; a_work];

/// Needed in the computation of \alpha(k, q), see [1], (3.7), (4.6)
/// \alpha(k, q) = (\alpha_{k+1, k}^{(q)}/eps)^{1/(2k + 1)}
///             = (eps^{(A_{k+1}-A_1 + 1)/(A_{q+1}-A_1+1)}/eps)^{1/(2k + 1)}
///             = (eps^{(A_{k+1}-A_{q+1})/(A_{q+1}-A_1+1)})^{1/(2k + 1)}.
/// alpha[k, q] corresponds to \alpha(k+1, q+1)
fn alpha_exp<T: Real>(k: usize, q: usize) -> T {
    // TODO: make const as soon as traits are const
    -T::from(a_work(q + 1) - a_work(k + 1))
        / (T::from(a_work(q + 1) - a_work(0) + 1) * T::from(2 * (k + 1) as u16 + 1))
}

#[derive(PartialEq, Debug)]
enum StepSizeInstruction<T: Real> {
    RestartWithReduction(T),
    NextStepWithAdaption(T),
    NextSubStep,
}

/// Maximal eps tolerance, so that k_max >= K_MIN.
fn eps_max<T: Real, const K_MIN: usize>() -> T {
    // TODO: make const as soon as traits are const, and #57241 is solved
    let a = T::from(A_WORK[K_MIN + 1]) / T::from(A_WORK[K_MIN]);
    //    alpha[k][k + 1] > a
    // <=> rho_eps^(alpha_exp(k, k+1)) > a
    // <=> rho_eps > a^(1 / alpha_exp(k, k+1))
    a.powf(T::from(1) / alpha_exp(K_MIN - 1, K_MIN))
}

fn build_alpha<T: Real, const K_SIZE: usize>(eps_tolerance: T) -> [[T; K_SIZE]; K_SIZE] {
    const SAFETY_FACTOR: u16 = 4;
    let rho_eps = eps_tolerance / T::from(SAFETY_FACTOR);
    let mut alpha = [[T::from(0); K_SIZE]; K_SIZE];
    for q in 1..K_SIZE {
        for k in 0..q {
            // See explanation in alpha_exp above:
            alpha[k][q] = rho_eps.powf(alpha_exp(k, q));
        }
        alpha[q][q] = T::from(1);
    }
    alpha
}

impl<T: Real + Consts> StepSizeMonitor<T> {
    const SAFETY_FACTOR: u16 = 4;
    /// Maximal factor for a step size increase
    const H_MAX_MULTIPLIER: u16 = 10;
    /// Another safety factor, applied when suggesting a step size reduction due to out of order window (to define it here, we have to split it into nominator, denominator)
    const SIGMA_NOMINATOR: u16 = 7;
    const SIGMA_DENOMINATOR: u16 = 10;

    /// Create a new [StepSizeMonitor] with tolerance `eps_tolerance`
    pub fn new(eps_tolerance: T) -> Self {
        let eps_tolerance = T::min(eps_max::<_, K_MIN>(), eps_tolerance);
        let alpha = build_alpha(eps_tolerance);
        let mut k_max = 0;
        // Determine max number of iterations, so that work per unit step size is decreasing.
        // See [1], (4.8'):
        for k in (1..K_MAX).rev() {
            if T::from(A_WORK[k + 1]) * alpha[k][k + 1] > T::from(A_WORK[k + 2]) {
                k_max = k + 1; // Condition satisfied, except for k_max
                break;
            }
        }
        Self {
            k_max,
            q_opt: k_max,
            alpha,
            eps_tolerance,
            initialized: false,
            e: [T::from(0); K_MAX],
        }
    }
    fn e_from_eps(&self, k: usize, eps: T) -> T {
        // Step size estimate H_k = H / e[k],
        // See [1], (4.3)
        let eps_rel = eps / self.eps_tolerance;
        (eps_rel * T::from(Self::SAFETY_FACTOR)).powf(T::from(1) / T::from(2u16 * (k as u16 + 1)))
    }

    /// Record a new residual for step k and suggest step size reductions if needed.
    fn monitor_eps(&mut self, k: usize, eps: T, previous_restart: bool) -> StepSizeInstruction<T> {
        let (k_max, q_opt) = (self.k_max, self.q_opt);
        self.e[k - 1] = self.e_from_eps(k, eps);

        // Error criterion and monitoring condition only if in order window
        // or in initialization phase (see [1], p. 415):
        if k + 1 < q_opt && self.initialized {
            // below order window, lower bound
            return StepSizeInstruction::NextSubStep;
        }
        // Convergence criterion:
        if eps < self.eps_tolerance {
            let mut work_min = T::MAX;
            let mut e_opt = T::from(1);
            // Determine q_opt, by minimizing work per unit step size:
            let mut q_opt = 1;
            let e_min = T::from(1) / T::from(Self::H_MAX_MULTIPLIER);
            for l in 1..=k {
                let e = T::max(self.e[l - 1], e_min);
                let work = e * T::from(A_WORK[l]);
                if work < work_min {
                    e_opt = e;
                    work_min = work;
                    q_opt = l;
                }
            }
            // Step size increase (Only if not already increased during current basic step):
            // Try to increase q_opt beyond k, where no e values are available.
            // Estimate by using the alpha coefficients.
            if q_opt >= k && q_opt != k_max && !previous_restart {
                let e = T::max(e_opt / self.alpha[q_opt - 1][q_opt], e_min);
                if T::from(A_WORK[q_opt + 1]) * e <= work_min {
                    e_opt = e;
                    q_opt += 1;
                }
            }
            self.q_opt = q_opt;
            self.initialized = true;
            return StepSizeInstruction::NextStepWithAdaption(T::from(1) / e_opt);
        }
        if k == self.k_max || k == q_opt + 1 {
            // Above order window => suggest change H -> H/e[k-1] ~ H_k
            return StepSizeInstruction::RestartWithReduction(
                T::from(Self::SIGMA_NOMINATOR) / T::from(Self::SIGMA_DENOMINATOR) / self.e[k - 1],
            );
        } else if self.e[k - 1] > self.alpha[k - 1][q_opt] {
            // H too big, apply [1] (4.10)
            return StepSizeInstruction::RestartWithReduction(
                self.alpha[k - 1][q_opt - 1] / self.e[k - 1],
            );
        }
        StepSizeInstruction::NextSubStep
    }
}

/// Abstraction of constants needed in the routine
pub trait Consts {
    /// Maximum factor for stepsize reduction
    const REDMAX: Self;
    /// Minimum factor for stepsize reduction
    const REDMIN: Self;
}

impl Consts for f64 {
    const REDMAX: Self = 1.0e-5; // Maximum factor for stepsize reduction
    const REDMIN: Self = 0.7; // Minimum factor for stepsize reduction
}
impl Consts for f32 {
    const REDMAX: Self = 1.0e-5; // Maximum factor for stepsize reduction
    const REDMIN: Self = 0.7; // Minimum factor for stepsize reduction
}

/// Gragg-Bulirsch-Stoer method over one basic step H with step size monitoring.
///
/// # Arguments
///
/// * `y` - Initial value. Will be overridden by the value `y(x+H)`, where `H` is the basic step size.
/// * `dy` - Derivative `dy/dx` at `x`.
/// * `x` - Initial value of the independent variable.
/// * `h` - Basic step size to be attempted. Cam be adapted by the step size monitor.
/// * `y_scale` - Scaling factor for the residuals.
/// * `f` - Right hand side of the ODE: `dy/dx(x) = f(x, y(x))` with a 3rd output argument,
///         such that a call to `f(x, y, dy)` computes `dy`.
/// * `monitor` - Step size monitor to be (re)used.
///
/// # Return value
///
/// Estimate for the step size for the following step.
#[allow(clippy::too_many_arguments)]
fn gbs_step<T: Real + Consts, const N: usize>(
    y: &mut [T; N],
    dy: &[T; N],
    x: &mut T,
    mut h: T,
    y_scale: &[T; N],
    f: impl Fn(T, &[T; N], &mut [T; N]),
    monitor: &mut StepSizeMonitor<T>,
) -> Result<T, Error> {
    let y_backup = *y; // Save the starting values
    let mut reduct = false;
    let mut yseq = [T::from(0); N];
    let k_max = monitor.k_max;
    let mut extrapolator = ZeroExtrapolator::<T, N, K_MAX>::new();
    let mut x_next;
    loop {
        extrapolator.reset();
        for k in 0..=k_max {
            // Evaluate the sequence of modified midpoint integrations
            x_next = *x + h;
            if x_next == (*x) {
                return Err(Error::Underflow);
            }
            modified_midpoint_rule(
                &y_backup,
                dy,
                *x,
                h,
                n_subdivisions_per_step(k) as u16,
                &mut yseq,
                &f,
            );
            let xest = h / T::from(n_subdivisions_per_step(k) as u16);
            let xest_sq = xest * xest; // Squared, since error series is even.
            extrapolator.extend_to(xest_sq, &yseq);
            *y = *extrapolator.lim_0();
            if y[0].is_nan() {
                return Err(Error::Nan);
            }
            if k != 0 {
                let delta = extrapolator.delta_y_max(y_scale);
                match monitor.monitor_eps(k, delta, reduct) {
                    StepSizeInstruction::NextSubStep => {}
                    StepSizeInstruction::NextStepWithAdaption(s) => {
                        *x = x_next;
                        return Ok(h * s);
                    }
                    StepSizeInstruction::RestartWithReduction(s) => {
                        let s = T::min(s, T::REDMIN);
                        let s = T::max(s, T::REDMAX);
                        h *= s;
                        reduct = true; // Reduce stepsize by at least REDMIN and at most REDMAX.
                        break;
                    }
                }
            }
        }
    }
}

struct ZeroExtrapolator<T: Real, const N: usize, const M: usize> {
    seq_len: usize,
    x: [T; M],
    p: [[T; N]; M],
}

impl<T: Real, const N: usize, const M: usize> ZeroExtrapolator<T, N, M> {
    fn new() -> Self {
        Self {
            seq_len: 0,
            x: [T::from(0); M],
            p: [[T::from(0); N]; M],
        }
    }

    fn reset(&mut self) {
        self.seq_len = 0;
    }

    /// Extrapolate a sequence of points `y[i] = y(x[i])` along a curve `y(x)` in N-dim space to 0.
    /// Assumtion: `x[i+1] > x[i] > 0`.
    /// The extrapolation to 0 is done by fitting a polynomial of degree m to the m given points `y[i]`
    /// using Neville's algorithm.
    /// p_{k, l}(x_i) = y_i for all k <= i <= l
    /// Tableau:
    /// p_{0,0}(0) = y_0
    ///                    p_{0,1}(0)
    /// p_{1,1}(0) = y_1                p_{0,2}(0)
    ///                    p_{1,2}(0)                p_{0,3}(0)
    /// p_{2,2}(0) = y_2                p_{1,3}(0)                p_{0,4}(0)
    ///                    p_{2,3}(0)                p_{1,4}(0)
    /// p_{3,3}(0) = y_3                p_{2,4}(0)
    ///                    p_{3,4}(0)
    /// p_{4,4}(0) = y_4
    ///
    /// Each call to `extend_to` adds a diagonal p_{m,m}(0), ..., p_{0,m}(0) to the tableau,
    /// by using the recursion
    /// $$p_{i,j}(0) = (x_i p_{i+1,j}(0) - x_j p_{i,j-1}(0)) / (x_i - x_j).$$
    fn extend_to(&mut self, x_new: T, y_new: &[T; N]) {
        let m = self.seq_len;
        self.seq_len += 1;
        self.x[m] = x_new;
        let (x, p) = (&self.x, &mut self.p);
        // p[k] = p_{k,m}(0)
        p[m][..N].copy_from_slice(&y_new[..N]);
        // p_{i,j}(0) = (x_i p_{i+1,j}(0) - x_j p_{i,j-1}(0)) / (x_i - x_j)
        // p_{k+1,m}(0) = p[k+1]
        // p_{k,m-1}(0) = p'[k]
        // => p[k] = (x_k p[k+1] - x_m p'[k]) / (x_k - x_m)
        // (p': values from previous iteration)
        for k in (0..m).rev() {
            let dx = x[k] - x[m];
            let xi_m = x[m] / dx;
            let xi_k = x[k] / dx;
            for j in 0..N {
                p[k][j] = xi_k * p[k + 1][j] - xi_m * p[k][j];
            }
        }
    }
    fn lim_0(&self) -> &[T; N] {
        &self.p[0]
    }
    fn delta_y_max(&self, scale: &[T; N]) -> T {
        let mut delta_max = T::EPSILON;
        for i in 0..N {
            delta_max = T::max(delta_max, ((self.p[0][i] - self.p[1][i]) / scale[i]).abs());
        }
        delta_max
    }
}

/// Modified midpoint rule.
/// See e.g. Josef Stoer, Roland Bulirsch: Numerische Mathematik 2 (https://doi.org/10.1007/b137272)
/// Step from y0 = y(x0) to y(x0+delta_x) in n_step substeps, where dy0 := y'(x0) = f(x0, y0).
/// The is written into y1 = y(x0+delta_x).
fn modified_midpoint_rule<T: Real, const N: usize>(
    y0: &[T; N],
    dy0: &[T; N],
    x0: T,
    delta_x: T,
    n_step: u16,
    y1: &mut [T; N],
    f: impl Fn(T, &[T; N], &mut [T; N]),
) {
    // Substep size:
    let h = delta_x / T::from(n_step);
    let mut eta_1 = *y0; // eta(x0)
    let mut eta_2 = [T::from(0); N]; // eta(x0+h)
    for i in 0..N {
        eta_2[i] = y0[i] + h * dy0[i];
    }
    let mut x = x0 + h;
    let dy: &mut [T; N] = y1; // Will use y1 for temporary storage of derivatives
    for _ in 1..n_step {
        // eta(x0+(n-1)h), eta(x0+nh) -> eta(x0+(n+1)h):
        f(x, &eta_2, dy);
        for i in 0..N {
            (eta_1[i], eta_2[i]) = (eta_2[i], eta_1[i] + T::from(2) * h * dy[i]);
        }
        x += h;
    }
    // eta(x0+delta_x):
    f(x, &eta_2, dy);
    for i in 0..N {
        y1[i] = (eta_1[i] + eta_2[i] + h * y1[i]) / T::from(2);
    }
}

struct OdeIntIter<
    'a,
    T: Real + Consts,
    F: Fn(T, &[T; N], &mut [T; N]),
    H: Fn(T, &[T; N], &[T; N]) -> T,
    const N: usize,
> {
    y_scale: [T; N],
    dy: [T; N],
    x: T,
    h: T,
    hmin: T,
    hmax: T,
    y: [T; N],
    f: F,
    step_size_cutoff: H,
    monitor: &'a mut StepSizeMonitor<T>,
    iter_count: usize,
}

/// Solve `dy/dx(x) = f(x, y(x))` with initial values `y(x0) = y0`,
/// until step_size_cutoff(x, y, dy) returns 0 within accuracy eps.
/// For example, to solve from `x0` to some value `x1`, set
/// `step_size_cutoff = |x, _y, _dy| (x1 -x).abs()`.
///
/// # Arguments
///
/// * `y0` - Initial value for `y`.
/// * `x0` - Initial value for `x`.
/// * `h` - Initial guess for the step size, it will be automatically adapted by [StepSizeMonitor].
/// * `h_range` - minimum (can be zero) and maximum allowed stepsize.
/// * `f` - Right hand side of the ODE, with a 3rd output argument,
///          such that a call to `f(x, y, dy)` computes `dy`.
/// * `step_size_cutoff` - Stop criterion - A function taking the current values of
///        `x`, `y(x)` and `dx/dy(x)` and returning an upper bound for the step size.
///        If it falls below `monitor.eps_tolerance`, the integration is terminated.
/// * `monitor` - Step size monitor to be (re)used.
///
/// # Return value
///
/// An iterator over the steps along the solution (y, dy).
pub fn odeint<'a, T: Real + Consts, const N: usize>(
    y0: &[T; N],
    x0: T,
    h: T,
    h_range: (T, T),
    f: impl Fn(T, &[T; N], &mut [T; N]) + 'a,
    step_size_cutoff: impl Fn(T, &[T; N], &[T; N]) -> T + 'a,
    monitor: &'a mut StepSizeMonitor<T>,
) -> impl Iterator<Item = Result<(T, [T; N], [T; N]), Error>> + 'a {
    let mut dy = [T::from(0); N];
    f(x0, y0, &mut dy);
    OdeIntIter {
        y_scale: [T::from(0); N],
        dy,
        x: x0,
        h,
        hmin: h_range.0,
        hmax: h_range.1,
        y: *y0,
        f,
        step_size_cutoff,
        monitor,
        iter_count: 0,
    }
}

impl<
        T: Real + Consts,
        F: Fn(T, &[T; N], &mut [T; N]),
        H: Fn(T, &[T; N], &[T; N]) -> T,
        const N: usize,
    > OdeIntIter<'_, T, F, H, N>
{
    fn try_next(&mut self) -> Result<(T, [T; N], [T; N]), Error> {
        const MAX_STEP: usize = 10000; // Terminate after MAX_STEP iterations.
        self.h = self.h.clamp_abs(self.hmax);

        for i in 0..N {
            self.y_scale[i] = self.y[i].abs() + (self.dy[i] * self.h).abs() + T::EPSILON;
        }
        self.h = gbs_step(
            &mut self.y,
            &self.dy,
            &mut self.x,
            self.h,
            &self.y_scale,
            &self.f,
            self.monitor,
        )?;
        if self.h.abs() <= self.hmin {
            return Err(Error::StepSizeTooSmall(format!(
                "odeint: step size h={} too small at x={}",
                self.h, self.x
            )));
        }
        (self.f)(self.x, &self.y, &mut self.dy);
        let h_cutoff = (self.step_size_cutoff)(self.x, &self.y, &self.dy);
        self.h = self.h.clamp_abs(h_cutoff);
        if self.iter_count > MAX_STEP {
            return Err(Error::CountExcess);
        }
        self.iter_count += 1;
        Ok((self.x, self.y, self.dy))
    }
}

trait ClampAbs: PartialOrd + Sized + Copy + std::ops::Neg<Output = Self> {
    fn clamp_abs(self, bound: Self) -> Self {
        if self > bound {
            return bound;
        }
        if self < -bound {
            return -bound;
        }
        self
    }
}
impl<T: Real> ClampAbs for T {}

impl<
        T: Real + Consts,
        F: Fn(T, &[T; N], &mut [T; N]),
        H: Fn(T, &[T; N], &[T; N]) -> T,
        const N: usize,
    > Iterator for OdeIntIter<'_, T, F, H, N>
{
    type Item = Result<(T, [T; N], [T; N]), Error>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.h.abs() <= self.monitor.eps_tolerance {
            return None;
        }
        Some(self.try_next())
    }
}

/// Errors which might occur during odeint
#[derive(Debug, Clone)]
pub enum Error {
    /// Numerical underflow
    Underflow,
    /// Extrapolation returned NAN
    Nan,
    /// Step size underflow
    StepSizeTooSmall(String),
    /// Counter exceeded maximum
    CountExcess,
}

impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Underflow => write!(f, "odeint.gbs_step: step size underflow"),
            Self::Nan => write!(f, "odeint.gbs_step: extrapolation returned NaN"),
            Self::StepSizeTooSmall(s) => write!(f, "odeint: {}", s),
            Self::CountExcess => write!(f, "odeint: too many steps"),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::{assert_almost_eq, assert_near, assert_vec_almost_eq, assert_vec_near};
    #[test]
    fn test_a_work() {
        assert_eq!(A_WORK, [3, 7, 13, 21, 31, 43, 57, 73, 91, 111]);
    }
    #[test]
    fn test_eps_max() {
        const K_MIN: usize = 3;
        let eps = eps_max::<f64, K_MIN>() + 1.0e-5;
        let step_size_monitor = StepSizeMonitor::<f64>::new(eps);
        assert_near!(eps, 3.78e-4, 1.0e-5);
        assert_eq!(step_size_monitor.k_max, K_MIN);
    }

    #[test]
    fn test_step_size_monitor_chooses_kmax_correctly() {
        let step_size_monitor = StepSizeMonitor::<f64>::new(1.0e-6);
        let condition = |k: usize| {
            a_work(k + 1) as f64 * step_size_monitor.alpha[k][k + 1] > a_work(k + 2) as f64
        };
        assert_eq!(step_size_monitor.k_max, 6);
        let expected_conforming_k = (1..K_MAX).filter(|k| condition(*k)).collect::<Vec<_>>();
        let conforming_k = (1..step_size_monitor.k_max).collect::<Vec<_>>();
        assert_eq!(conforming_k, expected_conforming_k);
    }
    fn assert_is_reduction(instruction: StepSizeInstruction<f64>) {
        match instruction {
            StepSizeInstruction::RestartWithReduction(x) => assert!(x < 1.),
            _ => panic!("Unexpected StepSizeInstruction variant: {:?}", instruction),
        }
    }
    fn eps_from_e(e: f64, k: usize, eps_tolerance: f64) -> f64 {
        e.powi(2 * (k + 1) as i32) / StepSizeMonitor::<f64>::SAFETY_FACTOR as f64 * eps_tolerance
    }
    #[test]
    fn test_step_size_monitor_suggests_step_reduction_for_too_big_step() {
        for (eps_tolerance, expected_k_max) in [(1.0e-6, 6), (1.0e-10, K_MAX)] {
            let mut step_size_monitor = StepSizeMonitor::<f64>::new(eps_tolerance);
            assert_eq!(step_size_monitor.k_max, expected_k_max);
            for violating_k in 1..step_size_monitor.q_opt {
                for k in 1..violating_k {
                    let eps = eps_tolerance + f64::EPSILON;
                    assert_eq!(
                        step_size_monitor.monitor_eps(k, eps, false),
                        StepSizeInstruction::NextSubStep,
                        "for k={k}"
                    );
                }
                let eps = eps_from_e(
                    step_size_monitor.alpha[violating_k - 1][step_size_monitor.q_opt],
                    violating_k,
                    eps_tolerance,
                ) + 1.;
                assert_is_reduction(step_size_monitor.monitor_eps(violating_k, eps, false));
            }
        }
    }
    #[test]
    fn test_step_size_monitor_suggests_step_reduction_for_too_big_step_when_initialized() {
        for (eps_tolerance, expected_k_max) in [(1.0e-6, 6), (1.0e-10, K_MAX)] {
            let mut step_size_monitor = StepSizeMonitor::<f64>::new(eps_tolerance);
            assert_eq!(step_size_monitor.k_max, expected_k_max);
            step_size_monitor.initialized = true;
            for q_opt in 1..=step_size_monitor.k_max {
                step_size_monitor.q_opt = q_opt;
                for violating_k in 1..q_opt {
                    for k in 1..violating_k {
                        let eps = eps_tolerance + f64::EPSILON;
                        assert_eq!(
                            step_size_monitor.monitor_eps(k, eps, false),
                            StepSizeInstruction::NextSubStep,
                            "(for k={k}, q_opt={q_opt}, violating_k={violating_k})"
                        );
                    }
                    for k in violating_k..q_opt - 1 {
                        let eps = eps_from_e(
                            step_size_monitor.alpha[violating_k - 1][step_size_monitor.q_opt],
                            k,
                            eps_tolerance,
                        ) + 1.;
                        assert_eq!(
                            step_size_monitor.monitor_eps(k, eps, false),
                            StepSizeInstruction::NextSubStep,
                            "(for k={k}, q_opt={q_opt}, violating_k={violating_k})"
                        );
                    }
                    let eps = eps_from_e(
                        step_size_monitor.alpha[violating_k - 1][step_size_monitor.q_opt],
                        usize::max(violating_k, q_opt - 1),
                        eps_tolerance,
                    ) + 1.;
                    assert_is_reduction(step_size_monitor.monitor_eps(
                        usize::max(violating_k, q_opt - 1),
                        eps,
                        false,
                    ));
                }
            }
        }
    }
    #[test]
    fn test_step_size_monitor_suggests_step_reduction_when_reaching_k_max() {
        for (eps_tolerance, expected_k_max) in [(1.0e-6, 6), (1.0e-10, K_MAX)] {
            let mut step_size_monitor = StepSizeMonitor::<f64>::new(eps_tolerance);
            assert_eq!(step_size_monitor.k_max, expected_k_max);
            let eps = eps_tolerance + f64::EPSILON;
            for k in 1..step_size_monitor.k_max {
                assert_eq!(
                    step_size_monitor.monitor_eps(k, eps, false),
                    StepSizeInstruction::NextSubStep,
                    "for k={k}"
                );
            }
            let k_max = step_size_monitor.k_max;
            assert_is_reduction(step_size_monitor.monitor_eps(k_max, eps, false));
        }
    }

    #[test]
    fn test_step_size_monitor_suggests_step_reduction_when_leaving_order_window() {
        for (eps_tolerance, expected_k_max) in [(1.0e-6, 6), (1.0e-10, K_MAX)] {
            let mut step_size_monitor = StepSizeMonitor::<f64>::new(eps_tolerance);
            assert_eq!(step_size_monitor.k_max, expected_k_max);
            step_size_monitor.initialized = true;
            let eps = eps_tolerance + f64::EPSILON;
            for q_opt in 1..step_size_monitor.k_max {
                step_size_monitor.q_opt = q_opt;
                for k in 1..=step_size_monitor.q_opt {
                    assert_eq!(
                        step_size_monitor.monitor_eps(k, eps, false),
                        StepSizeInstruction::NextSubStep,
                        "for k={k}"
                    );
                }
                assert_is_reduction(step_size_monitor.monitor_eps(q_opt + 1, eps, false));
            }
        }
    }
    fn assert_is_convergence(instruction: StepSizeInstruction<f64>) {
        let StepSizeInstruction::NextStepWithAdaption(_) = instruction else {
            panic!("Expected `ExitWithAdaption`, got `{instruction:?}`");
        };
    }
    fn eps_k(k: usize, q: usize, p: &StepSizeMonitor<f64>) -> f64 {
        0.5 * (eps_from_e(p.alpha[k - 1][q], k, p.eps_tolerance) + p.eps_tolerance)
    }
    #[derive(Debug)]
    struct StepSizeMonitorTestCase {
        name: &'static str,
        eps: [f64; K_MAX + 1],
        eps_converge: f64,
        previous_restart: bool,
        expected_q_opt: usize,
    }
    #[test]
    fn test_step_size_monitor_detects_convergence_during_initialization() {
        for (eps_tolerance, expected_k_max) in [(1.0e-6, 6), (1.0e-10, K_MAX)] {
            for k_converge in 1..=expected_k_max {
                let mut step_size_monitor = StepSizeMonitor::<f64>::new(eps_tolerance);
                assert_eq!(step_size_monitor.k_max, expected_k_max);
                let test_cases = &[
                    StepSizeMonitorTestCase {
                        name: "q_opt = k_max",
                        eps: std::array::from_fn(|l| {
                            eps_k(l + 1, expected_k_max, &step_size_monitor)
                        }),
                        eps_converge: eps_tolerance - f64::EPSILON,
                        previous_restart: false,
                        expected_q_opt: usize::min(k_converge + 1, expected_k_max),
                    },
                    StepSizeMonitorTestCase {
                        name: "q_opt = k_max (with previous restart)",
                        eps: std::array::from_fn(|l| {
                            eps_k(l + 1, expected_k_max, &step_size_monitor)
                        }),
                        eps_converge: eps_tolerance - f64::EPSILON,
                        previous_restart: true,
                        expected_q_opt: k_converge,
                    },
                    StepSizeMonitorTestCase {
                        name: "q_opt = k_converge",
                        eps: [eps_tolerance + f64::EPSILON; K_MAX + 1],
                        eps_converge: eps_from_e(
                            eps_tolerance * A_WORK[0] as f64 / A_WORK[k_converge] as f64,
                            k_converge,
                            eps_tolerance,
                        ),
                        previous_restart: false,
                        expected_q_opt: k_converge,
                    },
                    StepSizeMonitorTestCase {
                        name: "q_opt = k_converge (with previous restart)",
                        eps: [eps_tolerance + f64::EPSILON; K_MAX + 1],
                        eps_converge: eps_from_e(
                            eps_tolerance * A_WORK[0] as f64 / A_WORK[k_converge] as f64,
                            k_converge,
                            eps_tolerance,
                        ),
                        previous_restart: true,
                        expected_q_opt: k_converge,
                    },
                ];
                for test_case in test_cases {
                    step_size_monitor.initialized = false;
                    step_size_monitor.q_opt = step_size_monitor.k_max;
                    for k in 1..k_converge {
                        assert_eq!(
                            step_size_monitor.monitor_eps(
                                k,
                                test_case.eps[k - 1],
                                test_case.previous_restart
                            ),
                            StepSizeInstruction::NextSubStep,
                            "for k={k},test_case={}: {test_case:?}",
                            test_case.name
                        );
                    }
                    assert_is_convergence(step_size_monitor.monitor_eps(
                        k_converge,
                        test_case.eps_converge,
                        test_case.previous_restart,
                    ));
                    assert_eq!(
                        step_size_monitor.q_opt, test_case.expected_q_opt,
                        "for k_converge={k_converge},test_case={}: {test_case:?}",
                        test_case.name
                    );
                }
            }
        }
    }
    #[test]
    fn test_step_size_monitor_detects_convergence_after_initialization() {
        for (eps_tolerance, expected_k_max) in [(1.0e-6, 6), (1.0e-10, K_MAX)] {
            for q_opt in K_MIN..=expected_k_max {
                // Only check until upper bound of order window.
                // Above order window is checked in the step reduction tests.
                for k_converge in 1..=usize::min(expected_k_max, q_opt + 1) {
                    let mut step_size_monitor = StepSizeMonitor::<f64>::new(eps_tolerance);
                    assert_eq!(step_size_monitor.k_max, expected_k_max);
                    let test_cases = &[
                        StepSizeMonitorTestCase {
                            name: "alpha(k, q_opt)",
                            eps: std::array::from_fn(|l| eps_k(l + 1, q_opt, &step_size_monitor)),
                            eps_converge: eps_tolerance - f64::EPSILON,
                            previous_restart: false,
                            //    1   2   3   4   5   6
                            // 3  1 \ 3   4   5
                            // 4  1   2 \ 4   5   4
                            // 5  1   2   3 \ 5   6   5
                            // 6  1   2   3   4 \ 6   6
                            expected_q_opt: usize::min(
                                if (k_converge >= 5 && k_converge > q_opt && expected_k_max == 6)
                                    || k_converge == K_MAX
                                {
                                    q_opt
                                } else if k_converge + 1 >= q_opt {
                                    k_converge + 1
                                } else {
                                    k_converge
                                },
                                expected_k_max,
                            ),
                        },
                        StepSizeMonitorTestCase {
                            name: "alpha(k, q_opt) = k_max (with previous restart)",
                            eps: std::array::from_fn(|l| eps_k(l + 1, q_opt, &step_size_monitor)),
                            eps_converge: eps_tolerance - f64::EPSILON,
                            previous_restart: true,
                            expected_q_opt: usize::min(
                                if (k_converge >= 5 && k_converge > q_opt && expected_k_max == 6)
                                    || k_converge == K_MAX
                                {
                                    q_opt
                                } else {
                                    k_converge
                                },
                                expected_k_max,
                            ),
                        },
                        StepSizeMonitorTestCase {
                            name: "q_opt = k_converge",
                            eps: [eps_tolerance + f64::EPSILON; K_MAX + 1],
                            eps_converge: eps_from_e(
                                eps_tolerance * A_WORK[0] as f64 / A_WORK[k_converge] as f64,
                                k_converge,
                                eps_tolerance,
                            ),
                            previous_restart: false,
                            expected_q_opt: k_converge,
                        },
                        StepSizeMonitorTestCase {
                            name: "q_opt = k_converge (with previous restart)",
                            eps: [eps_tolerance + f64::EPSILON; K_MAX + 1],
                            eps_converge: eps_from_e(
                                eps_tolerance * A_WORK[0] as f64 / A_WORK[k_converge] as f64,
                                k_converge,
                                eps_tolerance,
                            ),
                            previous_restart: true,
                            expected_q_opt: k_converge,
                        },
                    ];
                    for test_case in test_cases {
                        step_size_monitor.initialized = true;
                        step_size_monitor.q_opt = q_opt;
                        let expected_k_converge = usize::max(k_converge, q_opt - 1);
                        for k in 1..expected_k_converge {
                            assert_eq!(
                                step_size_monitor.monitor_eps(
                                    k,
                                    if k < k_converge {
                                        test_case.eps[k - 1]
                                    } else {
                                        test_case.eps_converge
                                    },
                                    test_case.previous_restart
                                ),
                                StepSizeInstruction::NextSubStep,
                                "for k={k},k_converge={k_converge},q_opt={q_opt},test_case={}: {test_case:?}",
                                test_case.name
                            );
                        }
                        assert_is_convergence(step_size_monitor.monitor_eps(
                            expected_k_converge,
                            test_case.eps_converge,
                            test_case.previous_restart,
                        ));
                        assert_eq!(
                            step_size_monitor.q_opt, test_case.expected_q_opt,
                            "for k_converge={k_converge},q_opt={q_opt},test_case={}: {test_case:?}",
                            test_case.name
                        );
                    }
                }
            }
        }
    }

    #[test]
    fn test_modified_midpoint_rule_works_for_linear_solution() {
        let f = |_: f64, _y: &[f64; 1], dy: &mut [f64; 1]| {
            dy[0] = 1.;
        };
        let y0 = [0.];
        let x0 = 0.;
        let mut dy0 = [0.];
        f(x0, &y0, &mut dy0);
        let mut y1 = [0.];
        let delta_x = 10.;
        let n_step = 10;
        modified_midpoint_rule(&y0, &dy0, x0, delta_x, n_step, &mut y1, f);
        assert_vec_almost_eq!(y1, [10.]);
    }

    #[test]
    fn test_lim_0() {
        let mut extrapolator = ZeroExtrapolator::<f64, 2, 4>::new();
        // y = [x+1, x^3 - 1]
        extrapolator.extend_to(2., &[3., 7.]);
        // p_{0,0}(x) = [3, 7]
        assert_vec_almost_eq!(extrapolator.lim_0(), [3., 7.]);
        extrapolator.extend_to(1., &[2., 0.]);
        // p_{0,1}(x) = [x+1, -7+14 x]
        assert_vec_almost_eq!(extrapolator.lim_0(), [1., -7.]);
        extrapolator.extend_to(0.5, &[1.5, -7. / 8.]);
        // p_{0,2}(x) = [x+1, -7/8 + 7/2*(x-1/2)^2]
        assert_vec_almost_eq!(extrapolator.lim_0(), [1., 0.]);
        extrapolator.extend_to(0.25, &[1.25, f64::powi(2., -6) - 1.]);
        // p_{0,3}(x) = [x+1, x^3 - 1]
        assert_vec_almost_eq!(extrapolator.lim_0(), [1., -1.]);

        let dy = extrapolator.delta_y_max(&[1., 1.]);
        assert_almost_eq!(dy, 0.125);
    }

    #[test]
    fn test_build_alpha() {
        let expected_alpha = [
            [
                0.,
                36.644_676_208_496_094,
                129.654_220_581_054_7,
                236.038_070_678_710_94,
                329.363_983_154_296_9,
                404.282_409_667_968_75,
                462.835_906_982_421_9,
                508.470_855_712_890_6,
            ],
            [
                0.,
                1.,
                5.301_319_599_151_611,
                11.690_737_724_304_2,
                18.148_387_908_935_547,
                23.786_447_525_024_414,
                28.436_052_322_387_695,
                32.194_137_573_242_19,
            ],
            [
                0.,
                0.,
                1.,
                2.653_058_290_481_567_4,
                4.564_480_304_718_018,
                6.373_066_902_160_644_5,
                7.943_544_387_817_383,
                9.258_136_749_267_578,
            ],
            [
                0.,
                0.,
                0.,
                1.,
                1.904_328_107_833_862_3,
                2.830_253_839_492_798,
                3.676_153_421_401_977_5,
                4.409_090_995_788_574,
            ],
            [
                0.,
                0.,
                0.,
                0.,
                1.,
                1.581_451_654_434_204,
                2.140_054_464_340_21,
                2.640_923_023_223_877,
            ],
            [
                0.,
                0.,
                0.,
                0.,
                0.,
                1.,
                1.409_661_412_239_074_7,
                1.789_711_713_790_893_6,
            ],
            [0., 0., 0., 0., 0., 0., 1., 1.306_119_203_567_504_9],
            [0., 0., 0., 0., 0., 0., 0., 1.],
        ];
        let alpha = build_alpha::<f64, K_MAX>(1.0e-8);
        for i in 0..K_MAX {
            assert_vec_near!(alpha[i], expected_alpha[i], 1.0e-5);
        }
    }

    #[test]
    fn test_odeint_works_for_vortex() {
        let f_vortex = |_: f64, y: &[f64; 2], dy: &mut [f64; 2]| {
            dy[0] = -y[1];
            dy[1] = y[0];
        };
        let step_size_cutoff =
            |x: f64, _: &[f64; 2], _: &[f64; 2]| f64::max(0., 0.5 * std::f64::consts::PI - x);
        let y0: [f64; 2] = [1., 0.];
        let x0 = 0.;
        let h1 = 0.1;
        const EPS: f64 = 1.0e-5;
        let mut monitor = StepSizeMonitor::new(EPS);
        let ode_iter = odeint(
            &y0,
            x0,
            h1,
            (1.0e-9, 2.),
            f_vortex,
            step_size_cutoff,
            &mut monitor,
        );
        let (_, y1, dy1) = ode_iter.last().unwrap().unwrap();

        assert_near!(y1[0], 0., EPS);
        assert_near!(y1[1], 1., EPS);
        assert_near!(dy1[0], -1., EPS);
        assert_near!(dy1[1], 0., EPS);
    }

    #[test]
    fn test_odeint_works_for_rotating_field() {
        let f = |x: f64, _: &[f64; 2], dy: &mut [f64; 2]| {
            dy[0] = -f64::sin(x);
            dy[1] = f64::cos(x);
        };
        let step_size_cutoff =
            |x: f64, _: &[f64; 2], _: &[f64; 2]| f64::max(0., 0.5 * std::f64::consts::PI - x);
        let y0: [f64; 2] = [1., 0.];
        let x0 = 0.;
        let h1 = 0.1;
        const EPS: f64 = 1.0e-5;
        let mut monitor = StepSizeMonitor::new(EPS);
        let ode_iter = odeint(&y0, x0, h1, (1.0e-9, 2.), f, step_size_cutoff, &mut monitor);
        let (_, y1, dy1) = ode_iter.last().unwrap().unwrap();

        assert_near!(y1[0], 0., EPS);
        assert_near!(y1[1], 1., EPS);
        assert_near!(dy1[0], -1., EPS);
        assert_near!(dy1[1], 0., EPS);
    }

    /// y'(x) = y(x)
    ///  y(0) = 1
    #[test]
    fn test_odeint_works_for_exp() {
        let f = |_: f64, y: &[f64; 1], dy: &mut [f64; 1]| {
            dy[0] = y[0];
        };
        let step_size_cutoff = |x: f64, _: &[f64; 1], _: &[f64; 1]| f64::max(0., 1. - x);
        let y0: [f64; 1] = [1.];
        let x0 = 0.;
        let h1 = 0.1;
        const EPS: f64 = 1.0e-5;
        let mut monitor = StepSizeMonitor::new(EPS);
        let trajectory = odeint(&y0, x0, h1, (1.0e-9, 2.), f, step_size_cutoff, &mut monitor)
            .collect::<Result<Vec<_>, _>>()
            .unwrap();

        assert!(trajectory[0].0 >= 0.);
        let (x1, [y1], _) = trajectory.last().cloned().unwrap();
        assert_near!(x1, 1., EPS);
        assert_near!(y1, std::f64::consts::E, EPS);
        let err = trajectory
            .into_iter()
            .map(|(x, [y], _)| (y - f64::exp(x)).abs())
            .reduce(f64::max)
            .unwrap();
        assert_near!(err, 0., EPS);
    }

    /// y'(x) = y(x)
    ///  y(1) = 1
    /// In reversed direction.
    #[test]
    fn test_odeint_works_for_exp_reversed() {
        let f = |_: f64, y: &[f64; 1], dy: &mut [f64; 1]| {
            dy[0] = y[0];
        };
        let step_size_cutoff = |x: f64, _: &[f64; 1], _: &[f64; 1]| f64::max(0., x);
        let y0: [f64; 1] = [1.];
        let x0 = 1.;
        let h1 = -0.1;
        const EPS: f64 = 1.0e-5;
        let mut monitor = StepSizeMonitor::new(EPS);
        let trajectory = odeint(&y0, x0, h1, (1.0e-9, 2.), f, step_size_cutoff, &mut monitor)
            .collect::<Result<Vec<_>, _>>()
            .unwrap();

        assert!(trajectory[0].0 <= 1.);
        let (x1, [y1], _) = trajectory.last().cloned().unwrap();
        assert_near!(x1, 0., EPS);
        assert_near!(y1, 1. / std::f64::consts::E, EPS);
        let err = trajectory
            .into_iter()
            .map(|(x, [y], _)| (y - f64::exp(x - 1.)).abs())
            .reduce(f64::max)
            .unwrap();
        assert_near!(err, 0., EPS);
    }

    /// Dipole field. Check that `step_size_cutoff` stops
    /// the trajectory before hitting the singularity.
    #[test]
    fn test_odeint_works_for_singularities() {
        let singularities = [([-7.3, 17.0], 1.0), ([-7.3, -17.0], -1.0)];
        let f = |_: f64, &[x, y]: &[f64; 2], dy: &mut [f64; 2]| {
            let (mut fx, mut fy) = (0., 0.);
            for ([sx, sy], q) in &singularities {
                let dx = x - sx;
                let dy = y - sy;
                let e0 = q / (dx * dx + dy * dy);
                fx += e0 * dx;
                fy += e0 * dy;
            }
            let l = f64::hypot(fx, fy);
            *dy = [fx / l, fy / l];
        };
        let r_boundary: f64 = 100.;
        let r = 1.0;
        let step_size_cutoff = |_: f64, p: &[f64; 2], _: &[f64; 2]| {
            if p[0].powi(2) + p[1].powi(2) > r_boundary.powi(2) {
                return 0.;
            }
            let t_circle = singularities
                .iter()
                .map(|([sx, sy], _)| (p[0] - sx).powi(2) + (p[1] - sy).powi(2))
                .reduce(|d0, d1| if d0 < d1 { d0 } else { d1 })
                .unwrap_or(f64::INFINITY)
                .sqrt();
            f64::max(0., t_circle - r)
        };
        let y0: [f64; 2] = [-7.75, 16.];
        let x0 = 0.;
        let h1 = 1.0;
        const EPS: f64 = 1.0e-8;
        const MAX_ITER: usize = 30;
        let mut monitor = StepSizeMonitor::new(EPS);
        let trajectory = odeint(&y0, x0, h1, (1.0e-9, 2.), f, step_size_cutoff, &mut monitor)
            .take(MAX_ITER)
            .collect::<Result<Vec<_>, _>>()
            .unwrap();

        assert!(trajectory.len() < MAX_ITER);
    }
}
