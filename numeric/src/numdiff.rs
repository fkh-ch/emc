//! Numeric derivatives

use crate::field::{Abs, Field, Real};

/// Compute the derivative of a function `f` at a point `x` by Ridders’
/// method of polynomial extrapolation.
/// `h` is the initial stepsize estimate (it should be an increment
/// in `x` over which `f` changes substantially).
pub fn numdiff<K>(f: impl Fn(K) -> K, x: K, h: K) -> K
where
    K: Field,
    <K as Abs>::Output: Real + Consts,
{
    numdiff_err(f, x, h).0
}

/// Compute the derivative of a function `f` at a point `x` by Ridders’
/// method of polynomial extrapolation.
/// `h` is the initial stepsize estimate (it should be an increment
/// in `x` over which `f` changes substantially).
/// An estimate of the error in the derivative is returned as `err`.
pub fn numdiff_err<K>(f: impl Fn(K) -> K, x: K, h: K) -> (K, <K as Abs>::Output)
where
    K: Field,
    <K as Abs>::Output: Real + Consts,
{
    lim_0_err(|h| (f(x + h) - f(x - h)) / (h * <K as Abs>::Output::TWO), h)
}

/// Compute the limit `x->0` of a function `f` by Ridders’
/// method of polynomial extrapolation.
/// `h` is the initial stepsize estimate (it should be an increment
/// in `x` over which `f` changes substantially).
pub fn lim_0<K>(f: impl Fn(K) -> K, h: K) -> K
where
    K: Field,
    <K as Abs>::Output: Real + Consts,
{
    lim_0_err(f, h).0
}

/// Compute the limit `x->0` of a function `f` using Ridders’
/// method of polynomial extrapolation.
///
/// `h` is the initial stepsize estimate (it should be an increment
/// in `x` over which `f` changes substantially).
/// An estimate of the error in the derivative is returned as `err`.
///
/// Source:
/// Ridders, C.J.F. 1982, Advances in Engineering Software, vol. 4, no. 2, pp. 75–76.
///
/// It essentially the original algorithm, without a termination criterion,
/// and instead returning the value with minimal residual over the Romberg
/// tableau.
pub fn lim_0_err<R, K>(f: impl Fn(K) -> K, mut h: K) -> (K, R)
where
    R: Real + Consts,
    K: Field + Abs<Output = R>,
{
    const N_TAB: usize = 10;
    assert!(h != K::ZERO, "lim_0_err: h must be nonzero.");

    // Column of Romberg tableau, stored from bottom to top:
    let mut a = [K::ZERO; N_TAB];
    a[0] = f(h);
    let mut err = R::MAX;
    let mut f_0 = a[0];
    // If the tableau is denoted by
    //      A_1 A_2 A_3 ...
    // m=1      B_1 B_2 ...
    // m=2          C_1 ...
    //       ...,
    // then the algorithm traverses the tableau column wise:
    // A_1, A_2, B_1, A_3, B_2, C_1, ...
    // i=0 => a = [A_1], i=1 => [B_1, A_2], ...
    for i in 1..N_TAB {
        h /= R::O;
        a[i] = f(h);
        let mut b_2m = R::OO; // B^(2 m)
        for m in (0..i).rev() {
            let f0_new = (a[m + 1] * b_2m - a[m]) / (b_2m - R::ONE);
            // Compare new value to previous order, for current h and previous one.
            let new_err = R::max((f0_new - a[m]).abs(), (f0_new - a[m + 1]).abs());
            if new_err <= err {
                err = new_err;
                f_0 = f0_new;
            }
            a[m] = f0_new;
            b_2m *= R::OO;
        }
    }
    (f_0, err)
}

/// Helper trait to provide generic constants for f64 and f32
pub trait Consts {
    /// Factor the order gets multiplied in each order step
    const O: Self;
    /// `OO = O * O`
    const OO: Self;
    /// Generic constant `1`
    const ONE: Self;
    /// Generic constant `2`
    const TWO: Self;
}

impl Consts for f64 {
    const O: Self = 2.0;
    const OO: Self = Self::O * Self::O;
    const ONE: Self = 1.0;
    const TWO: Self = 2.0;
}
impl Consts for f32 {
    const O: Self = 2.0;
    const OO: Self = Self::O * Self::O;
    const ONE: Self = 1.0;
    const TWO: Self = 2.0;
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::assert_near;

    #[test]
    fn test_numdiff_differentiates_x() {
        let f = |x| x;
        let dx = 1.0;
        let x = 2.0;
        let (df, err) = numdiff_err(f, x, dx);
        assert_near!(df, 1.0, 15.0 * err);
        assert!(err < 1e-10);
    }

    #[test]
    fn test_numdiff_differentiates_1_over_x() {
        let f = |x| 1.0 / x;
        let dx = 0.5;
        let x = 1.0;
        let (df, err) = numdiff_err(f, x, dx);
        assert_near!(df, -1.0, err);
        assert!(err < 1e-10);
    }
}
