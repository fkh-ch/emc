//! Compute field lines of vector fields

use crate::{curve::C1Point, odeint};

/// Computes paths (positions and tangents) along field lines of a given 2D field
/// with singularities.
/// The singularities need to be known in advance and have to be specified as
/// `source_singularities` (outward pointing field vectors in some neighborhood) and
/// `sink_singularities` (inward pointing field vectors in some neighborhood).
/// The field lines will start at `n_lines_at_singularity` equally distributed
/// points on a circle of radius `r_singularity` around each singularity in `source_singularities`.
/// The lines will be discontinued if they either hit a circle of radius `r_singulatity` of a singularity in `sink_singularities` or if they leave the circle centered at `center` and radius `r_boundary`.
/// In a second step, at each singularity in `sink_singularities`, angular gaps bigger than
/// `2\std::f64::consts::pi/n_lines_at_singularity` will be filled by field lines in
/// inverse direction.
pub fn trace_field_lines(
    f: impl Fn(f64, [f64; 2]) -> [f64; 2],
    center: [f64; 2],
    r_boundary: f64,
    source_singularities: &[(f64, f64)],
    sink_singularities: &[(f64, f64)],
    r_singularity: f64,
    n_lines_at_singularity: usize,
) -> (Vec<Vec<C1Point>>, Vec<odeint::Error>) {
    let d_crd = point_ring(r_singularity, n_lines_at_singularity).collect::<Vec<_>>();
    let r = r_singularity;
    let rr = r * r;
    // Stop criterion: field line comes close to a singularity:
    let step_size_cutoff = |_t: f64, p: &[f64; 2], _v: &[f64; 2]| {
        t_intersect(*p, center, r_boundary, sink_singularities, r)
    };

    let mut field_lines = Vec::new();
    let mut errors = Vec::new();

    const OUTWARD: f64 = 1.0;
    const INWARD: f64 = -1.0;
    const EPS: f64 = 1.0e-8;
    let mut monitor = odeint::StepSizeMonitor::new(EPS);

    // Normalize field, so field lines will be parametrized by arclength:
    let f_normalized = |t, p: &[f64; 2], dp: &mut [f64; 2]| {
        *dp = normalized(f(t, *p));
    };
    for p0 in field_line_start_points(
        source_singularities,
        |_| d_crd.iter().cloned(),
        &f,
        OUTWARD,
        rr,
    ) {
        let (line, err) =
            trace_field_line(p0, f_normalized, OUTWARD, step_size_cutoff, &mut monitor);
        field_lines.push(line);
        if let Some(err) = err {
            errors.push(err);
        }
    }

    // Stop criterion for sink singularities:
    let step_size_cutoff_sink = |_t: f64, p: &[f64; 2], _v: &[f64; 2]| {
        t_intersect(*p, center, r_boundary, source_singularities, r)
    };

    // End points from field lines starting from source singularities:
    let end_points = field_lines
        .iter()
        .filter_map(|l| l.last())
        .map(|C1Point { pos, .. }| pos)
        .cloned()
        .collect::<Vec<_>>();

    // Start field lines from sink singularities only if there is no
    // ending field line from source singularities in a neighborhood:
    let offsets = |(s_x, s_y): (f64, f64)| {
        const BUF_FAC: f64 = 1.01;
        let mut incoming_angles = end_points
            .iter()
            .map(|[x, y]| (x - s_x, y - s_y))
            .filter(|(dx, dy)| dx * dx + dy * dy <= BUF_FAC * rr)
            .map(|(dx, dy)| f64::atan2(dy, dx))
            .collect::<Vec<_>>();
        incoming_angles.sort_by(|a, b| a.partial_cmp(b).unwrap());
        fill_angular_gaps(&incoming_angles, d_crd.len())
            .into_iter()
            .map(|phi| (r * f64::cos(phi), r * f64::sin(phi)))
    };

    for p0 in field_line_start_points(sink_singularities, offsets, &f, INWARD, rr) {
        let (line, err) = trace_field_line(
            p0,
            f_normalized,
            INWARD,
            step_size_cutoff_sink,
            &mut monitor,
        );
        field_lines.push(line);
        if let Some(err) = err {
            errors.push(err);
        }
    }
    (field_lines, errors)
}

fn point_ring(r: f64, n: usize) -> impl Iterator<Item = (f64, f64)> {
    let (sin_alpha, cos_alpha) = f64::sin_cos(2. * std::f64::consts::PI / n as f64);
    std::iter::successors(Some((r, 0.)), move |(x, y)| {
        Some((cos_alpha * x - sin_alpha * y, sin_alpha * x + cos_alpha * y))
    })
    .take(n)
}

/// Create start points for field lines around singularities.
/// Omit start points in `r`-neighborhoods of multiple singularities (`rr = r*r`).
/// Also omit start points where the field `f` is pointing inward into some
/// circle with radius `r`, centered at one of the singularities.
fn field_line_start_points<'a, I: Iterator<Item = (f64, f64)> + 'a, F: Fn((f64, f64)) -> I + 'a>(
    singularities: &'a [(f64, f64)],
    offsets: F,
    f: &'a impl Fn(f64, [f64; 2]) -> [f64; 2],
    orientation: f64,
    rr: f64,
) -> impl Iterator<Item = [f64; 2]> + 'a {
    singularities
        .iter()
        .copied()
        .enumerate()
        .flat_map(move |(i, (s_x, s_y))| {
            offsets((s_x, s_y)).filter_map(move |(zeta_x, zeta_y)| {
                let p0 = [s_x + zeta_x, s_y + zeta_y];
                (!(is_on_other_discs(p0, singularities, i, rr)
                    || dot(f(0., p0), [zeta_x, zeta_y]) * orientation < 0.))
                    .then_some(p0)
            })
        })
}

fn squared_dist_to_points([p_x, p_y]: [f64; 2], points: &[(f64, f64)]) -> f64 {
    points
        .iter()
        .map(|(s_x, s_y)| (p_x - s_x).powi(2) + (p_y - s_y).powi(2))
        .reduce(|d0, d1| if d0 < d1 { d0 } else { d1 })
        .unwrap_or(f64::INFINITY)
}

fn squared_dist_to_other_points(p: [f64; 2], points: &[(f64, f64)], idx: usize) -> f64 {
    f64::min(
        squared_dist_to_points(p, &points[..idx]),
        squared_dist_to_points(p, &points[idx + 1..]),
    )
}

fn is_on_other_discs(p: [f64; 2], points: &[(f64, f64)], idx: usize, rr: f64) -> bool {
    if points.len() <= 1 {
        return false;
    }
    squared_dist_to_other_points(p, points, idx) <= rr
}

/// An estimate for the arclength t after which the field line hits one of the circles
/// or the boundary.
/// This will be used as a step size cutoff to end field lines close to singularities.
fn t_intersect(p: [f64; 2], c: [f64; 2], r_boundary: f64, circles: &[(f64, f64)], r: f64) -> f64 {
    if (p[0] - c[0]).powi(2) + (p[1] - c[1]).powi(2) > r_boundary.powi(2) {
        return 0.;
    }
    if circles.is_empty() {
        return f64::INFINITY;
    }
    let t_circle = squared_dist_to_points(p, circles).sqrt();
    f64::max(0., t_circle - r)
}

fn dot([x1, y1]: [f64; 2], [x2, y2]: [f64; 2]) -> f64 {
    x1 * x2 + y1 * y2
}

fn normalized([x, y]: [f64; 2]) -> [f64; 2] {
    let r = f64::hypot(x, y);
    [x / r, y / r]
}

fn with_sign([x, y]: [f64; 2], sign: f64) -> [f64; 2] {
    [sign * x, sign * y]
}

fn trace_field_line(
    p0: [f64; 2],
    f: impl Fn(f64, &[f64; 2], &mut [f64; 2]),
    direction: f64,
    step_size_cutoff: impl Fn(f64, &[f64; 2], &[f64; 2]) -> f64,
    stepsize_monitor: &mut odeint::StepSizeMonitor<f64>,
) -> (Vec<C1Point>, Option<odeint::Error>) {
    const H1: f64 = 1.;
    const H_RANGE: (f64, f64) = (1.0e-9, 2.);

    let mut tau = [0.; 2];
    f(0., &p0, &mut tau);
    let tau = with_sign(normalized(tau), direction);
    let t0 = 0.;
    let mut line = vec![C1Point { pos: p0, tau }];
    for v in odeint::odeint(
        &p0,
        t0,
        H1 * direction,
        H_RANGE,
        f,
        step_size_cutoff,
        stepsize_monitor,
    ) {
        match v {
            Ok((_, pos, tau)) => line.push(C1Point {
                pos,
                tau: with_sign(tau, direction),
            }),
            Err(e) => {
                let b = line.pop();
                if let (odeint::Error::CountExcess, Some(a), Some(b)) = (&e, line.first(), b) {
                    // Remove a ton of points:
                    let dist_to_last =
                        |p: &C1Point| f64::hypot(p.pos[0] - b.pos[0], p.pos[1] - b.pos[1]);
                    let d = 0.1 * dist_to_last(a);
                    while line.last().map(|p| dist_to_last(p) < d).unwrap_or(false) {
                        line.pop();
                    }
                    line.push(b);
                }
                return (line, Some(e));
            }
        }
    }
    (line, None)
}

fn fill_angular_gaps(angles: &[f64], n: usize) -> Vec<f64> {
    let phi_min = 2. * std::f64::consts::PI / n as f64;
    const BUF_FACTOR: f64 = 1.1;
    if angles.is_empty() {
        return (0..n).map(|i| phi_min * i as f64).collect();
    }
    if let [phi] = angles {
        return (1..n).map(|i| phi + phi_min * i as f64).collect();
    }
    let mut gaps = Vec::new();
    let gaps_between_angle = |phi0: f64, phi1: f64| {
        let m = f64::floor((phi1 - phi0) * BUF_FACTOR / phi_min) as usize;
        let d_phi = (phi1 - phi0) / m as f64;
        (1..m).map(move |i| phi0 + i as f64 * d_phi)
    };
    for (phi0, phi1) in angles
        .iter()
        .copied()
        .zip(angles.iter().copied().skip(1))
        .filter(|(phi0, phi1)| phi1 - phi0 > phi_min)
    {
        gaps.extend(gaps_between_angle(phi0, phi1));
    }
    if 2. * std::f64::consts::PI + angles[0] - angles.last().unwrap() > phi_min {
        gaps.extend(gaps_between_angle(
            *angles.last().unwrap(),
            2. * std::f64::consts::PI + angles[0],
        ));
    }
    gaps
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::{assert_almost_eq, assert_vec_almost_eq};
    use testing::Matcher;

    fn to_primary_domain(angles: &[f64]) -> Vec<f64> {
        angles
            .iter()
            .copied()
            .map(|x| f64::rem_euclid(x, std::f64::consts::PI))
            .collect()
    }

    #[test]
    fn test_point_ring_works_for_3_points() {
        let mut ring = point_ring(1., 3);
        let a = ring.next().unwrap();
        assert_almost_eq!(a, (1., 0.));
        assert_almost_eq!(ring.next().unwrap(), (-0.5, f64::sqrt(3.) / 2.));
        assert_almost_eq!(ring.next().unwrap(), (-0.5, -f64::sqrt(3.) / 2.));
        assert_eq!(ring.next(), None);
    }

    #[test]
    fn test_squared_dist_to_points_works_3_points() {
        let points = &[(0., 0.), (2., 0.), (0., 3.)];
        let d = squared_dist_to_points([0.5, 0.5], points);
        assert_almost_eq!(d, 0.5);
    }

    #[test]
    fn test_squared_dist_to_other_points_works_3_points() {
        let points = &[(0., 0.), (2., 0.), (0., 3.)];
        let d = squared_dist_to_other_points([0.5, 0.5], points, 0);
        assert_almost_eq!(d, 2.5);
        let d = squared_dist_to_other_points([0.5, 0.5], points, 1);
        assert_almost_eq!(d, 0.5);
        let d = squared_dist_to_other_points([0.5, 0.5], points, 2);
        assert_almost_eq!(d, 0.5);
    }

    #[test]
    fn test_fill_angular_gaps_does_not_find_gaps_for_saturated_angles() {
        use std::f64::consts::PI;
        let d = PI / 4.;
        let angles = [0., d, 2. * d, 3. * d, 4. * d, 5. * d, 6. * d, 7. * d];
        assert!(fill_angular_gaps(&angles, 8).is_empty());
    }

    #[test]
    fn test_fill_angular_gaps_returns_full_circle_for_empty_circle() {
        use std::f64::consts::PI;
        let d = PI / 4.;
        let expected_angles = [0., d, 2. * d, 3. * d, 4. * d, 5. * d, 6. * d, 7. * d];
        assert_eq!(fill_angular_gaps(&[], 8), &expected_angles);
    }

    #[test]
    fn test_fill_angular_gaps_completes_full_circle_for_one_point() {
        use std::f64::consts::PI;
        let phi = PI / 3.;
        let angles = [PI];
        let expected_angles = [-phi, phi];
        let actual_angles = fill_angular_gaps(&angles, 3);
        assert_vec_almost_eq!(
            to_primary_domain(&actual_angles),
            to_primary_domain(&expected_angles)
        );
    }

    #[test]
    fn test_fill_angular_gaps_fills_gaps_on_sparse_circle() {
        use std::f64::consts::PI;
        let phi = PI / 6.;
        // 30°, 60°, 90°, 180°, 210°
        // delta_phi = 60°
        // -> 270°, 330°
        let angles = [phi, 2. * phi, 3. * phi, PI, PI + phi];
        let expected_angles = [4. / 3. * PI + phi, 5. / 3. * PI + phi];
        let actual_angles = fill_angular_gaps(&angles, 6);
        assert_vec_almost_eq!(
            to_primary_domain(&actual_angles),
            to_primary_domain(&expected_angles)
        );
    }

    #[test]
    fn test_fill_angular_gaps_fills_remaining_point_in_triangle() {
        use std::f64::consts::PI;
        const EPS: f64 = 1.0e-5;
        let angles = [-(PI / 3. - EPS), PI / 3. - EPS];
        let expected_angles = [PI];
        let actual_angles = fill_angular_gaps(&angles, 3);
        assert_vec_almost_eq!(
            to_primary_domain(&actual_angles),
            to_primary_domain(&expected_angles)
        );
    }

    #[derive(Debug)]
    struct Ball {
        center: [f64; 2],
        radius: f64,
    }

    impl Matcher<[f64; 2]> for Ball {
        fn matches(&self, [x, y]: [f64; 2]) -> Result<(), String> {
            if (self.center[0] - x).powi(2) + (self.center[1] - y).powi(2) <= self.radius.powi(2) {
                Ok(())
            } else {
                Err(format!(
                    "[{x},{y}] ∉ B_{}([{},{}])",
                    self.radius, self.center[0], self.center[1]
                ))
            }
        }
    }

    struct CartesianProduct<M1, M2>(M1, M2);

    impl<M1: Matcher<f64>, M2: Matcher<f64>> Matcher<[f64; 2]> for CartesianProduct<M1, M2> {
        fn matches(&self, [x, y]: [f64; 2]) -> Result<(), String> {
            match (self.0.matches(x), self.1.matches(y)) {
                (Err(msg_x), Err(msg_y)) => Err(format!("x: {msg_x}, y: {msg_y}")),
                (Err(msg_x), Ok(())) => Err(format!("x: {msg_x}")),
                (Ok(()), Err(msg_y)) => Err(format!("y: {msg_y}")),
                _ => Ok(()),
            }
        }
    }
    trait PathSelector: Sized {
        fn select(&self, path: &[[f64; 2]]) -> Result<[f64; 2], String>;
    }

    fn starts_in<M: Matcher<[f64; 2]>>(matcher: M) -> SelectMatcher<First, M> {
        SelectMatcher {
            selector: First {},
            matcher,
        }
    }
    fn ends_in<M: Matcher<[f64; 2]>>(matcher: M) -> SelectMatcher<Last, M> {
        SelectMatcher {
            selector: Last {},
            matcher,
        }
    }

    #[derive(Debug)]
    struct First {}
    #[derive(Debug)]
    struct Last {}
    impl PathSelector for First {
        fn select(&self, path: &[[f64; 2]]) -> Result<[f64; 2], String> {
            path.first()
                .cloned()
                .ok_or("No first item in an empty list".to_string())
        }
    }
    impl PathSelector for Last {
        fn select(&self, path: &[[f64; 2]]) -> Result<[f64; 2], String> {
            path.last()
                .cloned()
                .ok_or("No last item in an empty list".to_string())
        }
    }

    #[derive(Debug)]
    struct SelectMatcher<S, M> {
        selector: S,
        matcher: M,
    }

    impl<S: PathSelector, M: Matcher<[f64; 2]>> Matcher<&[[f64; 2]]> for SelectMatcher<S, M> {
        fn matches(&self, obj: &[[f64; 2]]) -> Result<(), String> {
            let p = self.selector.select(obj)?;
            self.matcher.matches(p)
        }
    }

    #[derive(Debug)]
    struct MatcherAnd<M1, M2>(M1, M2);

    impl<T: Copy, M1: Matcher<T>, M2: Matcher<T>> Matcher<T> for MatcherAnd<M1, M2> {
        fn matches(&self, obj: T) -> Result<(), String> {
            self.0.matches(obj).and_then(|_| self.1.matches(obj))
        }
    }

    impl<M1, M2, M3> std::ops::BitAnd<M3> for MatcherAnd<M1, M2> {
        type Output = MatcherAnd<MatcherAnd<M1, M2>, M3>;
        fn bitand(self, rhs: M3) -> Self::Output {
            MatcherAnd(self, rhs)
        }
    }
    impl<S, M1, M2> std::ops::BitAnd<M2> for SelectMatcher<S, M1> {
        type Output = MatcherAnd<SelectMatcher<S, M1>, M2>;
        fn bitand(self, rhs: M2) -> Self::Output {
            MatcherAnd(self, rhs)
        }
    }

    fn to_path(c1_path: impl IntoIterator<Item = C1Point>) -> Vec<[f64; 2]> {
        c1_path.into_iter().map(|C1Point { pos, .. }| pos).collect()
    }

    macro_rules! assert_contains_matching_item {
        ($container:expr, $matcher:expr) => {
            match (&$container, &$matcher) {
                (container, matcher) => {
                    assert!(
                        container.into_iter().any(|g| matcher.matches(g).is_ok()),
                        "No matching item found: {}",
                        stringify!($matcher)
                    );
                }
            };
        };
    }

    #[test]
    fn test_trace_field_lines_works_for_dipole() {
        let center = [0., 0.];
        let r_range = 5.;
        let source_singularities = &[(1., 0.)];
        let sink_singularities = &[(-1., 0.)];
        let r_singularity = 0.125;
        let n_lines_at_singularity = 3;
        // Dipole with singularities at (1,0) and (-1,0):
        let f = |_t: f64, [x, y]: [f64; 2]| -> [f64; 2] {
            let r_1 = (x - 1.).powi(2) + y * y;
            let r_2 = (x + 1.).powi(2) + y * y;
            [(x - 1.) / r_1 - (x + 1.) / r_2, y / r_1 - y / r_2]
        };
        let (field_lines, errors) = trace_field_lines(
            f,
            center,
            r_range,
            source_singularities,
            sink_singularities,
            r_singularity,
            n_lines_at_singularity,
        );
        assert!(errors.is_empty());
        // Singularity (-1,0) should have 2 incoming field lines from (1,0).
        // there should be 1 outgoing field line:
        assert!(field_lines.len() == 4);
        let field_lines = field_lines.into_iter().map(to_path).collect::<Vec<_>>();
        const TOLERANCE: f64 = 1. / 16.;
        assert_contains_matching_item!(
            field_lines,
            starts_in(Ball {
                center: [1.125, 0.],
                radius: TOLERANCE
            }) & ends_in(CartesianProduct(5.0.., -TOLERANCE..TOLERANCE))
        );

        use crate::consts::SQRT_3;
        const ZETA: (f64, f64) = (0.5, SQRT_3 / 2.);
        assert_contains_matching_item!(
            field_lines,
            starts_in(Ball {
                center: [1. - r_singularity * ZETA.0, r_singularity * ZETA.1],
                radius: TOLERANCE
            }) & ends_in(Ball {
                center: [-1. + r_singularity * ZETA.0, r_singularity * ZETA.1],
                radius: TOLERANCE
            })
        );
        assert_contains_matching_item!(
            field_lines,
            starts_in(Ball {
                center: [1. - r_singularity * ZETA.0, -r_singularity * ZETA.1],
                radius: TOLERANCE
            }) & ends_in(Ball {
                center: [-1. + r_singularity * ZETA.0, -r_singularity * ZETA.1],
                radius: TOLERANCE
            })
        );
    }
}
