//! Root finding

use super::field::Real;

const MAX_ITER: usize = 100;

/// Brent’s method
///
/// # Arguments
/// * `f` - A function returning the functions value at `x`.
/// * `(x1, x2)` - Interval containing the root.
/// * `tolerance` - Tolerance of the solution.
/// * `max_iterations` - The maximum number of iterations to execute
///
/// # Return value
/// Either the root `x` such that `f(x) == 0` (within tolerance)
/// or `None` if no root could be found.
///
/// Source:
/// R. P. Brent, Algorithms for Minimization without Derivatives, Prentice-Hall, Englewood Cliffs, New Jersey, 1973. ISBN 0-13-022335-2,
/// Chapter 4: An Algorithm with Guaranteed Convergence for Finding a Zero of a Function
/// Available at: <https://maths-people.anu.edu.au/~brent/pub/pub011.html>
pub fn find_bracketed_zero<T: Real>(
    f: impl Fn(T) -> T,
    (x1, x2): (T, T),
    tolerance: T,
) -> Option<T> {
    let (mut f_a, mut f_b) = (f(x1), f(x2));
    if (f_a > T::ZERO && f_b > T::ZERO) || (f_a < T::ZERO && f_b < T::ZERO) {
        // root must be bracketed
        return None;
    }
    let (mut a, mut b, mut c) = (x1, x2, x2);
    let mut f_c = f_b;
    let mut d;
    let mut e = T::ZERO;
    for _ in 0..MAX_ITER {
        if (f_b > T::ZERO) == (f_c > T::ZERO) {
            c = a;
            f_c = f_a;
            d = b - a;
            e = d;
        }
        if T::abs(f_c) < T::abs(f_b) {
            a = b;
            b = c;
            c = a;
            f_a = f_b;
            f_b = f_c;
            f_c = f_a;
        }
        let tol = T::from(2) * T::EPSILON * T::abs(b) + tolerance / T::from(2);
        let m = (c - b) / T::from(2);
        if T::abs(m) <= tol || f_b == T::ZERO {
            // Convergence
            return Some(b);
        }
        // See if a bisection is forced:
        if T::abs(e) < tol || T::abs(f_a) <= T::abs(f_b) {
            d = m;
            e = m;
        } else {
            let s = f_b / f_a;
            // Attempt inverse quadratic interpolation.
            let (mut p, mut q): (T, T);
            if a == c {
                // Linear interpolation
                p = T::from(2) * m * s;
                q = T::from(1) - s;
            } else {
                // Inverse quadratic interpolation.
                q = f_a / f_c;
                let r = f_b / f_c;
                p = s * (T::from(2) * m * q * (q - r) - (b - a) * (r - T::from(1)));
                q = (q - T::from(1)) * (r - T::from(1)) * (s - T::from(1));
            }
            if p > T::ZERO {
                q = -q;
            } else {
                p = -p;
            }
            if T::from(2) * p < T::from(3) * m * q - T::abs(tol * q)
                && p < T::abs(e * q) / T::from(2)
            {
                d = p / q;
                e = d;
            } else {
                d = m;
                e = m;
            }
        }
        a = b;
        f_a = f_b;
        if T::abs(d) > tol {
            b += d;
        } else {
            b += tol.copysign(m);
        }
        f_b = f(b);
    }
    // Maximum number of iterations reached
    None
}

/// Newtons method
///
/// # Arguments
/// * `f` - A function returning both the function value and its first derivative at `x`.
/// * `(x1, x2)` - Interval containing the root.
/// * `tolerance` - Tolerance of the solution.
/// * `max_iterations` - The maximum number of iterations to execute
///
/// # Return value
/// Either the root `x` such that `f(x) == (0, _)` (within tolerance)
/// or `None` if no root could be found.
pub fn newtons_method<T: Real>(
    f: impl Fn(T) -> (T, T),
    (x1, x2): (T, T),
    tolerance: T,
) -> Option<T> {
    let ((y1, _), (y2, _)) = (f(x1), f(x2));
    if (y1 > T::ZERO) == (y2 > T::ZERO) {
        // root must be bracketed
        return None;
    }
    let mut x = (x1 + x2) / T::from(2);

    for _ in 0..MAX_ITER {
        let (y, dy) = f(x);
        let dx = y / dy;
        x -= dx;
        if x < x1 || x > x2 {
            // Out of bracket
            return None;
        }
        if T::abs(dx) <= tolerance / T::from(2) {
            return Some(x);
        }
    }
    None
}

#[cfg(test)]
mod test {
    use crate::assert_near;

    #[test]
    fn test_find_bracketed_zero_works() {
        let f = |x: f64| (x - 1.) * (x - 1.) * (x - 1.);
        let x0 = super::find_bracketed_zero(f, (0.5, 1.3), 1.0e-10).unwrap();

        assert_near!(x0, 1., 1.0e-10);
    }

    #[test]
    fn test_newtons_method_works() {
        let f = |x: f64| ((x - 1.) * (x - 1.) * (x - 1.), 3. * (x - 1.) * (x - 1.));
        let x0 = super::newtons_method(f, (0.5, 1.3), 1.0e-10).unwrap();

        assert_near!(x0, 1., 1.0e-10);
    }
}
