//! Numerical constants

/// √3
pub const SQRT_3: f64 = 1.7320508075688772;
