//! Abstraction layer for real numbers / algebraic fields

/// Abstraction of the minimum function as in [f64::min]
pub trait Min {
    /// Determine the minimum of 2 numbers
    fn min(self, other: Self) -> Self;
}
impl Min for f64 {
    fn min(self, other: Self) -> Self {
        f64::min(self, other)
    }
}
impl Min for f32 {
    fn min(self, other: Self) -> Self {
        f32::min(self, other)
    }
}

/// Abstraction of the maximum function as in [f64::max]
pub trait Max {
    /// Determine the maximum of 2 numbers
    fn max(self, other: Self) -> Self;
}
impl Max for f64 {
    fn max(self, other: Self) -> Self {
        f64::max(self, other)
    }
}
impl Max for f32 {
    fn max(self, other: Self) -> Self {
        f32::max(self, other)
    }
}

/// Abstraction of the power operation as in [f64::powf]
pub trait Powf {
    /// Raise `self` to the power of `exp`
    fn powf(self, exp: Self) -> Self;
}
impl Powf for f64 {
    fn powf(self, exp: Self) -> Self {
        f64::powf(self, exp)
    }
}
impl Powf for f32 {
    fn powf(self, exp: Self) -> Self {
        f32::powf(self, exp)
    }
}

/// Abstraction of testing if a number is `NAN` as in [f64::is_nan]
pub trait IsNan {
    #[allow(clippy::wrong_self_convention)]
    /// Determine if a number is `NAN`
    fn is_nan(self) -> bool;
}
impl IsNan for f64 {
    fn is_nan(self) -> bool {
        f64::is_nan(self)
    }
}
impl IsNan for f32 {
    fn is_nan(self) -> bool {
        f32::is_nan(self)
    }
}

/// Abstraction of the absolute value function as in [f64::abs]
pub trait Abs {
    /// Type returned by [Self::abs]
    type Output;
    /// Compute the absolute value of a number
    fn abs(self) -> Self::Output;
}
impl Abs for f64 {
    type Output = Self;
    fn abs(self) -> Self::Output {
        f64::abs(self)
    }
}
impl Abs for f32 {
    type Output = Self;
    fn abs(self) -> Self::Output {
        f32::abs(self)
    }
}

/// Abstraction of the `copysign` function as in [f64::copysign]
pub trait Copysign {
    /// Copy the sign from `sign` to `self`
    fn copysign(self, sign: Self) -> Self;
}
impl Copysign for f64 {
    fn copysign(self, sign: Self) -> Self {
        f64::copysign(self, sign)
    }
}
impl Copysign for f32 {
    fn copysign(self, sign: Self) -> Self {
        f32::copysign(self, sign)
    }
}

/// Abstraction of the machine epsilon constant as in [f64::EPSILON]
pub trait ConstEpsilon {
    /// Machine epsilon
    const EPSILON: Self;
}
impl ConstEpsilon for f64 {
    const EPSILON: Self = f64::EPSILON;
}
impl ConstEpsilon for f32 {
    const EPSILON: Self = f32::EPSILON;
}

/// Abstraction of the `MIN` constant as in [f64::MIN]
pub trait ConstMin {
    /// Minimal representable number
    const MIN: Self;
}
impl ConstMin for f64 {
    const MIN: Self = f64::MIN;
}
impl ConstMin for f32 {
    const MIN: Self = f32::MIN;
}

/// Abstraction of the `MAX` constant as in [f64::MAX]
pub trait ConstMax {
    /// Maximal representable number
    const MAX: Self;
}
impl ConstMax for f64 {
    const MAX: Self = f64::MAX;
}
impl ConstMax for f32 {
    const MAX: Self = f32::MAX;
}

/// Abstraction of the `0` constant
pub trait ConstZero {
    /// `0` as a generic constant
    const ZERO: Self;
}
impl ConstZero for f64 {
    const ZERO: Self = 0.;
}
impl ConstZero for f32 {
    const ZERO: Self = 0.;
}

/// Abstraction of the `NAN` constant
pub trait ConstNAN {
    /// `NAN` as a generic constant
    const NAN: Self;
}
impl ConstNAN for f64 {
    const NAN: Self = Self::NAN;
}
impl ConstNAN for f32 {
    const NAN: Self = Self::NAN;
}

/// Generalization of [f64] and [f32] floating point numbers as well as complex numbers
pub trait Field:
    PartialEq
    + Sized
    + 'static
    + Copy
    + ConstZero
    + ConstNAN
    + std::fmt::Display
    + std::fmt::Debug
    + std::ops::Neg<Output = Self>
    + std::ops::Add<Self, Output = Self>
    + std::ops::AddAssign<Self>
    + std::ops::Sub<Self, Output = Self>
    + std::ops::SubAssign<Self>
    + std::ops::Mul<Self, Output = Self>
    + std::ops::MulAssign<Self>
    + std::ops::Div<Self, Output = Self>
    + std::ops::DivAssign<Self>
    + std::ops::Add<<Self as Abs>::Output, Output = Self>
    + std::ops::AddAssign<<Self as Abs>::Output>
    + std::ops::Sub<<Self as Abs>::Output, Output = Self>
    + std::ops::SubAssign<<Self as Abs>::Output>
    + std::ops::Mul<<Self as Abs>::Output, Output = Self>
    + std::ops::MulAssign<<Self as Abs>::Output>
    + std::ops::Div<<Self as Abs>::Output, Output = Self>
    + std::ops::DivAssign<<Self as Abs>::Output>
    + IsNan
    + Abs
{
}

impl Field for f64 {}

impl Field for f32 {}

/// Generalization of f64 and f32 floating real point numbers
pub trait Real:
    Field
    + Abs<Output = Self>
    + PartialOrd
    + Min
    + Max
    + Powf
    + Copysign
    + ConstEpsilon
    + ConstMin
    + ConstMax
    + From<u16>
{
}

impl Real for f64 {}
impl Real for f32 {}
