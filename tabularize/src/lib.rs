//! Structures and traits to transform data to text based tables.
//!
//! The API is inspired by serde with a [Tabularizer] trait for formats
//! and a [Tabularize] trait for data.
#![warn(missing_docs)]

/// Abstraction of a table format, such as [CsvWriter]
pub trait Tabularizer {
    /// Error possibly occurring when writing using the tabularizer
    type Error;

    /// Start a new row
    fn start_row(&mut self) -> impl RowTabularizer<Error = Self::Error>;

    /// End a row
    fn end_row(&mut self) -> Result<(), Self::Error>;

    /// Start a file.
    ///
    /// Specific implementations can write some initial data like a
    /// byte order mark.
    fn start_file(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }

    /// High level table writer including header and body
    fn tabularize(&mut self, data: impl TabularizeWholeTable) -> Result<(), Self::Error>
    where
        Self: Sized,
    {
        self.start_file()?;
        data.tabularize_whole_table(self)
    }
}

/// Tabularize data including header and body
pub trait TabularizeWholeTable {
    /// Tabularize data including header and body using a [Tabularizer] `tabularizer`
    fn tabularize_whole_table<S: Tabularizer>(self, tabularizer: &mut S) -> Result<(), S::Error>;
}

impl<T: Tabularize> TabularizeWholeTable for T {
    fn tabularize_whole_table<S: Tabularizer>(self, tabularizer: &mut S) -> Result<(), S::Error> {
        self.header().tabularize_row(tabularizer.start_row())?;
        tabularizer.end_row()?;
        for row in self.into_iter() {
            row.tabularize_row(tabularizer.start_row())?;
            tabularizer.end_row()?;
        }
        Ok(())
    }
}

/// Tabularize a single row.
///
/// This is used by the [Tabularizer] trait, when starting a new row.
pub trait RowTabularizer: Sized {
    /// Error possibly occurring when writing using the tabularizer
    type Error;
    /// Tabularize a single value.
    fn tabularize_value<T: std::fmt::Display>(self, value: T) -> Result<(), Self::Error>;
    /// Tabularize a left and a right part of a row.
    fn tabularize_split<L: TabularizeRow, R: TabularizeRow>(
        self,
        left: L,
        right: R,
    ) -> Result<(), Self::Error>;
    /// Tabularize multiple values.
    fn tabularize_array(
        self,
        value: impl IntoIterator<Item: TabularizeRow>,
    ) -> Result<(), Self::Error>;
}

/// Prepare a table header
pub trait Header {
    /// Type of the header
    type Header: TabularizeRow;
    /// Prepare the header of a table
    fn header(&self) -> Self::Header;
}

pub use join_variant::{Either, JoinVariant};

/// Split data into rows and columns.
pub trait Tabularize: Header + IntoIterator<Item: TabularizeRow> {
    /// Join the table with another table.
    fn join<T: Tabularize>(self, other: T) -> Join<Self, T>
    where
        Self: Sized,
    {
        Join(self, other)
    }

    /// Join the table with `other`, where `other` can be
    /// an [std::option::Option] or an instance of (nesting possible) [Either].
    ///
    /// The implementation will first dispatch to a static table only
    /// containing the tables unwrapped tables contained in
    /// [Either] / [std::option::Option] and in a 2nd step perform the
    /// tabularization without any runtime branching.
    fn join_variant<T: join_variant::VisitProcessor>(self, other: T) -> JoinVariant<Self, T>
    where
        Self: Sized,
    {
        join_variant::JoinVariant(self, other)
    }
}

/// Prepare a row from a data structure.
///
/// Used by the [Tabularize] trait when a new row is being processed.
pub trait TabularizeRow {
    /// Feed a [RowTabularizer] with values forming a row.
    fn tabularize_row<S>(self, tabularizer: S) -> Result<(), S::Error>
    where
        S: RowTabularizer;
}

impl<T: Header + IntoIterator<Item: TabularizeRow>> Tabularize for T {}

/// 2 joined tables.
#[derive(Debug)]
pub struct Join<L, R>(L, R);
/// 2 concatenated rows or headers.
#[derive(Debug)]
pub struct Concat<L, R>(pub L, pub R);

impl<T: std::fmt::Display> TabularizeRow for T {
    fn tabularize_row<S: RowTabularizer>(self, tabularizer: S) -> Result<(), S::Error> {
        tabularizer.tabularize_value(self)
    }
}

impl<L: Header, R: Header> Header for Join<L, R> {
    type Header = Concat<L::Header, R::Header>;
    fn header(&self) -> Self::Header {
        Concat(self.0.header(), self.1.header())
    }
}

impl<L: IntoIterator, R: IntoIterator> IntoIterator for Join<L, R> {
    type IntoIter = std::iter::Map<
        std::iter::Zip<L::IntoIter, R::IntoIter>,
        fn((L::Item, R::Item)) -> Concat<L::Item, R::Item>,
    >;
    type Item = Concat<L::Item, R::Item>;
    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter().zip(self.1).map(|(a, b)| Concat(a, b))
    }
}

impl<L: TabularizeRow, R: TabularizeRow> TabularizeRow for Concat<L, R> {
    fn tabularize_row<S>(self, tabularizer: S) -> Result<(), S::Error>
    where
        S: RowTabularizer,
    {
        tabularizer.tabularize_split(self.0, self.1)
    }
}

impl<L, R> TabularizeRow for &Concat<L, R>
where
    L: TabularizeRow + Copy,
    R: TabularizeRow + Copy,
{
    fn tabularize_row<S>(self, tabularizer: S) -> Result<(), S::Error>
    where
        S: RowTabularizer,
    {
        tabularizer.tabularize_split(self.0, self.1)
    }
}

/// Single column definition of a [RecordTable]
///
/// A [SchemaColumn] consists of a label and a rule on how to obtain
/// the value from a data structure.
/// Multiple columns can be combined with tuples.
#[derive(Debug)]
pub struct SchemaColumn<'a, F> {
    /// Column label
    pub label: &'a str,
    /// Extract the data belonging to the column from the record
    pub access: F,
}

impl<'a, F> SchemaColumn<'a, F> {
    /// Create a new [SchemaColumn]
    pub const fn new(label: &'a str, access: F) -> Self {
        Self { label, access }
    }
}

/// Table consisting of rows obtained from a data structure each.
///
/// The header is usually a tuple of [SchemaColumn] instances and
/// defines the rule on how to obtain a row from an instance of the data
/// structure.
///
/// Typical usecase: convert an iterable of structs into a table where
/// each column corresponds to a field of the struct.
#[derive(Debug)]
pub struct RecordTable<H, I> {
    /// Header of the table
    pub header: H,
    /// body of the table
    pub body: I,
}

impl<H, I> RecordTable<H, I> {
    /// Create a new [RecordTable] with header and body
    pub const fn new(header: H, body: I) -> Self {
        Self { header, body }
    }
}

/// Label for a column of a [RecordTable]
#[derive(Debug)]
pub struct Label<S>(S);

impl<T: std::fmt::Display> TabularizeRow for Label<T> {
    fn tabularize_row<S>(self, tabularizer: S) -> Result<(), S::Error>
    where
        S: RowTabularizer,
    {
        tabularizer.tabularize_value(self.0)
    }
}

impl<'a, F> Header for SchemaColumn<'a, F> {
    type Header = Label<&'a str>;
    fn header(&self) -> Self::Header {
        Label(self.label)
    }
}

impl<H, I> Header for RecordTable<H, I>
where
    H: Header,
{
    type Header = H::Header;
    fn header(&self) -> Self::Header {
        self.header.header()
    }
}

impl<H1: Header, H2: Header> Header for (H1, H2) {
    type Header = Concat<H1::Header, H2::Header>;
    fn header(&self) -> Self::Header {
        Concat(self.0.header(), self.1.header())
    }
}
impl<H1: Header, H2: Header, H3: Header> Header for (H1, H2, H3) {
    type Header = Concat<Concat<H1::Header, H2::Header>, H3::Header>;
    fn header(&self) -> Self::Header {
        Concat(Concat(self.0.header(), self.1.header()), self.2.header())
    }
}
impl<H1: Header, H2: Header, H3: Header, H4: Header> Header for (H1, H2, H3, H4) {
    type Header = Concat<Concat<H1::Header, H2::Header>, Concat<H3::Header, H4::Header>>;
    fn header(&self) -> Self::Header {
        Concat(
            Concat(self.0.header(), self.1.header()),
            Concat(self.2.header(), self.3.header()),
        )
    }
}

/// Build a record from a data structure which can be tabularized as a row.
///
/// Used by [RecordTable] to obtain rows from data.
pub trait IntoRecord<Schema> {
    /// Record type
    ///
    /// Usually a single value which implements [std::fmt::Display] or
    /// multiple values concatenated with [Concat].
    type Record;
    /// Convert to a record using a schema
    fn into_record(self, schema: &Schema) -> Self::Record;
}

impl<'a, F, T, C> IntoRecord<SchemaColumn<'a, F>> for T
where
    F: Fn(T) -> C,
{
    type Record = C;
    fn into_record(self, schema: &SchemaColumn<'a, F>) -> Self::Record {
        (schema.access)(self)
    }
}

impl<L, R, T> IntoRecord<(L, R)> for T
where
    Self: IntoRecord<L> + IntoRecord<R> + Copy,
{
    type Record = Concat<<Self as IntoRecord<L>>::Record, <Self as IntoRecord<R>>::Record>;
    fn into_record(self, schema: &(L, R)) -> Self::Record {
        Concat(
            <Self as IntoRecord<L>>::into_record(self, &schema.0),
            <Self as IntoRecord<R>>::into_record(self, &schema.1),
        )
    }
}
impl<R1, R2, R3, T> IntoRecord<(R1, R2, R3)> for T
where
    Self: IntoRecord<R1> + IntoRecord<R2> + IntoRecord<R3> + Copy,
{
    type Record = Concat<
        Concat<<Self as IntoRecord<R1>>::Record, <Self as IntoRecord<R2>>::Record>,
        <Self as IntoRecord<R3>>::Record,
    >;
    fn into_record(self, schema: &(R1, R2, R3)) -> Self::Record {
        Concat(
            Concat(
                <Self as IntoRecord<R1>>::into_record(self, &schema.0),
                <Self as IntoRecord<R2>>::into_record(self, &schema.1),
            ),
            <Self as IntoRecord<R3>>::into_record(self, &schema.2),
        )
    }
}
impl<R1, R2, R3, R4, T> IntoRecord<(R1, R2, R3, R4)> for T
where
    Self: IntoRecord<R1> + IntoRecord<R2> + IntoRecord<R3> + IntoRecord<R4> + Copy,
{
    type Record = Concat<
        Concat<<Self as IntoRecord<R1>>::Record, <Self as IntoRecord<R2>>::Record>,
        Concat<<Self as IntoRecord<R3>>::Record, <Self as IntoRecord<R4>>::Record>,
    >;
    fn into_record(self, schema: &(R1, R2, R3, R4)) -> Self::Record {
        Concat(
            Concat(
                <Self as IntoRecord<R1>>::into_record(self, &schema.0),
                <Self as IntoRecord<R2>>::into_record(self, &schema.1),
            ),
            Concat(
                <Self as IntoRecord<R3>>::into_record(self, &schema.2),
                <Self as IntoRecord<R4>>::into_record(self, &schema.3),
            ),
        )
    }
}

impl<H, D> IntoIterator for RecordTable<H, D>
where
    D: IntoIterator,
    D::Item: IntoRecord<H>,
{
    type Item = <D::Item as IntoRecord<H>>::Record;
    type IntoIter = RecordTableRows<D::IntoIter, H>;
    fn into_iter(self) -> Self::IntoIter {
        RecordTableRows {
            rows: self.body.into_iter(),
            schema: self.header,
        }
    }
}

/// Iterator over rows of a [RecordTable].
#[derive(Debug)]
pub struct RecordTableRows<I, H> {
    rows: I,
    schema: H,
}

impl<I, H> Iterator for RecordTableRows<I, H>
where
    I: Iterator,
    I::Item: IntoRecord<H>,
{
    type Item = <I::Item as IntoRecord<H>>::Record;
    fn next(&mut self) -> Option<Self::Item> {
        self.rows.next().map(|row| row.into_record(&self.schema))
    }
}

/// Array of contiguous homogeneous data as a table
#[derive(Debug)]
pub struct ArrayTable<H, T> {
    /// Header of the table, determining the shape of the data
    pub header: H,
    /// Contiguous data
    pub body: T,
}

impl<'a, S, D> Header for ArrayTable<&'a [S], D>
where
    for<'b> &'b S: TabularizeRow,
{
    type Header = SliceRow<'a, S>;
    fn header(&self) -> Self::Header {
        SliceRow(self.header)
    }
}

/// Row of an [ArrayTable] for row major data.
#[derive(Debug)]
pub struct SliceRow<'a, T>(&'a [T]);

impl<T> TabularizeRow for SliceRow<'_, T>
where
    for<'a> &'a T: TabularizeRow,
{
    fn tabularize_row<S>(self, tabularizer: S) -> Result<(), S::Error>
    where
        S: RowTabularizer,
    {
        tabularizer.tabularize_array(self.0)
    }
}

impl<'a, S, T> IntoIterator for ArrayTable<&'a [S], &'a [T]> {
    type Item = SliceRow<'a, T>;
    type IntoIter = std::iter::Map<std::slice::ChunksExact<'a, T>, fn(&'a [T]) -> SliceRow<'a, T>>;
    fn into_iter(self) -> Self::IntoIter {
        self.body.chunks_exact(self.header.len()).map(SliceRow)
    }
}

/// Row of an [ArrayTable] for column major data.
#[derive(Debug)]
pub struct SliceColumn<'a, T>(std::iter::StepBy<std::slice::Iter<'a, T>>);

/// Treat contiguous data as column major when used with [ArrayTable].
#[derive(Debug)]
pub struct Transposed<T>(pub T);

impl<T> TabularizeRow for SliceColumn<'_, T>
where
    for<'a> &'a T: TabularizeRow,
{
    fn tabularize_row<S>(self, tabularizer: S) -> Result<(), S::Error>
    where
        S: RowTabularizer,
    {
        tabularizer.tabularize_array(self.0)
    }
}

/// Iterator over columns of row major data.
#[derive(Debug)]
pub struct SliceColumns<'a, T> {
    data: &'a [T],
    row_len: usize,
    col: usize,
}

impl<'a, T> Iterator for SliceColumns<'a, T> {
    type Item = SliceColumn<'a, T>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.col >= self.row_len {
            return None;
        }
        let slc = &self.data[self.col..];
        self.col += 1;
        Some(SliceColumn(slc.iter().step_by(self.row_len)))
    }
}

impl<'a, S, T> IntoIterator for ArrayTable<&'a [S], Transposed<&'a [T]>> {
    type Item = SliceColumn<'a, T>;
    type IntoIter = SliceColumns<'a, T>;
    fn into_iter(self) -> Self::IntoIter {
        SliceColumns {
            data: self.body.0,
            row_len: self.header.len(),
            col: 0,
        }
    }
}

/// Single column table
#[derive(Debug)]
pub struct Column<S, I> {
    /// Label of the column
    pub label: S,
    /// values of the column
    pub values: I,
}

impl<S, I> Column<S, I> {
    /// Create a new column with label and values
    pub const fn new(label: S, values: I) -> Self {
        Self { label, values }
    }
}

impl<'a, I> Header for Column<&'a str, I> {
    type Header = &'a str;
    fn header(&self) -> Self::Header {
        self.label
    }
}

impl<S, I> IntoIterator for Column<S, I>
where
    I: IntoIterator,
{
    type Item = I::Item;
    type IntoIter = I::IntoIter;
    fn into_iter(self) -> Self::IntoIter {
        self.values.into_iter()
    }
}

/// A [Tabularizer] writing in csv format.
#[derive(Debug)]
pub struct CsvWriter<W: std::io::Write> {
    writer: W,
    separator: char,
}

impl<W: std::io::Write> CsvWriter<W> {
    /// Create a new [CsvWriter] writing to `writer`
    pub fn new(writer: W) -> Self {
        Self {
            writer,
            separator: ',',
        }
    }
    /// Set the cell speparator character.
    pub fn with_separator(self, separator: char) -> Self {
        Self { separator, ..self }
    }
}

/// Row writer for [CsvWriter]
#[derive(Debug)]
pub struct CsvRowWriter<'a, W: std::io::Write> {
    writer: &'a mut CsvWriter<W>,
}

impl<W: std::io::Write> RowTabularizer for CsvRowWriter<'_, W> {
    type Error = std::io::Error;
    fn tabularize_value<T: std::fmt::Display>(self, value: T) -> Result<(), Self::Error> {
        write!(self.writer.writer, "{value}")
    }
    fn tabularize_split<L: TabularizeRow, R: TabularizeRow>(
        self,
        left: L,
        right: R,
    ) -> Result<(), Self::Error> {
        let CsvRowWriter { writer, .. } = self;
        left.tabularize_row(CsvRowWriter { writer })?;
        write!(writer.writer, "{}", writer.separator)?;
        right.tabularize_row(CsvRowWriter { writer })?;
        Ok(())
    }
    fn tabularize_array(
        self,
        value: impl IntoIterator<Item: TabularizeRow>,
    ) -> Result<(), Self::Error> {
        let mut values = value.into_iter();
        let CsvRowWriter { writer, .. } = self;
        if let Some(val) = values.next() {
            val.tabularize_row(CsvRowWriter { writer })?;
        } else {
            return Ok(());
        }
        for val in values {
            write!(writer.writer, "{}", writer.separator)?;
            val.tabularize_row(CsvRowWriter { writer })?;
        }
        Ok(())
    }
}

impl<W: std::io::Write> Tabularizer for CsvWriter<W> {
    type Error = std::io::Error;
    fn start_row(&mut self) -> impl RowTabularizer<Error = Self::Error> {
        CsvRowWriter { writer: self }
    }
    fn end_row(&mut self) -> Result<(), Self::Error> {
        writeln!(self.writer)
    }

    /// See: <https://en.wikipedia.org/wiki/Byte_order_mark#UTF-8>
    #[cfg(target_os = "windows")]
    fn start_file(&mut self) -> Result<(), Self::Error> {
        const BYTE_ORDER_MARK: &[u8] = b"\xEF\xBB\xBF";
        self.writer.write_all(BYTE_ORDER_MARK)
    }
}

/// Support for joining variants ([std::option::Option] or enums).
mod join_variant {
    use super::{Tabularize, TabularizeWholeTable, Tabularizer};

    /// Table joined with an optional other table
    pub struct JoinVariant<L, R>(pub L, pub R);

    pub trait ProcessTable {
        type Error;
        fn process_table(self, data: impl Tabularize) -> Result<(), Self::Error>;
    }

    impl<T: Tabularizer> ProcessTable for &mut T {
        type Error = T::Error;
        fn process_table(self, data: impl Tabularize) -> Result<(), Self::Error> {
            self.tabularize(data)
        }
    }

    pub trait VisitProcessor {
        fn visit_processor<P: ProcessTable>(
            self,
            tab: impl Tabularize,
            processor: P,
        ) -> Result<(), P::Error>;
    }

    impl<T: Tabularize> VisitProcessor for T {
        fn visit_processor<P: ProcessTable>(
            self,
            tab: impl Tabularize,
            processor: P,
        ) -> Result<(), P::Error> {
            processor.process_table(tab.join(self))
        }
    }

    impl<T: VisitProcessor> VisitProcessor for Option<T> {
        fn visit_processor<P: ProcessTable>(
            self,
            tab: impl Tabularize,
            processor: P,
        ) -> Result<(), P::Error> {
            if let Some(other_tab) = self {
                other_tab.visit_processor(tab, processor)
            } else {
                processor.process_table(tab)
            }
        }
    }

    /// Either one of 2 types
    #[derive(Debug)]
    pub enum Either<A, B> {
        /// 1. variant
        Either(A),
        /// 2. variant
        Or(B),
    }

    impl<A: VisitProcessor, B: VisitProcessor> VisitProcessor for Either<A, B> {
        fn visit_processor<P: ProcessTable>(
            self,
            tab: impl Tabularize,
            processor: P,
        ) -> Result<(), P::Error> {
            match self {
                Self::Either(other_tab) => other_tab.visit_processor(tab, processor),
                Self::Or(other_tab) => other_tab.visit_processor(tab, processor),
            }
        }
    }

    pub struct JoinVariantProcessor<P, V>(P, V);

    impl<P: ProcessTable, V: VisitProcessor> ProcessTable for JoinVariantProcessor<P, V> {
        type Error = P::Error;
        fn process_table(self, data: impl Tabularize) -> Result<(), Self::Error> {
            self.1.visit_processor(data, self.0)
        }
    }

    pub trait ChainProcessor {
        fn chain_processor<S: ProcessTable>(self, other: S) -> impl ProcessTable<Error = S::Error>;
    }

    impl<V: VisitProcessor> ChainProcessor for V {
        fn chain_processor<S: ProcessTable>(self, other: S) -> impl ProcessTable<Error = S::Error> {
            JoinVariantProcessor(other, self)
        }
    }

    impl<L: ChainProcessor, V: VisitProcessor> ChainProcessor for JoinVariant<L, V> {
        fn chain_processor<S: ProcessTable>(self, other: S) -> impl ProcessTable<Error = S::Error> {
            self.0.chain_processor(JoinVariantProcessor(other, self.1))
        }
    }

    impl<T: Tabularize, P: ChainProcessor> TabularizeWholeTable for JoinVariant<T, P> {
        fn tabularize_whole_table<S: Tabularizer>(
            self,
            tabularizer: &mut S,
        ) -> Result<(), S::Error> {
            self.1.chain_processor(tabularizer).process_table(self.0)
        }
    }

    impl<T: Tabularize, P> JoinVariant<T, P> {
        /// Join the table with `other`, where `other` can be
        /// an [std::option::Option] or an instance of (nesting possible) [Either].
        ///
        /// The implementation will first dispatch to a static table only
        /// containing the tables unwrapped tables contained in
        /// [Either] / [std::option::Option] and in a 2nd step perform the
        /// tabularization without any runtime branching.
        pub fn join_variant<V: VisitProcessor>(
            self,
            other: V,
        ) -> JoinVariant<T, JoinVariant<P, V>> {
            JoinVariant(self.0, JoinVariant(self.1, other))
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn dump_csv_string<T: TabularizeWholeTable>(tab: T) -> Result<String, std::io::Error> {
        let mut buf = Vec::new();
        let mut csv_writer = CsvWriter::new(&mut buf);
        csv_writer.tabularize(tab)?;
        Ok(std::str::from_utf8(buf.as_slice())
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))?
            .to_string())
    }

    #[test]
    fn test_csv_writer_writes_record_table() {
        struct Rec {
            a: f64,
            b: bool,
        }
        const COLUMNS: (
            SchemaColumn<fn(&Rec) -> f64>,
            SchemaColumn<fn(&Rec) -> bool>,
        ) = (
            SchemaColumn {
                label: "A",
                access: |rec| rec.a,
            },
            SchemaColumn {
                label: "B",
                access: |rec| rec.b,
            },
        );
        let tab: [Rec; 2] = [Rec { a: 1.0, b: false }, Rec { a: 2.0, b: true }];
        let csv_dump = dump_csv_string(RecordTable {
            body: &tab,
            header: COLUMNS,
        })
        .unwrap();

        assert_eq!(
            csv_dump,
            r#"A,B
1,false
2,true
"#
        );
    }

    #[test]
    fn test_csv_writer_writes_array_table() {
        let tab: &[f64] = &[1.0, 2.0, 3.0, 4.0];
        let header: &[&str] = &["A", "B"];
        let csv_dump = dump_csv_string(ArrayTable { body: tab, header }).unwrap();

        assert_eq!(
            csv_dump,
            r#"A,B
1,2
3,4
"#
        );
    }

    #[test]
    fn test_csv_writer_writes_array_table_with_structured_values() {
        struct Value {
            a: f64,
            b: bool,
        }
        impl TabularizeRow for &Value {
            fn tabularize_row<S>(self, tabularizer: S) -> Result<(), S::Error>
            where
                S: RowTabularizer,
            {
                tabularizer.tabularize_split(self.a, self.b)
            }
        }

        let tab = ArrayTable {
            header: &[Concat("a1", "b1"), Concat("a2", "b2")] as &[_],
            body: &[
                Value { a: 1.0, b: true },
                Value { a: 2.0, b: false },
                Value { a: 3.0, b: false },
                Value { a: 4.0, b: true },
            ] as &[_],
        };
        let csv_dump = dump_csv_string(tab).unwrap();

        assert_eq!(
            csv_dump,
            r#"a1,b1,a2,b2
1,true,2,false
3,false,4,true
"#
        );
    }

    #[test]
    fn test_csv_writer_writes_transposed_array_table() {
        let tab: &[f64] = &[1.0, 2.0, 3.0, 4.0];
        let header: &[&str] = &["A", "B"];
        let csv_dump = dump_csv_string(ArrayTable {
            body: Transposed(tab),
            header,
        })
        .unwrap();

        assert_eq!(
            csv_dump,
            r#"A,B
1,3
2,4
"#
        );
    }

    #[test]
    fn test_csv_writer_writes_column() {
        let tab = Column {
            label: "A",
            values: 2..4,
        };
        let csv_dump = dump_csv_string(tab).unwrap();

        assert_eq!(
            csv_dump,
            r#"A
2
3
"#
        );
    }

    struct A(f64);
    struct B(i32);

    const A_COLUMN: SchemaColumn<fn(&A) -> &f64> = SchemaColumn {
        label: "A",
        access: |rec| &rec.0,
    };
    const B_COLUMN: SchemaColumn<fn(&B) -> &i32> = SchemaColumn {
        label: "B",
        access: |rec| &rec.0,
    };

    #[test]
    fn test_write_csv_writes_2_joined_columns() {
        let col_a = &[A(1.), A(2.), A(3.)];
        let col_b = &[B(-1), B(-2), B(-3)];
        let multi_col = RecordTable {
            body: col_a,
            header: A_COLUMN,
        }
        .join(RecordTable {
            body: col_b,
            header: B_COLUMN,
        });
        let csv_dump = dump_csv_string(multi_col).unwrap();
        assert_eq!(
            csv_dump,
            r#"A,B
1,-1
2,-2
3,-3
"#
        );
    }

    #[test]
    fn test_write_csv_writes_3_conditionally_joined_columns() {
        struct C;
        struct D(bool);
        const D_COLUMN: SchemaColumn<fn(&D) -> bool> = SchemaColumn {
            label: "D",
            access: |rec| rec.0,
        };

        let col_a = &[A(1.), A(2.), A(3.)];
        let col_b = &[B(-1), B(-2), B(-3)];
        let col_d = &[D(false), D(true), D(false)];
        let tab_c: Option<RecordTable<SchemaColumn<fn(&C) -> bool>, &[C]>> = None;
        let multi_col = RecordTable {
            body: col_a,
            header: A_COLUMN,
        }
        .join_variant(Some(RecordTable {
            body: col_b,
            header: B_COLUMN,
        }))
        .join_variant(tab_c)
        .join_variant(Some(RecordTable {
            body: col_d,
            header: D_COLUMN,
        }));
        let csv_dump = dump_csv_string(multi_col).unwrap();
        assert_eq!(
            csv_dump,
            r#"A,B,D
1,-1,false
2,-2,true
3,-3,false
"#
        );
    }

    #[test]
    fn test_write_csv_writes_joined_columns_with_variants() {
        struct C;

        let col_a = &[A(1.), A(2.), A(3.)];
        let col_b = &[B(-1), B(-2), B(-3)];
        type TabB<'a> = RecordTable<SchemaColumn<'a, fn(&B) -> &i32>, &'a [B]>;
        type TabC<'a> = RecordTable<SchemaColumn<'a, fn(&C) -> bool>, &'a [C]>;
        let multi_col = RecordTable {
            body: col_a,
            header: A_COLUMN,
        }
        .join_variant(Either::<TabB, TabC>::Either(RecordTable {
            body: col_b,
            header: B_COLUMN,
        }));
        let csv_dump = dump_csv_string(multi_col).unwrap();
        assert_eq!(
            csv_dump,
            r#"A,B
1,-1
2,-2
3,-3
"#
        );
    }
}
