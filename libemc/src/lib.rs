//! Basic routines to compute impedances
#![warn(missing_docs)]

use numeric::complex::Complex;
use std::f64::consts::{FRAC_1_SQRT_2, FRAC_PI_4, PI, SQRT_2, TAU as TWO_PI};
// TODO: use std::f64::consts::SQRT_3 as soon as it gets stabilized:
const SQRT_3: f64 = 1.732_050_807_568_877_2_f64;

/// Eulers constant
const C_GAMMA: f64 = 1.8513803863860652;
/// Vacuum magnetic permeability \[Vs/(Am)]
pub const MU_0: f64 = 4.0E-7 * PI;
/// Vacuum permittivity \[C/V/m]
pub const EPS_0: f64 = 8.854187817E-12;

/// Guard wire
#[derive(Debug, Clone, PartialEq, metastruct::MetaStruct, serde::Serialize, serde::Deserialize)]
#[allow(non_snake_case)]
pub struct GuardWire {
    /// x coordinate \[m]
    pub x: f64,
    /// y coordinate \[m]
    pub y: f64,
    /// Conductor radius \[m]
    pub r: f64,
    /// Conductor resistance per length \[Ω/m]
    pub R_i: f64,
}

impl Default for GuardWire {
    fn default() -> Self {
        Self {
            x: 0.0,
            y: 30.0,
            r: 0.01,
            R_i: 0.00001,
        }
    }
}

/// 1,2 or 3 phase system
#[allow(non_snake_case)]
#[derive(Default, Debug, Clone, PartialEq)]
pub struct PhaseSystem {
    /// Vector of positions of phases \[m]
    pub positions: PhasePositions,
    /// Current \[A]
    pub I: Complex<f64>,
}

/// Type to represent an array containing either 1, 2 or 3 positions
#[derive(Debug, Clone, PartialEq)]
pub enum PhasePositions {
    /// Array containing 1 positions
    One([[f64; 2]; 1]),
    /// Array containing 2 positions
    Two([[f64; 2]; 2]),
    /// Array containing 3 positions
    Three([[f64; 2]; 3]),
}

impl PhasePositions {
    /// Extract a slice containing all positions.
    pub fn as_slice(&self) -> &[[f64; 2]] {
        match self {
            Self::One(p) => p,
            Self::Two(p) => p,
            Self::Three(p) => p,
        }
    }

    /// Convert an iterator of `[f64; 2]` to phases if the iterator has 1,2,3 elements
    pub fn try_collect(
        iter: impl IntoIterator<Item = [f64; 2]>,
    ) -> Result<Self, UnsupportedNumPhasesError> {
        let mut iter = iter.into_iter();
        let Some(a) = iter.next() else {
            return Err(UnsupportedNumPhasesError(0));
        };
        let Some(b) = iter.next() else {
            return Ok(Self::One([a]));
        };
        let Some(c) = iter.next() else {
            return Ok(Self::Two([a, b]));
        };
        let Some(_) = iter.next() else {
            return Ok(Self::Three([a, b, c]));
        };
        Err(UnsupportedNumPhasesError(4 + iter.count()))
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
struct Point2D {
    x: f64,
    y: f64,
}

impl serde::Serialize for PhasePositions {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let phases = self.as_slice();
        let mut seq = serializer.serialize_seq(Some(phases.len()))?;
        use serde::ser::SerializeSeq;
        for p in phases {
            seq.serialize_element(&Point2D { x: p[0], y: p[1] })?;
        }
        seq.end()
    }
}

impl<'de> serde::Deserialize<'de> for PhasePositions {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let ph = Vec::<Point2D>::deserialize(deserializer)?;
        Self::try_collect(ph.into_iter().map(|p| [p.x, p.y])).map_err(serde::de::Error::custom)
    }
}

impl Default for PhasePositions {
    fn default() -> Self {
        PhasePositions::One([Default::default()])
    }
}

/// Error occurring for an unsupported number of phases
#[derive(Debug)]
pub struct UnsupportedNumPhasesError(pub usize);

impl std::fmt::Display for UnsupportedNumPhasesError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Unsupported number of phases (1,2,3): {}.", self.0)
    }
}

/// Additional factor 1/2 as we are passing d^2 instead of d
fn z_g3le(d: &[f64; 3]) -> Complex<f64> {
    0.25 * MU_0
        * (-1. * SQRT_3 * f64::ln(d[1] / d[2])
            + Complex::i() * f64::ln(d[1] * d[2] / (d[0] * d[0])))
}

fn z_g2le(d: &[f64; 2]) -> Complex<f64> {
    0.5 * MU_0 * f64::ln(d[1] / d[0]) * Complex::i()
}

fn z_simple(f: f64, rho: f64, r: f64) -> Complex<f64> {
    let delta_e = C_GAMMA * (rho / (TWO_PI * f * MU_0)).sqrt();
    Complex::i() * MU_0 * f * f64::ln(delta_e / r)
}

/// Carston's series (4.1-25), (4.1-26)
/// (DR_ii + i DX_ii)/(omega mu_0 / (2 π))
/// Case p <= 4
/// zeta = e^{i θ}
fn delta_z_1(p: f64, zeta: Complex<f64>) -> Complex<f64> {
    let mut zeta_n = Complex::from(1.);
    let mut n = 0.0;
    let theta = zeta.arg();
    let ln_p = f64::ln(p);
    let mut c = 0.6159315;
    let mut b_0 = 0.5;
    let mut b_1 = FRAC_1_SQRT_2;
    let mut dz = b_0 * (FRAC_PI_4 + Complex::i() * (c - ln_p));

    assert!(p >= 0.);
    assert!(p < f64::from(u32::MAX));
    let i_max = 4 * usize::max(1, p as usize);
    let zeta = zeta * p;
    let ci = Complex::i();
    for _ in 0..i_max {
        // n = 1 + 4*k
        zeta_n *= zeta;
        n += 1.;
        b_1 /= n * (n + 2.);
        dz += b_1 * zeta_n.re * (-1. + ci);

        // n = 2 + 4*k
        zeta_n *= zeta;
        n += 1.;
        c += 1. / n + 1. / (n + 2.);
        b_0 /= n * (n + 2.);
        dz += b_0 * ((c - ln_p - ci * FRAC_PI_4) * zeta_n.re + theta * zeta_n.im);
        // n = 3 + 4*k
        zeta_n *= zeta;
        n += 1.;
        b_1 /= n * (n + 2.);
        dz += b_1 * zeta_n.re * (1. + ci);

        // n = 4 + 4*k
        zeta_n *= zeta;
        n += 1.;
        c += 1. / n + 1. / (n + 2.);
        b_0 /= n * (n + 2.);
        dz += b_0 * ((-FRAC_PI_4 - ci * (c - ln_p)) * zeta_n.re - ci * theta * zeta_n.im);

        b_0 = -b_0;
        b_1 = -b_1;
    }
    dz // Factor 2.E-4 \omega -> mu_0 f
}

/// Carston's series (4.1-27), (4.1-28)
/// (DR_ii + i DX_ii)/(omega mu_0 / (2 π))
/// Case p > 4
/// zeta = e^{i θ}
fn delta_z_2(p: f64, zeta: Complex<f64>) -> Complex<f64> {
    let c: [Complex<f64>; 7] = [
        1. + Complex::i(),
        Complex::from(-SQRT_2),
        1. - Complex::i(),
        Complex::from(0.),
        3. + 3. * Complex::i(),
        Complex::from(0.),
        -45. + 45. * Complex::i(),
    ];
    let mut dz = Complex::from(0.);
    let mut zeta_n = Complex::from(1.);

    let mut zeta = zeta / p;

    for v in c {
        zeta_n *= zeta;
        dz += zeta_n.re * v; // Re(zeta_n) = 1/p^n * cos(n*θ)
    }
    dz *= FRAC_1_SQRT_2;
    if zeta.re > 0.
    // θ > π/2
    {
        // w = 1/2 p exp(i (θ + 3π/4))
        // => zeta := 1/w = 2 /p exp(i θ) exp(i 3π/4) = √2/p exp(-i θ) (-1-i)
        zeta *= -SQRT_2 * (1. + Complex::i());
        dz += 0.5
            * PI.sqrt()
            * (2. / zeta).exp()
            * zeta
            * zeta.sqrt()
            * (1. + zeta * (-3. / 16. + zeta * (-15. / 512. + zeta * (-105. / 8192.))));
    }
    dz // Factor 2.E-4 \omega -> mu_0 f
}

fn delta_z(p: f64, zeta: Complex<f64>) -> Complex<f64> {
    if p < 4. {
        delta_z_1(p, zeta)
    } else {
        delta_z_2(p, zeta)
    }
}

/// Error occurring when the argument of the [f64::ln] function is not
/// in its domain.
#[derive(Debug)]
pub struct LnOutOfDomainError(&'static str, f64);

impl std::fmt::Display for LnOutOfDomainError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}: Trying to compute ln(p) for p={}", self.0, self.1)
    }
}

impl core::error::Error for LnOutOfDomainError {}

fn z(
    d: f64,
    s: f64,
    zeta: Complex<f64>,
    f: f64,
    rho: f64,
) -> Result<Complex<f64>, LnOutOfDomainError> {
    let p = s * (MU_0 * TWO_PI * f / rho).sqrt();
    let d_z = delta_z(p, zeta);
    let ln_s_d = f64::ln(s / d);
    if d_z.is_nan() || ln_s_d.is_nan() {
        return Err(LnOutOfDomainError("libemc::delta_z", p));
    }
    Ok(MU_0 * f * (2. * d_z + Complex::i() * ln_s_d))
}

fn z_1(
    phase_position: &[f64; 2],
    x: f64,
    y: f64,
    r: f64,
    f: f64,
    rho: f64,
) -> Result<Complex<f64>, LnOutOfDomainError> {
    let [p_x, p_y] = *phase_position;
    let (r, r_s) = if x != p_x || y != p_y {
        (0., 0.)
    } else if y == 0. {
        (r, r)
    } else {
        (r, 0.)
    };
    z_ij(f, rho, p_x - x, p_y - y + r, p_y + y + r_s)
}

fn hypotsq_r(a: f64, b: f64, r: f64) -> f64 {
    let d = hypotsq(a, b);
    if d == 0. {
        r * r
    } else {
        d
    }
}

fn z_2(
    phase_positions: &[[f64; 2]; 2],
    x: f64,
    y: f64,
    r: f64,
    f: f64,
) -> Result<Complex<f64>, LnOutOfDomainError> {
    let [[x1, y1], [x2, y2]] = phase_positions;
    let d = [hypotsq_r(x - x1, y - y1, r), hypotsq_r(x - x2, y - y2, r)];
    Ok(f * z_g2le(&d))
}

fn z_3(
    phase_positions: &[[f64; 2]; 3],
    x: f64,
    y: f64,
    r: f64,
    f: f64,
) -> Result<Complex<f64>, LnOutOfDomainError> {
    let [[x1, y1], [x2, y2], [x3, y3]] = phase_positions;
    let d = [
        hypotsq_r(x - x1, y - y1, r),
        hypotsq_r(x - x2, y - y2, r),
        hypotsq_r(x - x3, y - y3, r),
    ];
    Ok(f * z_g3le(&d))
}

/// Impedance in case of fault current
pub fn z_err(
    phase_position: &[f64; 2],
    x: f64,
    y: f64,
    r: f64,
    f: f64,
    rho: f64,
) -> Result<Complex<f64>, LnOutOfDomainError> {
    if x == phase_position[0] && y == phase_position[1] {
        z(r, y, Complex::from(0.), f, rho)
    } else {
        z_1(phase_position, x, y, r, f, rho)
    }
}

/// Mutual impedance of 2 conductors with vector distance `(dx, dy)`.
///
/// `dy_s` is the vertical distance to the mirrored (about the ground) conductor
#[allow(non_snake_case)]
fn z_ij(
    f: f64,
    rho: f64,
    dx: f64,
    mut dy: f64,
    dy_s: f64,
) -> Result<Complex<f64>, LnOutOfDomainError> {
    let d = f64::hypot(dx, dy); // d:      Distance of both conductors
    let mut D; // D:      Distance of one conductor to the image of the other
               // theta:  Angle at L1 for triangle L1' -- L1 -- L2
    let zeta: Complex<f64>; // zeta = e^(i theta)

    if dy + dy_s >= 0. && dy_s - dy >= 0. {
        // Both conductors over ground
        D = f64::hypot(dx, dy_s);
        zeta = (dy_s + Complex::i() * dx) / D;
    } else {
        D = dy + dy_s;
        if D < 0. {
            D = dy_s - dy;
        }
        if D < 0. {
            // Both conductors under ground
            zeta = Complex::from(1.);
        }
        // theta = 0
        else {
            // One conductor under ground. Rotate this conductor to ground
            dy = 0.5 * D;
            zeta = (dy + Complex::i() * (d * d - dy * dy).sqrt().copysign(dx)) / d;
        }
        D = d;
    }
    z(D, d, zeta, f, rho)
}

/// Self impedance of a conductor
#[allow(non_snake_case)]
fn z_ei(f: f64, rho: f64, R_i: f64, h: f64, r: f64) -> Result<Complex<f64>, LnOutOfDomainError> {
    let Z = if h < r {
        z_simple(f, rho, r)
    } else {
        z(r, 2. * h, Complex::from(1.), f, rho)?
    };
    Ok(Z + R_i + Complex::i() * MU_0 * 0.25 * f)
}

fn hypotsq(x: f64, y: f64) -> f64 {
    x * x + y * y
}

fn z_ee(c_1: &[f64], c_2: &[f64], rho: f64, f: f64) -> Result<Complex<f64>, LnOutOfDomainError> {
    z_ij(f, rho, c_2[0] - c_1[0], c_2[1] - c_1[1], c_2[1] + c_1[1])
}

/// Coupling impedance of a symmetrical n-phase system with an influenced conductor
fn z_pe(
    positions: &PhasePositions,
    gw: &GuardWire,
    rho: f64,
    f: f64,
) -> Result<Complex<f64>, LnOutOfDomainError> {
    match positions {
        PhasePositions::One([p]) => z_1(p, gw.x, gw.y, gw.r, f, rho),
        PhasePositions::Two(p) => z_2(p, gw.x, gw.y, gw.r, f),
        PhasePositions::Three(p) => z_3(p, gw.x, gw.y, gw.r, f),
    }
}

/// Compute the current induced in guard wires by other conductors
#[allow(non_snake_case)]
pub fn compute_i(
    systems: &[PhaseSystem],
    guard_wires: &[GuardWire],
    rho: f64,
    f: f64,
) -> Result<Vec<Complex<f64>>, LnOutOfDomainError> {
    let mut U = compute_u(systems, guard_wires, rho, f)?;

    if guard_wires.is_empty() {
        return Ok(U);
    }
    let mut Z = coupling_impedance_matrix(guard_wires, rho, f)?;

    // U becomes the vector of the currents in the grounded conductors:
    numeric::lu::solve(&mut Z, &mut U);
    Ok(U)
}

/// Compute the voltage per length induced in guard wires by other conductors
pub fn compute_u(
    systems: &[PhaseSystem],
    guard_wires: &[GuardWire],
    rho: f64,
    f: f64,
) -> Result<Vec<Complex<f64>>, LnOutOfDomainError> {
    let mut u: Vec<Complex<f64>> = vec![0f64.into(); guard_wires.len()];
    for (i, guardwire) in guard_wires.iter().enumerate() {
        for system in systems.iter() {
            u[i] -= z_pe(&system.positions, guardwire, rho, f)? * system.I;
        }
    }
    Ok(u)
}

/// Set up the matrix of coupling impedances to solve for the induced currents in influenced conductors
pub fn coupling_impedance_matrix(
    guard_wires: &[GuardWire],
    rho: f64,
    f: f64,
) -> Result<Vec<Complex<f64>>, LnOutOfDomainError> {
    let n = guard_wires.len();
    let mut z = vec![Complex::<f64>::from(0.); n * n];
    let mut z_mat = numeric::lu::MatrixViewMut { data: &mut z, n };
    for i in 0..n {
        let gw_i = &guard_wires[i];
        z_mat[(i, i)] = z_ei(f, rho, gw_i.R_i, gw_i.y, gw_i.r)?;
        for j in 0..i {
            let gw_j = &guard_wires[j];
            z_mat[(i, j)] = z_ee(&[gw_i.x, gw_i.y], &[gw_j.x, gw_j.y], rho, f)?;
            z_mat[(j, i)] = z_ee(&[gw_j.x, gw_j.y], &[gw_i.x, gw_i.y], rho, f)?;
        }
    }
    Ok(z)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_z_g3le() {
        assert_eq!(
            z_g3le(&[1., 2., 3.]),
            Complex::new(2.206297065917241e-7, 5.628978385526805e-7)
        );
    }

    #[test]
    fn test_z_g2le() {
        assert_eq!(z_g2le(&[1., 2.]), Complex::new(0., 4.355172180607204e-7));
    }

    #[test]
    fn test_z_simple() {
        assert_eq!(z_simple(1., 2., 3.), Complex::new(0., 7.211199193162024e-6));
    }

    #[test]
    fn test_delta_z() {
        assert!(delta_z(0.0, Complex::from(1.0)).is_nan());
        assert_eq!(
            delta_z(0.01, Complex::from(1.0)),
            Complex::new(0.39037939417338186, 2.61290297240657)
        );
        assert_eq!(
            delta_z(5.1, Complex::from(1.0)),
            Complex::new(0.11196968142235453, 0.13501705119522772)
        );
    }

    #[test]
    fn test_delta_z_1() {
        assert_eq!(
            delta_z_1(1., 0.5 + Complex::i() * 0.5 * SQRT_3),
            Complex::new(0.27426116496890757, 0.43914423293295407)
        );
    }

    #[test]
    fn test_delta_z_2() {
        assert_eq!(
            delta_z_2(1., 0.5 + Complex::i() * 0.5 * SQRT_3),
            Complex::new(-11.856056184835737, 15.706669539302833)
        );
    }

    #[test]
    fn test_z() {
        assert_eq!(
            z(1., 2., Complex::from(3.), 4., 5.).unwrap(),
            Complex::new(3.9130619271097868e-06, 3.322139650878914e-05)
        );
    }

    #[test]
    fn test_z_ei() {
        assert_eq!(
            z_ei(50., 0.1, 0.1, 1., 0.1).unwrap(),
            Complex::new(0.10004605647306264, 0.00037658617748879172)
        );
        assert_eq!(
            z_ei(50., 0.1, 0.1, 0.1, 1.1).unwrap(),
            Complex::new(0.1, 0.00022229372124433848)
        );
    }

    #[test]
    fn test_z_ij() {
        assert_eq!(
            z_ij(1., 2., 3., 4., 5.).unwrap(),
            Complex::new(9.819650139737043e-07, 6.3811191236175436e-06)
        );
    }

    #[test]
    fn test_compute_i() {
        assert_eq!(
            compute_i(&[PhaseSystem::default()], &[GuardWire::default()], 2., 3.).unwrap(),
            vec![Complex::new(0., 0.)]
        );
    }

    #[test]
    fn test_phase2json_1p() {
        let p = PhasePositions::default();
        let j = serde_json::to_string(&p).unwrap();
        assert_eq!(j, r#"[{"x":0.0,"y":0.0}]"#);
        let dp: PhasePositions = serde_json::from_str(&j).unwrap();
        assert_eq!(dp, p);
    }
    #[test]
    fn test_phase2json_2p() {
        let p = PhasePositions::Two(Default::default());
        let j = serde_json::to_string(&p).unwrap();
        assert_eq!(j, r#"[{"x":0.0,"y":0.0},{"x":0.0,"y":0.0}]"#);
        let dp: PhasePositions = serde_json::from_str(&j).unwrap();
        assert_eq!(dp, p);
    }
    #[test]
    fn test_phase2json_3p() {
        let p = PhasePositions::Three(Default::default());
        let j = serde_json::to_string(&p).unwrap();
        assert_eq!(
            j,
            r#"[{"x":0.0,"y":0.0},{"x":0.0,"y":0.0},{"x":0.0,"y":0.0}]"#
        );
        let dp: PhasePositions = serde_json::from_str(&j).unwrap();
        assert_eq!(dp, p);
    }

    mod towers {
        use super::*;

        pub struct Tower {
            pub system: PhaseSystem,
            pub guard_wire: GuardWire,
            pub rho: f64,
            pub f: f64,
        }
        pub const P3: Tower = Tower {
            system: PhaseSystem {
                positions: PhasePositions::Three([[-7., 20.], [-10., 10.], [8., 0.]]),
                I: Complex::new(1000., 0.),
            },
            guard_wire: GuardWire {
                x: 0.,
                y: 30.,
                r: 0.01,
                R_i: 0.00014,
            },
            rho: 200.,
            f: 50.,
        };
    }

    #[test]
    fn test_z_pe_3_phase_system() {
        let tower = &towers::P3;
        let z = z_pe(
            &tower.system.positions,
            &tower.guard_wire,
            tower.rho,
            tower.f,
        )
        .unwrap();
        let expected_z = Complex::new(1.786093205815325e-5, 4.8346075864019935e-5);
        assert_eq!(z, expected_z);
    }

    #[test]
    fn test_z_ei_3_phase_system() {
        let tower = &towers::P3;
        let z = z_ei(
            tower.f,
            tower.rho,
            tower.guard_wire.R_i,
            tower.guard_wire.y,
            tower.guard_wire.r,
        )
        .unwrap();
        let expected_z = Complex::new(0.00018706663581023175, 0.000758877117800277);
        assert_eq!(z, expected_z);
    }

    #[test]
    fn test_compute_i_for_3_phase_system() {
        let tower = &towers::P3;
        let i = compute_i(
            &[tower.system.clone()],
            &[tower.guard_wire.clone()],
            tower.rho,
            tower.f,
        )
        .unwrap()[0];
        let expected_i_abs = 65.94201185154776;
        assert_eq!(i.norm(), expected_i_abs);
    }
}
