//! Test utilities
#![warn(missing_docs)]

/// Trait defining `Self` as a "matcher" to `T`.
/// Can be used in tests alongside with [assert_match!].
pub trait Matcher<T> {
    /// Matches `self` against `obj`. On match, return `Ok(())`, otherwise
    /// `Err(msg)`, where `msg` gives details why the `self` does not match `obj`.
    fn matches(&self, obj: T) -> Result<(), String>;
}

impl Matcher<f64> for std::ops::Range<f64> {
    fn matches(&self, obj: f64) -> Result<(), String> {
        if self.contains(&obj) {
            Ok(())
        } else {
            Err(format!("{obj} ∉ {self:?}"))
        }
    }
}
impl Matcher<f64> for std::ops::RangeTo<f64> {
    fn matches(&self, obj: f64) -> Result<(), String> {
        if self.contains(&obj) {
            Ok(())
        } else {
            Err(format!("{obj} ∉ {self:?}"))
        }
    }
}
impl Matcher<f64> for std::ops::RangeFrom<f64> {
    fn matches(&self, obj: f64) -> Result<(), String> {
        if self.contains(&obj) {
            Ok(())
        } else {
            Err(format!("{obj} ∉ {self:?}"))
        }
    }
}

/// Matches a container `items` (`impl AsRef[_]`) with `reference_items`,
/// by comparing size and pairwise matching items by `cmp`.
pub fn containers_match<I, J, V: AsRef<[I]>, W: AsRef<[J]>, M>(
    items: V,
    reference_items: W,
    cmp: M,
) -> Result<(), String>
where
    for<'a> M: Fn(&'a I, &'a J) -> Result<(), String>,
{
    let slice = items.as_ref();
    let ref_slice = reference_items.as_ref();
    if let Some((i, msg)) = slice
        .iter()
        .zip(ref_slice.iter())
        .map(|(i, j)| cmp(i, j))
        .enumerate()
        .find_map(|(i, result)| result.err().map(|msg| (i, msg)))
    {
        return Err(format!("left[{i}] does not match right[{i}]: {msg}"));
    }
    let n = slice.len();
    let m = ref_slice.len();
    if n != m {
        return Err(format!("Length mismatch: {n} != {m}"));
    }
    Ok(())
}

/// Matches an expression against a "matcher" (impl [Matcher]).
#[macro_export]
macro_rules! assert_match {
    ($obj:expr, $matcher:expr) => {
        match (&$obj, &$matcher) {
            (obj, matcher) => {
                if let Err(msg) = matcher.matches(obj) {
                    panic!("Assertion failed: {}\n{msg}", stringify!($matcher))
                }
            }
        };
    };
}
