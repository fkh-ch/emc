+++
title = "Elektrisches Feld"
+++

# Elektrische Feldstärken

Berechnungen des elektrischen Felds werden in erster Linie für
Freileitungen und Freiluftschaltanlagen angewendet und werden meist
mit einem Ersatzladungsverfahren ausgeführt, weil diese Methode
ausreichende Präzision bei geringem Rechenaufwand ergibt.
Schlüsselkriterium zur Wahl dieser Methode ist, dass der Feldraum selbst nicht
diskretisiert werden muss. Wegen der Wechselbeziehung zwischen dem elektrischen
Feld und der Ladungsdichte setzt die Berechnung selbst in einfacheren Fällen
Matrixinversionen voraus. Deshalb sind nur wenige analytische Näherungsformeln
bekannt. Die Berechnungen werden fast ausschliesslich mit Computerprogrammen
ausgeführt.

Die elektrische Feldstärke in einem Raumpunkt $\boldsymbol{x}$ ergibt
sich aus dem Coulomb-Integral:

$$
\boldsymbol{E}(\boldsymbol{x})
= \frac{1}{4\pi \varepsilon_0}\iiint\limits_V
\rho(\boldsymbol{y}) \cdot \frac{\boldsymbol{x}-\boldsymbol{y}}{\|\boldsymbol{x}
-\boldsymbol{y}\|^3}\mathrm{d}V(\boldsymbol{y})
$$

|      |                                                                            |
|------|----------------------------------------------------------------------------|
| $V$: | Volumen [m³], bzw. Raum in welchem das elektrische Feld betrachtet wird, inkl. raumbegrenzende Objekte wie Erdboden oder Wände |
| $\rho(\boldsymbol{y})$:            | Ladungsdichte am Ort $\boldsymbol{y}$ [C/m³] im betrachteten Volumen $V$ |
| $\boldsymbol{E}(\boldsymbol{x}):$ | Elektrische Feldstärke [V/m] am Einwirkungsort $\boldsymbol{x}$. |

Vorgängig müssen die Ladungen, meist in Form von stückweise
definierten Linienladungen, rechnerisch aus der Lösung eines
Gleichungssystems bestimmt werden. Dabei müssen die von den Ladungen
erzeugten Potentiale mit den Potentialvorgaben an gewählten Aufpunkten
auf den Leiteroberflächen in Übereinstimmungen gebracht werden.

Die am Erdboden erzeugten Influenzladungen werden mittels des
Spiegelladungsverfahrens simuliert. Das Potential $0$ an der
Erdoberfläche wird erzwungen indem für jede
Linienladung in den Einzelleitern eine komplexe Spiegelladung mit umgekehrten
Vorzeichen mitgerechnet wird. Das Gleichungssystem zur Ermittlung aller komplexen
Linienladungen unter der Bedingung der Einhaltung der vorgegebenen komplexen
Potentiale an den Leiteroberflächen wird gemäss [oeding2004](#oeding2004) aufgestellt.

{{ figure(
  src="e_field.svg",
  caption='Elektrische Feldstärke und Leiteranordnungen. a Elektrische
  Feldstärke und Potentiallinien in der Nähe eines Rundleiters; b, c
  Leiter-Spiegelleiter-Anordnungen zur Berechnung der Kapazitäten, aus
  <a href="oeding2004">oeding2004</a>'
  )
}}

Für eine Freileitung mit einem Dreiphasensystem lautet das Gleichungssystem zur
Bestimmung der Linienladungen in den Einzelleitern:

$$
\left[ \begin{array}{c}
\underline{U}\_{\mathrm{L}1} \\\\
\underline{U}\_{\mathrm{L}2} \\\\
\underline{U}\_{\mathrm{L}3} \\\\
0
\end{array} \right]
= \left[ \begin{array}{ccc|c}
\underline{P}\_{\mathrm{L}1\mathrm{L}1}' &
\underline{P}\_{\mathrm{L}1\mathrm{L}2}' &
\underline{P}\_{\mathrm{L}1\mathrm{L}3}' &
\underline{P}\_{\mathrm{L}1\mathrm{Q}}'
\\\\
\underline{P}\_{\mathrm{L}2\mathrm{L}1}' &
\underline{P}\_{\mathrm{L}2\mathrm{L}2}' &
\underline{P}\_{\mathrm{L}2\mathrm{L}3}' &
\underline{P}\_{\mathrm{L}2\mathrm{Q}}'
\\\\
\underline{P}\_{\mathrm{L}3\mathrm{L}1}' &
\underline{P}\_{\mathrm{L}3\mathrm{L}2}' &
\underline{P}\_{\mathrm{L}3\mathrm{L}3}' &
\underline{P}\_{\mathrm{L}3\mathrm{Q}}'
\\\\ \hline
\underline{P}\_{\mathrm{Q}\mathrm{L}1}'  &
\underline{P}\_{\mathrm{Q}\mathrm{L}2}'  &
\underline{P}\_{\mathrm{Q}\mathrm{L}3}'  &
\underline{P}\_{\mathrm{Q}\mathrm{Q}}'
\end{array} \right]
\left[ \begin{array}{c}
\underline{Q}\_{\mathrm{L}1}' \\\\
\underline{Q}\_{\mathrm{L}2}' \\\\
\underline{Q}\_{\mathrm{L}3}' \\\\ \hline
\underline{Q}\_{\mathrm{Q}}'
\end{array} \right]
$$

Bzw. als Matrixgleichung geschrieben:

<a id="eq:matrix"></a>
$$
\begin{equation}
\tag{M}
\underline{\boldsymbol{U}} = P \underline{\boldsymbol{Q}}',
\end{equation}
$$

wobei es sich bei $\underline{\boldsymbol{U}}$ und
$\underline{\boldsymbol{Q}}'$ um komplexe Vektoren und bei $P$ um eine
skalare Matrix handelt. Die Koeffizienten ergeben sich gemäss [oeding2004](#oeding2004)[@oeding2004] zu:

$$
\begin{aligned}
P_{ii}' &= \frac{1}{2\pi \varepsilon_0} \ln\left( \frac{2
h_i}{r_i}\right), \\
P_{ik}' &= \frac{1}{2\pi \varepsilon_0} \ln\left( \frac{
d_{ik}'}{d_{ik}}\right) = P_{ki}'\\
\end{aligned}
$$

Begründung:
Da $d_{ii} = 0$ und $d_{ii}' = 2h_i$ und da $d_{ik} \gg r_i$ können die Matrixelemente inklusive die
Diagonalelemente einheitlich wie folgt geschrieben werden:

$$
P_{ik}' = \frac{1}{2\pi\varepsilon_0} \ln \left(
\frac{d_{ik}'}{\sqrt{d_{ik}^2 + r_i^2}}\right).
$$

Diese Beziehung wird im Programm EBL verwendet.
Durch Inversion der Matrixgleichung [(M)](#eq:matrix), können die Ladungen ausgerechnet werden.
Damit sind auch die mittleren Oberflächenfeldstärken (Betrag)
bestimmt:

$$
\underline{E}_i = \frac{\underline{q}_i}{2\pi \varepsilon_0 \cdot r_i}.
$$

Die Kapazitätsmatrix wird

$$
\underline{C}' = \underline{P}^{-1},
$$

wobei die Diagonalelemente die Erdkapazitäten der Seile und die übrigen Elemente die
Gegenkapazitäten darstellen. Werden Bündelleiter betrachtet, müssen die Elemente
jedes Bündels zusammengefasst werden um die Erd- und Gegenkapazitäten der Bündel
zu erhalten.
Die Feldstärke in einem beliebigen Raumpunkt $\boldsymbol{p}$ ergibt
sich zu
$$
\underline{\boldsymbol{E}}(\boldsymbol{p}) = \frac{1}{2\pi\varepsilon_0}\sum_{i=1}^n
\frac{\underline{q}_i (\boldsymbol{p} - \boldsymbol{x}_i)}{\|\boldsymbol{p} - \boldsymbol{x}_i\|^2}.
$$

{{ bibliography() }}
