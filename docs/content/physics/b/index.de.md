+++
title = "Magnetisches Feld"
+++

# Berechnung der magnetischen Flussdichten

Die Berechnung der magnetischen Flussdichte für Felder elektrischer Anlagen erfolgt
durch die Anwendung des Gesetzes von Biot-Savart bzw. des Ampère’schen Gesetzes:

$$
\boldsymbol{B}(\boldsymbol{x}) = \frac{\mu_0}{4\pi}\iiint\limits_V
\frac{1}{\|\boldsymbol{x}
-\boldsymbol{y}\|^3}\boldsymbol{j}(\boldsymbol{y}) \times (\boldsymbol{x}
-\boldsymbol{y}) \ \mathrm{d}V(\boldsymbol{y})
$$

|                   |                                                                                                  |
|-------------------|--------------------------------------------------------------------------------------------------|
| $V$:              | Volume [m³] containing the current circuits                                                      |
| $\boldsymbol{j}$: | Distribution of the current density [A/m²] in all conductors in the volume $V$                   |
| $\mu_0$:          | Magnetic permeability of vacuum [Vs/Am]                                                          |
| $\boldsymbol{B}$: | Magnetic flux density [Vs/m²] at the site of impact                            $\boldsymbol{x}$. |

Aus diesem Grundgesetz werden vereinfachte Beziehungen abgeleitet,
welche in Handformeln aber auch für die Beziehungen in den meisten
Berechnungsprogrammen zur Anwendung kommen. Die bekannten oder
vorausberechneten Ströme in den elektrischen Leitern einer Anlage
können in die Beziehungen eingesetzt und damit die Werte für die
magnetische Flussdichte direkt ausgerechnet werden. Bei Programmen,
die auch Lastflussberechnungen, induzierte Ströme, ggf. auch
Wirbelströme berücksichtigen, müssen Rechenschritte vorangestellt
werden, in welchen diese Ströme durch Lösung der Koppelmatrizen
bestimmt werden.

Da nur sinusförmige Stromverläufe zu betrachten sind,
werden die elektrischen Grössen als komplexe Effektivwerte geschrieben
(unterstrichene Grössen).

Für die magnetische Flussdichte eines
geraden, unendlich langen Leiters ergibt sich das Linienintegral:

$$
\underline{\boldsymbol{B}}(\boldsymbol{x}) = \frac{\mu_0 \cdot
\underline{I} \cdot \boldsymbol{e}_I \times (\boldsymbol{x} -
\boldsymbol{p})}{2\pi \cdot \| \boldsymbol{x} - \boldsymbol{p} \|^3}
$$

|                               |                                                                                        |
|-------------------------------|----------------------------------------------------------------------------------------|
| $\underline{I}$:              | Komplexer Effektivwert des Stroms im geraden Leiter [A]                                |
| $\boldsymbol{e}_I$:           | Einheitsvektor in Richtung des Stromflusses im Leiter                                  |
| $\underline{\boldsymbol{B}}$: | Komplexer magnetischer Flussdichtevektor [Vs/m²] am Einwirkungsort $\boldsymbol{x}$. |


Bei Freileitungen kann der Leiter in $z$-Richtung gelegt werden. Der
Flussdichtevektor $\underline{\boldsymbol{B}}$ liegt dann in der $xy$-Ebene.

In der betrachteten Querschnittsfläche wird der Verbindungsvektor
$\boldsymbol{r} = \boldsymbol{x} -\boldsymbol{p}$
vom Ortsvektor $\boldsymbol{p} = (x_0, y_0)$ des Stromfaden zum
Punkt der Magnetfeldeinwirkung
$\boldsymbol{x} = (x_0, y_0)$
wie folgt geschrieben.

$$
\boldsymbol{r} = \boldsymbol{x} - \boldsymbol{p}
= \binom{x - x_0}{y - y_0} = \binom{\Delta x}{\Delta y}
$$

Der Flussdichtevektor ergibt sich gemäss der Abbildung [unten](#fig:vec_components):
$$
\underline{\boldsymbol{B}}(\boldsymbol{x}) = \frac{\mu_0 \cdot
\underline{I}}{2\pi \cdot (\Delta x^2 + \Delta y^2)} \binom{\Delta
y}{-\Delta x}.
$$

{{ figure(
  src="vector_components.svg",
  ref="fig:vec_components",
  caption="Komponenten der Geometrie-Vektoren in der
  Querschnittsfläche einer Leitungstrasse zur Bestimmung des
  magnetischen Flussdichtevektors im Punkt $\boldsymbol{x}$ eines
  einzigen stromdurchflossenen Leiters im Punkt $\boldsymbol{p}$"
  )
}}
