+++
title = "Magnetic Field"
+++

# Computation of the magnetic flux densities

The computation of the magnetic flux density for fields emerging from
electrical installations is performed by applying the laws of
Biot-Savart and Ampère:

$$
\boldsymbol{B}(\boldsymbol{x}) = \frac{\mu_0}{4\pi}\iiint\limits_V
\frac{1}{\|\boldsymbol{x}
-\boldsymbol{y}\|^3}\boldsymbol{j}(\boldsymbol{y}) \times (\boldsymbol{x}
-\boldsymbol{y}) \ \mathrm{d}V(\boldsymbol{y})
$$

|                   |                                                                                                  |
|-------------------|--------------------------------------------------------------------------------------------------|
| $V$:              | Volume [m³] containing the current circuits                                                      |
| $\boldsymbol{j}$: | Distribution of the current density [A/m²] in all conductors in the volume $V$                   |
| $\mu_0$:          | Magnetic permeability of vacuum [Vs/Am]                                                          |
| $\boldsymbol{B}$: | Magnetic flux density [Vs/m²] at the site of impact                            $\boldsymbol{x}$. |

Simplified relationships are derived from this basic law, which are
used in manual formulas but also for the relationships in most
calculation programs. The known or pre-computed currents in the
electrical conductors of a system can be substituted into the
relationships to compute the values for the magnetic flux density
directly. For programs that also take into account load flow
calculations, induced currents and possibly also eddy currents,
calculation steps must be carried out beforehand in which these
currents are determined by solving the coupling matrices.

For programs that also take into account load flow calculations, induced currents and possibly also eddy currents, calculation steps must be carried out beforehand in which these currents are determined by solving the coupling matrices.

Since only sinusoidal currents are to be considered,
the electrical variables are written as complex effective values
(underlined values).

For the magnetic flux density of a
straight, infinitely long conductor, the line integral is obtained:

$$
\underline{\boldsymbol{B}}(\boldsymbol{x}) = \frac{\mu_0 \cdot
\underline{I} \cdot \boldsymbol{e}_I \times (\boldsymbol{x} -
\boldsymbol{p})}{2\pi \cdot \| \boldsymbol{x} - \boldsymbol{p} \|^3}
$$

|                               |                                                                                        |
|-------------------------------|----------------------------------------------------------------------------------------|
| $\underline{I}$:              | Complex effective value of the current in the straight conductor [A]              |
| $\boldsymbol{e}_I$:           | Unit vector in the direction of the current flow in the conductor                                  |
| $\underline{\boldsymbol{B}}$: | Complex magnetic flux density vector [Vs/m²] at the point of impact $\boldsymbol{x}$. |


For overhead lines, the conductor can be laid in the z-direction. The
flux density vector $\underline{\boldsymbol{B}}$ then lies in the $xy$-plane.

In the cross-sectional area under consideration, the connection vector
$\boldsymbol{r} = \boldsymbol{x} -\boldsymbol{p}$
from the position vector $\boldsymbol{p} = (x_0, y_0)$ of the current filament to the
point of the magnetic field effect
$\boldsymbol{x} = (x_0, y_0)$
is written as follows:

$$
\boldsymbol{r} = \boldsymbol{x} - \boldsymbol{p}
= \binom{x - x_0}{y - y_0} = \binom{\Delta x}{\Delta y}
$$

The flux density vector is given by Figure [@fig:delta]:
$$
\underline{\boldsymbol{B}}(\boldsymbol{x}) = \frac{\mu_0 \cdot
\underline{I}}{2\pi \cdot (\Delta x^2 + \Delta y^2)} \binom{\Delta
y}{-\Delta x}.
$$

{{ figure(
  src="vector_components.svg",
  ref="fig:vec_components",
  caption="Components of the geometry vectors in the cross-sectional
  area of a line route to determine the magnetic flux density vector
  at the point $\boldsymbol{x}$ of a single current-carrying conductor
  at the point $\boldsymbol{p}$"
  )
}}
