+++
title = "Willkommen bei der EBL Dokumentation!"

[extra]
summary = """Zweidimensionale Berechnungen der Verteilung der RMS-Werte
der elektrischen Feldstärken und der magnetischen Flussdichte
von sowie der Koronaeffekte von Starkstromfreileitungen (bei Kabeln
nur magnetische Flussmitteldichte)."""
+++
