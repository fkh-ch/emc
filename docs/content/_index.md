+++
title = "Welcome to the EBL documentation!"
insert_anchor_links = "right"

[extra]
summary = """Two-dimensional calculations of the distribution of the RMS values
of the electric field strengths and the magnetic flux density
as well as the corona effects of overhead power lines (for cables
only magnetic flux density)."""
+++
