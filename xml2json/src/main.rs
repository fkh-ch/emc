use clap::Parser;
use ebl::project::{
    Angle, CombinedPlot, ContourLineLevel, ContourLinePlot, FieldLinePlot, Figure, GuardWire,
    ShieldProperties, SinglePlot, WebColor,
};

/// EBL xml -> json converter
#[derive(clap::Parser, Debug)]
#[clap(about)]
struct Cli {
    path: std::path::PathBuf,
}

fn main() -> Result<(), String> {
    let opt = Cli::parse();
    let project = ebl::Project::from(XmlEblData::try_from_xml(
        &std::fs::File::open(&opt.path)
            .map_err(|e| format!("Error reading from {}: {e}", opt.path.display()))?,
    )?);
    let json_path = opt.path.with_extension("json");
    serde_json::to_writer_pretty(
        &std::fs::File::create(&json_path)
            .map_err(|e| format!("Error writing to {}: {e}", json_path.display()))?,
        &project,
    )
    .map_err(|e| format!("{e}"))?;

    Ok(())
}

#[derive(Debug, Default, PartialEq, serde::Deserialize, serde::Serialize)]
struct XmlCombinedPlotData {
    /// left border isoline plot
    x_min: f64,
    /// right border isoline plot
    x_max: f64,
    /// y coordinate start
    y_min: f64,
    /// y coordinate end
    y_max: f64,
    /// Plot subgrid
    #[serde(rename = "Subgrid")]
    sub_grid: bool,
    /// Support points for picture rows
    m: u32,
    #[serde(
        rename = "isoline",
        default,
        deserialize_with = "deserialize_contour_line_levels_legacy"
    )]
    isolines: Vec<ContourLineLevel>,
    pub r: f64,
    pub n_d_crd: u32,
    pub field_lines_active: bool,
}

impl XmlCombinedPlotData {
    fn into_figure(self, with_contour_lines: bool) -> Figure<CombinedPlot> {
        let plots = CombinedPlot {
            contour_lines: ContourLinePlot {
                enabled: with_contour_lines,
                resolution: 1.0,
                levels: self.isolines,
                level_colors: Default::default(),
            },
            field_lines: FieldLinePlot {
                enabled: self.field_lines_active,
                r_singularity: self.r,
                n_lines_at_singularity: self.n_d_crd as usize,
            },
        };
        Figure {
            x_range: self.x_min..self.x_max,
            y_range: self.y_min..self.y_max,
            with_subgrid: self.sub_grid,
            plots,
            width: self.m as f64,
        }
    }
}

#[derive(Debug, Default, PartialEq, serde::Deserialize, serde::Serialize)]
struct XmlIsoLinePlotData {
    /// left border isoline plot
    x_min: f64,
    /// right border isoline plot
    x_max: f64,
    /// y coordinate start
    y_min: f64,
    /// y coordinate end
    y_max: f64,
    /// Plot subgrid
    #[serde(rename = "Subgrid")]
    sub_grid: bool,
    /// Support points for picture rows
    m: u32,
    #[serde(
        rename = "isoline",
        deserialize_with = "deserialize_contour_line_levels_legacy"
    )]
    isolines: Vec<ContourLineLevel>,
}

impl XmlIsoLinePlotData {
    fn into_figure(self, enabled: bool) -> Figure<SinglePlot> {
        Figure {
            x_range: self.x_min..self.x_max,
            y_range: self.y_min..self.y_max,
            with_subgrid: self.sub_grid,
            width: self.m as f64,
            plots: SinglePlot {
                contour_lines: ContourLinePlot {
                    enabled,
                    resolution: 1.0,
                    levels: self.isolines,
                    level_colors: Default::default(),
                },
            },
        }
    }
}

fn deserialize_contour_line_levels_legacy<'de, D>(de: D) -> Result<Vec<ContourLineLevel>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    #[derive(Debug, serde::Deserialize)]
    pub struct ContourLineLevelLegacy {
        /// Level value of the contour line
        pub value: f64,
        /// Color of the contour line
        #[serde(deserialize_with = "deserialize_webcolor_legacy")]
        pub color: WebColor,
    }
    use serde::Deserialize;
    Ok(Vec::<ContourLineLevelLegacy>::deserialize(de)?
        .into_iter()
        .map(|level| ContourLineLevel {
            value: level.value,
            color: Some(level.color),
        })
        .collect())
}

fn deserialize_webcolor_legacy<'de, D>(de: D) -> Result<WebColor, D::Error>
where
    D: serde::Deserializer<'de>,
{
    use serde::Deserialize;
    let buf = String::deserialize(de)?;
    webcolor_from_str_legacy(&buf).map_err(serde::de::Error::custom)
}

fn webcolor_from_str_legacy(s: &str) -> Result<WebColor, ebl::project::ParseWebColorError> {
    let s = s.strip_prefix('#').ok_or(ebl::project::ParseWebColorError(
        "Webcolor does not start with #".to_string(),
    ))?;
    match s.len() {
        // Only r value: #ff
        2 => {
            let r = u8::from_str_radix(&s[..1], 16)?;
            Ok(WebColor {
                r: (r << 4) | r,
                g: 0,
                b: 0,
            })
        }
        // short form e.g. #fff
        3 => {
            let r = u8::from_str_radix(&s[..1], 16)?;
            let g = u8::from_str_radix(&s[1..2], 16)?;
            let b = u8::from_str_radix(&s[2..], 16)?;
            Ok(WebColor {
                r: (r << 4) | r,
                g: (g << 4) | g,
                b: (b << 4) | b,
            })
        }
        // Blue omitted!?
        4 => Ok(WebColor {
            r: u8::from_str_radix(&s[..2], 16)?,
            g: u8::from_str_radix(&s[2..], 16)?,
            b: 0,
        }),
        // imagine a zero for the last nibble
        5 => Ok(WebColor {
            r: u8::from_str_radix(&s[..2], 16)?,
            g: u8::from_str_radix(&s[2..4], 16)?,

            b: u8::from_str_radix(&s[4..], 16)? << 4,
        }),
        6 => Ok(WebColor {
            r: u8::from_str_radix(&s[..2], 16)?,
            g: u8::from_str_radix(&s[2..4], 16)?,

            b: u8::from_str_radix(&s[4..], 16)?,
        }),
        n => Err(ebl::project::ParseWebColorError(format!(
            "Expected a string length of 4,...,7, got: {}: {s}",
            n + 1
        ))),
    }
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
struct XmlPhase {
    pub x: f64,
    pub y: f64,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
#[allow(non_snake_case)]
struct XmlSystem {
    /// # part conductors per phase in system
    pub n_t: u32,

    /// Starting angle for part conductor
    pub beta_s: f64,
    /// Radius of gyration for bundle
    pub r_k: f64,
    /// Radius of conductors
    pub r_s: f64,
    /// Voltage / phases
    pub U: f64,
    pub phi_U: f64,
    /// Current / phase
    pub I: f64,
    pub phi_I: f64,

    /// Coord bundle
    #[serde(rename = "phase")]
    pub phases: Vec<XmlPhase>,
    /// true for shielded conductors
    pub shield: bool,
    /// Radius of shields \[m]
    pub r_shield: f64,
    /// Resistance per length for shields [Ohm/m]
    pub R_shield: f64,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
#[allow(non_snake_case)]
struct XmlEblData {
    #[serde(rename = "system")]
    pub systems: Vec<XmlSystem>,
    #[serde(rename = "guard_wire")]
    pub guardwires: Vec<libemc::GuardWire>,
    /// number of systems
    #[serde(default)]
    pub n: u32,
    /// Number of sampling points for the transverse profile
    pub m: u32,

    /// E field (components)  on / off
    pub ve: bool,
    /// E field (absolute value) on / off
    pub e: bool,
    /// B field (components) on / off
    pub vb: bool,
    /// B field (absolute value) on / off
    pub b: bool,
    /// Compute induced current for ground conductors
    pub auto_I: bool,
    /// Output conductor picture
    pub bmp_cnd: bool,
    /// isolines_E.active
    pub bmp_E: bool,
    /// isolines_B.active
    pub bmp_B: bool,
    /// combined_plot_E.active
    pub bmp_Phi: bool,
    /// combined_plot_B.active
    pub bmp_a: bool,
    pub graph_E: bool,
    pub graph_B: bool,

    #[serde(rename = "E-Isoline_plot")]
    pub isolines_E: XmlIsoLinePlotData,
    #[serde(rename = "B-Isoline_plot")]
    pub isolines_B: XmlIsoLinePlotData,
    #[serde(rename = "E-combined_plot")]
    pub combined_plot_E: XmlCombinedPlotData,
    #[serde(rename = "B-combined_plot")]
    pub combined_plot_B: XmlCombinedPlotData,
    pub x_min_P: f64,
    /// x coordinate starting and ending point
    pub x_max_P: f64,

    /// Reference level for conductors \[m]
    pub y_E: f64,
    /// Altitude of profile over ground \[m]
    pub y_P: f64,
    /// Fall of ground [°]
    pub alpha: f64,
    /// Specific resistance of ground [Ohm m]
    pub rho: f64,
    /// Frequency \[Hz] used for computing induced currents
    pub f: f64,

    pub L: bool,
    pub h: f64,
    pub R_Int: f64,
    pub dL: bool,
}

impl XmlEblData {
    pub fn try_from_xml<R: std::io::Read>(reader: R) -> Result<Self, String> {
        serde_xml_rs::from_reader(reader).map_err(|e| format!("{e}"))
    }
}

impl From<XmlEblData> for ebl::Project {
    fn from(x: XmlEblData) -> Self {
        let figures = ebl::project::Figures {
            e: x.isolines_E.into_figure(x.bmp_E),
            b: x.isolines_B.into_figure(x.bmp_B),
            u_re: x.combined_plot_E.into_figure(x.bmp_Phi),
            a_re: x.combined_plot_B.into_figure(x.bmp_a),
            ..Default::default()
        };
        Self {
            systems: x.systems.into_iter().map(|v| v.into()).collect(),
            guardwires: x
                .guardwires
                .into_iter()
                .map(|g| GuardWire {
                    x: g.x,
                    y: g.y,
                    d: 2. * g.r,
                    R_i: g.R_i,
                })
                .collect(),
            transverse_profile: ebl::project::TransverseProfile {
                x_min: x.x_min_P,
                x_max: x.x_max_P,
                y: x.y_P,
                m: x.m,
                columns: ebl::project::TransverseProfileColumns {
                    with_e_abs: x.ve,
                    with_e_components: x.e,
                    with_b_abs: x.vb,
                    with_b_components: x.b,
                },
                plots: ebl::project::TransverseProfilePlots {
                    with_conductor_picture: x.bmp_cnd,
                    with_e_profile: x.graph_E,
                    with_b_profile: x.graph_B,
                },
            },
            corona_sound: ebl::sound::CoronaSound {
                h_0: x.h,
                rain_rate: x.R_Int,
                enable_atmospheric_air_absorption: x.dL,
                annual_assessment: Default::default(),
                enabled: x.L,
            },
            figures,
            y_ref: x.y_E,
            alpha: Angle::from_degrees(x.alpha),
            rho: x.rho,
            f_grid: x.f,
        }
    }
}

fn dist_from_r(r: f64, n_t: u32) -> f64 {
    use std::f64::consts::PI;
    2. * r * f64::sin(PI / n_t as f64)
}

impl From<XmlSystem> for ebl::project::System {
    fn from(x: XmlSystem) -> Self {
        let shield = ShieldProperties {
            d: 2. * x.r_shield,
            R_i: x.R_shield,
            enabled: x.shield,
        };
        Self {
            n_t: x.n_t,
            beta_s: Angle(x.beta_s),
            a: dist_from_r(x.r_k, x.n_t),
            d: x.r_s * 2.,
            U: x.U,
            phi_U: Angle(x.phi_U),
            I: x.I,
            phi_I: Angle(x.phi_I),
            phases: x.phases.into_iter().map(|v| v.into()).collect(),
            shield,
        }
    }
}

impl From<XmlPhase> for ebl::project::Phase {
    fn from(x: XmlPhase) -> Self {
        Self { x: x.x, y: x.y }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_load_from_xml() {
        let xml = include_bytes!("../../ebl/tests/M3.xml");
        let ed = ebl::Project::from(XmlEblData::try_from_xml(xml.as_slice()).unwrap());
        let json = include_bytes!("../../ebl/tests/M3.json");
        let mut expected = ebl::Project::from_json(json.as_slice()).unwrap();
        for sys in &mut expected.systems {
            sys.shield = ShieldProperties {
                d: 0.0,
                R_i: 0.0,
                ..sys.shield
            };
        }
        assert_eq!(ed, expected);
    }

    #[test]
    fn test_webcolor_from_str_legacy() {
        assert_eq!(
            webcolor_from_str_legacy("#fff").unwrap(),
            WebColor::new(0xff, 0xff, 0xff)
        );
        assert_eq!(
            webcolor_from_str_legacy("#888").unwrap(),
            WebColor::new(0x88, 0x88, 0x88)
        );
        assert_eq!(
            webcolor_from_str_legacy("#1234").unwrap(),
            WebColor::new(0x12, 0x34, 0x00)
        );
        assert_eq!(
            webcolor_from_str_legacy("#080808").unwrap(),
            WebColor::new(0x08, 0x08, 0x08)
        );
        assert_eq!(
            webcolor_from_str_legacy("#808081").unwrap(),
            WebColor::new(0x80, 0x80, 0x81)
        );
    }
}
