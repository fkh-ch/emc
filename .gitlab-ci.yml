---
image: "rust:slim"

variables:
  PACKAGE_REGISTRY_EXE_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/windows/${CI_COMMIT_TAG}"

test:
  stage: test
  needs: []
  script:
    - cargo test --all-features

lint:
  stage: test
  needs: []
  before_script:
    - apt-get update
    - apt-get install -y pkg-config libglib2.0-dev libgtk-3-dev
  script:
    - rustup component add clippy
    - cargo clippy --all-features -- -D warnings

format:
  stage: test
  needs: []
  script:
    - rustup component add rustfmt
    - cargo fmt --check

doc:
  stage: build
  image: alpine
  script:
    - apk add zola
    - cd docs
    - zola build
    - mv public ..
  artifacts:
    paths:
      - public

doc:api:
  stage: build
  script:
    - |
      RUSTDOCFLAGS="-D warnings --html-in-header ./docs/docs-header.html"
      cargo doc --lib --all-features --no-deps
    - mkdir -p public
    - mv target/doc public/api
  artifacts:
    paths:
      - public/api

exe:
  stage: build
  rules:
    - if: $CI_COMMIT_TAG
  tags:
    - saas-windows-medium-amd64
  before_script:
    - Invoke-WebRequest -Uri "https://win.rustup.rs" -OutFile "rustup-init.exe"
    - .\rustup-init.exe --profile minimal -y
    - $env:PATH+=";$env:USERPROFILE\.cargo\bin"
  script:
    - cargo build --release
  artifacts:
    paths:
      - target/release/*.exe

pages:
  stage: deploy
  needs:
    - job: doc
    - job: doc:api
  script:
    - ":"
  artifacts:
    paths:
      - public

package:
  stage: deploy
  image: curlimages/curl:latest
  rules:
    - if: '$CI_COMMIT_TAG'
  needs:
    - job: exe
  script:
    - >
      for f in target/release/*.exe ; do \
        curl --header "JOB-TOKEN: $CI_JOB_TOKEN" \
             --upload-file "$f" "${PACKAGE_REGISTRY_EXE_URL}/$(basename "$f")" ; \
      done

create_gitlab_release:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - apk add git
  script:
    - >
      for f in target/release/*.exe ; do \
         exe="$(basename "$f")" ; \
         assets="${assets}${assets:+,}{\"name\":\"${exe}\",\"url\":\"${PACKAGE_REGISTRY_EXE_URL}/${exe}\"}"; \
      done
    - assets="[${assets}]"
    - TAG_MESSAGE="$(git tag --format='%(contents)' -l $CI_COMMIT_TAG)"
    - >
      release-cli create \
        --name "$CI_COMMIT_TAG" \
        --description "$TAG_MESSAGE" \
        --tag-name "$CI_COMMIT_TAG" \
        --ref "$CI_COMMIT_TAG" \
        --assets-link "$assets"
