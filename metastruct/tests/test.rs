use metastruct::{MetaFields, MetaStruct};

#[derive(MetaStruct, MetaFields)]
pub struct Data {
    /// doc xxx
    pub x: i32,
    pub y: u32,
    pub z: f64,
    /// A: doc
    pub a: bool,
    /// doc [unit]
    pub b: f64,
    /// C: doc []
    ///
    /// C description
    pub c: u32,
    /// D: doc [unit] ...
    pub d: u32,
}

#[test]
fn test_derive_meta_struct_works() {
    assert_eq!(Data::META.x.metadata.ident, "x");
    assert_eq!(Data::META.a.metadata.ident, "a");
    assert_eq!(Data::META.b.metadata.ident, "b");
    assert_eq!(Data::META.c.metadata.ident, "c");
}

#[test]
fn test_derive_meta_field_extracts_correct_metadata() {
    let expected_meta_x = metastruct::MetaData {
        ident: "x",
        symbol: None,
        label: "doc xxx",
        unit: None,
        description: None,
    };
    let expected_meta_a = metastruct::MetaData {
        ident: "a",
        symbol: Some("A"),
        label: "doc",
        unit: None,
        description: None,
    };
    let expected_meta_b = metastruct::MetaData {
        ident: "b",
        symbol: None,
        label: "doc",
        unit: Some("unit"),
        description: None,
    };
    let expected_meta_c = metastruct::MetaData {
        ident: "c",
        symbol: Some("C"),
        label: "doc",
        unit: Some(""),
        description: Some("C description"),
    };
    let expected_meta_d = metastruct::MetaData {
        ident: "d",
        symbol: Some("D"),
        label: "doc ...",
        unit: Some("unit"),
        description: None,
    };
    let (meta_x, (meta_a, (meta_b, (meta_c, meta_d)))) = &Data::META_FIELDS;
    assert_eq!(meta_x.metadata, expected_meta_x);
    assert_eq!(meta_a.metadata, expected_meta_a);
    assert_eq!(meta_b.metadata, expected_meta_b);
    assert_eq!(meta_c.metadata, expected_meta_c);
    assert_eq!(meta_d.metadata, expected_meta_d);
}

#[test]
fn test_derive_meta_field_extracts_builds_correct_accessors() {
    let data = Data {
        x: -1,
        y: 1,
        z: 4.0,
        a: true,
        b: 0.25,
        c: 2,
        d: 3,
    };
    let (meta_x, (meta_a, (meta_b, (meta_c, meta_d)))) = &Data::META_FIELDS;

    assert_eq!(*(meta_x.access)(&data), data.x);
    assert_eq!(*(meta_a.access)(&data), data.a);
    assert!(
        ((meta_b.access)(&data) - data.b) < f64::EPSILON,
        "|x-{}| >= f64::EPSILON! x={}",
        data.b,
        (meta_b.access)(&data)
    );
    assert_eq!(*(meta_c.access)(&data), data.c);
    assert_eq!(*(meta_d.access)(&data), data.d);
}

#[test]
fn test_derive_meta_field_extracts_builds_correct_mut_accessors() {
    let mut data = Data {
        x: -1,
        y: 1,
        z: 4.0,
        a: true,
        b: 0.25,
        c: 2,
        d: 3,
    };
    let (meta_x, (meta_a, (meta_b, (meta_c, meta_d)))) = &Data::META_FIELDS;

    *(meta_x.access_mut)(&mut data) = -2;
    *(meta_a.access_mut)(&mut data) = false;
    *(meta_b.access_mut)(&mut data) = 0.125;
    *(meta_c.access_mut)(&mut data) = 3;
    *(meta_d.access_mut)(&mut data) = 42;
    assert_eq!(data.x, -2);
    assert_eq!(data.a, false);
    assert_eq!(data.b, 0.125);
    assert_eq!(data.c, 3);
    assert_eq!(data.d, 42);
}

#[test]
fn test_sub_field_works() {
    #[derive(MetaStruct)]
    pub struct Inner {
        /// data doc
        pub data: i32,
    }
    pub struct Outer {
        pub inner: Inner,
    }
    let outer = Outer {
        inner: Inner { data: -1 },
    };
    let meta_field = metastruct::sub_field!(<Outer>::inner, <Inner>::data);

    assert_eq!(*(meta_field.access)(&outer), -1);
}

#[test]
fn test_derive_metastruct_works_for_generics() {
    #[derive(MetaStruct)]
    pub struct Data<T> {
        /// generic field
        pub field: T,
    }
    assert_eq!(Data::<()>::META.field.metadata.label, "generic field");
}

#[test]
fn test_derive_metafields_works_for_generics() {
    #[derive(MetaFields)]
    pub struct Data<T> {
        /// generic field
        pub field: T,
    }
    assert_eq!(Data::<()>::META_FIELDS.0.metadata.label, "generic field");
}
