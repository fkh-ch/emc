//! Static metadata for named fields of structs
#![warn(missing_docs)]

#[doc(hidden)]
pub use metastruct_derive::*;

/// Metadata of a field of a struct
#[derive(PartialEq, Debug, Clone)]
pub struct MetaData {
    /// Identifier of the field
    pub ident: &'static str,
    /// Symbol
    pub symbol: Option<&'static str>,
    /// Label
    pub label: &'static str,
    /// Unit
    pub unit: Option<&'static str>,
    /// Description
    pub description: Option<&'static str>,
}

impl MetaData {
    /// Create a new [MetaData] instance
    pub const fn new(ident: &'static str, label: &'static str) -> Self {
        Self {
            ident,
            label,
            symbol: None,
            unit: None,
            description: None,
        }
    }
    /// Set a symbol
    pub const fn symbol(self, symbol: &'static str) -> Self {
        Self {
            symbol: Some(symbol),
            ..self
        }
    }
    /// Set a unit
    pub const fn unit(self, unit: &'static str) -> Self {
        Self {
            unit: Some(unit),
            ..self
        }
    }
    /// Set a description
    pub const fn description(self, description: &'static str) -> Self {
        Self {
            description: Some(description),
            ..self
        }
    }
}

/// Metadata and mutable / immutable accessors of a field of a struct
#[derive(Debug, Clone)]
pub struct MetaField<S, T> {
    /// Metadata
    pub metadata: MetaData,
    /// Read only access
    pub access: fn(&S) -> &T,
    /// Write access
    pub access_mut: fn(&mut S) -> &mut T,
}

/// Metadata and immutable accessor of a field of a struct
#[derive(Debug, Clone)]
pub struct MetaFieldReadOnly<S, T> {
    /// Metadata
    pub metadata: MetaData,
    /// Read only access
    pub access: fn(&S) -> &T,
}

/// Collection of [MetaField] instances for a subset of fields or all fields of a struct
///
/// In contrast to [MetaStruct], `META_FIELDS` should be filled with a tuple chain:
/// (), (_,), (_, _), (_, (_, _)), (_, (_, (_, _))), ...
pub trait MetaFields: Sized {
    /// Type of [Self::META_FIELDS]
    type FieldsType;
    /// Chain of structs holding the info about the fields of `Self`
    const META_FIELDS: Self::FieldsType;
}

/// Collection of [MetaField] instances for a subset of fields or all fields of a struct
///
/// In contrast to [MetaFields], `META` should be filled with a struct
pub trait MetaStruct: Sized {
    /// Type of [Self::META]
    type MetaType;
    /// Struct holding info about the fields of `Self` as fields named
    /// after the fields of `Self`
    const META: Self::MetaType;
}

/// Static info about the type held by e.g. [MetaField]
pub trait MetaType {
    /// Type held by `Self`
    type Type;
}
impl<S, T> MetaType for MetaField<S, T> {
    type Type = T;
}

/// Create a [MetaField] instance which accesses a subfield of a field
#[macro_export]
macro_rules! sub_field {
    (<$outer_type:ty>:: $inner:ident, <$inner_type:ty>:: $sub_field:ident) => {{
        metastruct::MetaField::<$outer_type, _> {
            metadata: <$inner_type as metastruct::MetaStruct>::META
                .$sub_field
                .metadata,
            access: |s| {
                (<$inner_type as metastruct::MetaStruct>::META
                    .$sub_field
                    .access)(&s.$inner)
            },
            access_mut: |s| {
                (<$inner_type as metastruct::MetaStruct>::META
                    .$sub_field
                    .access_mut)(&mut s.$inner)
            },
        }
    }};
    (<$outer_type:ty>:: $middle:ident, <$middle_type:ty>:: $inner:ident, <$inner_type:ty>:: $sub_field:ident) => {{
        metastruct::MetaField::<$outer_type, _> {
            metadata: <$inner_type as metastruct::MetaStruct>::META
                .$sub_field
                .metadata,
            access: |s| {
                let medium =
                    (<$middle_type as metastruct::MetaStruct>::META.$inner.access)(&s.$middle);
                (<$inner_type as metastruct::MetaStruct>::META
                    .$sub_field
                    .access)(medium)
            },
            access_mut: |s| {
                let medium = (<$middle_type as metastruct::MetaStruct>::META
                    .$inner
                    .access_mut)(&mut s.$middle);
                (<$inner_type as metastruct::MetaStruct>::META
                    .$sub_field
                    .access_mut)(medium)
            },
        }
    }};
}
