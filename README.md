<div align="center">

![logo](./docs/static/img/logo.svg){width=25%}

[![pipeline status](https://gitlab.com/fkh-ch/emc/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/fkh-ch/emc/-/commits/main)
[![Checked with clippy](https://img.shields.io/badge/clippy-checked-blue)](https://doc.rust-lang.org/stable/clippy/usage.html)
[![License](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# EMC programs

Simulation tool to predict the electromagnetic
fields and the corona sound levels at ground for overhead power lines.

[Install](#installation) •
[Usage](#usage) •
[Docs](https://fkh-ch.gitlab.io/emc/ )

</div>

## Install

Prerequisites: [Rust](https://www.rust-lang.org/).
Once installed, clone the git repository and run

```sh
cargo build --release
```

This will create binaries, e.g. `ebl-cli` (with an `.exe` extension on
windows) in the `target/release/` subfolder.

## Usage

### EBL

```sh
ebl-cli <PATH>
```

where `<PATH>` leads to an EBL project json file.

**xml -> json converter**: The `xml2json` / `xml2json.exe` tool can
convert from the legady xml format to EBL project files:

```sh
xml2json <PATH> # <- path to an xml file
```

This will create a json file with the same basename as the input file
and a `.json` extension (example: `project.xml` -> `project.json`).

## Development
### Building docs

everything in docstrings that is between dollar signs `$E = mc^2$` will be interpreted as inline latex math.

For multiline enclose in double dollar signs:

```md
$$
f(x) = \int_{-\infty}^\infty
\hat f(\xi)\,e^{2 \pi i \xi x}
\,d\xi
$$
```

To successfully build the docs follow these commands

```sh
# use the alias (includes the --no-deps flag)
cargo d
# or do this
cargo doc --no-deps
```
