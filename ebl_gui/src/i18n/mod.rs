//! GUI internationalization (i18n)

mod dict;

#[derive(Debug, Clone, PartialEq)]
pub struct Language {
    /// ISO 639-1 Code (alpha-2)
    pub code: &'static str,
    pub country_code: &'static str,
}

pub const DE_CH: Language = Language {
    country_code: "de-CH",
    code: "de",
};
pub const EN_US: Language = Language {
    country_code: "en-US",
    code: "en",
};

pub const LANGUAGES: &[Language] = &[DE_CH, EN_US];

#[derive(Debug, Clone)]
pub struct Text {
    pub text: &'static str,
    translations: [&'static str; LANGUAGES.len()],
}

#[macro_export]
macro_rules! i18n_lookup {
    ( $s:expr ) => {{
        const TEXT: &'static i18n::Text = match i18n::lookup_dict_static($s) {
            Some(s) => s,
            None => {
                panic!("Not in dictionary");
            }
        };
        TEXT
    }};
}

#[macro_export]
macro_rules! i18n_lookup_opt {
    ( $s:expr ) => {{
        const TEXT: Option<&'static i18n::Text> = match i18n::lookup_dict_static_opt($s) {
            Some(Some(s)) => Some(s),
            Some(None) => {
                panic!("Not in dictionary");
            }
            None => None,
        };
        TEXT
    }};
}

pub(super) use i18n_lookup as lookup;
pub(super) use i18n_lookup_opt as lookup_opt;

pub const fn lookup_dict_static(text: &'static str) -> Option<&'static Text> {
    let mut index = 0;
    while index < dict::DICTIONARY.len() {
        if const_str_equal(text, dict::DICTIONARY[index].text) {
            return Some(&dict::DICTIONARY[index]);
        }
        index += 1;
    }
    None
}

pub const fn lookup_dict_static_opt(text: Option<&'static str>) -> Option<Option<&'static Text>> {
    match text {
        Some(text) => Some(lookup_dict_static(text)),
        None => None,
    }
}

impl Text {
    pub const fn new(text: &'static str) -> Self {
        Self {
            text,
            translations: [""; LANGUAGES.len()],
        }
        .with_translation(&EN_US, text)
    }
    pub const fn with_translation(mut self, lang: &'static Language, text: &'static str) -> Self {
        let index = match index_of(lang.country_code) {
            Some(index) => index,
            None => {
                panic!("Invalid language");
            }
        };
        self.translations[index] = text;
        self
    }
    pub const fn translate(&self, lang: &'static Language) -> &'static str {
        let index = index_of(lang.country_code);
        if let Some(index) = index {
            self.translations[index]
        } else {
            self.text
        }
    }
}

pub fn get_language(country_code: &str) -> Option<&'static Language> {
    index_of(country_code).map(|index| &LANGUAGES[index])
}

const fn index_of(country_code: &str) -> Option<usize> {
    let mut index = 0;
    while index < LANGUAGES.len() {
        if const_str_equal(country_code, LANGUAGES[index].country_code) {
            return Some(index);
        }
        index += 1;
    }
    None
}

const fn const_str_equal(lhs: &str, rhs: &str) -> bool {
    const_bytes_equal(lhs.as_bytes(), rhs.as_bytes())
}

const fn const_bytes_equal(lhs: &[u8], rhs: &[u8]) -> bool {
    if lhs.len() != rhs.len() {
        return false;
    }
    let mut i = 0;
    while i < lhs.len() {
        if lhs[i] != rhs[i] {
            return false;
        }
        i += 1;
    }
    true
}
