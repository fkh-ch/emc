//! Translations used in the GUI

use super::{Text, DE_CH, EN_US};

pub const DICTIONARY: &[Text] = &[
    Text::new("New project").with_translation(&DE_CH, "Neues Projekt"),
    Text::new("<new project>").with_translation(&DE_CH, "<Neues Projekt>"),
    Text::new("Open project").with_translation(&DE_CH, "Projekt öffnen"),
    Text::new("Open a json file...").with_translation(&DE_CH, "json Datei öffnen..."),
    Text::new("Save project").with_translation(&DE_CH, "Projekt speichern"),
    Text::new("Save project as").with_translation(&DE_CH, "Projekt speichern unter"),
    Text::new("Save a project").with_translation(&DE_CH, "Projekt speichern"),
    Text::new("Clear error").with_translation(&DE_CH, "Fehlernachricht entfernen"),
    Text::new("Compute and Export").with_translation(&DE_CH, "Rechnen und exportieren"),
    Text::new("Conductor Geometry").with_translation(&DE_CH, "Leitergeometrie"),
    Text::new("Reference level for conductor configuration")
        .with_translation(&EN_US, "Reference level y_ref for vertical positions of conductors")
        .with_translation(&DE_CH, "Referenzniveau y_ref für vertikale Positionen von Leitern"),
    Text::new("Number of sampling points for transverse profile")
        .with_translation(&DE_CH, "Anzahl Profilstützpunkte Querprofil"),
    Text::new("x range transverse profile").with_translation(&DE_CH, "x-Intervall Querprofil"),
    Text::new("Slope of ground in the direction of the transverse profile")
        .with_translation(&DE_CH, "Geländeneigungswinkel in Querprofilrichtung"),
    Text::new("Hight of profile above ground").with_translation(&DE_CH, "Höhe Profil über Boden"),
    Text::new("Resistivity of ground").with_translation(&DE_CH, "Spezifischer Erdbodenwiderstand"),
    Text::new("AC frequency used for computing induced currents")
        .with_translation(&DE_CH, "AC Frequenz für die Berechnung induzierter Ströme"),
    Text::new("Guard Wires").with_translation(&DE_CH, "Erdseile"),
    Text::new("Guard Wire").with_translation(&DE_CH, "Erdseil"),
    Text::new("Add a guard wire").with_translation(&DE_CH, "Erdseil hinzufügen"),
    Text::new("Remove this guard wire").with_translation(&DE_CH, "Dieses Erdseil entfernen"),
    Text::new("System").with_translation(&DE_CH, "System"),
    Text::new("Systems").with_translation(&DE_CH, "Systeme"),
    Text::new("Add a system").with_translation(&DE_CH, "System hinzufügen"),
    Text::new("Remove this system").with_translation(&DE_CH, "Dieses System entfernen"),
    Text::new("x coordinate")
        .with_translation(&EN_US, "x")
        .with_translation(&DE_CH, "x"),
    Text::new("y coordinate")
        .with_translation(&EN_US, "y (relative to y_ref)")
        .with_translation(&DE_CH, "y (relativ zu y_ref)"),
    Text::new("y [m] (relative to y_ref)")
        .with_translation(&DE_CH, "y [m] (relativ zu y_ref)"),
    Text::new("Conductor ⌀")
        .with_translation(&EN_US, "Diameter")
        .with_translation(&DE_CH, "Durchmesser"),
    Text::new("Conductor resistance per length")
        .with_translation(&EN_US, "R_i")
        .with_translation(&DE_CH, "R_i"),
    Text::new("System voltage / currents").with_translation(&DE_CH, "Systemspannung/-Strom"),
    Text::new("System voltage").with_translation(&DE_CH, "Systemspannung"),
    Text::new("System current").with_translation(&DE_CH, "Systemstrom"),
    Text::new("Phase shift of voltage").with_translation(&DE_CH, "Phasenwinkel Spannung"),
    Text::new("Phase shift of current").with_translation(&DE_CH, "Phasenwinkel Strom"),
    Text::new("Cable shielding").with_translation(&DE_CH, "Kabelschirmung"),
    Text::new("Resistance per length of shields").with_translation(&DE_CH, "Widerstand pro Länge"),
    Text::new("⌀ of shields").with_translation(&EN_US,  "Diameter").with_translation(&DE_CH, "Durchmesser"),
    Text::new("# partial conductors per phase in system")
        .with_translation(&EN_US, "Partial conductors per bundle")
        .with_translation(&DE_CH, "Leiterseile pro Bündel"),
    Text::new("Distance between 2 partial conductors").with_translation(&DE_CH, "Abstand Teilleiter"),
    Text::new("⌀ of partial conductors").with_translation(&EN_US,  "Diameter").with_translation(&DE_CH, "Durchmesser"),
    Text::new("Starting angle for 1st partial conductor")
        .with_translation(&DE_CH, "Startwinkel 1. Teilleiter"),
    Text::new("Bundles").with_translation(&DE_CH, "Bündel"),
    Text::new("Add a bundle").with_translation(&DE_CH, "Bündel hinzufügen"),
    Text::new("Remove last bundle").with_translation(&DE_CH, "Letztes Bündel entfernen"),
    Text::new("Transverse Profile").with_translation(&DE_CH, "Querprofil"),
    Text::new("Weather / Environment").with_translation(&DE_CH, "Wetter / Umgebung"),
    Text::new("Altitude above sea level").with_translation(&DE_CH, "Höhe über Meeresspiegel"),
    Text::new("Atmospheric air absorption")
        .with_translation(&DE_CH, "Atmosphärische Luft-Absorption"),
    Text::new("Relative rain duration t_i/t_0 at night with r≥0.2 mm/h").with_translation(
        &DE_CH,
        "Relative Regendauer t_i/t_0 über Nacht mit r≥0.2 mm/h",
    ),
    Text::new("Rain Rate").with_translation(&DE_CH, "Regenrate"),
    Text::new("Dry").with_translation(&DE_CH, "Trocken"),
    Text::new("Fog / Wet").with_translation(&DE_CH, "Nebel / feucht"),
    Text::new("Light Rain").with_translation(&DE_CH, "Leichter Regen"),
    Text::new("Heavy Rain").with_translation(&DE_CH, "Heavy Regen"),
    Text::new("Compute Corona Sound").with_translation(&DE_CH, "Corona-Schall rechnen"),
    Text::new("Compute Annual Assessment").with_translation(&DE_CH, "Jahresbeurteilung rechnen"),
    Text::new("To determine the annual assessment level, the emission level for a rainfall rate of 1 mm/h is calculated using the BPA-mod method. This level is time-weighted and offset with the following correction levels.").with_translation(&DE_CH, "Zur Bestimmung des Jahresbeurteilungspegels wird der Emissionspegel für eine Niederschlagsrate von 1 mm/h mittels der BPA-mod Methode berechnet. Dieser Pegel wird Zeit-gewichtet und mit den folgenden Korrektur-Werten angepasse."),
    Text::new("Correction for industrial and commercial noise (fixed): 5 dB(A)").with_translation(&DE_CH, "Korrektur für industriellen und kommerziellen Schall: 5 dB(A) (fix)"),
    Text::new("Correction for audible pulse content (not applicable, fixed): 0 dB(A)").with_translation(&DE_CH, "Korrektur für hörbaren Puls-Gehalt: 0 dB(A) (nicht anwendbar, fix)"),
    Text::new("Correction for audible tonality content").with_translation(&DE_CH, "Korrektur für hörbaren tonalen Schall-Anteil"),
    Text::new("Plot subgrid").with_translation(&DE_CH, "Untergitter"),
    Text::new("Plots").with_translation(&DE_CH, "Plots"),
    Text::new("Enable 2D plots").with_translation(&DE_CH, "2D Plots ausgeben"),
    Text::new("Field Lines").with_translation(&DE_CH, "Feldlinien"),
    Text::new("Contour Lines").with_translation(&DE_CH, "Isolinien"),
    Text::new("Levels").with_translation(&DE_CH, "Niveaus"),
    Text::new("Level colors").with_translation(&DE_CH, "Niveau-Farben"),
    Text::new("Remove this level").with_translation(&DE_CH, "Diesen Niveau-Wert entfernen"),
    Text::new("Remove this color").with_translation(&DE_CH, "Diese Farbe entfernen"),
    Text::new("override").with_translation(&DE_CH, "manuell"),
    Text::new("Add a level").with_translation(&DE_CH, "Niveau-Wert hinzufügen"),
    Text::new("Add a color").with_translation(&DE_CH, "Farbe hinzufügen"),
    Text::new("Plots / Tables").with_translation(&DE_CH, "Diagramme / Tabellen"),
    Text::new("Total width of the resulting plot")
        .with_translation(&EN_US, "Total image width")
        .with_translation(&DE_CH, "Totale Bildbreite"),
    Text::new("x range of the plot").with_translation(&DE_CH, "x-Intervall Plot"),
    Text::new("y range of the plot").with_translation(&DE_CH, "y-Intervall Plot"),
    Text::new("Radius around singularities where field lines should start / stop")
        .with_translation(&EN_US, "Radius around singularities")
        .with_translation(&DE_CH, "Radius um Singularitäten"),
    Text::new("Number of field lines around source singularities")
        .with_translation(&DE_CH, "Anzahl Feldlinien ausgehend von Singularitäten"),
    Text::new("Resolution").with_translation(&DE_CH, "Auflösung"),
    Text::new("Contour lines with a diameter greater than resolution are resolvable")
        .with_translation(
            &DE_CH,
            "Isolinien mit einem Durchmesser grösser als die Auflösung sind detektierbar",
        ),
    Text::new("Add a new figure").with_translation(&DE_CH, "Ein neues Diagramm hinzufügen"),
    Text::new("Figure").with_translation(&DE_CH, "Diagramm"),
    Text::new("Electric Field").with_translation(&DE_CH, "Elektrisches Feld"),
    Text::new("Magnetic Field").with_translation(&DE_CH, "Magnetfeld"),
    Text::new("Electric Potential").with_translation(&DE_CH, "Elektrisches Potential"),
    Text::new("Current").with_translation(&DE_CH, "Strom"),
    Text::new("Magnetic Vector Potential").with_translation(&DE_CH, "Magnetisches Vektorpotential"),
    Text::new("Graph E Field").with_translation(&DE_CH, "Graph E-Feld"),
    Text::new("Graph B Field").with_translation(&DE_CH, "Graph B-Feld"),
    Text::new("Render a graph of the E field").with_translation(&DE_CH, "E-Feld Graph erstellen"),
    Text::new("Render a graph of the B field").with_translation(&DE_CH, "B-Feld Graph erstellen"),
    Text::new("Render a conductor cross-section diagram")
        .with_translation(&DE_CH, "Leiterquerschnittsbild erstellen"),
    Text::new("Include |E| column").with_translation(&DE_CH, "Spalte |E| hinzufügen"),
    Text::new("Include columns for re(E_x), im(E_x), re(E_y), im(E_y)").with_translation(
        &DE_CH,
        "Spalten re(E_x), im(E_x), re(E_y), im(E_y) hinzufügen",
    ),
    Text::new("Include |B| column").with_translation(&DE_CH, "Spalte |B| hinzufügen"),
    Text::new("Include columns for re(B_x), im(B_x), re(B_y), im(B_y)").with_translation(
        &DE_CH,
        "Spalten re(B_x), im(B_x), re(B_y), im(B_y) hinzufügen",
    ),
    Text::new("Conductor cross-section").with_translation(&DE_CH, "Leiterquerschnittsbild"),
    Text::new("Table").with_translation(&DE_CH, "Tabelle"),
];
