use crate::error::{IoError, SerializationError};
use crate::i18n;
use ebl::project::ContourLineLevel;

#[derive(Debug, Default, serde::Serialize, serde::Deserialize)]
pub struct Config {
    #[serde(with = "serde_language", default)]
    pub language: Option<&'static i18n::Language>,
    #[serde(default)]
    pub contour_levels: ContourLevels,
}

#[derive(Debug, Default, serde::Serialize, serde::Deserialize)]
pub struct ContourLevels {
    #[serde(default)]
    pub e: Option<Vec<ContourLineLevel>>,
    #[serde(default)]
    pub b: Option<Vec<ContourLineLevel>>,
    #[serde(default)]
    pub u: Option<Vec<ContourLineLevel>>,
    #[serde(default)]
    pub a: Option<Vec<ContourLineLevel>>,
}

impl Config {
    /// Tries:
    /// - ${XDG_CONFIG_HOME}/ebl/config.json
    /// - ${HOME}/.config/ebl/config.json
    #[cfg(not(target_os = "windows"))]
    fn path() -> Option<std::path::PathBuf> {
        std::env::var("XDG_CONFIG_HOME")
            .map(|p| std::path::PathBuf::from(p).join(".config"))
            .ok()
            .or_else(|| home_path(".config"))
            .map(|p| p.join("ebl/config.json"))
    }

    #[cfg(target_os = "windows")]
    fn path() -> Option<std::path::PathBuf> {
        let exe = std::env::args().next()?;
        Some(std::path::PathBuf::from(exe).parent()?.join("config.json"))
    }

    pub fn from_path(
        path: &std::path::Path,
    ) -> Result<Result<Self, SerializationError>, std::io::Error> {
        from_path(path, "Load config file")
    }

    pub fn from_config_dir() -> Result<Option<Self>, SerializationError> {
        let Some(config_path) = Self::path() else {
            return Ok(None);
        };
        Self::from_path(&config_path).ok().transpose()
    }
}

#[derive(Debug, Default, serde::Serialize, serde::Deserialize)]
pub struct Cache {
    #[serde(default)]
    pub dialog_path: Option<std::path::PathBuf>,
}

impl Cache {
    /// Tries:
    /// - ${HOME}/.config/ebl/config.json
    #[cfg(not(target_os = "windows"))]
    fn path() -> Option<std::path::PathBuf> {
        home_path(".cache/ebl/cache.json")
    }

    #[cfg(target_os = "windows")]
    fn path() -> Option<std::path::PathBuf> {
        let exe = std::env::args().next()?;
        Some(std::path::PathBuf::from(exe).parent()?.join("cache.json"))
    }

    pub fn from_path(
        path: &std::path::Path,
    ) -> Result<Result<Self, SerializationError>, std::io::Error> {
        from_path(path, "Load cache file")
    }

    pub fn from_config_dir() -> Result<Option<Self>, SerializationError> {
        let Some(cache_path) = Self::path() else {
            return Ok(None);
        };
        Self::from_path(&cache_path).ok().transpose()
    }

    pub fn write(&self) -> Result<(), IoError> {
        let Some(path) = Self::path() else {
            return Ok(());
        };
        if let Some(parent) = path.parent() {
            std::fs::create_dir_all(parent).map_err(|e| IoError {
                message: "Create cache directory",
                path: parent.to_owned(),
                detail: format!("{e}"),
            })?;
        }
        let file = std::fs::File::create(&path).map_err(|e| IoError {
            message: "Write to cache file",
            path: path.clone(),
            detail: format!("{e}"),
        })?;
        serde_json::to_writer(file, self).map_err(|e| IoError {
            message: "Write to cache file",
            path: path.clone(),
            detail: format!("{e}"),
        })?;
        Ok(())
    }

    pub fn set_dir(&mut self, path: Option<std::path::PathBuf>) -> Result<(), IoError> {
        self.dialog_path = path;
        self.write()
    }
}

fn from_path<T>(
    path: &std::path::Path,
    message: &'static str,
) -> Result<Result<T, SerializationError>, std::io::Error>
where
    for<'de> T: serde::Deserialize<'de>,
{
    let content = match std::fs::read_to_string(path) {
        Err(err) if err.kind() == std::io::ErrorKind::NotFound => {
            return Err(err);
        }
        Err(err) => {
            return Ok(Err(SerializationError {
                path: path.to_owned(),
                message,
                column: 0,
                line: 0,
                detail: format!("{err}"),
            }));
        }
        Ok(content) => content,
    };
    match serde_json::from_str(&content) {
        Ok(config) => Ok(Ok(config)),
        Err(err) => Ok(Err(SerializationError {
            path: path.to_owned(),
            message,
            column: err.column(),
            line: err.line(),
            detail: format!("{err}"),
        })),
    }
}

#[cfg(not(target_os = "windows"))]
fn home_path(sub_path: &str) -> Option<std::path::PathBuf> {
    let home_dir = std::env::var("HOME").ok()?;
    Some(std::path::PathBuf::from(home_dir).join(sub_path))
}

mod serde_language {
    use crate::i18n;
    pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<&'static i18n::Language>, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let code = <Option<String> as serde::Deserialize>::deserialize(deserializer)?;
        code.map(|code| {
            i18n::get_language(&code).ok_or(serde::de::Error::custom(format!(
                "Unsupported language: {code}"
            )))
        })
        .transpose()
    }
    pub fn serialize<S>(
        language: &Option<&'static i18n::Language>,
        serializer: S,
    ) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        use serde::Serialize;
        language.map(|l| l.country_code).serialize(serializer)
    }
}
