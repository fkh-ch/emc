#[derive(Debug, Clone)]
pub enum Error {
    Io(IoError),
    Serialization(SerializationError),
    Compute(String),
}

impl From<ebl::Error> for Error {
    fn from(value: ebl::Error) -> Self {
        Self::Compute(format!("{value}"))
    }
}

#[derive(Debug, Clone)]
pub struct IoError {
    pub message: &'static str,
    pub path: std::path::PathBuf,
    pub detail: String,
}

impl std::fmt::Display for IoError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let Self {
            detail,
            path,
            message,
        } = self;
        let path = path.to_string_lossy();
        write!(f, "{message} {path}: {detail}")
    }
}

#[derive(Debug, Clone)]
pub struct SerializationError {
    pub message: &'static str,
    pub path: std::path::PathBuf,
    pub detail: String,
    pub line: usize,
    pub column: usize,
}

impl std::fmt::Display for SerializationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let Self {
            detail,
            path,
            message,
            line,
            column,
        } = self;
        let path = path.to_string_lossy();
        write!(f, "{message} {path}:{line},{column}: {detail}")
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Io(error) => write!(f, "{error}"),
            Self::Serialization(error) => write!(f, "{error}"),
            Self::Compute(error) => write!(f, "{error}"),
        }
    }
}

impl From<IoError> for Error {
    fn from(value: IoError) -> Self {
        Self::Io(value)
    }
}

impl From<SerializationError> for Error {
    fn from(value: SerializationError) -> Self {
        Self::Serialization(value)
    }
}
