//! Widget drawing a quad

use iced::{
    advanced::{
        layout::{Limits, Node},
        renderer,
        widget::Tree,
        Layout, Widget,
    },
    Color, Length,
};

/// Widget drawing a quad
#[derive(Debug)]
pub struct Quad {
    /// Width of the quad
    pub width: Length,
    /// Height of the quad
    pub height: Length,

    /// Color of the quad
    pub color: Color,
    /// Border of the quad
    pub border: iced::Border,
    /// Shadow of the quad
    pub shadow: iced::Shadow,
}

impl Quad {
    pub fn square(size: Length, color: Color) -> Self {
        Self {
            width: size,
            height: size,
            color,
            ..Default::default()
        }
    }
}

impl Default for Quad {
    fn default() -> Self {
        Self {
            width: Length::Fill,
            height: Length::Fill,

            color: Color::from([0.5; 3]),
            border: iced::Border {
                color: Color::TRANSPARENT,
                width: 0.0,
                radius: 0.0.into(),
            },
            shadow: iced::Shadow::default(),
        }
    }
}

impl<Message, Theme, Renderer> Widget<Message, Theme, Renderer> for Quad
where
    Renderer: renderer::Renderer,
{
    fn size(&self) -> iced::Size<Length> {
        iced::Size::new(self.width, self.height)
    }

    fn layout(&self, _tree: &mut Tree, _renderer: &Renderer, limits: &Limits) -> Node {
        let limits = limits.width(self.width).height(self.height);
        Node::new(limits.max())
    }

    fn draw(
        &self,
        _state: &Tree,
        renderer: &mut Renderer,
        _theme: &Theme,
        _style: &renderer::Style,
        layout: Layout<'_>,
        _cursor: iced::mouse::Cursor,
        viewport: &iced::Rectangle,
    ) {
        let bounds = layout.bounds();
        if bounds.intersects(viewport) {
            renderer.fill_quad(
                renderer::Quad {
                    bounds,
                    border: self.border,
                    shadow: self.shadow,
                },
                self.color,
            );
        }
    }
}

impl<'a, Message, Theme, Renderer> From<Quad> for iced::Element<'a, Message, Theme, Renderer>
where
    Renderer: 'a + renderer::Renderer,
    Theme: 'a,
{
    fn from(value: Quad) -> Self {
        Self::new(value)
    }
}
