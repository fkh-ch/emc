//! Emoji icon widget

use iced::{
    advanced::{
        layout::{Limits, Node},
        renderer,
        text::{self, paragraph, Text},
        widget::{tree, Tree},
        Layout, Widget,
    },
    Length,
};

/// Emoji icon widget
#[derive(Debug)]
pub struct Icon<Renderer = iced::Renderer>
where
    Renderer: text::Renderer,
{
    /// Width and height of the icon
    pub size: Option<iced::Pixels>,
    /// Font used to display the `code_point`.
    pub font: Renderer::Font,
    /// Unicode code point used as the icon.
    pub code_point: char,
}

impl Icon<iced::Renderer> {
    pub fn new(code_point: char) -> Self {
        Self {
            size: None,
            font: iced::Font::default(),
            code_point,
        }
    }
    pub fn size(self, size: iced::Pixels) -> Self {
        Self {
            size: Some(size),
            ..self
        }
    }
}

impl<Message, Theme, Renderer> Widget<Message, Theme, Renderer> for Icon<Renderer>
where
    Renderer: text::Renderer,
{
    fn size(&self) -> iced::Size<Length> {
        iced::Size {
            width: Length::Shrink,
            height: Length::Shrink,
        }
    }

    fn tag(&self) -> tree::Tag {
        tree::Tag::of::<State<Renderer::Paragraph>>()
    }

    fn state(&self) -> tree::State {
        tree::State::new(State::<Renderer::Paragraph>::default())
    }

    fn layout(&self, tree: &mut Tree, renderer: &Renderer, limits: &Limits) -> Node {
        let state = tree.state.downcast_mut::<State<Renderer::Paragraph>>();
        let mut content = [0; 4];
        let line_height = text::LineHeight::default();
        let size = self.size.unwrap_or_else(|| renderer.default_size());
        let height = line_height.to_absolute(size);
        let text_bounds = limits.resolve(size, height, iced::Size::ZERO);

        let icon_text = Text {
            line_height,
            content: self.code_point.encode_utf8(&mut content) as &_,
            font: self.font,
            size,

            bounds: iced::Size::new(f32::INFINITY, text_bounds.height),
            horizontal_alignment: iced::alignment::Horizontal::Center,
            vertical_alignment: iced::alignment::Vertical::Center,
            shaping: text::Shaping::Advanced,
            wrapping: text::Wrapping::default(),
        };
        state.icon.update(icon_text);
        let icon_width = state.icon.min_width();
        Node::new(iced::Size::new(icon_width, text_bounds.height))
    }

    fn draw(
        &self,
        tree: &Tree,
        renderer: &mut Renderer,
        _theme: &Theme,
        _style: &renderer::Style,
        layout: Layout<'_>,
        _cursor: iced::mouse::Cursor,
        viewport: &iced::Rectangle,
    ) {
        let state = tree.state.downcast_ref::<State<Renderer::Paragraph>>();
        renderer.fill_paragraph(
            state.icon.raw(),
            layout.bounds().center(),
            _style.text_color,
            *viewport,
        );
    }
}

impl<'a, Message, Theme, Renderer> From<Icon<Renderer>>
    for iced::Element<'a, Message, Theme, Renderer>
where
    Renderer: 'a + text::Renderer,
    Theme: 'a,
{
    fn from(value: Icon<Renderer>) -> Self {
        Self::new(value)
    }
}

/// The state of an [`Icon`].
#[derive(Debug, Default, Clone)]
pub struct State<P: text::Paragraph> {
    icon: paragraph::Plain<P>,
}
