//! Display fields that can only be filled with a specific type.

use iced::advanced::{
    layout::{Layout, Limits, Node},
    widget::{tree::Tag, Operation, Tree, Widget},
    Shell,
};

pub trait StyleSheet:
    iced::widget::text_input::Catalog + iced::widget::text::Catalog + iced::widget::button::Catalog
{
}

impl<
        T: iced::widget::text_input::Catalog
            + iced::widget::text::Catalog
            + iced::widget::button::Catalog,
    > StyleSheet for T
{
}

#[derive(Debug, Clone)]
struct InternalOnInput(String);

/// Field which fires a `on_change` whenever the entered text is
/// parseable to the specified type.
pub struct ParsedInput<'a, T, Message, Theme = iced::Theme, Renderer = iced::Renderer>
where
    Renderer: iced::advanced::text::Renderer,
    Theme: StyleSheet,
{
    /// The underlying element of the [`ParsedInput`].
    text_input: iced::widget::text_input::TextInput<'a, InternalOnInput, Theme, Renderer>,
    on_change: Option<Box<dyn 'a + Fn(T) -> Message>>,
    text: String,
    value: T,
    new_value: ParseResult<T>,
}

enum ParseResult<T> {
    Ok(T),
    Error,
    Unchanged,
}

impl<'a, T, Message, Theme, Renderer> ParsedInput<'a, T, Message, Theme, Renderer>
where
    Message: Clone,
    Renderer: iced::advanced::text::Renderer,
    Theme: StyleSheet,
{
    #[must_use]
    pub fn new<F>(value: T, on_change: Option<F>) -> Self
    where
        T: 'a + std::fmt::Display + std::str::FromStr + Clone + Default,
        F: 'a + Fn(T) -> Message,
    {
        let text_input = iced::widget::text_input::TextInput::new("", &value.to_string())
            .width(iced::Length::Fixed(127.0));
        let text_input = if on_change.is_some() {
            text_input.on_input(InternalOnInput)
        } else {
            text_input
        };
        Self {
            text_input,
            text: value.to_string(),
            value,
            on_change: if let Some(cb) = on_change {
                Some(Box::new(cb))
            } else {
                None
            },
            new_value: ParseResult::Unchanged,
        }
    }
    fn reset(&mut self)
    where
        T: std::fmt::Display,
    {
        self.text = self.value.to_string();
        self.new_value = ParseResult::Unchanged;
    }

    /// Sets the [Id](iced::widget::text_input::Id) of the internal [`iced::widget::TextInput`]
    #[must_use]
    pub fn id(mut self, id: iced::widget::text_input::Id) -> Self {
        self.text_input = self.text_input.id(id);
        self
    }

    /// Sets the [Font](iced::advanced::text::Renderer::Font) of the [`ParsedInput`].
    #[must_use]
    pub fn font(mut self, font: Renderer::Font) -> Self {
        self.text_input = self.text_input.font(font);
        self
    }

    /// Sets the width of the [`ParsedInput`].
    #[must_use]
    pub fn width(mut self, width: impl Into<iced::Length>) -> Self {
        self.text_input = self.text_input.width(width);
        self
    }

    /// Sets the padding of the [`ParsedInput`].
    #[must_use]
    pub fn padding(mut self, padding: impl Into<iced::Padding>) -> Self {
        self.text_input = self.text_input.padding(padding);
        self
    }

    /// Sets the text size of the [`ParsedInput`].
    #[must_use]
    pub fn size(mut self, size: impl Into<iced::Pixels>) -> Self {
        self.text_input = self.text_input.size(size);
        self
    }

    /// Sets the [`text::LineHeight`](iced::widget::text::LineHeight) of the [`ParsedInput`].
    #[must_use]
    pub fn line_height(mut self, line_height: impl Into<iced::widget::text::LineHeight>) -> Self {
        self.text_input = self.text_input.line_height(line_height);
        self
    }

    /// Sets the style of the input of the [`ParsedInput`].
    #[must_use]
    pub fn style(
        mut self,
        style: impl Fn(&Theme, iced::widget::text_input::Status) -> iced::widget::text_input::Style + 'a,
    ) -> Self
    where
        <Theme as iced::widget::text_input::Catalog>::Class<'a>:
            From<iced::widget::text_input::StyleFn<'a, Theme>>,
    {
        self.text_input = self.text_input.style(style);
        self
    }

    /// Gets the current value of the [`ParsedInput`].
    pub fn text(&self) -> &T {
        &self.value
    }
}

impl<'a, T, Message, Theme, Renderer> Widget<Message, Theme, Renderer>
    for ParsedInput<'a, T, Message, Theme, Renderer>
where
    Message: 'a + Clone,
    Renderer: 'a + iced::advanced::text::Renderer,
    Theme: StyleSheet,
    T: std::str::FromStr + Clone + PartialEq + std::fmt::Display,
{
    fn tag(&self) -> Tag {
        <iced::widget::text_input::TextInput<_, _, _> as Widget<_, _, _>>::tag(&self.text_input)
    }
    fn state(&self) -> iced::advanced::widget::tree::State {
        <iced::widget::text_input::TextInput<_, _, _> as Widget<_, _, _>>::state(&self.text_input)
    }

    fn children(&self) -> Vec<Tree> {
        <iced::widget::text_input::TextInput<_, _, _> as Widget<_, _, _>>::children(
            &self.text_input,
        )
    }

    fn diff(&self, tree: &mut Tree) {
        <iced::widget::text_input::TextInput<_, _, _> as Widget<_, _, _>>::diff(
            &self.text_input,
            tree,
        );
    }

    fn size(&self) -> iced::Size<iced::Length> {
        <iced::widget::text_input::TextInput<_, _, _> as Widget<_, _, _>>::size(&self.text_input)
    }

    fn layout(&self, tree: &mut Tree, renderer: &Renderer, limits: &Limits) -> Node {
        <iced::widget::text_input::TextInput<_, _, _> as Widget<_, _, _>>::layout(
            &self.text_input,
            tree,
            renderer,
            limits,
        )
    }

    fn draw(
        &self,
        tree: &Tree,
        renderer: &mut Renderer,
        theme: &Theme,
        style: &iced::advanced::renderer::Style,
        layout: Layout<'_>,
        cursor: iced::mouse::Cursor,
        viewport: &iced::Rectangle,
    ) {
        <iced::widget::text_input::TextInput<_, _, _> as Widget<_, _, _>>::draw(
            &self.text_input,
            tree,
            renderer,
            theme,
            style,
            layout,
            cursor,
            viewport,
        );
        let invalid_input = matches!(self.new_value, ParseResult::Error);
        if invalid_input {
            let text_style = iced::widget::text_input::Catalog::style(
                theme,
                &<Theme as iced::widget::text_input::Catalog>::default(),
                iced::widget::text_input::Status::Disabled,
            );
            let warning_bounds = layout.bounds();
            let warning_bounds = iced::Rectangle {
                x: warning_bounds.x + 0.9 * warning_bounds.width,
                width: 0.1 * warning_bounds.width,
                ..warning_bounds
            };
            renderer.fill_text(
                iced::advanced::text::Text {
                    content: "!".to_string(),
                    bounds: iced::Size::new(warning_bounds.width, warning_bounds.height),
                    size: iced::Pixels(renderer.default_size().0),
                    font: renderer.default_font(),
                    horizontal_alignment: iced::alignment::Horizontal::Center,
                    vertical_alignment: iced::alignment::Vertical::Center,
                    line_height: iced::widget::text::LineHeight::Relative(1.3),
                    shaping: iced::advanced::text::Shaping::Advanced,
                    wrapping: iced::widget::text::Wrapping::default(),
                },
                iced::Point::new(warning_bounds.center_x(), warning_bounds.center_y()),
                text_style.value,
                warning_bounds,
            );
        }
    }

    fn mouse_interaction(
        &self,
        tree: &Tree,
        layout: Layout<'_>,
        cursor: iced::mouse::Cursor,
        viewport: &iced::Rectangle,
        renderer: &Renderer,
    ) -> iced::mouse::Interaction {
        <iced::widget::text_input::TextInput<_, _, _> as Widget<_, _, _>>::mouse_interaction(
            &self.text_input,
            tree,
            layout,
            cursor,
            viewport,
            renderer,
        )
    }

    fn operate(
        &self,
        tree: &mut Tree,
        layout: Layout<'_>,
        renderer: &Renderer,
        operation: &mut dyn Operation<()>,
    ) {
        <iced::widget::text_input::TextInput<_, _, _> as Widget<_, _, _>>::operate(
            &self.text_input,
            tree,
            layout,
            renderer,
            operation,
        );
    }

    #[allow(clippy::too_many_lines, clippy::cognitive_complexity)]
    fn on_event(
        &mut self,
        tree: &mut Tree,
        event: iced::Event,
        layout: Layout<'_>,
        cursor: iced::mouse::Cursor,
        renderer: &Renderer,
        clipboard: &mut dyn iced::advanced::Clipboard,
        shell: &mut Shell<Message>,
        viewport: &iced::Rectangle,
    ) -> iced::event::Status {
        let pressed_key = if let iced::Event::Keyboard(iced::keyboard::Event::KeyPressed {
            key: iced::keyboard::Key::Named(key),
            ..
        }) = &event
        {
            Some(*key)
        } else {
            None
        };
        if let Some(iced::keyboard::key::Named::Escape) = &pressed_key {
            self.reset();
        }

        let text_input_state = tree
            .state
            .downcast_ref::<iced::widget::text_input::State<Renderer::Paragraph>>();
        let is_focussed_before = text_input_state.is_focused();
        let mut messages = Vec::new();
        let mut sub_shell = Shell::new(&mut messages);
        let status = self.text_input.on_event(
            tree,
            event,
            layout,
            cursor,
            renderer,
            clipboard,
            &mut sub_shell,
            viewport,
        );
        let text_input_state = tree
            .state
            .downcast_ref::<iced::widget::text_input::State<Renderer::Paragraph>>();
        let is_focussed_after = text_input_state.is_focused();
        let unfocus = is_focussed_before && !is_focussed_after;

        let redraw_request = sub_shell.redraw_request();
        if sub_shell.is_layout_invalid() {
            shell.invalidate_layout();
        }
        if sub_shell.are_widgets_invalid() {
            shell.invalidate_widgets();
        }
        if let Some(redraw) = redraw_request {
            shell.request_redraw(redraw);
        }

        for InternalOnInput(new_text) in messages {
            self.text = new_text;
            if let Ok(value) = T::from_str(&self.text) {
                if self.value != value {
                    self.new_value = ParseResult::Ok(value);
                } else {
                    self.new_value = ParseResult::Unchanged;
                }
            } else {
                self.new_value = ParseResult::Error;
            }
            shell.invalidate_layout();
        }
        if let (ParseResult::Ok(new_value), Some(on_change), true) = (
            &self.new_value,
            &self.on_change,
            (unfocus || pressed_key == Some(iced::keyboard::key::Named::Enter)),
        ) {
            shell.publish(on_change(new_value.clone()));
            return iced::event::Status::Captured;
        }
        if unfocus {
            self.reset();
            shell.invalidate_widgets();
        }
        status
    }
}

impl<'a, T, Message, Theme, Renderer> From<ParsedInput<'a, T, Message, Theme, Renderer>>
    for iced::Element<'a, Message, Theme, Renderer>
where
    Message: 'a + Clone,
    Renderer: 'a + iced::advanced::text::Renderer,
    Theme: StyleSheet + 'a,
    T: 'a + std::str::FromStr + Clone + PartialEq + std::fmt::Display,
{
    fn from(parsed_input: ParsedInput<'a, T, Message, Theme, Renderer>) -> Self {
        iced::Element::new(parsed_input)
    }
}
