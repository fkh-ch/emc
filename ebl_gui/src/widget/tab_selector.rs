pub struct TabSelector<T, Message> {
    tabs: T,
    selected_tab_index: usize,
    select: fn(usize) -> Message,
}

pub struct TabSelectorWithClose<T, Message> {
    tab_selector: TabSelector<T, Message>,
    close: fn(usize) -> Message,
}

pub struct H<T>(pub T);
pub struct V<T> {
    pub tabs: T,
    pub width: iced::Length,
}

impl<T> V<T> {
    pub fn width(self, width: impl Into<iced::Length>) -> Self {
        Self {
            width: width.into(),
            ..self
        }
    }
}

impl<T, Message> H<TabSelector<T, Message>> {
    pub fn with_close(self, close: fn(usize) -> Message) -> H<TabSelectorWithClose<T, Message>> {
        H(TabSelectorWithClose {
            tab_selector: self.0,
            close,
        })
    }
}

pub fn h_tab_selector<T, Message>(
    tabs: T,
    selected_tab_index: usize,
    select: fn(usize) -> Message,
) -> H<TabSelector<T, Message>> {
    H(TabSelector {
        tabs,
        selected_tab_index,
        select,
    })
}

pub fn v_tab_selector<T, Message>(
    tabs: T,
    selected_tab_index: usize,
    select: fn(usize) -> Message,
) -> V<TabSelector<T, Message>> {
    V {
        tabs: TabSelector {
            tabs,
            selected_tab_index,
            select,
        },
        width: iced::Length::Fill,
    }
}

impl<'a, Message, T> From<H<TabSelector<T, Message>>> for iced::Element<'a, Message>
where
    Message: 'a + Clone,
    T: Iterator<Item: iced::widget::text::IntoFragment<'a>>,
{
    fn from(value: H<TabSelector<T, Message>>) -> Self {
        iced::widget::row(
            value
                .0
                .entries()
                .map(|(label, msg)| tab_selector(label, msg).into()),
        )
        .spacing(2)
        .align_y(iced::Alignment::Center)
        .into()
    }
}

impl<'a, Message, T> From<V<TabSelector<T, Message>>> for iced::Element<'a, Message>
where
    Message: 'a + Clone,
    T: Iterator<Item: iced::widget::text::IntoFragment<'a>>,
{
    fn from(value: V<TabSelector<T, Message>>) -> Self {
        iced::widget::column(
            value
                .tabs
                .entries()
                .map(|(label, msg)| tab_selector(label, msg).width(value.width).into()),
        )
        .spacing(2)
        .into()
    }
}

impl<'a, Message, T> From<H<TabSelectorWithClose<T, Message>>> for iced::Element<'a, Message>
where
    Message: 'a + Clone,
    T: Iterator<Item: iced::widget::text::IntoFragment<'a>>,
{
    fn from(value: H<TabSelectorWithClose<T, Message>>) -> Self {
        iced::widget::row(value.0.tab_selector.entries().enumerate().flat_map(
            |(index, (label, msg))| {
                [
                    tab_selector(label, msg).into(),
                    close_button((value.0.close)(index)).into(),
                ]
                .into_iter()
            },
        ))
        .spacing(2)
        .align_y(iced::Alignment::Center)
        .into()
    }
}

impl<T, Message> TabSelector<T, Message>
where
    T: Iterator,
{
    fn entries(self) -> impl Iterator<Item = (<T as Iterator>::Item, Option<Message>)> {
        self.tabs.enumerate().map(move |(index, label)| {
            let message = (self.selected_tab_index != index).then(|| (self.select)(index));
            (label, message)
        })
    }
}

fn close_button<'a, Message: Clone + 'a, Renderer>(
    on_press: Message,
) -> iced::widget::Button<'a, Message, iced::Theme, Renderer>
where
    Renderer: iced::advanced::Renderer + iced::advanced::text::Renderer + 'a,
{
    iced::widget::button::<_, _, Renderer>(iced::widget::text("x"))
        .on_press(on_press)
        .style(iced::widget::button::danger)
}

fn tab_selector<'a, Message: Clone + 'a, Renderer>(
    label: impl iced::widget::text::IntoFragment<'a>,
    on_press: Option<Message>,
) -> iced::widget::Button<'a, Message, iced::Theme, Renderer>
where
    Renderer: iced::advanced::Renderer + iced::advanced::text::Renderer + 'a,
{
    let button = iced::widget::button::<_, _, Renderer>(iced::widget::text(label));
    let button = if let Some(on_press) = on_press {
        button
            .on_press(on_press)
            .style(iced::widget::button::secondary)
    } else {
        button
    };
    button
}
