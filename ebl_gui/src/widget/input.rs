use crate::i18n;
use crate::widget;

pub struct MetaData {
    pub symbol: Option<&'static str>,
    pub label: &'static i18n::Text,
    pub unit: Option<&'static str>,
    pub description: Option<&'static i18n::Text>,
}

pub trait Property<S> {
    type Value;
    fn get(&self, data: &S) -> Self::Value;
    fn set(&self, data: &mut S, new_value: Self::Value);
}

impl<S, T> Property<S> for metastruct::MetaField<S, T>
where
    T: Copy,
{
    type Value = T;
    fn get(&self, data: &S) -> Self::Value {
        *(self.access)(data)
    }
    fn set(&self, data: &mut S, new_value: Self::Value) {
        *(self.access_mut)(data) = new_value;
    }
}

#[derive(Debug, Clone)]
pub struct PropertyUpdate<T, P> {
    pub property: P,
    pub value: T,
}

impl<T, P> PropertyUpdate<T, P> {
    pub fn apply_to<S>(self, state: &mut S)
    where
        P: Property<S, Value = T>,
    {
        self.property.set(state, self.value);
    }
}

pub struct Input<'a, S, P> {
    data: &'a S,
    property: P,
    is_enabled: bool,
}

impl<'a, S, P> Input<'a, S, P> {
    pub fn enabled(self, is_enabled: bool) -> Input<'a, S, P> {
        Input { is_enabled, ..self }
    }
}

pub const fn input<S, P>(data: &S, property: P) -> Input<S, P> {
    Input {
        data,
        property,
        is_enabled: true,
    }
}

impl<'a, Message, S, P> From<Input<'a, S, P>> for iced::Element<'a, Message>
where
    P: 'a + Property<S> + Clone,
    Message: 'a + Clone + From<PropertyUpdate<P::Value, P>>,
    P::Value: std::fmt::Display + std::str::FromStr + Clone + Default + PartialEq,
{
    fn from(value: Input<'a, S, P>) -> Self {
        widget::parsed_input::ParsedInput::new(
            value.property.get(value.data),
            value.is_enabled.then_some(move |v: P::Value| {
                PropertyUpdate {
                    property: value.property.clone(),
                    value: v,
                }
                .into()
            }),
        )
        .into()
    }
}

pub struct LabelledElement<E> {
    pub element: E,
    pub metadata: MetaData,
    pub language: &'static i18n::Language,
}

pub fn labelled<E>(
    element: E,
    label: &'static i18n::Text,
    language: &'static i18n::Language,
) -> LabelledElement<E> {
    LabelledElement {
        element,
        metadata: MetaData {
            label,
            symbol: None,
            unit: None,
            description: None,
        },
        language,
    }
}

impl<E> LabelledElement<E> {
    pub fn symbol(self, symbol: &'static str) -> Self {
        Self {
            metadata: self.metadata.symbol(symbol),
            ..self
        }
    }
    pub fn unit(self, unit: &'static str) -> Self {
        Self {
            metadata: self.metadata.unit(unit),
            ..self
        }
    }
    pub fn description(self, description: &'static i18n::Text) -> Self {
        Self {
            metadata: self.metadata.description(description),
            ..self
        }
    }
}

impl<S, P> LabelledElement<Input<'_, S, P>> {
    pub fn enabled(self, is_enabled: bool) -> Self {
        Self {
            element: self.element.enabled(is_enabled),
            ..self
        }
    }
}

pub const fn labelled_input<'a, S, P>(
    data: &'a S,
    property: P,
    metadata: MetaData,
    language: &'static i18n::Language,
) -> LabelledElement<Input<'a, S, P>> {
    LabelledElement {
        element: Input {
            data,
            property,
            is_enabled: true,
        },
        metadata,
        language,
    }
}

#[macro_export]
macro_rules! labelled_input {
    ($data: expr, $outer: expr, $property: expr, $language: expr) => {{
        $crate::widget::input::labelled_input(
            $data,
            $outer($property),
            metadata!($property),
            $language,
        )
    }};
}

impl<'a, Message, E> From<LabelledElement<E>> for iced::Element<'a, Message>
where
    iced::Element<'a, Message>: From<E>,
    Message: 'a,
{
    fn from(value: LabelledElement<E>) -> Self {
        let text = value.metadata.label.translate(value.language);
        let unit = value
            .metadata
            .unit
            .map(|u| format!("[{u}]"))
            .unwrap_or_default();
        let element = iced::widget::row([
            iced::widget::text(text).width(200.0).into(),
            iced::widget::text(unit).width(50.0).into(),
            value.element.into(),
        ])
        .spacing(10)
        .align_y(iced::Alignment::Center)
        .into();
        if let Some(description) = value.metadata.description {
            iced::widget::tooltip(
                element,
                description.translate(value.language),
                iced::widget::tooltip::Position::Right,
            )
            .style(iced::widget::container::rounded_box)
            .into()
        } else {
            element
        }
    }
}

#[macro_export]
macro_rules! metadata {
    ($meta_field: expr) => {{
        $crate::widget::input::MetaData {
            symbol: $meta_field.metadata.symbol,
            label: i18n::lookup!($meta_field.metadata.label),
            unit: $meta_field.metadata.unit,
            description: i18n::lookup_opt!($meta_field.metadata.description),
        }
    }};
}

impl MetaData {
    pub const fn symbol(self, symbol: &'static str) -> Self {
        Self {
            symbol: Some(symbol),
            ..self
        }
    }
    pub const fn unit(self, unit: &'static str) -> Self {
        Self {
            unit: Some(unit),
            ..self
        }
    }
    pub const fn description(self, description: &'static i18n::Text) -> Self {
        Self {
            description: Some(description),
            ..self
        }
    }
}

pub struct CheckBox<'a, S, P> {
    pub element: Input<'a, S, P>,
    pub label: &'static i18n::Text,
    pub language: &'static i18n::Language,
}

pub const fn checkbox<'a, S, P>(
    data: &'a S,
    property: P,
    metadata: MetaData,
    language: &'static i18n::Language,
) -> CheckBox<'a, S, P> {
    CheckBox {
        element: Input {
            data,
            property,
            is_enabled: true,
        },
        label: metadata.label,
        language,
    }
}

#[macro_export]
macro_rules! checkbox {
    ($data: expr, $outer: expr, $property: expr, $language: expr) => {{
        $crate::widget::input::checkbox($data, $outer($property), metadata!($property), $language)
    }};
}

impl<S, P> CheckBox<'_, S, P> {
    pub fn enabled(self, is_enabled: bool) -> Self {
        Self {
            element: self.element.enabled(is_enabled),
            ..self
        }
    }
}

impl<'a, Message, S, P> From<CheckBox<'a, S, P>> for iced::Element<'a, Message>
where
    Message: 'a + From<PropertyUpdate<P::Value, P>>,
    P: 'a + Property<S, Value = bool> + Clone,
{
    fn from(value: CheckBox<'a, S, P>) -> Self {
        let text = value.label.translate(value.language);
        iced::widget::checkbox(text, value.element.property.get(value.element.data))
            .on_toggle_maybe(value.element.is_enabled.then_some(move |v: bool| {
                PropertyUpdate {
                    property: value.element.property.clone(),
                    value: v,
                }
                .into()
            }))
            .into()
    }
}
