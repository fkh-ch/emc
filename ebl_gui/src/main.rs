use metastruct::MetaStruct;
use widget::input;

mod config;
mod error;
mod i18n;
mod widget;

pub fn main() -> iced::Result {
    iced::application(Ebl::title, Ebl::update, Ebl::view)
        .theme(Ebl::theme)
        .subscription(Ebl::subscription)
        .resizable(true)
        .run_with(Ebl::new)
}

#[derive(Debug, Clone)]
struct Buffer {
    path: Option<std::path::PathBuf>,
    project: ebl::Project,
    is_dirty: bool,
}

impl Buffer {
    fn new_from_config(config: &config::Config) -> Self {
        let fig = |levels: &Option<Vec<ebl::project::ContourLineLevel>>| ebl::project::Figure::<
            ebl::project::SinglePlot,
        > {
            plots: ebl::project::SinglePlot {
                contour_lines: ebl::project::ContourLinePlot {
                    levels: levels.clone().unwrap_or_default(),
                    ..Default::default()
                },
            },
            ..Default::default()
        };
        Self {
            is_dirty: false,
            path: None,
            project: ebl::Project {
                figures: ebl::project::Figures {
                    e: fig(&config.contour_levels.e),
                    b: fig(&config.contour_levels.b),
                    u: fig(&config.contour_levels.u),
                    a: fig(&config.contour_levels.a),
                    ..Default::default()
                },
                ..Default::default()
            },
        }
    }
}

struct Ebl {
    buffers: Vec<Buffer>,
    selected_project_index: usize,
    config: config::Config,
    cache: config::Cache,
    theme: iced::Theme,
    selected_tab_index: usize,
    selected_quantity_index: usize,
    language: &'static i18n::Language,
    is_loading: bool,
    errors: Vec<error::Error>,
}

fn language_from_env() -> Option<&'static i18n::Language> {
    std::env::var("LANG").ok().and_then(|code| {
        let code = code.split_once('.').map(|(code, _)| code).unwrap_or(&code);
        let code = code.replace('_', "-");
        i18n::get_language(&code)
    })
}

impl Ebl {
    fn is_dirty(&self) -> bool {
        self.buffer().map(|b| b.is_dirty).unwrap_or(false)
    }
    fn buffer(&self) -> Option<&Buffer> {
        self.buffers.get(self.selected_project_index)
    }
    fn buffer_mut(&mut self) -> Option<&mut Buffer> {
        self.buffers.get_mut(self.selected_project_index)
    }
    fn project(&self) -> Option<&ebl::Project> {
        self.buffers
            .get(self.selected_project_index)
            .map(|p| &p.project)
    }

    fn new() -> (Self, iced::Task<Message>) {
        let mut errors = Vec::new();
        let config = config::Config::from_config_dir()
            .map_err(|e| errors.push(e.into()))
            .unwrap_or_default()
            .unwrap_or_default();
        let cache = config::Cache::from_config_dir()
            .map_err(|e| errors.push(e.into()))
            .unwrap_or_default()
            .unwrap_or_default();

        let path = std::env::args().nth(1).map(std::path::PathBuf::from);
        let language = config
            .language
            .or_else(language_from_env)
            .unwrap_or(&i18n::EN_US);
        let open_from_arg = if let Some(path) = path {
            iced::Task::perform(load_file(path), |p| {
                Message::FileOpened(Some(p.map(Box::new)).transpose())
            })
        } else {
            iced::Task::none()
        };
        let init_task = open_from_arg;
        (
            Self {
                buffers: Vec::new(),
                selected_project_index: 0,
                config,
                cache,
                theme: iced::Theme::Dark,
                language,
                is_loading: false,
                selected_tab_index: 0,
                selected_quantity_index: 0,
                errors,
            },
            init_task,
        )
    }

    fn title(&self) -> String {
        if let Some(file) = self
            .buffer()
            .and_then(|p| p.path.as_deref())
            .and_then(std::path::Path::file_name)
        {
            format!("EBL - {}", file.to_string_lossy())
        } else {
            "EBL".to_string()
        }
    }

    fn view(&self) -> iced::Element<Message> {
        let error_bar = |error, index| {
            iced::widget::container(
                iced::widget::row![
                    iced::widget::text(format!("{error}")),
                    iced::widget::horizontal_space(),
                    danger_tooltip_button(
                        "x",
                        i18n::lookup!("Clear error").translate(self.language),
                        Some(Message::ClearError(index))
                    )
                ]
                .spacing(10),
            )
            .style(|theme| {
                iced::widget::container::background(
                    iced::widget::button::danger(theme, iced::widget::button::Status::Active)
                        .background
                        .unwrap_or(iced::Color::from_rgb(0.765, 0.259, 0.247).into()),
                )
            })
            .into()
        };
        let toolbar = iced::widget::row![
            tooltip_button(
                widget::icon::Icon::new('➕'),
                i18n::lookup!("New project").translate(self.language),
                (!self.is_loading).then_some(Message::NewProject)
            ),
            tooltip_button(
                widget::icon::Icon::new('📂'),
                i18n::lookup!("Open project").translate(self.language),
                (!self.is_loading).then_some(Message::OpenFile(None))
            ),
            tooltip_button(
                widget::icon::Icon::new('📥'),
                i18n::lookup!("Save project").translate(self.language),
                self.is_dirty().then_some(Message::SaveFile)
            ),
            tooltip_button(
                iced::widget::row![widget::icon::Icon::new('📝'), widget::icon::Icon::new('📥')],
                i18n::lookup!("Save project as").translate(self.language),
                self.is_dirty().then_some(Message::SaveFileAs)
            ),
            iced::widget::container(iced::widget::vertical_rule(1))
                .center_y(iced::Length::Fixed(20.0)),
            tooltip_button(
                widget::icon::Icon::new('🧮'),
                i18n::lookup!("Compute and Export").translate(self.language),
                self.project().is_some().then_some(Message::Compute)
            ),
            iced::widget::horizontal_space(),
            iced::widget::button(widget::icon::Icon::new(
                if self.theme != iced::Theme::Light {
                    '☀'
                } else {
                    '☾'
                }
            ))
            .on_press(Message::ThemeSelected(
                if self.theme != iced::Theme::Light {
                    iced::Theme::Light
                } else {
                    iced::Theme::Dark
                }
            )),
        ]
        .spacing(10)
        .align_y(iced::Alignment::Center);

        let project_tab_labels = self
            .buffers
            .iter()
            .map(|p| {
                p.path
                    .as_ref()
                    .and_then(|p| p.file_name())
                    .map(|p| p.to_string_lossy().to_string())
                    .unwrap_or(
                        i18n::lookup!("<new project>")
                            .translate(self.language)
                            .to_string(),
                    )
            })
            .collect::<Vec<_>>();

        iced::widget::column(
            self.errors
                .iter()
                .enumerate()
                .map(|(index, error)| error_bar(error, index)),
        )
        .push(
            widget::tab_selector::h_tab_selector(
                project_tab_labels.into_iter(),
                self.selected_project_index,
                Message::SelectProject,
            )
            .with_close(Message::CloseProject),
        )
        .push(toolbar)
        .push(widget::tab_selector::h_tab_selector(
            translated_labels(TABS, self.language),
            self.selected_tab_index,
            Message::SwitchTab,
        ))
        .push_maybe(
            self.project()
                .map(|project| (TABS[self.selected_tab_index].view)(self, project, self.language)),
        )
        .spacing(10)
        .padding(10)
        .into()
    }

    fn update(&mut self, message: Message) -> iced::Task<Message> {
        match message {
            Message::NewProject => self.buffers.push(Buffer::new_from_config(&self.config)),
            Message::SelectProject(index) => {
                self.selected_project_index = index;
            }
            Message::CloseProject(index) => {
                self.buffers.remove(index);
                if index > self.buffers.len() && !self.buffers.is_empty() {
                    self.selected_project_index = self.buffers.len() - 1
                }
            }
            Message::OpenFile(path) => {
                if !self.is_loading {
                    self.is_loading = true;
                    if let Some(path) = path {
                        return iced::Task::perform(load_file(path), |p| {
                            Message::FileOpened(Some(p.map(Box::new)).transpose())
                        });
                    }
                    return iced::Task::perform(
                        open_file_with_dialog(self.language, self.cache.dialog_path.clone()),
                        |r| Message::FileOpened(r.map(|o| o.map(Box::new))),
                    );
                }
            }
            Message::FileOpened(result) => {
                self.is_loading = false;
                match result {
                    Ok(Some(project_with_path)) => {
                        if let Some(p) = &project_with_path.path {
                            self.update_cache(p);
                        }
                        self.buffers.push(*project_with_path);
                    }
                    Err(err) => self.errors.push(err),
                    _ => {}
                }
            }
            Message::SaveFile => {
                if let Some(p) = (!self.is_loading)
                    .then_some(())
                    .and_then(|_| self.buffer().cloned())
                {
                    self.is_loading = true;
                    if let Some(file) = p.path {
                        return iced::Task::perform(save_file(file.clone(), p.project), |p| {
                            Message::FileSaved(Some(p).transpose())
                        });
                    } else {
                        return iced::Task::perform(
                            save_file_with_dialog(
                                self.language,
                                self.cache.dialog_path.clone(),
                                p.clone(),
                            ),
                            Message::FileSaved,
                        );
                    }
                }
            }
            Message::SaveFileAs => {
                if let Some(p) = (!self.is_loading)
                    .then_some(())
                    .and_then(|_| self.buffer().cloned())
                {
                    self.is_loading = true;
                    return iced::Task::perform(
                        save_file_with_dialog(self.language, self.cache.dialog_path.clone(), p),
                        Message::FileSaved,
                    );
                }
            }
            Message::FileSaved(result) => {
                self.is_loading = false;
                match result {
                    Ok(Some(path)) => {
                        if let Some(buffer) = self.buffer_mut() {
                            buffer.path = Some(path);
                            buffer.is_dirty = false;
                        }
                    }
                    Err(err) => {
                        self.errors.push(err);
                    }
                    _ => {}
                }
            }
            Message::SwitchTab(tab_index) => {
                self.selected_tab_index = tab_index;
            }
            Message::SwitchQuantityTab(tab_index) => {
                self.selected_quantity_index = tab_index;
            }
            Message::ClearError(index) => {
                self.errors.remove(index);
            }
            Message::UpdateValue(update) => {
                if let Some(buffer) = self.buffer_mut() {
                    update.apply_to(&mut buffer.project);
                    buffer.is_dirty = true;
                }
            }
            Message::ThemeSelected(theme) => {
                self.theme = theme;
            }
            Message::Compute => {
                if let Some(p) = self.buffer() {
                    if let Err(err) = compute(p) {
                        self.errors.push(err);
                    }
                }
            }
            Message::MaximizeWindow(id) => {
                return iced::window::maximize(id, true);
            }
        }
        iced::Task::none()
    }

    fn subscription(&self) -> iced::Subscription<Message> {
        iced::Subscription::batch([
            iced::keyboard::on_key_press(|key, modifiers| match key.as_ref() {
                iced::keyboard::Key::Character("s") if modifiers.command() => {
                    Some(Message::SaveFile)
                }
                _ => None,
            }),
            iced::window::open_events().map(Message::MaximizeWindow),
        ])
    }

    fn theme(&self) -> iced::theme::Theme {
        self.theme.clone()
    }

    fn update_cache(&mut self, path: &std::path::Path) {
        if let Err(err) = self.cache.set_dir(path.parent().map(|p| p.to_owned())) {
            self.errors.push(err.into());
        }
    }
}

#[derive(Debug, Clone)]
enum Message {
    NewProject,
    SelectProject(usize),
    CloseProject(usize),
    OpenFile(Option<std::path::PathBuf>),
    FileOpened(Result<Option<Box<Buffer>>, error::Error>),
    SaveFile,
    SaveFileAs,
    FileSaved(Result<Option<std::path::PathBuf>, error::Error>),
    ThemeSelected(iced::Theme),
    Compute,
    SwitchTab(usize),
    SwitchQuantityTab(usize),
    UpdateValue(ValueUpdate),
    ClearError(usize),
    MaximizeWindow(iced::window::Id),
}

#[derive(Debug, Clone)]
enum ValueUpdate {
    Float(PropertyUpdate<f64>),
    Angle(PropertyUpdate<ebl::project::Angle>),
    Integer(PropertyUpdate<u32>),
    BigInteger(PropertyUpdate<usize>),
    Boolean(PropertyUpdate<bool>),
    OptColor(PropertyUpdate<Option<ebl::project::WebColor>>),
    Color(PropertyUpdate<ebl::project::WebColor>),
    Shields(PropertyUpdate<Option<ebl::project::ShieldProperties>>),
    RemoveGuardWire(usize),
    AddGuardWire,
    RemoveSystem(usize),
    AddSystem,
    AddBundleToSystem(usize),
    RemoveBundleFromSystem(usize),
    SystemBundleX(usize, usize, f64),
    SystemBundleY(usize, usize, f64),
    AddContourLineToPlot(FigureProperty),
    RemoveContourLineFromPlot {
        figure: FigureProperty,
        level_index: usize,
    },
    AddLevelColorToPlot(FigureProperty),
    RemoveLevelColorFromPlot {
        figure: FigureProperty,
        index: usize,
    },
}

impl ValueUpdate {
    fn apply_to(self, project: &mut ebl::Project) {
        match self {
            Self::Float(update) => update.apply_to(project),
            Self::Angle(update) => update.apply_to(project),
            Self::Integer(update) => update.apply_to(project),
            Self::BigInteger(update) => update.apply_to(project),
            Self::Boolean(update) => update.apply_to(project),
            Self::OptColor(update) => update.apply_to(project),
            Self::Color(update) => update.apply_to(project),
            Self::Shields(update) => update.apply_to(project),
            Self::RemoveGuardWire(index) => {
                project.guardwires.remove(index);
            }
            Self::AddGuardWire => {
                project.guardwires.push(Default::default());
            }
            Self::RemoveSystem(index) => {
                project.systems.remove(index);
            }
            Self::AddSystem => {
                project.systems.push(Default::default());
            }
            Self::AddBundleToSystem(index) => {
                project.systems[index].phases.push(Default::default());
            }
            Self::RemoveBundleFromSystem(index) => {
                project.systems[index].phases.pop();
            }
            Self::SystemBundleX(index, bundle_index, new_value) => {
                project.systems[index].phases[bundle_index].x = new_value;
            }
            Self::SystemBundleY(index, bundle_index, new_value) => {
                project.systems[index].phases[bundle_index].y = new_value;
            }
            Self::AddContourLineToPlot(figure) => {
                (figure.access_mut)(&mut project.figures)
                    .plots
                    .contour_lines
                    .levels
                    .push(ebl::project::ContourLineLevel {
                        value: 0.,
                        color: None,
                    });
            }
            Self::RemoveContourLineFromPlot {
                figure,
                level_index,
            } => {
                (figure.access_mut)(&mut project.figures)
                    .plots
                    .contour_lines
                    .levels
                    .remove(level_index);
            }
            Self::AddLevelColorToPlot(figure) => {
                let contour_lines = &(figure.access)(&project.figures).plots.contour_lines;
                let color = ebl::project::DEFAULT_COLORS
                    [contour_lines.level_colors.len() % ebl::project::DEFAULT_COLORS.len()]
                .clone();
                (figure.access_mut)(&mut project.figures)
                    .plots
                    .contour_lines
                    .level_colors
                    .push(color);
            }
            Self::RemoveLevelColorFromPlot { figure, index } => {
                (figure.access_mut)(&mut project.figures)
                    .plots
                    .contour_lines
                    .level_colors
                    .remove(index);
            }
        }
    }
}

impl From<ValueUpdate> for Message {
    fn from(value: ValueUpdate) -> Self {
        Self::UpdateValue(value)
    }
}

impl<T> From<PropertyUpdate<T>> for Message
where
    ValueUpdate: From<PropertyUpdate<T>>,
{
    fn from(value: PropertyUpdate<T>) -> Self {
        Self::UpdateValue(value.into())
    }
}

type PropertyUpdate<T> = input::PropertyUpdate<T, ProjectProperty<T>>;
type FigureProperty = &'static metastruct::MetaField<
    ebl::project::Figures,
    ebl::project::Figure<ebl::project::SinglePlot>,
>;

#[derive(Debug, Clone)]
enum ProjectProperty<T: 'static> {
    Project(&'static metastruct::MetaField<ebl::Project, T>),
    GuardWire {
        index: usize,
        sub_field: &'static metastruct::MetaField<ebl::project::GuardWire, T>,
    },
    System {
        index: usize,
        sub_field: &'static metastruct::MetaField<ebl::project::System, T>,
    },
    Figure {
        figure: FigureProperty,
        sub_field:
            &'static metastruct::MetaField<ebl::project::Figure<ebl::project::SinglePlot>, T>,
    },
    PlotLevel {
        figure: FigureProperty,
        level_index: usize,
        sub_field: &'static metastruct::MetaField<ebl::project::ContourLineLevel, T>,
    },
    LevelColor {
        figure: FigureProperty,
        index: usize,
        sub_field: &'static metastruct::MetaField<ebl::project::WebColor, T>,
    },
}

impl<T: Default + Clone> input::Property<ebl::Project> for ProjectProperty<T> {
    type Value = T;
    fn get(&self, data: &ebl::Project) -> Self::Value {
        match self {
            Self::Project(field) => (field.access)(data).clone(),
            Self::GuardWire { index, sub_field } => {
                (sub_field.access)(&data.guardwires[*index]).clone()
            }
            Self::System { index, sub_field } => (sub_field.access)(&data.systems[*index]).clone(),
            Self::Figure { figure, sub_field } => {
                (sub_field.access)((figure.access)(&data.figures)).clone()
            }
            Self::PlotLevel {
                figure,
                level_index,
                sub_field,
            } => (sub_field.access)(
                &(figure.access)(&data.figures).plots.contour_lines.levels[*level_index],
            )
            .clone(),
            Self::LevelColor {
                figure,
                index,
                sub_field,
            } => (sub_field.access)(
                &(figure.access)(&data.figures)
                    .plots
                    .contour_lines
                    .level_colors[*index],
            )
            .clone(),
        }
    }
    fn set(&self, project: &mut ebl::Project, new_value: Self::Value) {
        let property = match self {
            Self::Project(field) => (field.access_mut)(project),
            Self::GuardWire { index, sub_field } => {
                (sub_field.access_mut)(&mut project.guardwires[*index])
            }
            Self::System { index, sub_field } => {
                (sub_field.access_mut)(&mut project.systems[*index])
            }
            Self::Figure { figure, sub_field } => {
                (sub_field.access_mut)((figure.access_mut)(&mut project.figures))
            }
            Self::PlotLevel {
                figure,
                level_index,
                sub_field,
            } => (sub_field.access_mut)(
                &mut (figure.access_mut)(&mut project.figures)
                    .plots
                    .contour_lines
                    .levels[*level_index],
            ),
            Self::LevelColor {
                figure,
                index,
                sub_field,
            } => (sub_field.access_mut)(
                &mut (figure.access_mut)(&mut project.figures)
                    .plots
                    .contour_lines
                    .level_colors[*index],
            ),
        };
        *property = new_value;
    }
}

impl From<PropertyUpdate<f64>> for ValueUpdate {
    fn from(value: PropertyUpdate<f64>) -> Self {
        Self::Float(value)
    }
}
impl From<PropertyUpdate<ebl::project::Angle>> for ValueUpdate {
    fn from(value: PropertyUpdate<ebl::project::Angle>) -> Self {
        Self::Angle(value)
    }
}
impl From<PropertyUpdate<u32>> for ValueUpdate {
    fn from(value: PropertyUpdate<u32>) -> Self {
        Self::Integer(value)
    }
}
impl From<PropertyUpdate<usize>> for ValueUpdate {
    fn from(value: PropertyUpdate<usize>) -> Self {
        Self::BigInteger(value)
    }
}
impl From<PropertyUpdate<bool>> for ValueUpdate {
    fn from(value: PropertyUpdate<bool>) -> Self {
        Self::Boolean(value)
    }
}
impl From<PropertyUpdate<Option<ebl::project::WebColor>>> for ValueUpdate {
    fn from(value: PropertyUpdate<Option<ebl::project::WebColor>>) -> Self {
        Self::OptColor(value)
    }
}
impl From<PropertyUpdate<ebl::project::WebColor>> for ValueUpdate {
    fn from(value: PropertyUpdate<ebl::project::WebColor>) -> Self {
        Self::Color(value)
    }
}
impl From<PropertyUpdate<Option<ebl::project::ShieldProperties>>> for ValueUpdate {
    fn from(value: PropertyUpdate<Option<ebl::project::ShieldProperties>>) -> Self {
        Self::Shields(value)
    }
}

struct Tab {
    pub label: &'static i18n::Text,
    pub view: for<'a> fn(
        &'a Ebl,
        &'a ebl::Project,
        &'static i18n::Language,
    ) -> iced::Element<'a, Message>,
}

fn translated_labels(
    tabs: &'static [Tab],
    language: &'static i18n::Language,
) -> impl Iterator<Item = &'static str> {
    tabs.iter().map(move |t| t.label.translate(language))
}

const TABS: &[Tab] = &[
    Tab {
        label: i18n::lookup!("Conductor Geometry"),
        view: |_, project, lang| {
            use ProjectProperty::Project as Target;

            iced::widget::column![
                labelled_input!(project, Target, &ebl::Project::META.rho, lang),
                labelled_input!(project, Target, &ebl::Project::META.f_grid, lang),
                labelled_input!(project, Target, &ebl::Project::META.alpha, lang).unit("°"),
                labelled_input!(project, Target, &ebl::Project::META.y_ref, lang),
                iced::widget::scrollable(
                    iced::widget::column![
                        iced::widget::row![
                            iced::widget::text(i18n::lookup!("Guard Wires").translate(lang)),
                            tooltip_button(
                                "+",
                                i18n::lookup!("Add a guard wire").translate(lang),
                                Some(ValueUpdate::AddGuardWire.into())
                            )
                        ]
                        .spacing(10),
                        iced::widget::row(
                            (0..project.guardwires.len())
                                .map(|index| make_guard_wire_box(project, index, lang))
                        )
                        .spacing(10)
                        .padding(10),
                        iced::widget::row![
                            iced::widget::text(i18n::lookup!("Systems").translate(lang)),
                            tooltip_button(
                                "+",
                                i18n::lookup!("Add a system").translate(lang),
                                Some(ValueUpdate::AddSystem.into())
                            )
                        ]
                        .spacing(10),
                        iced::widget::row(
                            (0..project.systems.len())
                                .map(|index| make_system_box(project, index, lang))
                        )
                        .spacing(10)
                        .padding(10),
                    ]
                    .spacing(10)
                )
                .direction(iced::widget::scrollable::Direction::Both {
                    vertical: Default::default(),
                    horizontal: Default::default()
                })
            ]
            .spacing(10)
            .padding(10)
            .into()
        },
    },
    Tab {
        label: i18n::lookup!("Weather / Environment"),
        view: |_, project, lang| {
            use ProjectProperty::Project as Target;
            let rain_rate = project.corona_sound.rain_rate;

            iced::widget::column![
                iced::widget::checkbox(
                    i18n::lookup!("Compute Corona Sound").translate(lang),
                    project.corona_sound.enabled
                )
                .on_toggle(|value| {
                    PropertyUpdate {
                        value,
                        property: ProjectProperty::Project(&metastruct::sub_field!(
                            <ebl::Project>::corona_sound,
                            <ebl::project::CoronaSound>::enabled
                        )),
                    }
                    .into()
                }),
                labelled_input!(
                    project,
                    Target,
                    &metastruct::sub_field!(
                        <ebl::Project>::corona_sound,
                        <ebl::project::CoronaSound>::h_0
                    ),
                    lang
                )
                .enabled(project.corona_sound.enabled),
                iced::widget::text(i18n::lookup!("Rain Rate").translate(lang)),
                iced::widget::column(RAIN_RATE_LEVELS.iter().enumerate().map(|(index, level)| {
                    let label = format!("{} ({} mm/h)", level.name.translate(lang), level.value);
                    radio(
                        label,
                        Some(index),
                        Some(RainRateLevel::index(rain_rate)),
                        |_| {
                            PropertyUpdate {
                                value: level.value,
                                property: Target(&metastruct::sub_field!(
                                    <ebl::Project>::corona_sound,
                                    <ebl::project::CoronaSound>::rain_rate
                                )),
                            }
                            .into()
                        },
                        project.corona_sound.enabled,
                    )
                })),
                iced::widget::row![
                    radio(
                        String::new(),
                        None,
                        Some(RainRateLevel::index(rain_rate)),
                        |_| {
                            Message::from(PropertyUpdate {
                                value: 0.0,
                                property: Target(&metastruct::sub_field!(
                                    <ebl::Project>::corona_sound,
                                    <ebl::project::CoronaSound>::rain_rate
                                )),
                            })
                        },
                        project.corona_sound.enabled
                    ),
                    widget::input::input(
                        project,
                        Target(&metastruct::sub_field!(
                            <ebl::Project>::corona_sound,
                            <ebl::project::CoronaSound>::rain_rate
                        ))
                    )
                    .enabled(project.corona_sound.enabled),
                    iced::widget::text(" mm/h")
                ],
                iced::widget::checkbox(
                    i18n::lookup!("Atmospheric air absorption").translate(lang),
                    project.corona_sound.enable_atmospheric_air_absorption
                )
                .on_toggle_maybe(project.corona_sound.enabled.then_some(|value| {
                    PropertyUpdate {
                        value,
                        property: Target(&metastruct::sub_field!(
                            <ebl::Project>::corona_sound,
                            <ebl::project::CoronaSound>::enable_atmospheric_air_absorption
                        )),
                    }
                    .into()
                })),
                iced::widget::checkbox(
                    i18n::lookup!("Compute Annual Assessment").translate(lang),
                    project.corona_sound.annual_assessment.enabled
                )
                .on_toggle_maybe(project.corona_sound.enabled.then_some(
                    |value: bool| {
                        PropertyUpdate {
                            value,
                            property: Target(&metastruct::sub_field!(
                                <ebl::Project>::corona_sound,
                                <ebl::project::CoronaSound>::annual_assessment,
                                <ebl::sound::AnnualAssessment>::enabled
                            )),
                        }
                        .into()
                    }
                )),
                iced::widget::text(i18n::lookup!("To determine the annual assessment level, the emission level for a rainfall rate of 1 mm/h is calculated using the BPA-mod method. This level is time-weighted and offset with the following correction levels.").translate(lang)),
                labelled_input!(
                    project,
                    Target,
                    &metastruct::sub_field!(
                        <ebl::Project>::corona_sound,
                        <ebl::project::CoronaSound>::annual_assessment,
                        <ebl::sound::AnnualAssessment>::ti_t0
                    ),
                    lang
                )
                .enabled(project.corona_sound.annual_assessment.enabled),
                iced::widget::text(i18n::lookup!("Correction for industrial and commercial noise (fixed): 5 dB(A)").translate(lang)),
                labelled_input!(
                    project,
                    Target,
                    &metastruct::sub_field!(
                        <ebl::Project>::corona_sound,
                        <ebl::project::CoronaSound>::annual_assessment,
                        <ebl::sound::AnnualAssessment>::c
                    ),
                    lang
                )
                .enabled(project.corona_sound.annual_assessment.enabled),
                iced::widget::text(i18n::lookup!("Correction for audible pulse content (not applicable, fixed): 0 dB(A)").translate(lang)),
            ]
            .spacing(10)
            .padding(10)
            .into()
        },
    },
    Tab {
        label: i18n::lookup!("Plots / Tables"),
        view: |ebl, project, lang| {
            iced::widget::scrollable(
                iced::widget::row![
                    iced::widget::column![
                        widget::tab_selector::v_tab_selector(
                            translated_labels(QUANTITY_TABS, ebl.language),
                            ebl.selected_quantity_index,
                            Message::SwitchQuantityTab,
                        )
                        .width(400.),
                        iced::widget::text(i18n::lookup!("Table").translate(lang)),
                        iced::widget::container(
                            iced::widget::column![
                                checkbox!(
                                    project,
                                    ProjectProperty::Project,
                                    &metastruct::sub_field!(
                                        <ebl::Project>::transverse_profile,
                                        <ebl::project::TransverseProfile>::columns,
                                        <ebl::project::TransverseProfileColumns>::with_e_abs
                                    ),
                                    lang
                                ),
                                checkbox!(
                                    project,
                                    ProjectProperty::Project,
                                    &metastruct::sub_field!(
                                        <ebl::Project>::transverse_profile,
                                        <ebl::project::TransverseProfile>::columns,
                                        <ebl::project::TransverseProfileColumns>::with_e_components
                                    ),
                                    lang
                                ),
                                checkbox!(
                                    project,
                                    ProjectProperty::Project,
                                    &metastruct::sub_field!(
                                        <ebl::Project>::transverse_profile,
                                        <ebl::project::TransverseProfile>::columns,
                                        <ebl::project::TransverseProfileColumns>::with_b_abs
                                    ),
                                    lang
                                ),
                                checkbox!(
                                    project,
                                    ProjectProperty::Project,
                                    &metastruct::sub_field!(
                                        <ebl::Project>::transverse_profile,
                                        <ebl::project::TransverseProfile>::columns,
                                        <ebl::project::TransverseProfileColumns>::with_b_components
                                    ),
                                    lang
                                )
                            ]
                            .spacing(10)
                            .padding(10)
                        )
                        .style(framed_box),
                        iced::widget::text(i18n::lookup!("Transverse Profile").translate(lang)),
                        iced::widget::container(
                            iced::widget::column![
                                labelled_input!(
                                    project,
                                    ProjectProperty::Project,
                                    &metastruct::sub_field!(
                                        <ebl::Project>::transverse_profile,
                                        <ebl::project::TransverseProfile>::m
                                    ),
                                    lang
                                ),
                                widget::input::LabelledElement {
                                    element: iced::widget::row![
                                        widget::input::input(
                                            project,
                                            ProjectProperty::Project(&metastruct::sub_field!(
                                                <ebl::Project>::transverse_profile,
                                                <ebl::project::TransverseProfile>::x_min
                                            ))
                                        ),
                                        " - ",
                                        widget::input::input(
                                            project,
                                            ProjectProperty::Project(&metastruct::sub_field!(
                                                <ebl::Project>::transverse_profile,
                                                <ebl::project::TransverseProfile>::x_max
                                            ))
                                        )
                                    ],
                                    metadata: widget::input::MetaData {
                                        symbol: None,
                                        label: i18n::lookup!("x range transverse profile"),
                                        unit: Some("m"),
                                        description: None
                                    },
                                    language: lang
                                },
                                labelled_input!(
                                    project,
                                    ProjectProperty::Project,
                                    &metastruct::sub_field!(
                                        <ebl::Project>::transverse_profile,
                                        <ebl::project::TransverseProfile>::y
                                    ),
                                    lang
                                ),
                            ]
                            .spacing(10)
                            .padding(10)
                        )
                        .style(framed_box),
                        checkbox!(
                            project,
                            ProjectProperty::Project,
                            &metastruct::sub_field!(
                                <ebl::Project>::transverse_profile,
                                <ebl::project::TransverseProfile>::plots,
                                <ebl::project::TransverseProfilePlots>::with_conductor_picture
                            ),
                            lang
                        ),
                    ]
                    .spacing(10)
                    .padding(10),
                    (QUANTITY_TABS[ebl.selected_quantity_index].view)(ebl, project, ebl.language)
                ]
                .spacing(10)
                .padding(10),
            )
            .into()
        },
    },
];

const EMPTY_METADATA: metastruct::MetaData = metastruct::MetaData::new("", "");

const QUANTITY_TABS: &[Tab] = &[
    Tab {
        label: i18n::lookup!("Electric Field"),
        view: |ebl, project, lang| {
            iced::widget::column![
                checkbox!(
                    project,
                    ProjectProperty::Project,
                    &metastruct::sub_field!(
                        <ebl::Project>::transverse_profile,
                        <ebl::project::TransverseProfile>::plots,
                        <ebl::project::TransverseProfilePlots>::with_e_profile
                    ),
                    lang
                ),
                iced::widget::horizontal_rule(1),
                make_2d_figure_box(
                    project,
                    &metastruct::MetaField {
                        metadata: EMPTY_METADATA,
                        access: |figures| &figures.e,
                        access_mut: |figures| &mut figures.e,
                    },
                    lang,
                    &ebl.config.contour_levels,
                    "V/m",
                )
            ]
            .spacing(10)
            .into()
        },
    },
    Tab {
        label: i18n::lookup!("Magnetic Field"),
        view: |ebl, project, lang| {
            iced::widget::column![
                checkbox!(
                    project,
                    ProjectProperty::Project,
                    &metastruct::sub_field!(
                        <ebl::Project>::transverse_profile,
                        <ebl::project::TransverseProfile>::plots,
                        <ebl::project::TransverseProfilePlots>::with_b_profile
                    ),
                    lang
                ),
                iced::widget::horizontal_rule(1),
                make_2d_figure_box(
                    project,
                    &metastruct::MetaField {
                        metadata: EMPTY_METADATA,
                        access: |figures| &figures.b,
                        access_mut: |figures| &mut figures.b,
                    },
                    lang,
                    &ebl.config.contour_levels,
                    "µT",
                )
            ]
            .spacing(10)
            .into()
        },
    },
    Tab {
        label: i18n::lookup!("Electric Potential"),
        view: |ebl, project, lang| {
            make_2d_figure_box(
                project,
                &metastruct::MetaField {
                    metadata: EMPTY_METADATA,
                    access: |figures| &figures.u,
                    access_mut: |figures| &mut figures.u,
                },
                lang,
                &ebl.config.contour_levels,
                "V",
            )
        },
    },
    Tab {
        label: i18n::lookup!("Magnetic Vector Potential"),
        view: |ebl, project, lang| {
            make_2d_figure_box(
                project,
                &metastruct::MetaField {
                    metadata: EMPTY_METADATA,
                    access: |figures| &figures.a,
                    access_mut: |figures| &mut figures.a,
                },
                lang,
                &ebl.config.contour_levels,
                "µT m",
            )
        },
    },
];
struct RainRateLevel {
    name: &'static i18n::Text,
    value: f64,
}

const RAIN_RATE_LEVELS: &[RainRateLevel] = &[
    RainRateLevel {
        name: i18n::lookup!("Dry"),
        value: 0.0,
    },
    RainRateLevel {
        name: i18n::lookup!("Fog / Wet"),
        value: 0.3,
    },
    RainRateLevel {
        name: i18n::lookup!("Light Rain"),
        value: 0.75,
    },
    RainRateLevel {
        name: i18n::lookup!("Heavy Rain"),
        value: 6.5,
    },
];

impl RainRateLevel {
    fn index(value: f64) -> Option<usize> {
        const EPS: f64 = 1.0E-5;
        for (index, level) in RAIN_RATE_LEVELS.iter().enumerate() {
            if (level.value - EPS..level.value + EPS).contains(&value) {
                return Some(index);
            }
        }
        None
    }
}

async fn open_file_with_dialog(
    lang: &'static i18n::Language,
    path: Option<std::path::PathBuf>,
) -> Result<Option<Buffer>, error::Error> {
    let dialog = rfd::AsyncFileDialog::new()
        .set_title(i18n::lookup!("Open a json file...").translate(lang))
        .add_filter("json", &["json"]);
    let dialog = if let Some(path) = path {
        dialog.set_directory(path)
    } else {
        dialog
    };
    let Some(picked_file) = dialog.pick_file().await else {
        return Ok(None);
    };
    Some(load_file(picked_file.path().to_owned()).await).transpose()
}

async fn load_file(path: std::path::PathBuf) -> Result<Buffer, error::Error> {
    let contents = tokio::fs::read_to_string(&path)
        .await
        .map_err(|e| error::IoError {
            message: "Load project",
            path: path.clone(),
            detail: format!("{e}"),
        })?;
    Ok(Buffer {
        is_dirty: false,
        path: Some(path.clone()),
        project: serde_json::from_str(contents.as_ref()).map_err(|err| {
            error::SerializationError {
                path,
                message: "Load project",
                column: err.column(),
                line: err.line(),
                detail: format!("{err}"),
            }
        })?,
    })
}

async fn save_file(
    path: std::path::PathBuf,
    project: ebl::Project,
) -> Result<std::path::PathBuf, error::Error> {
    let contents =
        serde_json::to_string_pretty(&project).map_err(|err| error::SerializationError {
            path: path.clone(),
            message: "Save project",
            column: err.column(),
            line: err.line(),
            detail: format!("{err}"),
        })?;
    tokio::fs::write(&path.with_extension("json"), contents)
        .await
        .map_err(|e| error::IoError {
            message: "Save project",
            path: path.clone(),
            detail: format!("{e}"),
        })?;

    Ok(path)
}

async fn save_file_with_dialog(
    lang: &'static i18n::Language,
    path: Option<std::path::PathBuf>,
    project_with_path: Buffer,
) -> Result<Option<std::path::PathBuf>, error::Error> {
    let dialog = rfd::AsyncFileDialog::new()
        .set_title(i18n::lookup!("Save a project").translate(lang))
        .add_filter("json", &["json"]);
    let file_name = project_with_path.path.and_then(|p| {
        let file_name = p.file_name()?;
        Some(file_name.to_string_lossy().to_string())
    });
    let dialog = if let Some(path) = file_name {
        dialog.set_file_name(path)
    } else if let Some(path) = path {
        dialog.set_directory(path)
    } else {
        dialog
    };
    let Some(picked_file) = dialog.save_file().await else {
        return Ok(None);
    };
    Some(save_file(picked_file.path().to_owned(), project_with_path.project).await).transpose()
}

fn compute(Buffer { path, project, .. }: &Buffer) -> Result<(), error::Error> {
    let base = path
        .as_deref()
        .map(|p| p.with_extension(""))
        .unwrap_or("untitled".into());
    ebl::process(&base, project)?;
    Ok(())
}

fn make_guard_wire_box<'a>(
    project: &'a ebl::Project,
    index: usize,
    lang: &'static i18n::Language,
) -> iced::Element<'a, Message> {
    const GW_FIELDS: &<ebl::project::GuardWire as metastruct::MetaStruct>::MetaType =
        &ebl::project::GuardWire::META;
    let target = |sub_field| ProjectProperty::GuardWire { index, sub_field };
    iced::widget::container(
        iced::widget::column![
            iced::widget::row![
                iced::widget::text(format!(
                    "{} {}",
                    i18n::lookup!("Guard Wire").translate(lang),
                    index + 1
                )),
                danger_tooltip_button(
                    "x",
                    i18n::lookup!("Remove this guard wire").translate(lang),
                    Some(ValueUpdate::RemoveGuardWire(index).into())
                )
            ]
            .spacing(10),
            labelled_input!(project, target, &GW_FIELDS.x, lang),
            labelled_input!(project, target, &GW_FIELDS.y, lang),
            labelled_input!(project, target, &GW_FIELDS.d, lang),
            labelled_input!(project, target, &GW_FIELDS.R_i, lang),
        ]
        .spacing(10)
        .padding(10),
    )
    .style(framed_box)
    .into()
}

fn make_system_box<'a>(
    project: &'a ebl::Project,
    index: usize,
    lang: &'static i18n::Language,
) -> iced::Element<'a, Message> {
    const SYS_FIELDS: &<ebl::project::System as metastruct::MetaStruct>::MetaType =
        &ebl::project::System::META;
    let target = |sub_field| ProjectProperty::System { index, sub_field };
    let int_target = |sub_field| ProjectProperty::System { index, sub_field };
    let angle_target = |sub_field| ProjectProperty::System { index, sub_field };
    let sys = &project.systems[index];
    let shield_target = move || ProjectProperty::System {
        index,
        sub_field: &metastruct::sub_field!(
            <ebl::project::System>::shield,
            <ebl::project::ShieldProperties>::enabled
        ),
    };
    const SHIELD_RADIUS_FIELD: metastruct::MetaField<ebl::project::System, f64> = metastruct::sub_field!(
        <ebl::project::System>::shield,
        <ebl::project::ShieldProperties>::d
    );
    const SHIELD_R_FIELD: metastruct::MetaField<ebl::project::System, f64> = metastruct::sub_field!(
        <ebl::project::System>::shield,
        <ebl::project::ShieldProperties>::R_i
    );

    iced::widget::container(
        iced::widget::column![
            iced::widget::row![
                iced::widget::text(format!(
                    "{} {}",
                    i18n::lookup!("System").translate(lang),
                    index + 1
                )),
                danger_tooltip_button(
                    "x",
                    i18n::lookup!("Remove this system").translate(lang),
                    Some(ValueUpdate::RemoveSystem(index).into())
                )
            ]
            .spacing(10),
            iced::widget::row![
                iced::widget::text(i18n::lookup!("Bundles").translate(lang)),
                tooltip_button(
                    "+",
                    i18n::lookup!("Add a bundle").translate(lang),
                    Some(ValueUpdate::AddBundleToSystem(index).into())
                ),
                danger_tooltip_button(
                    "-",
                    i18n::lookup!("Remove last bundle").translate(lang),
                    (!sys.phases.is_empty())
                        .then_some(ValueUpdate::RemoveBundleFromSystem(index).into())
                )
            ]
            .spacing(10),
            iced::widget::row![
                iced::widget::text("x [m]").width(140.0),
                iced::widget::text(i18n::lookup!("y [m] (relative to y_ref)").translate(lang)),
            ],
            iced::widget::column(sys.phases.iter().enumerate().map(|(i, phase)| {
                iced::widget::row![
                    widget::parsed_input::ParsedInput::new(
                        phase.x,
                        Some(
                            move |new_value| ValueUpdate::SystemBundleX(index, i, new_value).into()
                        )
                    ),
                    widget::parsed_input::ParsedInput::new(
                        phase.y,
                        Some(
                            move |new_value| ValueUpdate::SystemBundleY(index, i, new_value).into()
                        )
                    ),
                ]
                .spacing(10)
                .into()
            }))
            .spacing(10),
            labelled_input!(project, int_target, &SYS_FIELDS.n_t, lang),
            labelled_input!(project, target, &SYS_FIELDS.d, lang),
            labelled_input!(project, target, &SYS_FIELDS.a, lang),
            labelled_input!(project, angle_target, &SYS_FIELDS.beta_s, lang).unit("°"),
            iced::widget::text(i18n::lookup!("System voltage / currents").translate(lang)),
            widget::input::labelled(
                iced::widget::row![
                    widget::input::input(project, target(&SYS_FIELDS.U)),
                    widget::input::input(project, angle_target(&SYS_FIELDS.phi_U))
                ]
                .spacing(10),
                i18n::lookup!("System voltage"),
                lang
            )
            .unit("V, °")
            .symbol("U"),
            widget::input::labelled(
                iced::widget::row![
                    widget::input::input(project, target(&SYS_FIELDS.I)),
                    widget::input::input(project, angle_target(&SYS_FIELDS.phi_I))
                ]
                .spacing(10),
                i18n::lookup!("System current"),
                lang
            )
            .unit("I, °")
            .symbol("I"),
            iced::widget::Checkbox::new(
                i18n::lookup!("Cable shielding").translate(lang),
                sys.shield.enabled
            )
            .on_toggle(move |value| {
                PropertyUpdate {
                    value,
                    property: shield_target(),
                }
                .into()
            }),
            labelled_input!(project, target, &SHIELD_RADIUS_FIELD, lang)
                .enabled(sys.shield.enabled),
            labelled_input!(project, target, &SHIELD_R_FIELD, lang).enabled(sys.shield.enabled),
        ]
        .spacing(10)
        .padding(10),
    )
    .style(framed_box)
    .into()
}

fn make_2d_figure_box<'a>(
    project: &'a ebl::Project,
    figure: FigureProperty,
    lang: &'static i18n::Language,
    _contour_levels: &'a config::ContourLevels,
    unit: &'static str,
) -> iced::Element<'a, Message> {
    type MetaField<T> = metastruct::MetaField<ebl::project::Figure<ebl::project::SinglePlot>, T>;
    const X_MIN_FIELD: MetaField<f64> = metastruct::MetaField {
        metadata: metastruct::MetaData::new("", "x_min").unit("m"),
        access: |figure| &figure.x_range.start,
        access_mut: |figure| &mut figure.x_range.start,
    };
    const X_MAX_FIELD: MetaField<f64> = metastruct::MetaField {
        metadata: metastruct::MetaData::new("", "x_max").unit("m"),
        access: |figure| &figure.x_range.end,
        access_mut: |figure| &mut figure.x_range.end,
    };
    const Y_MIN_FIELD: MetaField<f64> = metastruct::MetaField {
        metadata: metastruct::MetaData::new("", "y_min").unit("m"),
        access: |figure| &figure.y_range.start,
        access_mut: |figure| &mut figure.y_range.start,
    };
    const Y_MAX_FIELD: MetaField<f64> = metastruct::MetaField {
        metadata: metastruct::MetaData::new("", "y_max").unit("m"),
        access: |figure| &figure.y_range.end,
        access_mut: |figure| &mut figure.y_range.end,
    };
    const CONTOUR_LINES_FIELD: MetaField<bool> = metastruct::MetaField {
        metadata: metastruct::MetaData::new("", "Contour Lines"),
        access: |figure| &figure.plots.contour_lines.enabled,
        access_mut: |figure| &mut figure.plots.contour_lines.enabled,
    };
    const LEVEL_VALUE_FIELD: metastruct::MetaField<ebl::project::ContourLineLevel, f64> =
        metastruct::MetaField {
            metadata: metastruct::MetaData::new("", "Value"),
            access: |level| &level.value,
            access_mut: |level| &mut level.value,
        };
    const LEVEL_COLOR_FIELD: metastruct::MetaField<
        ebl::project::ContourLineLevel,
        ebl::project::WebColor,
    > = metastruct::MetaField {
        metadata: metastruct::MetaData::new("", "Color"),
        access: |level| {
            level
                .color
                .as_ref()
                .unwrap_or(&ebl::project::WebColor::BLACK)
        },
        access_mut: |level| level.color.get_or_insert(ebl::project::WebColor::BLACK),
    };
    const OPT_LEVEL_COLOR_FIELD: metastruct::MetaField<
        ebl::project::ContourLineLevel,
        Option<ebl::project::WebColor>,
    > = metastruct::MetaField {
        metadata: metastruct::MetaData::new("", "Color"),
        access: |level| &level.color,
        access_mut: |level| &mut level.color,
    };
    const SELF_FIELD: metastruct::MetaField<ebl::project::WebColor, ebl::project::WebColor> =
        metastruct::MetaField {
            metadata: metastruct::MetaData::new("", "Color"),
            access: |color| color,
            access_mut: |color| color,
        };
    let target = |sub_field| ProjectProperty::Figure { figure, sub_field };

    let fig = (figure.access)(&project.figures);

    iced::widget::column![
        checkbox!(
            project,
            |sub_field| ProjectProperty::Figure { figure, sub_field },
            &CONTOUR_LINES_FIELD,
            lang
        ),
        labelled_input!(
            project,
            target,
            &ebl::project::Figure::<ebl::project::SinglePlot>::META.width,
            lang
        )
        .enabled(fig.plots.contour_lines.enabled),
        widget::input::LabelledElement {
            element: iced::widget::row![
                widget::input::input(project, target(&X_MIN_FIELD))
                    .enabled(fig.plots.contour_lines.enabled),
                " - ",
                widget::input::input(project, target(&X_MAX_FIELD))
                    .enabled(fig.plots.contour_lines.enabled)
            ],
            metadata: widget::input::MetaData {
                symbol: None,
                label: i18n::lookup!("x range of the plot"),
                unit: Some("m"),
                description: None
            },
            language: lang
        },
        widget::input::LabelledElement {
            element: iced::widget::row![
                widget::input::input(project, target(&Y_MIN_FIELD))
                    .enabled(fig.plots.contour_lines.enabled),
                " - ",
                widget::input::input(project, target(&Y_MAX_FIELD))
                    .enabled(fig.plots.contour_lines.enabled)
            ],
            metadata: widget::input::MetaData {
                symbol: None,
                label: i18n::lookup!("y range of the plot"),
                unit: Some("m"),
                description: None
            },
            language: lang
        },
        checkbox!(
            project,
            |sub_field| ProjectProperty::Figure { figure, sub_field },
            &ebl::project::Figure::<ebl::project::SinglePlot>::META.with_subgrid,
            lang
        )
        .enabled(fig.plots.contour_lines.enabled),
        iced::widget::container(
            iced::widget::column![
                labelled_input!(
                    project,
                    target,
                    &metastruct::sub_field!(
                        <ebl::project::Figure<ebl::project::SinglePlot>>::plots,
                        <ebl::project::SinglePlot>::contour_lines,
                        <ebl::project::ContourLinePlot>::resolution
                    ),
                    lang
                )
                .enabled(fig.plots.contour_lines.enabled),
                iced::widget::row![
                    iced::widget::text(i18n::lookup!("Levels").translate(lang)),
                    tooltip_button(
                        "+",
                        i18n::lookup!("Add a level").translate(lang),
                        (fig.plots.contour_lines.enabled)
                            .then_some(ValueUpdate::AddContourLineToPlot(figure).into())
                    )
                ]
                .spacing(10),
                iced::widget::column((0..fig.plots.contour_lines.levels.len()).map(
                    |level_index| {
                        let level_target = |sub_field| ProjectProperty::PlotLevel {
                            figure,
                            level_index,
                            sub_field,
                        };
                        iced::widget::row![
                            danger_tooltip_button(
                                "x",
                                i18n::lookup!("Remove this level").translate(lang),
                                (fig.plots.contour_lines.enabled).then_some(
                                    ValueUpdate::RemoveContourLineFromPlot {
                                        figure,
                                        level_index
                                    }
                                    .into()
                                )
                            ),
                            widget::input::input(project, level_target(&LEVEL_VALUE_FIELD))
                                .enabled(fig.plots.contour_lines.enabled),
                            iced::widget::text(unit),
                            widget::color_quad::Quad::square(
                                iced::Length::Fixed(30.),
                                convert_color(fig.plots.contour_lines.level_color(level_index))
                            ),
                            iced::widget::checkbox(
                                i18n::lookup!("override").translate(lang),
                                fig.plots.contour_lines.levels[level_index].color.is_some()
                            )
                            .on_toggle_maybe(
                                (fig.plots.contour_lines.enabled).then_some(
                                    move |enabled: bool| {
                                        input::PropertyUpdate {
                                            property: ProjectProperty::PlotLevel {
                                                figure,
                                                level_index,
                                                sub_field: &OPT_LEVEL_COLOR_FIELD,
                                            },
                                            value: enabled.then(|| {
                                                fig.plots
                                                    .contour_lines
                                                    .level_color(level_index)
                                                    .clone()
                                            }),
                                        }
                                        .into()
                                    }
                                )
                            )
                        ]
                        .push_maybe(
                            fig.plots.contour_lines.levels[level_index]
                                .color
                                .is_some()
                                .then(|| {
                                    widget::input::input(
                                        project,
                                        ProjectProperty::PlotLevel {
                                            figure,
                                            level_index,
                                            sub_field: &LEVEL_COLOR_FIELD,
                                        },
                                    )
                                    .enabled(fig.plots.contour_lines.enabled)
                                }),
                        )
                        .spacing(10)
                        .into()
                    },
                )),
                iced::widget::row![
                    iced::widget::text(i18n::lookup!("Level colors").translate(lang)),
                    tooltip_button(
                        "+",
                        i18n::lookup!("Add a color").translate(lang),
                        (fig.plots.contour_lines.enabled)
                            .then_some(ValueUpdate::AddLevelColorToPlot(figure).into())
                    )
                ]
                .spacing(10),
                iced::widget::column((0..fig.plots.contour_lines.level_colors.len()).map(
                    |index| {
                        iced::widget::row![
                            danger_tooltip_button(
                                "x",
                                i18n::lookup!("Remove this color").translate(lang),
                                (fig.plots.contour_lines.enabled).then_some(
                                    ValueUpdate::RemoveLevelColorFromPlot { figure, index }.into()
                                )
                            ),
                            widget::color_quad::Quad::square(
                                iced::Length::Fixed(30.),
                                convert_color(&fig.plots.contour_lines.level_colors[index])
                            ),
                            widget::input::input(
                                project,
                                ProjectProperty::LevelColor {
                                    figure,
                                    index,
                                    sub_field: &SELF_FIELD,
                                },
                            )
                            .enabled(fig.plots.contour_lines.enabled)
                        ]
                        .spacing(10)
                        .into()
                    },
                )),
            ]
            .padding(10)
        )
        .style(framed_box),
    ]
    .spacing(10)
    .into()
}

fn convert_color(c: &ebl::project::WebColor) -> iced::Color {
    iced::Color::from_rgb(c.r as f32 / 255., c.g as f32 / 255., c.b as f32 / 255.)
}

fn radio<'a, T: Copy + Eq>(
    label: String,
    value: T,
    actual_value: Option<T>,
    update: impl Fn(T) -> Message,
    enabled: bool,
) -> iced::Element<'a, Message> {
    if enabled {
        iced::widget::radio(label, value, actual_value, update).into()
    } else {
        iced::widget::text(label).into()
    }
}

fn tooltip_button<'a, Message: Clone + 'a>(
    content: impl Into<iced::Element<'a, Message>>,
    tooltip: &'a str,
    on_press: Option<Message>,
) -> iced::Element<'a, Message> {
    tooltip_button_with_style(content, tooltip, on_press, false)
}

fn danger_tooltip_button<'a, Message: Clone + 'a>(
    content: impl Into<iced::Element<'a, Message>>,
    tooltip: &'a str,
    on_press: Option<Message>,
) -> iced::Element<'a, Message> {
    tooltip_button_with_style(content, tooltip, on_press, true)
}

fn tooltip_button_with_style<'a, Message: Clone + 'a>(
    content: impl Into<iced::Element<'a, Message>>,
    tooltip: &'a str,
    on_press: Option<Message>,
    is_danger: bool,
) -> iced::Element<'a, Message> {
    let button = iced::widget::button(
        iced::widget::container(content)
            .width(30)
            .center_x(iced::Length::Shrink),
    );
    if let Some(on_press) = on_press {
        let button = if is_danger {
            button.style(iced::widget::button::danger)
        } else {
            button
        };
        iced::widget::tooltip(
            button.on_press(on_press),
            tooltip,
            iced::widget::tooltip::Position::FollowCursor,
        )
        .style(iced::widget::container::rounded_box)
        .into()
    } else {
        button.style(iced::widget::button::secondary).into()
    }
}

fn framed_box(theme: &iced::Theme) -> iced::widget::container::Style {
    let palette = theme.extended_palette();
    iced::widget::container::Style {
        border: iced::Border {
            width: 1.0,
            radius: 0.0.into(),
            color: palette.background.strong.color,
        },
        ..Default::default()
    }
}
